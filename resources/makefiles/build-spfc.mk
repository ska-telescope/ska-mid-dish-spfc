EXE:=ska-mid-dish-spfc-builder
CI_REGISTRY_IMAGE:=$(CI_REGISTRY)/ska-telescope/ska-mid-dish-spfc
IMAGE_NAME:=spfc-builder
BUILDER_NAME:=spfc_builder

## TARGET: image-build
## SYNOPSIS: make image-build, command builds the SPFC tango.
## HOOKS: none
## VARS:
##   	CI_REGISTRY_USER : Registry user.
##		CI_REGISTRY_PASSWORD : Registry password.
##		CI_REGISTRY: Gitlab registry where registry name is found
image-build:
	docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	docker buildx build --push --platform linux/arm/v5 -f Dockerfile -t "$(CI_REGISTRY_IMAGE):$(TAG)" .

## TARGET: image-pre-build
## SYNOPSIS: make image-pre-build, command prepares the platform before tango is built
## HOOKS: none
## VARS:
##   	BUILDER_NAME : Handle used for compiling the tangoW.
image-pre-build:
	docker buildx create --use --platform=linux/arm/v5  --driver=docker-container --name $(BUILDER_NAME)
	docker buildx use --default $(BUILDER_NAME)
	docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
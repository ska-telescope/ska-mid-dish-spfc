## TARGET: itf-register-spfc
## SYNOPSIS: make itf-register-spfc, command registers the SPFC to the tango database where DishLMC is registered.
## HOOKS: none
## VARS:
##   	KUBE_NAMESPACE : Namespace to which SPFC will be registered
## 			this is the same namespace where the Dish LMC is currently deployed.
## 		TANGO_HOST : This is the IP address and port of the tango database where the Dish LMC 
##			is currently registered.
##		DISH_ID : This is the dish index the SPFC will communicate with. It must be in the format
##					 index_SKA00*. The * represents the dish index like index_SKA004, for dish 4.
##		SPFC_TASK: (Optional) This is the task to be performed by the pod job, either register the
##					SPFC to tangoDB or to deploy software to the SPFC devkit.
##		K8S_CHART: The name of the chart that contains the register-spfc task. 

K8S_CHART=ska-spfc-deployer

itf-register-spfc: export SPFC_TASK=register-spfc
itf-register-spfc: export KUBE_NAMESPACE=register-spfc
itf-register-spfc: export K8S_CHART_PARAMS = --set job.tango_host=$(TANGO_HOST) \
		--set job.spfc_task=$(SPFC_TASK) --set global.dish_id=$(DISH_ID) \
		--set job.namespace=$(KUBE_NAMESPACE)
itf-register-spfc:														  
	@make k8s-install-chart K8S_CHART=ska-spfc-deployer KUBE_APP=ska-spfc-deployer KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=spfc-deployer
.PHONY: itf-register-spfc

itf-spfc-uninstall-spfc-deployer:
	@make k8s-uninstall-chart K8S_CHART=ska-spfc-deployer KUBE_APP=ska-spfc-deployer KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=spfc-deployer
.PHONY: itf-spfc-uninstall-chart

## TARGET: itf-deploy-to-spfc
## SYNOPSIS: make itf-deploy-to-spfc, command deploys software and configuration files to SPFC device.
## HOOKS: none
## VARS:
##   	KUBE_NAMESPACE : Namespace to which SPFC will be registered
## 			this is the same namespace where the Dish LMC is currently deployed.
##		SPFC_TASK: (Optional) This is the task to be performed by the pod job, either register the
##					SPFC to tangoDB or to deploy software to the SPFC devkit.
##		K8S_CHART: The name of the chart that contains the deploy-to-spfc task. 

itf-deploy-to-spfc: export SPFC_TASK=update-spfc
itf-deploy-to-spfc: export  K8S_CHART_PARAMS = --set job.spfc_task=$(SPFC_TASK) \
		--set job.namespace=$(KUBE_NAMESPACE)

itf-deploy-to-spfc:
	@make k8s-install-chart K8S_CHART=ska-spfc-deployer KUBE_APP=ska-spfc-deployer KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=spfc-deployer
.PHONY: itf-deploy-to-spfc 

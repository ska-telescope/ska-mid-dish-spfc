ARG PLATFORM=linux/arm/v5
FROM --platform=${PLATFORM} debian:trixie-slim AS build
 
SHELL ["/usr/bin/bash", "-c"]

ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /log
 
# This is the build stage
RUN apt-get update -y -q
RUN apt-get upgrade -y -q
ARG DEBIAN_FRONTEND=noninteractive
ENV DEBIAN_FRONTEND=${DEBIAN_FRONTEND}
RUN apt-get install --no-install-recommends --no-install-suggests -y -q build-essential libzmq3-dev libjpeg62-turbo-dev libgrpc++-dev libprotobuf-dev protobuf-compiler-grpc opentelemetry-cpp-dev rsync git cmake curl python3-dev vim mc telnet  libssh-4 libssh-dev libssl-dev openssl ca-certificates pkg-config  iputils-ping openssh-client tzdata
 
# Somehow tell Docker with an env variable where to install all of the Tango Controls stuff:
ENV TANGO_VERSION=10.0.0
ENV TANGO_INSTALLATION_DIR=/root/tango/tango.${TANGO_VERSION}
ENV TANGO_HOME=${TANGO_INSTALLATION_DIR}
 
# Clone build_tango
RUN git config --global http.sslverify false
RUN git clone --depth=1 --branch=${TANGO_VERSION} https://gitlab.com/tjuerges/build_tango.git
 
# # Build everything. The scripts will acknowledge a set TANGO_INSTALLATION_DIR env var.
WORKDIR /build_tango/
RUN /build_tango/omniORB.sh
RUN /build_tango/cppzmq.sh
RUN /build_tango/tango-idl.sh
# TODO
# This is a temporary fix for an incompatibility of cppTango with
# opentelemetry 1.17.
# When cppTango 10.1.0 has been released, fix this.
# Then replace what is below with
# /build_tango/cppTango.sh
#
RUN git clone --recurse-submodules --single-branch https://gitlab.com/tango-controls/cppTango.git /cppTango
RUN TANGO_REPO_LOCAL_DIR=/cppTango TANGO_BRANCH_OR_TAG=c1d7fabad06133ff1548674254f6bd6a931f9fda /build_tango/cppTango.sh
# End of the fix.

RUN /build_tango/tango_admin.sh
 
# Build is done. 
# Here one needs to copy all dynamically linked libs to the TANGO_INSTALLATION_DIR (see README.md of build_tango!) Example: export TANGO_INSTALLATION_DIR=/root/.local/tango/tango.9.5.0
RUN for i in $(ldd ${TANGO_INSTALLATION_DIR}/bin/tango_admin | egrep -v ${TANGO_INSTALLATION_DIR} | cut -d'>' -f2 | cut -d'(' -f1); do cp ${i} ${TANGO_INSTALLATION_DIR}/lib; done

####################
FROM build AS spfc-make
ENV CURRENT_DIR=.
ENV TANGO_HOST=172.16.1.91:10000
COPY --from=build ${TANGO_INSTALLATION_DIR}/lib/ /lib/

# Timezone setup
ENV TZ=Africa/Johannesburg
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

WORKDIR /root/
# Configure mc with Lynx-like option
RUN mkdir -p /root/.config/mc/mcedit
RUN printf '%s\n' '[Panels]' 'navigate_with_arrows=true' > .config/mc/ini

# Now clone and build SPFx
LABEL package.date=2024-03-15
RUN  git clone --depth=1 --branch=main  https://gitlab.com/ska-telescope/ska-mid-dish-spfc.git
WORKDIR ${CURRENT_DIR}/ska-mid-dish-spfc/src/ska_emss/

# TODO
# The include path for omniORB should be set in the Makefiles.
RUN make OMNI_HOME=${TANGO_INSTALLATION_DIR}/../omniORB.4.3.2

RUN mkdir -p /var/lib/spfc/spfc /var/lib/spfc/spf1 /var/lib/spfc/spf2 /var/lib/spfc/spf345 /var/lib/spfc/spfhe /var/lib/spfc/spfvac
RUN mkdir -p /root/ska/spf1 /root/ska/spf2 /root/ska/spf345 /root/ska/spfhe /root/ska/spfvac /root/ska/spfc
RUN mkdir -p ${TANGO_INSTALLATION_DIR}/bin/spf1 ${TANGO_INSTALLATION_DIR}/bin/spf2 ${TANGO_INSTALLATION_DIR}/bin/spf345 ${TANGO_INSTALLATION_DIR}/bin/spfhe ${TANGO_INSTALLATION_DIR}/bin/spfvac ${TANGO_INSTALLATION_DIR}/bin/spfc

# TODO
# Needs fixing (perhaps by using rsync --archive) because the symlinks are now 
# copied as real files. This leads to the same file with two different names in 
# for example ${TANGO_INSTALLATION_DIR}/bin/spfhe.
# -rwxr-xr-x root/root    835420 2024-11-26 12:46 root/tango/tango.10.0.0/bin/spfhe/SpfHe
# -rwxr-xr-x root/root    835420 2024-11-26 12:46 root/tango/tango.10.0.0/bin/spfhe/SpfHe_ARM_v02.15
RUN cp ./spf1/bin/* ${TANGO_INSTALLATION_DIR}/bin/spf1
RUN rm -rf /root/ska/spf1/Spf1
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/spf1/Spf1 /root/ska/spf1/Spf1

RUN cp ./spf2/bin/* ${TANGO_INSTALLATION_DIR}/bin/spf2
RUN rm -rf /root/ska/spf2/Spf2
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/spf2/Spf2 /root/ska/spf2/Spf2

RUN cp ./spf345/bin/* ${TANGO_INSTALLATION_DIR}/bin/spf345
RUN rm -rf /root/ska/spf345/Spf345
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/spf345/Spf345 /root/ska/spf345/Spf345

RUN cp ./spfHe/bin/* ${TANGO_INSTALLATION_DIR}/bin/spfhe
RUN rm -rf /root/ska/spfhe/SpfHe
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/spfhe/SpfHe /root/ska/spfhe/SpfHe

RUN cp ./spfVac/bin/* ${TANGO_INSTALLATION_DIR}/bin/spfvac
RUN rm -rf /root/ska/spfvac/SpfVac
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/spfvac/SpfVac /root/ska/spfvac/SpfVac

RUN cp ./spfc/bin/* ${TANGO_INSTALLATION_DIR}/bin/spfc
RUN rm -rf /root/ska/spfc/Spfc
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/spfc/Spfc /root/ska/spfc/Spfc

# Here one needs to copy all dynamically linked libs to the TANGO_INSTALLATION_DIR (see README.md of build_tango!) Example: export TANGO_INSTALLATION_DIR=/root/.local/tango/tango.9.5.0
WORKDIR /build_tango/

RUN for i in $(ldd ${TANGO_INSTALLATION_DIR}/bin/spf1/Spf1 | egrep -v ${TANGO_INSTALLATION_DIR} | cut -d'>' -f2 | cut -d'(' -f1); do cp ${i} ${TANGO_INSTALLATION_DIR}/lib; done
RUN for i in $(ldd ${TANGO_INSTALLATION_DIR}/bin/spf2/Spf2 | egrep -v ${TANGO_INSTALLATION_DIR} | cut -d'>' -f2 | cut -d'(' -f1); do cp ${i} ${TANGO_INSTALLATION_DIR}/lib; done
RUN for i in $(ldd ${TANGO_INSTALLATION_DIR}/bin/spf345/Spf345 | egrep -v ${TANGO_INSTALLATION_DIR} | cut -d'>' -f2 | cut -d'(' -f1); do cp ${i} ${TANGO_INSTALLATION_DIR}/lib; done
RUN for i in $(ldd ${TANGO_INSTALLATION_DIR}/bin/spfhe/SpfHe | egrep -v ${TANGO_INSTALLATION_DIR} | cut -d'>' -f2 | cut -d'(' -f1); do cp ${i} ${TANGO_INSTALLATION_DIR}/lib; done
RUN for i in $(ldd ${TANGO_INSTALLATION_DIR}/bin/spfvac/SpfVac | egrep -v ${TANGO_INSTALLATION_DIR} | cut -d'>' -f2 | cut -d'(' -f1); do cp ${i} ${TANGO_INSTALLATION_DIR}/lib; done
RUN for i in $(ldd ${TANGO_INSTALLATION_DIR}/bin/spfc/Spfc | egrep -v ${TANGO_INSTALLATION_DIR} | cut -d'>' -f2 | cut -d'(' -f1); do cp ${i} ${TANGO_INSTALLATION_DIR}/lib; done

CMD ["uname", "-a"]

####################
FROM spfc-make AS spfc-sim-make
WORKDIR /root/

# Now clone and build SPFx
LABEL package.date=2024-03-15
RUN rm -R /root/ska-mid-dish-spfc/
RUN git clone --depth=1 --branch=main  https://gitlab.com/ska-telescope/ska-mid-dish-spfc.git
WORKDIR /root/ska-mid-dish-spfc/src/ska_emss/
RUN make OMNI_HOME=${TANGO_INSTALLATION_DIR}/../omniORB.4.3.2 sim

RUN mkdir -p /var/lib/spfc/spfhe/sim /var/lib/spfc/spfvac/sim
RUN mkdir -p /root/ska/sim/fpc1 /root/ska/sim/fpc2 /root/ska/sim/fpc345 /root/ska/sim/hcc /root/ska/sim/vac ${TANGO_INSTALLATION_DIR}/bin/sim/{fpc1,fpc2,fpc345,hcc,vac}

RUN cp ./spf1/bin/* ${TANGO_INSTALLATION_DIR}/bin/sim/fpc1
RUN rm -rf /root/ska/sim/fpc1/Spf1
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/sim/fpc1/Spf1 /root/ska/sim/fpc1/Spf1

RUN cp ./spf2/bin/* ${TANGO_INSTALLATION_DIR}/bin/sim/fpc2
RUN rm -rf /root/ska/sim/fpc2/Spf2
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/sim/fpc2/Spf2 /root/ska/sim/fpc2/Spf2

RUN cp ./spf345/bin/* ${TANGO_INSTALLATION_DIR}/bin/sim/fpc345
RUN rm -rf /root/ska/sim/fpc345/Spf345
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/sim/fpc345/Spf345 /root/ska/sim/fpc345/Spf345

RUN cp ./spfHe/bin/* ${TANGO_INSTALLATION_DIR}/bin/sim/hcc
RUN rm -rf /root/ska/sim/hcc/SpfHe
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/sim/hcc/SpfHe /root/ska/sim/hcc/SpfHe

RUN cp ./spfVac/bin/* ${TANGO_INSTALLATION_DIR}/bin/sim/vac
RUN rm -rf /root/ska/sim/vac/SpfVac
RUN ln -s ${TANGO_INSTALLATION_DIR}/bin/sim/vac/SpfVac /root/ska/sim/vac/SpfVac

WORKDIR /root/ska/sim/
RUN ln -sf ../spfc/Spfc Spfc


FROM spfc-sim-make
# The final stage. Create all the tar balls.

# Creat a tarball that contains everything in the TANGO_INSTALLATION_DIR
# and store it in /root.
# No compression because it takes aeons to do it in the emulated docker.
WORKDIR /root/
RUN tar -cf tango-${TANGO_VERSION}.tar ${TANGO_INSTALLATION_DIR}/

# Create a tarball that contains the simulation executables and store it in
# /root.
 RUN tar -czf spfcSim-pkg.tar.gz ska/sim/

# Create a tar ball with all of the SPFC executables including the simluator
# ones.
RUN tar -czf spfc-pkg.tar.gz ska/{spf1,spf2,spf345,spfVac,spfHe}

ENTRYPOINT bash
CMD ["-c", "sleep infinity"]

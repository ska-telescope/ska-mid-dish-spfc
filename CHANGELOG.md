###############
Version History
###############

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

## Unreleased
*************

0.1.5
*****

* [REL-1423] - Made some ska-spfc-deployer chart variables to be optional for ease of use on other repos.

0.1.4
*****

* [REL-1419] - Updated ska-spfc-deployer chart variables for usage outside of ska-mid-dish-spfc

0.1.3
*****

* [AT-1986] - Added custom oci image build to work with ARM32 Docker image

0.1.2
*****

* [AT-1986] - Added missing gitlab includes

0.1.1
*****

* [AT-1986] - First release

  * Register SPFC to tangoDB

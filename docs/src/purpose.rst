=================================
Purpose and general documentation
=================================

Welcome to SPFC repository.


## Getting started

Clone the project (Makefiles incl):

```bash
git clone --recurse-submodules git@gitlab.com:ska-telescope/ska-mid-dish-spfc.git
```

(VSCode users) reopen the project in a devcontainer so that you have tango etc pre-installed. Once that's done, from a terminal inside this running container, you can do any of the following.


### Description

Single Pixel Feeder Controller (SPFC) is a linux device that controls and monitors the following devices:
1. SPF Band 1
2. SPF Band 2
3. SPF Band 345
4. SPF Vaccum services
5. SPF Helium services

SPFC is a self contained credit card size device powered by 400 MHZ ARM processor, running Busybox Linux OS on 512 MB ROM. It has 128MB of RAM. SPFC has Python2 installed. One can SSH into it to change a tango device configuration file before registering to a tango database. Please note that no SSH is needed in order to register it to the tango database. There are two types of SPFC devices in the ITF:

1. SPF Controller which is the SPFC unit used by multiple software teams.
2. SPF Dev which is the SPFC development unit we use as Atlas team for testing.

## Linting

This is useful for keeping your code up to standard.

```bash
make python-format && make python-lint
```

## Testing

Always test your code, preferably locally. This command is the one that tests the python code on your local machine, and may require the use of mocks:

```bash
make python-test
```

### Manual Testing

You can do manual testing against a local deployment running in minikube. In order to create the environment, you will need to have a running minikube, see <https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube>. Once that has been setup, run the following:

```
eval $(minikube docker-env)
make oci-build-all
make itf-spfc-install-chart
```

This deploys the `ska-spfc-deployer` helm chart which is a pod job that registers the SPFC Tango device to the database and also deploys latest configuration files and binaries to the SPFC.


### Registering SPFC device to tango database

There is a set of commands that one can run to register the SPFC device to the tango database.
This is important, for example, in cases where registration SPFC needs to communicate with Dish.LMC

To use the SPFC registration to tango device, please ensure that the TANGO_HOST environment variable is set.
To set the TANGO_HOST variable one can simply run the `export TANGO_HOST=<ipaddress>:<port_number>` command.
Secondly please update the `resources\makefiles\k8s-installs.mk` with correct SPFC configuration information.

We can extract the tango database port from the cluster under specified namespace and service name using the
following command. 
`export TANGO_HOST=$(kubectl -n ${NAMESPACE} get svc ${SERVICE_NAME} -o jsonpath={.status.loadBalancer.ingress[0].ip})`
please see `ska-mid-dish-spfc/resources/makefiles/k8s-installs.mk` file with correct variable values to use as a guide.

Please note:
Before running any SPFC command please ensure that the TANGO_HOST variable is set.

Once done, you can simply run the `make itf-spfc-install-chart --dry-run` to preview steps that will be taken to register
the SPFC. Once satisfied with the preview, you can then `make itf-spfc-install-chart` to register the SPFC to tango DB.
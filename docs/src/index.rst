.. ska-mid-dish-spfc documentation master file, created by
   sphinx-quickstart on Tue Nov 28 17:57:55 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ska-mid-dish-spfc's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Documentation

   purpose

.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 2
   :caption: Readme
   
   README

.. toctree::
   :maxdepth: 2
   :caption: Changelog
   
   CHANGELOG
..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

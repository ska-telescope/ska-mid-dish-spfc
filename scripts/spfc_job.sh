#!/bin/bash
set -eu
set -o pipefail
#Configure SSH for this pod
mkdir  ~/.ssh
SPFC_KEY_FILE=~/.ssh/spfc_rsa
SPFC_HOST=""
echo "$SPFC_PRIVATE_KEY" > $SPFC_KEY_FILE
chmod 600 $SPFC_KEY_FILE
echo "SPFC_IP_ADDRESSES_YAML=${SPFC_IP_ADDRESSES_YAML}"

#$DISH_ID will be in SKA001 format. If its from gitlab CI/CD variables it will contain "_".
if [[ -z  ${DISH_ID} ]];
then
    echo "DISH_ID is not set."
    exit 1;
elif [[ $DISH_ID == *"_"* ]];
then
    DISH_ID=$(echo "$DISH_ID" | cut -d'_' -f2)
else
    DISH_ID=${DISH_ID}
fi
echo "DISH_ID=${DISH_ID}"

#Install pytango, this image does not contain it as yet
pip install pytango pyyaml
export SPFC_HOST=$(python3 /scripts/parse_yaml.py ${DISH_ID})

#Install dnsutils in order to use dig to resolve the TangoDB namespace to IP address
apt-get update && apt-get install dnsutils iputils-ping -y

cat << EOF > ~/.ssh/config
    Host $SPFC_HOST
    KexAlgorithms +diffie-hellman-group1-sha1,diffie-hellman-group14-sha1
    PubkeyAcceptedKeyTypes=+ssh-rsa
    HostKeyAlgorithms=+ssh-rsa
    IdentityFile ~/.ssh/spfc_rsa
EOF

echo "Task selected: Register ${DISH_ID} on TangoDB."
if [[ $SPFC_TASK = "register-spfc" || -z "$SPFC_TASK" ]];
then
    export TANGO_INSTALLATION_DIR=/home/ska/tango.9.5.0
    export LD_LIBRARY_PATH=${TANGO_INSTALLATION_DIR}/lib
    LOCAL_CONFIG_FILE=~/tango_host.ini

    touch ~/.ssh/tango_host.ini
    touch $LOCAL_CONFIG_FILE

    #Configure the SPFC
    scp -o StrictHostKeyChecking=no $LOCAL_CONFIG_FILE $SPFC_USER@$SPFC_HOST:/home/$SPFC_USER

    #Copy the spfc_config.ini file from remote so that we can get more information, like serial number of the SPFC host
    scp -o StrictHostKeyChecking=no $SPFC_USER@$SPFC_HOST:/var/lib/spfc/spfc/spfc_config.ini ~
    echo "Getting serial number..."
    SER="`awk "/SPFC=/ {print}" ~/spfc_config.ini`"
    export SERIAL_NO="`echo ${SER} | awk '{split($0,SERIAL_NO,"="); print SERIAL_NO[2]}'`"
    export TANGO_HOST=$(dig +short @kube-service-extdns-extdns-coredns-metallb.extdns.svc.miditf.internal.skao.int tango-databaseds.${KUBE_NAMESPACE}.svc.miditf.internal.skao.int):10000


    #Update SPFC config file
    ssh $SPFC_USER@$SPFC_HOST "
    cat << EOF > /home/skao_user/tango_host.ini
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
    export TANGO_INSTALLATION_DIR=${TANGO_INSTALLATION_DIR}
    TANGO_HOST=${TANGO_HOST}
EOF"

    #start devide registration
    ssh -T $SPFC_USER@$SPFC_HOST sh -s << EOF
        export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
        export TANGO_INSTALLATION_DIR=${TANGO_INSTALLATION_DIR}
        export TANGO_HOST=${TANGO_HOST}

        echo "Registering SPFC devices..."
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-server Spf1/s${SERIAL_NO} Spf1 ${DISH_ID}/spf/spf1
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-server Spf2/s${SERIAL_NO} Spf2 ${DISH_ID}/spf/spf2
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-server Spf345/s${SERIAL_NO} Spf345 ${DISH_ID}/spf/spf345
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-server SpfHe/s${SERIAL_NO} SpfHe ${DISH_ID}/spf/spfHe
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-server SpfVac/s${SERIAL_NO} SpfVac ${DISH_ID}/spf/spfVac
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-server Spfc/s${SERIAL_NO} Spfc ${DISH_ID}/spf/spfc

        echo "Adding device properties..."
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spf1 ttyPort /dev/ttyS1

        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spf2 SpfHeLocation ${DISH_ID}/spf/spfhe
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spf2 SpfVacLocation ${DISH_ID}/spf/spfvac
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spf2 ttyPort /dev/ttyS2

        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spf345 SpfHeLocation ${DISH_ID}/spf/spfhe
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spf345 SpfVacLocation ${DISH_ID}/spf/spfvac
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spf345 ttyPort /dev/ttyS3

        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spfc Spf1Location ${DISH_ID}/spf/spf1
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spfc Spf2Location ${DISH_ID}/spf/spf2
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spfc Spf345Location ${DISH_ID}/spf/spf345
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spfc SpfHeLocation ${DISH_ID}/spf/spfhe
        ${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ${TANGO_INSTALLATION_DIR}/bin/tango_admin --add-property ${DISH_ID}/spf/spfc SpfVacLocation ${DISH_ID}/spf/spfvac
EOF
    ssh -T $SPFC_USER@$SPFC_HOST "sudo /bin/systemctl restart spfc-sim.target"
    echo "...SPFC registration complete..."
    #Once the SPFC registration is complete, we shall update the SPFC to be in simulation mode.
    #By default SPFC deployment is in simualtion mode, unless the SPF_DEVICES variable is set to false
    #Converting to lowercase for simplistic string comparison
    for ((i=0; i<5; i++)); do
        if [[ $(ping -q -c2 1 $SPFC_HOST) ]];
        then
            echo "Configuring SPFC to simulated mode."    
            if [[ -z "${SPF_DEVICES-}" || "${SPF_DEVICES,,}" = "false" ]];
            then
                #Changing SPFC to somulation mode
                ssh -T $SPFC_USER@$SPFC_HOST "unset LD_LIBRARY_PATH"
                #wait for the SPFC simulator service to start.
                sleep 15
                echo $(ssh -T $SPFC_USER@$SPFC_HOST "systemctl status spfcsim")
                python3 /scripts/spfc_sim.py ${DISH_ID}
            elif [[ "${SPF_DEVICES,,}" = "true" ]];
            then
                echo "SPF_DEVICES=${SPF_DEVICES}. SPFC uses real hardware."
            else
                echo "SPF_DEVICES=${SPF_DEVICES}. Unknown value."
            fi
            ssh -T $SPFC_USER@$SPFC_HOST "export LD_LIBRARY_PATH=/home/ska/tango.9.5.0/lib"
            echo "***SPFC configured to simulation mode with success.***"
            break
        else
            sleep 5
            echo "${i} Retrying SPFC ping."
            echo $(ping $SPFC_HOST)
        fi
    done

elif [[ $SPFC_TASK = "update-spfc" ]];
then
    SPFC_HOST=$SPFC_HOST_DEV #Restrict this update SPFC task to only work for the devkit, not the production one.
    #We only do manual update to the production SPFC and do manual testing. That is beta software to be first run on dekit.
    echo "Updating SPFC with latest files..."
    declare -a dirs=("spf1" "spf2" "spf345" "spfhe" "spfvac" "spfc")
    declare -a files=("Spf1" "Spf2" "Spf345" "SpfHe" "SpfVac" "Spfc")

    cd /spfc-cfg-files && tar -xf spfc-pkg.tar.gz

    #Copy binary and config files for all devices
    for i in "${!dirs[@]}"
    do
        echo "Copying..."${files[i]}
        scp -o StrictHostKeyChecking=no /spfc-cfg-files/ska/${dirs[i]}/${files[i]} $SPFC_USER@$SPFC_HOST:/home/ska/${dirs[i]}
    done

    #Copy sim binaries
    declare -a sim_dirs=("fpc1" "fpc2" "fpc345" "hcc" "vac")
    declare -a sim_files=("Spf1_ARMsim_v2.05" "Spf2_ARMsim_v2.04" "Spf345_ARMsim_v1.07" "SpfHe_ARMsim_v02.15" "SpfVac_ARMsim_v2.09")

    tar -xf spfcSim-pkg.tar.gz
    for i in "${!sim_dirs[@]}"
    do
        echo "Copying..."${sim_files[i]}" and "${files[i]}
        scp -o StrictHostKeyChecking=no /spfc-cfg-files/ska/sim/${sim_dirs[i]}/${sim_files[i]} $SPFC_USER@$SPFC_HOST:/home/ska/sim/${sim_dirs[i]}
        scp -o StrictHostKeyChecking=no /spfc-cfg-files/ska/sim/${sim_dirs[i]}/${files[i]} $SPFC_USER@$SPFC_HOST:/home/ska/sim/${sim_dirs[i]}
    done

    #Copy sim config files
    cd /spfc-cfg-files/ska-mid-dish-spfc/src/ska_emss/
    declare -a cgf_files=("spf1sim.service" "spf2sim.service" "spf345sim.service" "spfhesim.service" "spfvacsim.service" "spfcsim.service")
    for i in "${!dirs[@]}"
    do
        scp -o StrictHostKeyChecking=no ${dirs[i]}/systemd/system/${cgf_files[i]} $SPFC_USER@$SPFC_HOST:/lib/systemd/system
    done

    #Copy spf_starter script
    scp -o StrictHostKeyChecking=no /spfc-cfg-files/ska_emss/scripts/spf_starter.sh $SPFC_USER@$SPFC_HOST:/home/ska/
else
    echo "Error: Unknown command: ${SPFC_TASK}"
fi
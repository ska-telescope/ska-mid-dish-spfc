from tango import DeviceProxy, ExtractAs
from time import sleep
import os
import sys

def spfc_full_mode(spfc):
    #Set b2ExpectedOnline to True so that there is a simulated feed connected on the virtual port.
    print("Set b2ExpectedOnline True")
    spfc.b2ExpectedOnline = True
    sleep(6)
    print("Set feedmode 2,0")
    spfc.SetFeedMode([2,0])
    sleep(6)
    print("Set feedmode 2,1")
    spfc.SetFeedMode([2,1])
    sleep(6)
    print("Set b2LnaPidTempSetPoint 4")
    spfc.b2LnaPidTempSetPoint = 4
    sleep(6)
    print("Set b2LnaPidPowerState True")
    spfc.b2LnaPidPowerState = True
    sleep(6)
    print(spfc.b2CapabilityState)


print("****Configuring SPFC to simulation mode***")
dish_id = (sys.argv[1])
spfc = DeviceProxy(dish_id+"/spf/spfc")
spfv = DeviceProxy(dish_id+"/spf/spfvac")
spfc_full_mode(spfc)

#Wait for 2 minutes for b2CapabilityState to change from OPERATE-DEGRADED to OPERATE-FULL        
sleep(120)
print( f"Mode={ str(spfc.b2CapabilityState)  }")     
if("OPERATE-FULL" in str(spfc.b2CapabilityState)):
    print("SPFC is in OPERATE-FULL state. Configuration is complete.")
else:
    print("SPFC is in error state. Retrying...")
    if( "Error" in str(spfv.read_attribute("State", extract_as=ExtractAs.String))):
        spfv.SetVacuumMode = 0
        sleep(6)
        spfc.SendFeedCommand([2,1])
        sleep(6)
        spfc.SetFeedMode([2,1])
    spfc_full_mode(spfc)
import argparse
import os
import yaml

parser = argparse.ArgumentParser(description="yaml file parser. Pass dish index in the form `ska001`")
parser.add_argument("dish_id", nargs='?',default="ska001")
args=parser.parse_args()

yaml_file_contents = os.getenv("SPFC_IP_ADDRESSES_YAML")
y = yaml.safe_load(str(yaml_file_contents))
dish_id = str(args.dish_id).lower()
print(y[dish_id])
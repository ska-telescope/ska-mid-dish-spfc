#!/usr/bin/env /bin/bash
[[ ${#} -ne 1 ]] && { echo "The SPF type must be provided as the only parameter! Example: ${0} Vac"; exit 0; }

# Create a temporary directory just for this process.
tmpDir=$(mktemp -d /tmp/$(basename ${0}).XXXXXX) || { echo "Cannot create a TMP directory!"; exit 1; }
# This trap makes sure that it will automatically be deleted once this process stops running.
trap ' { [[ -d ${tmpDir} ]] && rm -rf ${tmpDir}; exit 0; }; ' ABRT EXIT HUP INT TERM QUIT
# Now export this directory as the new TMP directory for the processes that we run from here.
export TMP=${tmpDir}
export TEMP=${TMP}

cd /home/ska/sim/vac/
[[ -f /var/lib/spfc/spfc/spfc_config.ini ]] && source /var/lib/spfc/spfc/spfc_config.ini
[[ -f /home/skao_user/tango_host.ini ]] && source /home/skao_user/tango_host.ini
export LD_LIBRARY_PATH=${TANGO_INSTALLATION_DIR}/lib 
echo "Starting SpfVac Simulator ..."
${TANGO_INSTALLATION_DIR}/lib/ld-linux.so.3 ./SpfVac s${SPFC} $spfvacServ

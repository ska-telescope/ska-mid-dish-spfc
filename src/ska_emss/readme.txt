# BUILD
In order to build only one device at a time, run $make from each sub folder.
Or to build all Spfc sub-devices, run make from the /ska_emss folder.

The binary name will be versioned in each subfolder in eg.: ./spfX/bin/SpfX_ARM_v2.05 with a symbolic link SpfX pointing to it.
/*
 * system.h
 *
 *  Created on: 15 Dec 2016
 *      Author: theuns
 */

#ifndef BSP_SYSTEM_H_
#define BSP_SYSTEM_H_

/* Pin definitions */
#define PIN_A_0 		(0 + (32 * 1))
#define PIN_A_1 		(1 + (32 * 1))
#define PIN_A_2 		(2 + (32 * 1))
#define PIN_A_3 		(3 + (32 * 1))
#define PIN_A_4 		(4 + (32 * 1))
#define PIN_A_5 		(5 + (32 * 1))
#define PIN_A_24 		(24 + (32 * 1))
#define PIN_A_30 		(30 + (32 * 1))
#define PIN_A_31 		(31 + (32 * 1))

#define PIN_B_0 		(0 + (32 * 2))
#define PIN_B_1 		(1 + (32 * 2))
#define PIN_B_2 		(2 + (32 * 2))
#define PIN_B_3 		(3 + (32 * 2))
#define PIN_B_4 		(4 + (32 * 2))
#define PIN_B_5 		(5 + (32 * 2))
#define PIN_B_6 		(6 + (32 * 2))
#define PIN_B_7 		(7 + (32 * 2))
#define PIN_B_8 		(8 + (32 * 2))
#define PIN_B_9 		(9 + (32 * 2))
#define PIN_B_10 		(10 + (32 * 2))
#define PIN_B_11 		(11 + (32 * 2))
#define PIN_B_12 		(12 + (32 * 2))
#define PIN_B_13 		(13 + (32 * 2))
#define PIN_B_14 		(14 + (32 * 2))
#define PIN_B_15 		(15 + (32 * 2))
#define PIN_B_16 		(16 + (32 * 2))
#define PIN_B_17 		(17 + (32 * 2))
#define PIN_B_18 		(18 + (32 * 2))
#define PIN_B_19 		(19 + (32 * 2))
#define PIN_B_20 		(20 + (32 * 2))
#define PIN_B_21 		(21 + (32 * 2))
#define PIN_B_22 		(22 + (32 * 2))
#define PIN_B_23 		(23 + (32 * 2))
#define PIN_B_24 		(24 + (32 * 2))
#define PIN_B_25 		(25 + (32 * 2))
#define PIN_B_26 		(26 + (32 * 2))
#define PIN_B_27 		(27 + (32 * 2))
#define PIN_B_28 		(28 + (32 * 2))
#define PIN_B_29 		(29 + (32 * 2))
#define PIN_B_30 		(30 + (32 * 2))
#define PIN_B_31 		(31 + (32 * 2))


#define PIN_VACPUMP_VALVE		PIN_B_0
//#define N_PIN_VACPUMP_VALVE	"GPIOB0:"
#define PIN_VACPUMP_ON_OFF		PIN_A_3
//#define N_PIN_VACPUMP_ON_OFF	"GPIOA3:"

#define PIN_VACPUMP_READY		PIN_A_1
#define N_PIN_VACPUMP_READY		"GPIOA1:"
#define PIN_VACPUMP_RUNNING		PIN_A_2
#define N_PIN_VACPUMP_RUNNING	"GPIOA2:"


#define PIN_HCOMPR_nRESET		PIN_B_30
#define N_PIN_HCOMPR_nRESET		"GPIOB30:"
#define PIN_HCOMPR_RUNNING		PIN_B_31
#define N_PIN_HCOMPR_RUNNING	"GPIOB31:"


#define TXD1					PIN_B_6
#define N_TXD1					"GPIOB6:"
#define RXD1					PIN_B_7
#define N_RXD1					"GPIOB7:"
#define TXD2					PIN_B_8
#define N_TXD2					"GPIOB8:"
#define RXD2					PIN_B_9
#define N_RXD2					"GPIOB9:"
#define TXD3					PIN_B_10
#define N_TXD3					"GPIOB10:"
#define RXD3					PIN_B_11
#define N_RXD3					"GPIOB11:"


#define LED_B1_ONLINE				PIN_B_22
#define N_LED_B1_ONLINE			"GPIOB22:"
#define LED_B2_ONLINE				PIN_B_20
#define N_LED_B2_ONLINE			"GPIOB20:"
#define LED_B345_ONLINE				PIN_B_26
#define N_LED_B345_ONLINE			"GPIOB26:"
#define LED_AUX_ONLINE				PIN_B_24
#define N_LED_AUX_ONLINE			"GPIOB24:"

#define LED_VACPUMP_READY			PIN_B_13
#define N_LED_VACPUMP_READY			"GPIOB13:"
#define LED_VACPUMP_RUNNING			PIN_B_12
#define N_LED_VACPUMP_RUNNING		"GPIOB12:"
#define LED_HCOMPR_ERROR			PIN_B_21
#define N_LED_HCOMPR_ERROR			"GPIOB21:"
#define LED_HCOMPR_RUNNING			PIN_B_25
#define N_LED_HCOMPR_RUNNING		"GPIOB25:"

/* ADC Channels */
#define ADC_PCB_VOLT_CH 		0
#define ADC_PCB_CURRENT_CH 		1
#define ADC_PCB_TEMP_CH 		2

#define TIME_PATH 					"/sys/class/i2c-adapter/i2c-0/0-006b/elapsed_time"

extern string devName;

#define BASE_PATH_SD		"/media/card"
#define BASE_PATH_LOCAL		"/var/log"

#define LOG_PATH			"/spfc/logs"


#define BASE_LIB_PATH		"/var/lib/spfc"

#define SPFC_SETTINGS_PATH		BASE_LIB_PATH "/spfc"
#define SPFC_UPLOAD_PATH		BASE_LIB_PATH "/uploads"
#define SPFC_SETTINGS			SPFC_SETTINGS_PATH "/spfc_settings.ini" /* Variables and constants for software. Variable */
#define SPFC_CONFIG				SPFC_SETTINGS_PATH "/spfc_config.ini"	/* Serial number and hardware specific settings. Fixed */

/* SPF 345 paths */
#define SPF345_SETTINGS_PATH	BASE_LIB_PATH "/spf345"
#define SPF345_SETTINGS			SPF345_SETTINGS_PATH "/spf345_settings.ini" /* Variables and constants for software. Variable */
#define SPF345_CONFIG			SPF345_SETTINGS_PATH "/spf345_config.ini"   /* Serial number and hardware specific settings. Fixed */
#define SPF345_LOG_SD			BASE_PATH_SD LOG_PATH "/spf345"				/* Logging path when SD card is available */
#define SPF345_LOG_LOCAL		BASE_PATH_LOCAL LOG_PATH "/spf345"			/* Logging path when SD card is NOT available */
#define SPF345_CONFIG_UL		SPFC_UPLOAD_PATH "/spf345_ConfigData.ini" /* Serial number and hardware specific settings UPLOAD file. */
#define SPF3_REC_GAIN			SPF345_SETTINGS_PATH "/RecGain3.csv"  /* Receiver gain model file*/
#define SPF3_REC_NOISE			SPF345_SETTINGS_PATH "/RecNoise3.csv" /* Receiver noise model file*/
#define SPF3_CAL_NOISE			SPF345_SETTINGS_PATH "/CalNoise3.csv" /* Calibration noise model file*/
#define SPF4_REC_GAIN			SPF345_SETTINGS_PATH "/RecGain4.csv"  /* Receiver gain model file*/
#define SPF4_REC_NOISE			SPF345_SETTINGS_PATH "/RecNoise4.csv" /* Receiver noise model file*/
#define SPF4_CAL_NOISE			SPF345_SETTINGS_PATH "/CalNoise4.csv" /* Calibration noise model file*/
#define SPF5A_REC_GAIN			SPF345_SETTINGS_PATH "/RecGain5a.csv"  /* Receiver gain model file*/
#define SPF5A_REC_NOISE			SPF345_SETTINGS_PATH "/RecNoise5a.csv" /* Receiver noise model file*/
#define SPF5A_CAL_NOISE			SPF345_SETTINGS_PATH "/CalNoise5a.csv" /* Calibration noise model file*/
#define SPF5B_REC_GAIN			SPF345_SETTINGS_PATH "/RecGain5b.csv"  /* Receiver gain model file*/
#define SPF5B_REC_NOISE			SPF345_SETTINGS_PATH "/RecNoise5b.csv" /* Receiver noise model file*/
#define SPF5B_CAL_NOISE			SPF345_SETTINGS_PATH "/CalNoise5b.csv" /* Calibration noise model file*/
#define SPF6_REC_GAIN			SPF345_SETTINGS_PATH "/RecGain6.csv"  /* Receiver gain model file*/
#define SPF6_REC_NOISE			SPF345_SETTINGS_PATH "/RecNoise6.csv" /* Receiver noise model file*/
#define SPF6_CAL_NOISE			SPF345_SETTINGS_PATH "/CalNoise6.csv" /* Calibration noise model file*/
#define SPF345_TEMP_CAL_CURVES	SPF345_SETTINGS_PATH "/TempCalCurves.csv"  /* Receiver temperature calibration curves file*/
#define SPF345_PRESS_CAL_CURVES	SPF345_SETTINGS_PATH "/PressCalCurves.csv" /* Receiver pressure calibration curves file*/
#define SPF3_REC_GAIN_UL		SPFC_UPLOAD_PATH "/RecGain3.csv"  /* Receiver gain model UPLOAD file*/
#define SPF3_REC_NOISE_UL		SPFC_UPLOAD_PATH "/RecNoise3.csv" /* Receiver noise model UPLOAD file*/
#define SPF3_CAL_NOISE_UL		SPFC_UPLOAD_PATH "/CalNoise3.csv" /* Calibration noise model UPLOAD file*/
#define SPF4_REC_GAIN_UL		SPFC_UPLOAD_PATH "/RecGain4.csv"  /* Receiver gain model UPLOAD file*/
#define SPF4_REC_NOISE_UL		SPFC_UPLOAD_PATH "/RecNoise4.csv" /* Receiver noise model UPLOAD file*/
#define SPF4_CAL_NOISE_UL		SPFC_UPLOAD_PATH "/CalNoise4.csv" /* Calibration noise model UPLOAD file*/
#define SPF5A_REC_GAIN_UL		SPFC_UPLOAD_PATH "/RecGain5a.csv"  /* Receiver gain model UPLOAD file*/
#define SPF5A_REC_NOISE_UL		SPFC_UPLOAD_PATH "/RecNoise5a.csv" /* Receiver noise model UPLOAD file*/
#define SPF5A_CAL_NOISE_UL		SPFC_UPLOAD_PATH "/CalNoise5a.csv" /* Calibration noise model UPLOAD file*/
#define SPF5B_REC_GAIN_UL		SPFC_UPLOAD_PATH "/RecGain5b.csv"  /* Receiver gain model UPLOAD file*/
#define SPF5B_REC_NOISE_UL		SPFC_UPLOAD_PATH "/RecNoise5b.csv" /* Receiver noise model UPLOAD file*/
#define SPF5B_CAL_NOISE_UL		SPFC_UPLOAD_PATH "/CalNoise5b.csv" /* Calibration noise model UPLOAD file*/
#define SPF6_REC_GAIN_UL		SPFC_UPLOAD_PATH "/RecGain6.csv"  /* Receiver gain model UPLOAD file*/
#define SPF6_REC_NOISE_UL		SPFC_UPLOAD_PATH "/RecNoise6.csv" /* Receiver noise model UPLOAD file*/
#define SPF6_CAL_NOISE_UL		SPFC_UPLOAD_PATH "/CalNoise6.csv" /* Calibration noise model UPLOAD file*/

#define SPFHE_SETTINGS_PATH		BASE_LIB_PATH "/spfhe"
#define SPFHE_SETTINGS			SPFHE_SETTINGS_PATH "/spfhe_settings.ini"	/* Variables and constants for software. Variable */
#define SPFHE_CONFIG			SPFHE_SETTINGS_PATH "/spfhe_config.ini"   	/* Serial number and hardware specific settings. Fixed */
#define SPFHE_LOG_SD			BASE_PATH_SD LOG_PATH "/spfhe"				/* Logging path when SD card is available */
#define SPFHE_LOG_LOCAL			BASE_PATH_LOCAL LOG_PATH "/spfhe"			/* Logging path when SD card is NOT available */

#define SPFVAC_SETTINGS_PATH	BASE_LIB_PATH "/spfvac"
#define SPFVAC_SETTINGS			SPFVAC_SETTINGS_PATH "/spfvac_settings.ini"	/* Variables and constants for software. Variable */
#define SPFVAC_CONFIG			SPFVAC_SETTINGS_PATH "/spfvac_config.ini"   /* Serial number and hardware specific settings. Fixed */

#define UPLOADS_PATH			BASE_LIB_PATH "/uploads"
#define RSC_SERIAL				BASE_LIB_PATH "/spfcSerial.ini"



#endif /* BSP_SYSTEM_H_ */

// IniParser
// The purpose of this class is to provide the means to easily store key/value
// pairs in a config file, seperated by independant sections. Sections may not
// have duplicate keys, although two or more sections can have the same key.
// Simple support for comments is included. Each key, and each section may have
// it's own multiline comment.
//
// An example might look like this;
//
// [UserSettings]
// Name=Joe User
// Date of Birth=12/25/01
//
// ;
// ; Settings unique to this server
// ;
// [ServerSettings]
// Port=1200
// IP_Address=127.0.0.1
// MachineName=ADMIN
//

#include <vector>

#include <iostream>
#include <stdarg.h>
#include <fstream>

#include <float.h>
#include <ctype.h>
#include <climits>
#include <cstring>

#include <string>
//#include <string.h>
//#include <strings.h> // for strcasecmp

#include "utilities.h" // differetn trim function
#include "IniParser.h"

// IniParser
// Our default constructor.  If it can load the file, it will do so and populate
// the section list with the values from the file.
IniParser::IniParser(std::string szFileName)
{
	m_bDirty = false;
	m_szFileName = szFileName;
	m_Flags = (AUTOCREATE_SECTIONS | AUTOCREATE_KEYS);
	m_t_Section = new t_Section;
	m_Sections.push_back(*m_t_Section);

	if (!Load(m_szFileName))
	{
		SetFileName(m_szFileName);
	}
}

IniParser::IniParser()
{
	Clear();
	m_Flags = (AUTOCREATE_SECTIONS | AUTOCREATE_KEYS);
	m_t_Section = new t_Section;
	m_Sections.push_back(*m_t_Section);
}

// ~IniParser
// Saves the file if any values have changed since the last save.
IniParser::~IniParser()
{
	if (m_bDirty)
	{
		/* TJS - DEBUG */
		std::cout << "DIRTY!" << std::endl;
		Save();
	}
	delete m_t_Section;
}

// Reload
// Reload any changed values from file.
bool IniParser::Reload()
{
	return Load(m_szFileName);
}

// Clear
// Resets the member variables to their defaults
void IniParser::Clear()
{
	m_bDirty = false;
	m_szFileName = std::string("");
	m_Sections.clear();
}

// SetFileName
// Set's the m_szFileName member variable. For use when creating the IniParser
// object by hand (-vs- loading it from a file
void IniParser::SetFileName(std::string szFileName)
{
	if (m_szFileName.size() != 0 && CompareNoCase(szFileName, m_szFileName) != 0)
	{
		m_bDirty = true;

		Report(E_WARN, "[IniParser::SetFileName] The filename has changed from <%s> to <%s>.",
			   m_szFileName.c_str(), szFileName.c_str());
	}

	m_szFileName = szFileName;
}

// Load
// Attempts to load in the text file. If successful it will populate the 
// Section list with the key/value pairs found in the file. Note that comments
// are saved so that they can be rewritten to the file later.
bool IniParser::Load(std::string szFileName)
{
	// We don't want to create a new file here.  If it doesn't exist, just
	// return false and report the failure.
	std::fstream File(szFileName.c_str());

	if ( File.is_open() )
	{
		bool bDone = false;
		bool bAutoKey = (m_Flags & AUTOCREATE_KEYS) == AUTOCREATE_KEYS;
		bool bAutoSec = (m_Flags & AUTOCREATE_SECTIONS) == AUTOCREATE_SECTIONS;
		
		std::string szLine;
		std::string szComment;
		//char buffer[MAX_BUFFER_LEN];
		t_Section* pSection = GetSection("");

		// These need to be set, we'll restore the original values later.
		m_Flags |= AUTOCREATE_KEYS;
		m_Flags |= AUTOCREATE_SECTIONS;

		while ( std::getline(File, szLine) )
		{
			/*
			memset(buffer, 0, MAX_BUFFER_LEN);
			File.getline(buffer, MAX_BUFFER_LEN);

			szLine = buffer;

			File.getline(szLine);
			*/
			Trim(szLine);

			//bDone = ( File.eof() || File.bad() || File.fail() );

			if ( szLine.find_first_of(CommentIndicators) == 0 )
			{
				szComment += "\n";
				szComment += szLine;
			}
			else
			if ( szLine.find_first_of('[') == 0 ) // new section
			{
				szLine.erase( 0, 1 );
				szLine.erase( szLine.find_last_of(']'), 1 );

				CreateSection(szLine, szComment);
				pSection = GetSection(szLine);
				szComment = "";
			}
			else 
			if ( szLine.size() > 0 ) // we have a key, add this key/value pair
			{
				std::string szKey = GetNextWord(szLine);
				std::string szValue = szLine;

				if ( szKey.size() > 0 && szValue.size() > 0 )
				{
					SetValue(szKey, szValue, szComment, pSection->szName);
					szComment = "";
				}
			}
		}

		// Restore the original flag values.
		if ( !bAutoKey )
		{
			m_Flags &= ~AUTOCREATE_KEYS;
		}

		if ( !bAutoSec )
		{
			m_Flags &= ~AUTOCREATE_SECTIONS;
		}
	}
	else
	{
		Report(E_INFO, "[IniParser::Load] Unable to open file [%s]. Does it exist?", szFileName.c_str());
		return false;
	}

	/* TJS - The file cannot be dirty when we load it.. */
	m_bDirty = false;

	File.close();

	return true;
}


// Save
// Attempts to save the Section list and keys to the file. Note that if Load
// was never called (the IniParser object was created manually), then you
// must set the m_szFileName variable before calling save.
bool IniParser::Save()
{
	if ( KeyCount() == 0 && SectionCount() == 0 )
	{
		// no point in saving
		Report(E_INFO, "[IniParser::Save] Nothing to save.");
		return false; 
	}

	if ( m_szFileName.size() == 0 )
	{
		Report(E_ERROR, "[IniParser::Save] No filename has been set.");
		return false;
	}

	std::fstream File(m_szFileName.c_str(), std::ios::out|std::ios::trunc);

	if ( File.is_open() )
	{
		SectionItor s_pos;
		KeyItor k_pos;
		t_Section Section;
		t_Key Key;

		for (s_pos = m_Sections.begin(); s_pos != m_Sections.end(); s_pos++)
		{
			Section = (*s_pos);
			bool bWroteComment = false;

			if ( Section.szComment.size() > 0 )
			{
				bWroteComment = true;
				//WriteLn(File, "\n%s", CommentStr(Section.szComment).c_str());
				WriteLn(File, "\n" + CommentStr(Section.szComment));
			}

			if ( Section.szName.size() > 0 )
			{

				//WriteLn(File, "%s[%s]", bWroteComment ? "" : "\n", Section.szName.c_str());
				if (bWroteComment)
				{
					WriteLn(File, "\n[" + Section.szName + "]");
				}
				else
				{
					WriteLn(File, "[" + Section.szName + "]");
				}
			}

			for (k_pos = Section.Keys.begin(); k_pos != Section.Keys.end(); k_pos++)
			{
				Key = (*k_pos);

				if ( Key.szKey.size() > 0 && Key.szValue.size() > 0 )
				{
					WriteLn(File,
						(Key.szComment.size() > 0 ? "\n" : "") +
						(CommentStr(Key.szComment)) +
						(Key.szComment.size() > 0 ? "\n" : "") +
						(Key.szKey) +
						(EqualIndicators[0]) +
						(Key.szValue));
				}
			}
		}
	}
	else
	{
		Report(E_ERROR, "[IniParser::Save] Unable to save file.");
		return false;
	}

	m_bDirty = false;
	
	File.flush();
	File.close();

	return true;
}

// SetKeyComment
// Set the comment of a given key. Returns true if the key is not found.
bool IniParser::SetKeyComment(std::string szKey, std::string szComment, std::string szSection)
{
	KeyItor k_pos;
	t_Section* pSection;

	if ( (pSection = GetSection(szSection)) == NULL )
		return false;

	for (k_pos = pSection->Keys.begin(); k_pos != pSection->Keys.end(); k_pos++)
	{
		if ( CompareNoCase( (*k_pos).szKey, szKey ) == 0 )
		{
			(*k_pos).szComment = szComment;
			m_bDirty = true;
			return true;
		}
	}

	return false;
}

// SetSectionComment
// Set the comment for a given section. Returns false if the section
// was not found.
bool IniParser::SetSectionComment(std::string szSection, std::string szComment)
{
	SectionItor s_pos;

	for (s_pos = m_Sections.begin(); s_pos != m_Sections.end(); s_pos++)
	{
		if ( CompareNoCase( (*s_pos).szName, szSection ) == 0 ) 
		{
		    (*s_pos).szComment = szComment;
			m_bDirty = true;
			return true;
		}
	}

	return false;
}


// SetValue
// Given a key, a value and a section, this function will attempt to locate the
// Key within the given section, and if it finds it, change the keys value to
// the new value. If it does not locate the key, it will create a new key with
// the proper value and place it in the section requested.
bool IniParser::SetValue(std::string szKey, std::string szValue, std::string szComment, std::string szSection)
{
	t_Key* pKey = GetKey(szKey, szSection);
	t_Section* pSection = GetSection(szSection);

	if (pSection == NULL)
	{
		if ( !(m_Flags & AUTOCREATE_SECTIONS) || !CreateSection(szSection,""))
			return false;

		pSection = GetSection(szSection);
	}

	// Sanity check...
	if ( pSection == NULL )
		return false;

	// if the key does not exist in that section, and the value passed 
	// is not std::string("") then add the new key.
	if ( pKey == NULL && szValue.size() > 0 && (m_Flags & AUTOCREATE_KEYS))
	{
		pKey = new t_Key;

		pKey->szKey = szKey;
		pKey->szValue = szValue;
		pKey->szComment = szComment;
		
		m_bDirty = true;
		
		pSection->Keys.push_back(*pKey);

		return true;
	}

	if ( pKey != NULL )
	{
		pKey->szValue = szValue;
		pKey->szComment = szComment;

		m_bDirty = true;
		
		return true;
	}

	return false;
}

// SetFloat
// Passes the given float to SetValue as a string
bool IniParser::SetFloat(std::string szKey, float fValue, std::string szComment, std::string szSection)
{
	return SetValue(szKey, std::to_string(fValue), szComment, szSection);
}

// SetDouble
// Passes the given double to SetValue as a string
bool IniParser::SetDouble(std::string szKey, double dValue, std::string szComment, std::string szSection)
{
	return SetValue(szKey, std::to_string(dValue), szComment, szSection);
}
// SetInt
// Passes the given int to SetValue as a string
bool IniParser::SetInt(std::string szKey, int nValue, std::string szComment, std::string szSection)
{
	return SetValue(szKey, std::to_string(nValue), szComment, szSection);
}

// SetUInt
// Passes the given int to SetValue as a string
bool IniParser::SetUInt(std::string szKey, unsigned int nValue, std::string szComment, std::string szSection)
{
	return SetValue(szKey, std::to_string(nValue), szComment, szSection);
}

// SetBool
// Passes the given bool to SetValue as a string
bool IniParser::SetBool(std::string szKey, bool bValue, std::string szComment, std::string szSection)
{
	std::string szValue = bValue ?  "True" : "False";

	return SetValue(szKey, szValue, szComment, szSection);
}

// GetValue
// Returns the key value as a std::string object. A return value of
// std::string("") indicates that the key could not be found.
std::string IniParser::GetValue(std::string szKey, std::string szSection)
{
	t_Key* pKey = GetKey(szKey, szSection);

	return (pKey == NULL) ? std::string("") : util::trim(pKey->szValue);
}

// GetString
// Returns the key value as a std::string object. A return value of
// std::string("") indicates that the key could not be found.
std::string IniParser::GetString(std::string szKey, std::string szSection)
{
	return GetValue(szKey, szSection);
}

// GetFloat
// Returns the key value as a float type. Returns FLT_MIN if the key is
// not found.
float IniParser::GetFloat(std::string szKey, std::string szSection)
{
	std::string szValue = GetValue(szKey, szSection);

	if ( szValue.size() == 0 )
		return FLT_MIN;

	return (float)atof( szValue.c_str() );
}

// GetDouble
// Returns the key value as a double type. Returns DBL_MIN if the key is
// not found.
double IniParser::GetDouble(std::string szKey, std::string szSection)
{
	std::string szValue = GetValue(szKey, szSection);

	if ( szValue.size() == 0 )
		return DBL_MIN;

	return stod( szValue );
}

// GetInt
// Returns the key value as an integer type. Returns INT_MIN if the key is
// not found.
int	IniParser::GetInt(std::string szKey, std::string szSection)
{
	std::string szValue = GetValue(szKey, szSection);

	if ( szValue.size() == 0 )
		return INT_MIN;

	return atoi( szValue.c_str() );
}

// GetInt
// Returns the key value as an integer type. Returns INT_MIN if the key is
// not found.
unsigned int IniParser::GetUInt(std::string szKey, std::string szSection)
{
	std::string szValue = GetValue(szKey, szSection);

	if ( szValue.size() == 0 )
	{
		return 0;
	}

	return stoul(szValue);
}

// GetBool
// Returns the key value as a bool type. Returns false if the key is
// not found.
bool IniParser::GetBool(std::string szKey, std::string szSection)
{
	bool bValue = false;
	std::string szValue = GetValue(szKey, szSection);

	/* TJS - Added because true is returned when szValue == "" */
	if ( szValue.size() == 0 ) return false;

	if ( (szValue.find("1") == 0) || (CompareNoCase(szValue, "true") == 0) || (CompareNoCase(szValue, "yes") == 0) )
	{
		bValue = true;
	}

	return bValue;
}

// DeleteSection
// Delete a specific section. Returns false if the section cannot be 
// found or true when sucessfully deleted.
bool IniParser::DeleteSection(std::string szSection)
{
	SectionItor s_pos;

	for (s_pos = m_Sections.begin(); s_pos != m_Sections.end(); s_pos++)
	{
		if ( CompareNoCase( (*s_pos).szName, szSection ) == 0 ) 
		{
			m_Sections.erase(s_pos);
			return true;
		}
	}

	return false;
}

// DeleteKey
// Delete a specific key in a specific section. Returns false if the key
// cannot be found or true when sucessfully deleted.
bool IniParser::DeleteKey(std::string szKey, std::string szFromSection)
{
	KeyItor k_pos;
	t_Section* pSection;

	if ( (pSection = GetSection(szFromSection)) == NULL )
		return false;

	for (k_pos = pSection->Keys.begin(); k_pos != pSection->Keys.end(); k_pos++)
	{
		if ( CompareNoCase( (*k_pos).szKey, szKey ) == 0 )
		{
			pSection->Keys.erase(k_pos);
			return true;
		}
	}

	return false;
}

// CreateKey
// Given a key, a value and a section, this function will attempt to locate the
// Key within the given section, and if it finds it, change the keys value to
// the new value. If it does not locate the key, it will create a new key with
// the proper value and place it in the section requested.
bool IniParser::CreateKey(std::string szKey, std::string szValue, std::string szComment, std::string szSection)
{
	bool bAutoKey = (m_Flags & AUTOCREATE_KEYS) == AUTOCREATE_KEYS;
	bool bReturn  = false;

	m_Flags |= AUTOCREATE_KEYS;

	bReturn = SetValue(szKey, szValue, szComment, szSection);

	if ( !bAutoKey )
		m_Flags &= ~AUTOCREATE_KEYS;

	return bReturn;
}


// CreateSection
// Given a section name, this function first checks to see if the given section
// already exists in the list or not, if not, it creates the new section and
// assigns it the comment given in szComment.  The function returns true if
// successfully created, or false otherwise.
bool IniParser::CreateSection(std::string szSection, std::string szComment)
{
	t_Section* pSection = GetSection(szSection);

	if ( pSection )
	{
		Report(E_INFO, "[IniParser::CreateSection] Section <%s> already exists. Aborting.", szSection.c_str());
		return false;
	}

	pSection = new t_Section;

	pSection->szName = szSection;
	pSection->szComment = szComment;
	m_Sections.push_back(*pSection);
	m_bDirty = true;

	return true;
}

// CreateSection
// Given a section name, this function first checks to see if the given section
// already exists in the list or not, if not, it creates the new section and
// assigns it the comment given in szComment.  The function returns true if
// successfully created, or false otherwise. This version accpets a KeyList
// and sets up the newly created Section with the keys in the list.
bool IniParser::CreateSection(std::string szSection, std::string szComment, KeyList Keys)
{
	if ( !CreateSection(szSection, szComment) )
		return false;

	t_Section* pSection = GetSection(szSection);

	if ( !pSection )
		return false;

	KeyItor k_pos;

	pSection->szName = szSection;
	for (k_pos = Keys.begin(); k_pos != Keys.end(); k_pos++)
	{
		t_Key* pKey = new t_Key;
		pKey->szComment = (*k_pos).szComment;
		pKey->szKey = (*k_pos).szKey;
		pKey->szValue = (*k_pos).szValue;

		pSection->Keys.push_back(*pKey);
	}

	m_Sections.push_back(*pSection);
	m_bDirty = true;

	return true;
}

// SectionCount
// Simply returns the number of sections in the list.
int IniParser::SectionCount()
{ 
	return m_Sections.size(); 
}

// KeyCount
// Returns the total number of keys contained within all the sections.
int IniParser::KeyCount()
{
	int nCounter = 0;
	SectionItor s_pos;

	for (s_pos = m_Sections.begin(); s_pos != m_Sections.end(); s_pos++)
		nCounter += (*s_pos).Keys.size();

	return nCounter;
}


// Protected Member Functions ///////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

// GetKey
// Given a key and section name, looks up the key and if found, returns a
// pointer to that key, otherwise returns NULL.
t_Key*	IniParser::GetKey(std::string szKey, std::string szSection)
{
	KeyItor k_pos;
	t_Section* pSection;

	// Since our default section has a name value of std::string("") this should
	// always return a valid section, whether or not it has any keys in it is
	// another matter.
	if ( (pSection = GetSection(szSection)) == NULL )
		return NULL;

	for (k_pos = pSection->Keys.begin(); k_pos != pSection->Keys.end(); k_pos++)
	{
		if ( CompareNoCase( (*k_pos).szKey, szKey ) == 0 )
			return (t_Key*)&(*k_pos);
	}

	return NULL;
}

// GetSection
// Given a section name, locates that section in the list and returns a pointer
// to it. If the section was not found, returns NULL
t_Section* IniParser::GetSection(std::string szSection)
{
	SectionItor s_pos;

	for (s_pos = m_Sections.begin(); s_pos != m_Sections.end(); s_pos++)
	{
		if ( CompareNoCase( (*s_pos).szName, szSection ) == 0 ) 
			return (t_Section*)&(*s_pos);
	}

	return NULL;
}


std::string IniParser::CommentStr(std::string szComment)
{
	std::string szNewStr = std::string("");

	Trim(szComment);

        if ( szComment.size() == 0 )
          return szComment;
	
	if ( szComment.find_first_of(CommentIndicators) != 0 )
	{
		szNewStr = CommentIndicators[0];
		szNewStr += " ";
	}

	szNewStr += szComment;

	return szNewStr;
}



// Utility Functions ////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

// GetNextWord
// Given a key +delimiter+ value string, pulls the key name from the string,
// deletes the delimiter and alters the original string to contain the
// remainder.  Returns the key
std::string GetNextWord(std::string& CommandLine)
{
	int nPos = CommandLine.find_first_of(EqualIndicators);
	std::string sWord = std::string("");

	if ( nPos > -1 )
	{
		sWord = CommandLine.substr(0, nPos);
		CommandLine.erase(0, nPos+1);
	}
	else
	{
		sWord = CommandLine;
		CommandLine = std::string("");
	}

	Trim(sWord);
	return sWord;
}


// CompareNoCase
// it's amazing what features std::string lacks.  This function simply
// does a lowercase compare against the two strings, returning 0 if they
// match.
int CompareNoCase(std::string str1, std::string str2)
{
	return strcasecmp(str1.c_str(), str2.c_str()); // #include <strings.h>
}

// Trim
// Trims whitespace from both sides of a string.
void Trim(std::string& szStr)
{
	std::string szTrimChars = WhiteSpace;
	
	szTrimChars += EqualIndicators;
	int nPos, rPos;

	// trim left
	nPos = szStr.find_first_not_of(szTrimChars);

	if ( nPos > 0 )
		szStr.erase(0, nPos);

	// trim right and return
	nPos = szStr.find_last_not_of(szTrimChars);
	rPos = szStr.find_last_of(szTrimChars);

	if ( rPos > nPos && rPos > -1)
		szStr.erase(rPos, szStr.size()-rPos);
}

// WriteLn
// Writes the formatted output to the file stream, returning the number of
// bytes written.
/*
int WriteLnOld(std::fstream& stream, char* fmt, ...)
{
	char buf[MAX_BUFFER_LEN];
	int nLength;
	std::string szMsg;

	memset(buf, 0, MAX_BUFFER_LEN);
	va_list args;

	va_start (args, fmt);
	  nLength = vsnprintf(buf, MAX_BUFFER_LEN, fmt, args);
	va_end (args);


	if ( buf[nLength] != '\n' && buf[nLength] != '\r' )
	{
		buf[nLength++] = '\n';
	}


	stream.write(buf, nLength);

	return nLength;
}
*/

int WriteLn(std::fstream& stream, std::string line)
{
	stream << line << std::endl;

	return line.length() + 1; // we add 1 to indicate the newline character
}

// Report
// A simple reporting function. Outputs the report messages to stdout
// This is a dumb'd down version of a similar function of mine, so if
// it looks like it should do more than it does, that's why...
void Report(e_DebugLevel DebugLevel, const char *fmt, ...)
{
	char buf[MAX_BUFFER_LEN];
	int nLength;
	std::string szMsg;

	va_list args;

	std::memset(buf, 0, MAX_BUFFER_LEN);

	va_start (args, fmt);
	  nLength = vsnprintf(buf, MAX_BUFFER_LEN, fmt, args);
	va_end (args);


	if ( buf[nLength] != '\n' && buf[nLength] != '\r' )
		buf[nLength++] = '\n';


	switch ( DebugLevel )
	{
		case E_DEBUG:
			szMsg = "<debug> ";
			break;
		case E_INFO:
			szMsg = "<info> ";
			break;
		case E_WARN:
			szMsg = "<warn> ";
			break;
		case E_ERROR:
			szMsg = "<error> ";
			break;
		case E_FATAL:
			szMsg = "<fatal> ";
			break;
		case E_CRITICAL:
			szMsg = "<critical> ";
			break;
	}


	szMsg += buf;

	std::cout << szMsg << std::endl;
}


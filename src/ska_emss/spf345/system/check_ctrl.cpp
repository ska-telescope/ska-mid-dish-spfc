/*
 * check_ctrl.cpp
 *
 *  Created on: 23 May 2017
 *      Author: theuns
 */

#include "check_ctrl.h"
#include "utilities.h"
#include <cmath>
#include <iostream>

using namespace util;

namespace FP345
{
CheckController::CheckController(stSettings settings, CircularFifo<stFeedDataFrame, HIST_BUFFER_SIZE> * buffPtr)
{
	_settings = settings;
	_buffPtr = buffPtr; // This is only a reference
}

CheckController::~CheckController()
{
}

void CheckController::ReloadSettings(stSettings settings)
{
	_settings = settings;
}


/* Return true if there are more or equal then @sampleCount samples in the circular buffer */
bool CheckController::ActiveSamples(int sampleCount)
{
	return (_buffPtr->elementCount() >= sampleCount ? true : false);
}

/**
 * PreVacuumed() check if feed has been pre-vacuumed below 10 mbar when connected
 */
bool CheckController::PreVacuumed()
{
	int count = MIN_COMPARED;
	double cryoPressure[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default

	int vote = 0;

	count = GetCryostatPressureHistoryArray(cryoPressure, count);

	if (count > 0) // if the amount requested is the amount returned
	{
		for (int i = 0; i < count; i++)
		{
			if ( (cryoPressure[i] > 0) && (cryoPressure[i] < _settings.constants.P_PreVac))
			{
				std::cout << "cryoP:" << cryoPressure[i] << " < " << _settings.constants.P_PreVac << std::endl;
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false;
}

/**
 * ValveSafetyCheck() returns True(1) if it is safe to open the vacuum valve
 */
bool CheckController::ValveSafetyCheck()
{
	stFeedDataFrame df = {};
	if (_buffPtr->peek(df, 0))
	{
		return ( ((df.sensors.b345ManifoldPressure > 0) && (df.sensors.b345ManifoldPressure < 2000) && (df.sensors.b345CryoPressure > 0) && (df.sensors.b345CryoPressure < 2000)) && (((df.sensors.b345ManifoldPressure <= _settings.constants.P_CryocoolerOn) || ((df.sensors.b345ManifoldPressure >= 800) && (df.sensors.b345CryoPressure >= 800)) || ((df.sensors.b345ManifoldPressure * 1.25) <= (df.sensors.b345CryoPressure))) ) ? 1 : 0);
	}
	return false; // not enough data
}

/**
 * samplerSafetyChecks() does safety checks for each new sample value acquired
 */
bool CheckController::IsPressureSafe()
{
	double histManifP[GRADIENT_CHECK_WINDOW];
	double histCryoP[GRADIENT_CHECK_WINDOW];
	double PmDt, PcDt;
	int hLenPc, hLenPm;

	/* Get the current dataframe */
	stFeedDataFrame currentDataFrame = {};
	if (_buffPtr->peek(currentDataFrame, 0))

	/* Get the history of manifold and cryostat pressures */
	hLenPm = GetManifoldPressureHistoryArray(histManifP, GRADIENT_CHECK_WINDOW);
	hLenPc = GetCryostatPressureHistoryArray(histCryoP, GRADIENT_CHECK_WINDOW);

	/* Check pressures if valve is open */
	if ( currentDataFrame.isValveOpen )
	{
		PmDt = logPressureDt(histManifP, hLenPm);
		PcDt = logPressureDt(histCryoP, hLenPc);

		if ( ManifoldPressureMoreThan(_settings.constants.P_SuddenRiseValveClose) && (PmDt > 0.05) && (PcDt > 0.05)
				&& (currentDataFrame.sensors.b345ManifoldPressure > 2.0*currentDataFrame.sensors.b345CryoPressure) )
		{
			//logKatcpError(cmu, E_PSuddenRise);
			return false;
		}
		return true;
	}
	return true; // Valve not open, so we can proceed
}

/**
 * Returns True if healthy
 */
bool CheckController::CheckSpdHealth()
{
	int count = MIN_COMPARED;
	double spdArray[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default

	int vote = 0;

	count = GetSpdHistoryArray(spdArray, count);

	if ((count > 0) && ActiveSamples(2)) // if the amount requested is the amount returned
	{
		for (int i = 0; i < count; i++)
		{
			if ( (spdArray[i] > 1.0)) // alarm if spfHealth below 1V
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return true; // Not enough data, so return true until enough data gives correct value;
}

bool CheckController::IsRfe1TempSafe()
{
	int count = MIN_COMPARED;
	double lnaArray[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default

	int vote = 0;

	count = GetB5aLnaTempHistoryArray(lnaArray, count);

	if (count > 0) // if the amount requested is the amount returned
	{
		for (int i = 0; i < count; i++)
		{
			if ( (lnaArray[i] > 0) && (lnaArray[i] < _settings.constants.TE_Rfe1PidMaxTemp))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false;
}

bool CheckController::IsCalSourceTempSafe()
{
	int count = MIN_COMPARED;
	double calSourceArray[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default

	int vote = 0;

	count = GetCalSourceTempHistoryArray(calSourceArray, count);

	if (count > 0) // if the amount requested is the amount returned
	{
		for (int i = 0; i < count; i++)
		{
			if ( /*(calSourceArray[i] > 0) && */ (calSourceArray[i]+273.15 < _settings.constants.TE_CalSourcePidMaxTemp)) // not in K, so <=0 check disables Cal PID
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false;
}

int CheckController::GetColdHeadTempHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.b345ColdheadStage1Temp;
	}
	return actualSamples;
}
int CheckController::GetSpdHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.b345SpdHealth;
	}

	return actualSamples;
}
int CheckController::GetB5aLnaTempHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.b5aLnaTemp;
	}

	return actualSamples;
}
int CheckController::GetCalSourceTempHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.b345CalSourceTemp;
	}

	return actualSamples;
}
int CheckController::GetOmtTempHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.b5aOmtTemp;
	}

	return actualSamples;
}

int CheckController::GetCryostatPressureHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.b345CryoPressure;
	}

	return actualSamples;
}
int CheckController::GetManifoldPressureHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.b345ManifoldPressure;
	}

	return actualSamples;
}

/**
 * ColdOpsAvailable() returns true if rfe1 temperature is within the available limits
 */
bool CheckController::ColdOpsAvailable(bool lnaPidOn, int setpoint)
{
	int count = STABILITY_WINDOW;
	bool coldOps = false;

	double lnaArray[STABILITY_WINDOW] = {0.0}; // Initialise all struct vars to their type-default

	count = GetB5aLnaTempHistoryArray(lnaArray, count);

	if (count > 0) // if the amount requested is the amount returned
	{
		// test if latest value is within offset values
		if ((lnaPidOn) && (setpoint < TEMP_SP_WARM) && (lnaArray[count-1] <= setpoint + _settings.constants.TS_MaxOffset) && (lnaArray[count-1] >= setpoint - _settings.constants.TS_MaxOffset))
		{	// test that min & max values are within the stability range
			if ( max_array(lnaArray, count) - min_array(lnaArray, count) <= _settings.constants.TS_StabilityDelta )
			{
				coldOps = true;
			}
		}
		else if ((lnaPidOn == false) && (lnaArray[count-1] <= _settings.constants.TS_OffMax)) // test for LNA_PID == OFF
		{	// test that min & max values are within the SP-OFF stability range
			if ( max_array(lnaArray, count) - min_array(lnaArray, count) <= _settings.constants.TS_OffStabilityDelta )
			{
				coldOps = true;
			}
		}

		return coldOps;
	}
	return false; // Not enough data, so return false;
}

bool CheckController::ColdOpsTemp()
{
	int count = MIN_COMPARED;
	double lnaArray[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default

	int vote = 0;

	count = GetB5aLnaTempHistoryArray(lnaArray, count);

	if (count > 0) // if the amount requested is the amount returned
	{
		for (int i = 0; i < count; i++)
		{
			if ( (lnaArray[i] > 0) && (lnaArray[i] < _settings.constants.T_ColdOperational) )
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false;
}

/**
 * notColdOperational() check if RX is still within user defined cold
 * operational specification. Returns True(1) if not within specs.
 */
int CheckController::NotColdOperational(bool lnaPidOn, int setpoint)
{
	// start the different tests
	if (!ColdOpsAvailable(lnaPidOn, setpoint))
	{
		return 1; // 1 indicates temperature fault
	}

	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED];
	int vote = 0;

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histCryoP[i] > 0) && (histCryoP[i] > _settings.constants.P_ColdOperationalDegraded) )
			{
				vote++;
			}
		}

		return ( (vote == count) ? 2 : 0); // 2 indicates pressure fault
	}
	return 0;
}

/**
 *  PressureTurboOn() reads the current cryostat pressure and compares it to a
 *  user defined constant. If less it returns true indicating that the
 *  turbo pump can be switched ON.
 */
bool CheckController::PressureTurboOn()
{
	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			std::cout << "PressureTurboOn - histCryoP[" << i << "] = " << histCryoP[i] << std::endl;

			if ( (histCryoP[i] > 0) && (histCryoP[i] < _settings.constants.P_TurboOn))
			{
				vote++;
			}
		}
		std::cout << "PressureTurboOn - Votes to enable turbo pump: " << vote << std::endl;
		return ((vote == count) ? true : false);
	}
	std::cout << "PressureTurboOn - not enough data." << std::endl;
	return false; // Not enough data, so return false;
}

/**
 *  PressureCryoOn() reads the current cryostat pressure and compares it to a
 *  user defined constant. If less it returns true indicating that the
 *  cryocooler can be switched ON.
 */
bool CheckController::PressureCryoOn()
{
	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			std::cout << "PressureCryoOn - histCryoP[" << i << "] = " << histCryoP[i] << std::endl;

			if ( (histCryoP[i] > 0) && (histCryoP[i] < _settings.constants.P_CryocoolerOn))
			{
				vote++;
			}
		}
		std::cout << "PressureCryoOn - Votes to enable cryo cooler: " << vote << std::endl;
		return ((vote == count) ? true : false);
	}
	std::cout << "PressureCryoOn - not enough data." << std::endl;
	return false; // Not enough data, so return false;
}

/**
 * manifPressureCryoOn() reads the current manifold pressure and compares it to
 * a user defined constant. Returns True(1) if less than constant.
 */
bool CheckController::ManifoldPressureCryoOn()
{
	int count = MIN_COMPARED;
	double histManifP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetManifoldPressureHistoryArray(histManifP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histManifP[i] > 0) && (histManifP[i] < _settings.constants.P_CryocoolerOn))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false;
}

/**
 * AmbientPressures() reads the current manifold & cryostat pressure and returns true
 * if both are at atmospheric pressure in order to open the valve.
 */
bool CheckController::AmbientPressures()
{
	int count = MIN_COMPARED;
	double histManifP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetManifoldPressureHistoryArray(histManifP, count);
	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histManifP[i] > 900) && (histCryoP[i] > 900) )
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false;
}

/**
 * tempCryoRecovery() returns true if the RFE1 temp is still below its recovery value
 */
bool CheckController::TempCryoRecovery()
{
	int count = MIN_COMPARED;
	double histRfe1T[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetB5aLnaTempHistoryArray(histRfe1T, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histRfe1T[i] > 0) && (histRfe1T[i] < _settings.constants.T_CryoRecovery))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 * ColdOpsError() returns 1 if the RFE1 temp goes above its error value
 * or returns 2 the cryoPressure goes above its error value
 */
int CheckController::ColdOpsError()
{
	int count = MIN_COMPARED;
	double histRfe1T[MIN_COMPARED], histCryoP[MIN_COMPARED];

	int tempErrorVote = 0;
	int cryoPErrorVote = 0;

	count = GetB5aLnaTempHistoryArray(histRfe1T, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histRfe1T[i] > 0) && (histRfe1T[i] > _settings.constants.TE_ColdOperational) )
			{
				tempErrorVote += 1;
			}
		}
		if (tempErrorVote == count) return 1;
	}

	count = GetCryostatPressureHistoryArray(histCryoP, count);
	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histCryoP[i] > 0) && (histCryoP[i] > _settings.constants.PE_CryoPump) )
			{
				cryoPErrorVote += 1;
			}
		}
		if (cryoPErrorVote == count) return 2;
	}
	return 0;
}

/**
 * RegenTemp() returns true when OMT temperature is higher or equal than the user defined RegenAmbient variable
 */
bool CheckController::RegenTemp()
{
	int count = MIN_COMPARED;
	double histOmtT[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetOmtTempHistoryArray(histOmtT, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			std::cout << "RegenTemp - histOmtT[" << i << "] = " << histOmtT[i] << std::endl;
			if ( (histOmtT[i] > 0) && (histOmtT[i] >= _settings.constants.T_RegenerationAmbient))
			{
				vote++;
			}
		}
		std::cout << "RegenTemp - Votes: " << vote << std::endl;
		return ((vote == count) ? true : false);
	}
	std::cout << "PressureCryoOn - not enough data." << std::endl;
	return false; // Not enough data, so return false
}

/**
 * CryoPumpTReached() checks if the RFE1_temperature has reached the defined CrypPumping temperature setpoint.
 */
bool CheckController::CryoPumpTReached()
{
	int count = MIN_COMPARED;
	double histRfe1T[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	double histColdHT[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetB5aLnaTempHistoryArray(histRfe1T, count);
	count = GetColdHeadTempHistoryArray(histColdHT, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( ((histRfe1T[i] > 0) && (histRfe1T[i] < _settings.constants.T_CryoPumping)) ||
				((histColdHT[i] > 0) && (histColdHT[i] < _settings.constants.T_CryoPumping)) )
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 * CryoPumpReached() checks if the cryostat_pressure has reached the defined CryoPumping pressure setpoint.
 */
bool CheckController::CryoPumpReached()
{
	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histCryoP[i] > 0) && (histCryoP[i] < _settings.constants.P_CryoPump))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false
}

bool CheckController::CryoPumpRecovered()
{
	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histCryoP[i] > 0) && (histCryoP[i] < _settings.constants.P_ColdOperationalDegraded))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 * cryoPumpRiseVal() determines if the pressure in the cryostat is above the CryoPumping Rising Lower limit.
 */
bool CheckController::CryoPumpRiseVal()
{
	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histCryoP[i] > 0) && (histCryoP[i] > _settings.constants.P_CryoPumpRising))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 * cryoPressureMoreThan() returns true if the previous MIN_COMPARED Cryostat
 * pressure values are all more than cPressure.
 */
bool CheckController::CryoPressureMoreThanManifoldPressure()
{
	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	double histManifP[1] = {0.0};
	int vote = 0;

	if (GetManifoldPressureHistoryArray(histManifP, 1) == 1) // Bit of an overkill to get only 1 value
	{
		std::cout << "CryoPressureMoreThanManifoldPressure - histManifP[0] = " << histManifP[0] << std::endl;

		count = GetCryostatPressureHistoryArray(histCryoP, count);

		if (count > 0)
		{
			for (int i = 0; i < count; i++)
			{
				std::cout << "CryoPressureMoreThanManifoldPressure - histCryoP[" << i << "] = " << histCryoP[i] << std::endl;
				if ( (histCryoP[i] > 0) && (histCryoP[i] > histManifP[0]))
				{
					vote++;
				}
			}
			std::cout << "CryoPressureMoreThanManifoldPressure - Votes: " << vote << std::endl;
			return ((vote == count) ? true : false);
		}
		std::cout << "CryoPressureMoreThanManifoldPressure - not enough data." << std::endl;
	}
	else
	{
		std::cout << "CryoPressureMoreThanManifoldPressure - no manifold pressure data." << std::endl;
	}
	return false; // Not enough data, so return false
}

/**
 * ManifoldPressureMoreThan() returns true if the previous MIN_COMPARED Manifold
 * pressure values are all more than pressure.
 */
bool CheckController::ManifoldPressureMoreThan(double pressure)
{
	int count = MIN_COMPARED;
	double histManifP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetManifoldPressureHistoryArray(histManifP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histManifP[i] > 0) && (histManifP[i] > pressure))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 * cryoPumpSatisfactory() returns true if the cryostat pressure has dropped below the CryoPumpSatisfy value
 */
bool CheckController::CryoPumpSatisfactory()
{
	int count = MIN_COMPARED;
	double histCryoP[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	int vote = 0;

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		for (int i = 0; i < count; i++)
		{
			if ( (histCryoP[i] > 0) && (histCryoP[i] < _settings.constants.P_CryoPumpSatisfy))
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return false; // Not enough data, so return false
}




/**
 *  rfe1TempDecrease() returns true if the average RFE1 temperature time derivative
 *  is less than 0
 */
bool CheckController::Rfe1TempDecrease()
{
	int count = GRADIENT_CHECK_WINDOW;
	double histRfe1T[GRADIENT_CHECK_WINDOW];

	count = GetB5aLnaTempHistoryArray(histRfe1T, count);

	if (count > 0)
	{
		return (tempDt(histRfe1T, count) < 0 ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  cryoPressureIncrease() returns 1 if the average cryostat pressure time
 *  derivative is more than 0
 */
bool CheckController::CryoPressureIncrease()
{
	int count = GRADIENT_CHECK_WINDOW;
	double histCryoP[GRADIENT_CHECK_WINDOW];

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		return (logPressureDt(histCryoP, count) > 0.0 ? true : false);
	}
	return false; // Not enough data, so return false
}
/**
 *  cryoPressureDecrease() returns 1 if the cryostat pressure time derivative
 *  is less than 0
 */
bool CheckController::CryoPressureDecrease()
{
	int count = GRADIENT_CHECK_WINDOW;
	double histCryoP[GRADIENT_CHECK_WINDOW];

	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		return (logPressureDt(histCryoP, count) < 0.0 ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  manifPressureDecrease() returns true if the manifold pressure time derivative
 *  is less than 0
 */
bool CheckController::ManifoldPressureDecrease()
{
	int count = GRADIENT_CHECK_WINDOW;
	double histManifP[GRADIENT_CHECK_WINDOW];

	count = GetManifoldPressureHistoryArray(histManifP, count);

	if (count > 0) // if the amount requested is the amount returned
	{
		return (logPressureDt(histManifP, count) < 0.0 ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 * logPressureDt() determines the time derivative of the log values of the
 * pressures in the PArray, averaged over aLen samples.
 */
double CheckController::logPressureDt (double PArray[], int count)
{
	double avgPDt = 0;
	for (int i = 1; i < count; i++)
	{
		avgPDt += log10(PArray[i]) - log10(PArray[i - 1]);
	}
	return avgPDt/(count - 1);
}

/**
 * tempDt() determines the time derivative of the temperature values
 * in the TArray, averaged over aLen samples.
 */
double CheckController::tempDt (double TArray[], int count)
{
	double avgTDt = 0;
	for (int i = 1; i < count; i++)
	{
		avgTDt += TArray[i] - TArray[i - 1];
	}
	return avgTDt/(count - 1);
}

}


/*
 * SensorLogger.h
 *
 *  Created on: 26 Jun 2017
 *      Author: theuns
 */

#ifndef SYSTEM_SENSORLOGGER_H_
#define SYSTEM_SENSORLOGGER_H_

#include "fpc_if.h"
#include <filesystem>

class SensorLogger
{
public:
	//SensorLogger(std::string logPath, std::string deviceId, std::string deviceSerial, std::string controllerId, std::string controllerSerial);
	SensorLogger(std::string deviceLocation, std::string deviceId, std::string deviceSerial);
	~SensorLogger();
	bool Log(stFeedDataFrame dataFrame);

private:
	std::string _logPath;
	std::string _deviceLocation;
	std::string _devLogLocation;
	std::string _deviceId;
	std::string _deviceSerial;
	std::string _controllerId;
	std::string _controllerSerial;

	bool SDCardAvailable();

};

#endif /* SYSTEM_SENSORLOGGER_H_ */

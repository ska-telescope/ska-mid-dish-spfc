/*
 * SensorLogger.cpp
 *
 *  Created on: 26 Jun 2017
 *      Author: theuns
 */

#include "SensorLogger.h"
#include "DataLogger.h"
#include "utilities.h"
#include "system.h"
#include "IniParser.h"
#include <iostream>
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string
#include <algorithm> // transform

SensorLogger::SensorLogger(std::string deviceLocation, std::string deviceId, std::string deviceSerial)
	: _deviceId(deviceId), _deviceSerial(deviceSerial)
{
	IniParser spfcConfig(SPFC_CONFIG); /* To get SPFC serial number */
    string spfcSerial = spfcConfig.GetString("SPFC", "Serial_Nr");

    if (spfcSerial == "")
    {
    	spfcSerial = "0000";
    }

    IniParser spfcSettings(SPFC_SETTINGS); /* To get logging location */
    bool logToSDCard = spfcSettings.GetBool("LogToSDCard", "Variables");

    if (logToSDCard && SDCardAvailable())
    {
    	_logPath = BASE_PATH_SD LOG_PATH;
    }
    else
    {
    	_logPath = BASE_PATH_LOCAL LOG_PATH;
    }


    _logPath = _logPath + "/" + _deviceId;

    /* If logpath does not exist, create it!
     * Using a system call is not the MOST efficient way, but this seems to be the easiest for now.
     * Permission to create directories are needed
     */
	//system(cmdLine.c_str()); // We can't do anything about it if the system call fails.
	std::filesystem::create_directories(_logPath.c_str());

    std::transform(_deviceId.begin(), _deviceId.end(), _deviceId.begin(), ::toupper);

    _controllerId = "SPFC";
    _controllerSerial = spfcSerial;
    _deviceLocation = deviceLocation;

    /* Try to identify the AP location from the device name
     * eg.: mid_dsh_0000/spf/spf345 -> M0000
     */
    if (deviceLocation.find("/") > 0)
    {
		if (deviceLocation.compare(0, 8, "mid_dsh_") == 0)
		{ // get the AP# between "mid_dsh_" and "/"
			_devLogLocation = "M" + deviceLocation.substr(8, deviceLocation.find("/")-8);
		} else
		{ // eg.: tent0003/spf/spf345 -> tent0003
			_devLogLocation = deviceLocation.substr(0, deviceLocation.find("/"));
		}
    } else
    { // eg.: tent3
    	_devLogLocation = deviceLocation;
    }
    std::replace(_devLogLocation.begin(), _devLogLocation.end(), '_', '-');

}

SensorLogger::~SensorLogger()
{
}

bool SensorLogger::Log(stFeedDataFrame dataFrame)
{
	bool retVal = false;

	int sensorId;
	std::string line;

	/* Timestamp */
	line += util::NumberToString(dataFrame.timestamp);
	line += "\t";

	/* Sensors */
	sensorId = 1;
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaHDrainVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaHGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaVDrainVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaVGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaHDrainVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaHGateVoltage,2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaVDrainVoltage,2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaVGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaHDrainVoltage, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaHGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaVDrainVoltage, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaVGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaHDrainVoltage, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaHGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaVDrainVoltage, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaVGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaHDrainVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaHGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaVDrainVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaVGateVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaHDrainCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaVDrainCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaHDrainCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaVDrainCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaHDrainCurrent, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaVDrainCurrent, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaHDrainCurrent, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaVDrainCurrent, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaHDrainCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaVDrainCurrent, 2);			line += "\t";

	line += std::to_string(sensorId++); line += ":"; line += util::NumberToSciString(dataFrame.sensors.b345CryoPressure, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToSciString(dataFrame.sensors.b345ManifoldPressure, 2);	line += "\t";

	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3OmtTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4OmtTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aOmtTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bOmtTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6OmtTemp, 2);					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aHornTemp, 2);				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bHornTemp, 2);				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6HornTemp, 2);					line += "\t";

	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b345BodyTemp, 2);				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b345ColdheadStage1Temp, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b345ColdheadStage2Temp, 2);		line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3WarmPlateTemp, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4WarmPlateTemp, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aWarmPlateTemp, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bWarmPlateTemp, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6WarmPlateTemp, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b345CalSourceTemp, 2);			line += "\t";

	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b345AnalogueInterfaceTemp, 2);	line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b345DigitalInterfaceTemp, 2);	line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b345SpdHealth, 2);				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b3LnaPidCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b4LnaPidCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5aLnaPidCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b5bLnaPidCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.b6LnaPidCurrent, 2);			line += "\t";

	/* IO */
	sensorId = 101;
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.LnaB3); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.LnaB4); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.LnaB5a); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.LnaB5b); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.LnaB6); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.CalSource); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.PidLnaB3); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.PidLnaB4); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.PidLnaB5a); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.PidLnaB5b); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.PidLnaB6); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.NegHeater); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.CryoMotor); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.VaValve); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.controlFlags.bits.VaTurbo); 			line += "\t";

	/* Control */
	sensorId = 201;
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b3LnaPidTempSetPoint); 					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b4LnaPidTempSetPoint); 					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b5aLnaPidTempSetPoint); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b5bLnaPidTempSetPoint); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b6LnaPidTempSetPoint); 					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b3WarmPlateTempSetPoint); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b4WarmPlateTempSetPoint); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b5aWarmPlateTempSetPoint); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b5bWarmPlateTempSetPoint); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b6WarmPlateTempSetPoint); 				line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.b345CalSourceTempSetPoint); 			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString((short)dataFrame.activeBand); 					line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.isValveOpen); 							line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.isTurboOn); 							line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.cryoMotorSpeed); 					//line += "\t";

	if (dataFrame.errorFlags.Word)
	{
		line += "\t"; line += std::to_string(sensorId++); line += ":"; line += util::numberToHexString(dataFrame.errorFlags.Word, 4); 					//line += "\t";
	}
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	//ss << std::put_time(std::localtime(&in_time_t), "%Y%m%d");

	char timeBuf[24];
	std::strftime(timeBuf, sizeof(timeBuf), "%Y%m%d", std::localtime(&in_time_t));
	ss << timeBuf;


	std::string fileName = _logPath + "/" + _devLogLocation + "_" + _controllerId + _controllerSerial + "_" + _deviceId + "-" + _deviceSerial + "_" + ss.str() + ".log";

	DataLogger::AppendLine(fileName, line);

	return retVal;
}

bool SensorLogger::SDCardAvailable()
{
	bool retVal = false;

	std::ifstream mmcSizeFile ("/sys/block/mmcblk0/size");
	if (mmcSizeFile.is_open())
	{
	    unsigned long long size;
	    retVal = (mmcSizeFile >> size) && (size > 0);
	    mmcSizeFile.close();
	}

	return retVal; // Will return false if file could not be opened */
}




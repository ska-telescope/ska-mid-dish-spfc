#ifndef fpc_ctrl_h
#define fpc_ctrl_h

#include <string.h>

#include "fpc_if.h"
#include "fp345_sm.h"
#include "IniParser.h"
#include "DataFrame.h"

class FeedPackageControl
{
public:
	FeedPackageControl();
    ~FeedPackageControl();
    void LoadIniSettings();
    void ReloadSettings();

    bool Run();
    bool Shutdown();

    /* Controller commands - called by Tango internals */
    bool Start();
    bool Stop();
    bool Reset();

    void SetSpfHeLocation(std::string spfHeLocation);
    void SetSpfVacLocation(std::string spfVacLocation);
    void SetSpf345Location(std::string spf345Location);
    void SetComPort(std::string ttyPort);

    /* Normal operational commands */
    enDeviceStatus GetDeviceStatus();
    enDeviceMode GetFeedMode();
    enCapabilityState GetCapabilityState();
    string GetCurrentState();
    stFeedDataFrame GetDataFrame();
    FP345::stFeedConfig GetFeedConfig();
    stLnaBiasParameters GetLnaHBiasParameters();
    stLnaBiasParameters GetLnaVBiasParameters();
    void SetLnaHBiasParameters(stLnaBiasParameters biasParams);
    void SetLnaVBiasParameters(stLnaBiasParameters biasParams);

    bool SetB345SerialNr(Tango::DevString serialNr);
    bool SetLnaSerialNrs(const Tango::ConstDevString *serialNr);
    bool SetLnaHSerialNr(Tango::DevString serialNr);
    bool SetLnaVSerialNr(Tango::DevString serialNr);
    bool SetLnaGain3H(const Tango::DevFloat meanGain);
    bool SetLnaGain3V(const Tango::DevFloat meanGain);
    bool SetLnaGain4H(const Tango::DevFloat meanGain);
    bool SetLnaGain4V(const Tango::DevFloat meanGain);
    bool SetLnaGain5aH(const Tango::DevFloat meanGain);
    bool SetLnaGain5aV(const Tango::DevFloat meanGain);
    bool SetLnaGain5bH(const Tango::DevFloat meanGain);
    bool SetLnaGain5bV(const Tango::DevFloat meanGain);
    bool SetLnaGain6H(const Tango::DevFloat meanGain);
    bool SetLnaGain6V(const Tango::DevFloat meanGain);
    bool SetLnaHBiasParams(const Tango::DevFloat *params);
    bool SetLnaVBiasParams(const Tango::DevFloat *params);
    string GetFirmwareVersion();

    bool SendRawCommand(string command, string & response);
    bool SetConfigFile(string & response);
    bool SetModelFiles(string & response);
    bool UpdateBias(string & response);
    bool UpdateConfigFile(string & response);

    /* Mode commands commands */
    bool SetMode(std::string mode);

    bool ClearErrors();

    bool SetExpectedOnline(bool state);
    bool GetExpectedOnline();
    bool SetStartupDefault(enStartupDefault state);
    enStartupDefault GetStartupDefault();

    bool SetActiveBand(enActiveBand band);
    bool SetB3TempSetpoint(float sp);
    bool SetB4TempSetpoint(float sp);
    bool SetB5aTempSetpoint(float sp);
    bool SetB5bTempSetpoint(float sp);
    bool SetB6TempSetpoint(float sp);
    bool SetB3WpTempSetpoint(float sp);
    bool SetB4WpTempSetpoint(float sp);
    bool SetB5aWpTempSetpoint(float sp);
    bool SetB5bWpTempSetpoint(float sp);
    bool SetB6WpTempSetpoint(float sp);
    bool SetCalSourceTempSetpoint(float sp);

    bool SetDefaultB3TempSetpoint(float sp);
    bool SetDefaultB4TempSetpoint(float sp);
    bool SetDefaultB5aTempSetpoint(float sp);
    bool SetDefaultB5bTempSetpoint(float sp);
    bool SetDefaultB6TempSetpoint(float sp);
    float  GetDefaultB3TempSetpoint();
    float  GetDefaultB4TempSetpoint();
    float  GetDefaultB5aTempSetpoint();
    float  GetDefaultB5bTempSetpoint();
    float  GetDefaultB6TempSetpoint();
    bool SetDefaultB3WpTempSetpoint(float sp);
    bool SetDefaultB4WpTempSetpoint(float sp);
    bool SetDefaultB5aWpTempSetpoint(float sp);
    bool SetDefaultB5bWpTempSetpoint(float sp);
    bool SetDefaultB6WpTempSetpoint(float sp);
    float GetDefaultB3WpTempSetpoint();
    float GetDefaultB4WpTempSetpoint();
    float GetDefaultB5aWpTempSetpoint();
    float GetDefaultB5bWpTempSetpoint();
    float GetDefaultB6WpTempSetpoint();
    bool SetDefaultCalSourceTempSetpoint(float sp);
    float  GetDefaultCalSourceTempSetpoint();
    float GetTS_OffMax();

    bool FeedDetected();

    bool SetB3LnaPowerState(bool state);
    bool SetB4LnaPowerState(bool state);
    bool SetB5aLnaPowerState(bool state);
    bool SetB5bLnaPowerState(bool state);
    bool SetB6LnaPowerState(bool state);
    bool SetCalSourcePowerState(bool state);
    bool SetB3PidLnaPowerState(bool state);
    bool SetB4PidLnaPowerState(bool state);
    bool SetB5aPidLnaPowerState(bool state);
    bool SetB5bPidLnaPowerState(bool state);
    bool SetB6PidLnaPowerState(bool state);

    bool SetMotorState(bool state);
    bool SetMotorSpeed(short rpm);
    bool SetTurboState(bool state);
    bool SetNegPumpState(bool state);

    bool SetVacuumValve(bool state);

    int IniKeyLoadOrCreateInt(std::string key, int value, std::string section);
    unsigned int IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section);
    double IniKeyLoadOrCreateDouble(std::string key, double value, std::string section);
    float IniKeyLoadOrCreateFloat(std::string key, float value, std::string section);

    /* To be defined */
    bool Test();

private:
    pthread_t sm_thread;

    IniParser * settingsFile;
    stSettings * settings;

};

#endif /* fpc_ctrl_h */

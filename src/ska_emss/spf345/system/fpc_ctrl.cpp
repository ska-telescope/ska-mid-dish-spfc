#include "fpc_ctrl.h"

#include "qpcpp.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>      // for memcpy() and memset()
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "fp345_sm.h"
#include "fpc_if.h"
#include "system.h"

using namespace std;
using namespace QP;
using namespace FP345;

static QP::QTicker l_ticker0(0); // ticker for tick rate 0
QP::QActive * the_Ticker0 = &l_ticker0;

extern "C" void Q_onAssert(char const * const module, int loc)
{
    cout << "Assertion failed in " << module
              << " location " << loc << endl;
    QS_ASSERTION(module, loc, static_cast<uint32_t>(10000U));
}

void QF::onStartup(void)
{
}
void QF::onCleanup(void)
{
    cout << endl << "Cleaning up" << endl;
}

void QP::QF_onClockTick(void)
{
	//QF::TICK_X(0U, (void *)0);  // perform the QF clock tick processing
	the_Ticker0->POST(0, 0); // post a don't-care event to Ticker0
}

void *SM_main(void * arg)
{
	static QF_MPOOL_EL(QEvt) smlPoolSto[100];
    //static QSubscrList subscrSto[MAX_PUB_SIG];
    static QEvt const *eventQSto[100]; // Event queue storage for SpfVac

    QF::init(); // initialize the framework and the underlying RT kernel

    // init publish-subscribe
    //QF::psInit(subscrSto, Q_DIM(subscrSto)); // init publish-subscribe

    // dynamic event allocation not used, no call to
    // initialize event pools...
    QF::poolInit(smlPoolSto, sizeof(smlPoolSto), sizeof(smlPoolSto[0]));

    // instantiate and start the active objects...
    //fp345AO->settings = (stSettings *)(arg); /* TODO: This passes the pointer to the AO, meaning it can edit the values */
    fp345AO->SetSettings((stSettings *)(arg));/* TODO: This passes the pointer to the AO, meaning it can edit the values */

    the_Ticker0->start(1U, // priority
                             0, 0, 0, 0);

    fp345AO->start(2U,                            // priority
                     eventQSto, Q_DIM(eventQSto),   // event queue
                     (void *)0, 0U);                // stack (unused

    QF::run(); // run the QF application - does not return
    cout << "QF::run() exited - QF framework halted" << endl;

    return (void*)0;
}


FeedPackageControl::FeedPackageControl()
{
	cout << "----------------- [FeedPackageControl()] -------------" << endl;
	sm_thread = 0;

	/* Read the config file */
	settingsFile = new IniParser(SPF345_SETTINGS);

	settings = new stSettings();

	LoadIniSettings();

}

FeedPackageControl::~FeedPackageControl()
{
	cout << "----------------- [~FeedPackageControl()] ------------" << endl;

	fp345AO->POST(Q_NEW(QEvt, SHUTDOWN_SIG), me);

	fp345AO->stop();
	QF::stop(); /* stop QF and cleanup */

	delete settings;
	delete settingsFile; /* Will save any unsaved stuff */
}

void FeedPackageControl::LoadIniSettings()
{
	cout << "----------------- [LoadIniSettings()] -------------" << endl;

	/* Read the config file */

	settings->variables.expected_online = IniKeyLoadOrCreateInt(N_EXPECTED_ONLINE, VAL_EXPECTED_ONLINE, INI_VARS);
	settings->variables.startup_default = static_cast<enStartupDefault>(IniKeyLoadOrCreateInt(N_STARTUP_DEFAULT, VAL_STARTUP_DEFAULT, INI_VARS));
	settings->variables.TS_b3 = IniKeyLoadOrCreateFloat(N_TS_B3, VAL_TS_B3, INI_VARS);
	settings->variables.TS_b4 = IniKeyLoadOrCreateFloat(N_TS_B4, VAL_TS_B4, INI_VARS);
	settings->variables.TS_b5a = IniKeyLoadOrCreateFloat(N_TS_B5A, VAL_TS_B5A, INI_VARS);
	settings->variables.TS_b5b = IniKeyLoadOrCreateFloat(N_TS_B5B, VAL_TS_B5B, INI_VARS);
	settings->variables.TS_b6 = IniKeyLoadOrCreateFloat(N_TS_B6, VAL_TS_B6, INI_VARS);
	settings->variables.TS_b3Wp = IniKeyLoadOrCreateFloat(N_TS_B3WP, VAL_TS_B3WP, INI_VARS);
	settings->variables.TS_b4Wp = IniKeyLoadOrCreateFloat(N_TS_B4WP, VAL_TS_B4WP, INI_VARS);
	settings->variables.TS_b5aWp = IniKeyLoadOrCreateFloat(N_TS_B5AWP, VAL_TS_B5AWP, INI_VARS);
	settings->variables.TS_b5bWp = IniKeyLoadOrCreateFloat(N_TS_B5BWP, VAL_TS_B5BWP, INI_VARS);
	settings->variables.TS_b6Wp = IniKeyLoadOrCreateFloat(N_TS_B6WP, VAL_TS_B6WP, INI_VARS);
	settings->variables.TS_CalSource = IniKeyLoadOrCreateFloat(N_TS_CAL_SOURCE, VAL_TS_CAL_SOURCE, INI_VARS);

	settings->constants.P_SuddenRiseValveClose = IniKeyLoadOrCreateFloat(N_P_RISE_VALVE_CLOSE, VAL_P_RISE_VALVE_CLOSE, INI_CONST);

	settings->constants.P_PreVac = IniKeyLoadOrCreateFloat(N_P_PRE_VAC, VAL_P_PRE_VAC, INI_CONST);
	settings->constants.P_CryocoolerOn = IniKeyLoadOrCreateFloat(N_P_CRYO_ON, VAL_P_CRYO_ON, INI_CONST);
	settings->constants.P_TurboOn = IniKeyLoadOrCreateFloat(N_P_TURBO_ON, VAL_P_TURBO_ON, INI_CONST);
	settings->constants.P_CryoPump = IniKeyLoadOrCreateFloat(N_P_CRYO_PUMP, VAL_P_CRYO_PUMP, INI_CONST);
	settings->constants.P_CryoPumpRising = IniKeyLoadOrCreateFloat(N_P_CRYO_PUMP_RISING, VAL_P_CRYO_PUMP_RISING, INI_CONST);
	settings->constants.P_CryoPumpSatisfy = IniKeyLoadOrCreateFloat(N_P_CRYO_PUMP_SATISFY, VAL_P_CRYO_PUMP_SATISFY, INI_CONST);
	settings->constants.PE_CryoPump = IniKeyLoadOrCreateFloat(N_PE_CRYO_PUMP, VAL_PE_CRYO_PUMP, INI_CONST);
	settings->constants.P_ColdOperationalDegraded = IniKeyLoadOrCreateFloat(N_P_COLD_OPERATIONAL_DEG, VAL_P_COLD_OPERATIONAL_DEG, INI_CONST);
	settings->constants.T_CryoPumping = IniKeyLoadOrCreateFloat(N_T_CRYO_PUMPING, VAL_T_CRYO_PUMPING, INI_CONST);
	settings->constants.T_CryoRecovery = IniKeyLoadOrCreateFloat(N_T_CRYO_RECOVERY, VAL_T_CRYO_RECOVERY, INI_CONST);
	settings->constants.T_ColdOperational = IniKeyLoadOrCreateFloat(N_T_COLD_OPERATIONAL, VAL_T_COLD_OPERATIONAL, INI_CONST);
	settings->constants.TS_MaxOffset = IniKeyLoadOrCreateFloat(N_TS_MAX_OFFSET, VAL_TS_MAX_OFFSET, INI_CONST);
	settings->constants.TS_StabilityDelta = IniKeyLoadOrCreateFloat(N_TS_STABILITY_DELTA, VAL_TS_STABILITY_DELTA, INI_CONST);
	settings->constants.TS_OffMax = IniKeyLoadOrCreateFloat(N_TS_OFF_MAX, VAL_TS_OFF_MAX, INI_CONST);
	settings->constants.TS_OffStabilityDelta = IniKeyLoadOrCreateFloat(N_TS_OFF_STABILITY_DELTA, VAL_TS_OFF_STABILITY_DELTA, INI_CONST);
	settings->constants.TE_ColdOperational = IniKeyLoadOrCreateFloat(N_TE_COLD_OPERATIONAL, VAL_TE_COLD_OPERATIONAL, INI_CONST);
	settings->constants.T_RegenerationAmbient = IniKeyLoadOrCreateFloat(N_T_REGEN_AMB, VAL_T_REGEN_AMB, INI_CONST);
	settings->constants.Max_VacuumErrorRetry = IniKeyLoadOrCreateUInt(N_MAX_VACUUM_E_RETRY, VAL_MAX_VACUUM_E_RETRY, INI_CONST);
	settings->constants.Max_CryoPumpingRetry = IniKeyLoadOrCreateUInt(N_MAX_CRYOPUMPING_RETRY, VAL_MAX_CRYOPUMPING_RETRY, INI_CONST);
	settings->constants.TE_Rfe1PidMaxTemp = IniKeyLoadOrCreateFloat(N_TMAX_RFE1_PID, VAL_TMAX_RFE1_PID, INI_CONST);
	settings->constants.TE_CalSourcePidMaxTemp = IniKeyLoadOrCreateFloat(N_TMAX_CAL_PID, VAL_TMAX_CAL_PID, INI_CONST);

    settings->timeouts.Timeout_VacPumpOn = IniKeyLoadOrCreateUInt(N_TO_VAC_PUMP_ON, VAL_TO_VAC_PUMP_ON, INI_TIMEOUTS);
    settings->timeouts.Timeout_CryoPressureDecrease = IniKeyLoadOrCreateUInt(N_TO_CRYO_PRESS_DEC, VAL_TO_CRYO_PRESS_DEC, INI_TIMEOUTS);
    settings->timeouts.Timeout_Rfe1TemperatureDecrease = IniKeyLoadOrCreateUInt(N_TO_CRYO_TEMP_DEC, VAL_TO_CRYO_TEMP_DEC, INI_TIMEOUTS);
    settings->timeouts.Timeout_CryoOnPressure = IniKeyLoadOrCreateUInt(N_TO_CRYO_ON_PRESS, VAL_TO_CRYO_ON_PRESS, INI_TIMEOUTS);
    settings->timeouts.Timeout_VSC = IniKeyLoadOrCreateUInt(N_TO_VSC, VAL_TO_VSC, INI_TIMEOUTS);
    settings->timeouts.Timeout_CompressorOn = IniKeyLoadOrCreateUInt(N_TO_COMPRESSOR_ON, VAL_TO_COMPRESSOR_ON, INI_TIMEOUTS);
    settings->timeouts.Timeout_CryoPumpPressure = IniKeyLoadOrCreateUInt(N_TO_CRYO_PUMP_PRESS, VAL_TO_CRYO_PUMP_PRESS, INI_TIMEOUTS);
    settings->timeouts.Timeout_ColdOpsTemp = IniKeyLoadOrCreateUInt(N_TO_COLD_OPS_TEMP, VAL_TO_COLD_OPS_TEMP, INI_TIMEOUTS);
    settings->timeouts.Timeout_AmbientTemp = IniKeyLoadOrCreateUInt(N_TO_AMBIENT_TEMP, VAL_TO_AMBIENT_TEMP, INI_TIMEOUTS);

    settingsFile->Save();
}

void FeedPackageControl::ReloadSettings()
{
	settingsFile->Reload();
	LoadIniSettings();
    fp345AO->settings = settings;
}

/* This function starts the state machine thread */
bool FeedPackageControl::Run()
{
	cout << "STARTING SM THREAD" << endl;

	bool retVal = false;

	pthread_attr_t attr;
	pthread_attr_init(&attr);

	// SCHED_FIFO corresponds to real-time preemptive priority-based scheduler
	// NOTE: This scheduling policy requires the superuser privileges
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);

	struct sched_param param;
	param.sched_priority = 1 + (sched_get_priority_max(SCHED_FIFO) - 60);

	pthread_attr_setschedparam(&attr, &param);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    int res = pthread_create(&sm_thread, &attr, SM_main, this->settings);
    if (res != 0)
    {
        perror("SM_main() pthread create");
        // TODO: handle error here ...
    }
    else
    {
        retVal = true;
    }

    return retVal;
}


bool FeedPackageControl::Reset()
{
	return fp345AO->POST(Q_NEW(QEvt, RESET_SIG), me);
}

bool FeedPackageControl::Shutdown()
{
    cout << "################## VIA TANGO ##################" << endl;
    cout << "Setting State to SHUTDOWN " << endl;

    return fp345AO->POST(Q_NEW(QEvt, SHUTDOWN_SIG), me);
}

bool FeedPackageControl::SetMode(std::string mode)
{
	bool retVal = false;

	cout << "Setting mode to: [" << mode << "]" << endl;

	if (mode == "MAINT")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, ITC_SIG), me);
	}
	else if (mode == "STANDBY")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, SOFT_OFF_SIG), me);
	}
	else if (mode == "OPER")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, OPERATIONAL_SIG), me);
	}
	else if (mode == "REGEN")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, REGENERATE_SIG), me);
	}
	else if (mode == "DL_FILES")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, DL_FILES_5A_SIG), me);
	}
	else if (mode == "UL_FILES")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, UL_FILES_5A_SIG), me);
	}
	else if (mode == "UL_CONFIG")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, UL_CONFIG_SIG), me);
	}
	else if (mode == "UD_CONFIG")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);
	}
	else if (mode == "UL_BIAS")
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, UL_BIAS_SIG), me);
	}
	return retVal;
}

bool FeedPackageControl::ClearErrors()
{
	fp345AO->lastError = -1;
	fp345AO->lastServError = -1;
	return fp345AO->POST(Q_NEW(QEvt, ERRORS_CLEARED_SIG), me);
}

bool FeedPackageControl::SetVacuumValve(bool state)
{
	bool retVal = false;

	if (state) // open vacuum valve
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, OPEN_VALVE_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, CLOSE_VALVE_SIG), me);
	}

	return retVal;
}

void FeedPackageControl::SetSpfHeLocation(std::string spfHeLocation)
{
	fp345AO->spfHeLocation = spfHeLocation; // operator= copies data

}
void FeedPackageControl::SetSpfVacLocation(std::string spfVacLocation)
{
	fp345AO->spfVacLocation = spfVacLocation; // operator= copies data
}
void FeedPackageControl::SetSpf345Location(std::string spf345Location)
{
	fp345AO->spf345Location = spf345Location; // operator= copies data
}
void FeedPackageControl::SetComPort(std::string ttyPort)
{
	fp345AO->ttyPort = ttyPort; // operator= copies data
}



bool FeedPackageControl::SetExpectedOnline(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal  = settingsFile->SetInt("expected_online", 1, "", "Variables"); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		fp345AO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
	}
	else
	{
		retVal  = settingsFile->SetInt("expected_online", 0, "", "Variables"); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		fp345AO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
		retVal &= fp345AO->POST(Q_NEW(QEvt, OFFLINE_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::GetExpectedOnline()
{
	return settings->variables.expected_online;
}

bool FeedPackageControl::SetStartupDefault(enStartupDefault state)
{
	bool retVal = false;

	retVal  = settingsFile->SetInt("startup_default", static_cast<short>(state), "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.startup_default = state;

	return retVal;
}
enStartupDefault FeedPackageControl::GetStartupDefault()
{
	return settings->variables.startup_default;
}

bool FeedPackageControl::SetActiveBand(enActiveBand band)
{
	return fp345AO->SetActiveBand(band);
}
bool FeedPackageControl::SetB3TempSetpoint(float sp)
{
	return fp345AO->SetB3LnaTempSetpoint(sp);
}
bool FeedPackageControl::SetB4TempSetpoint(float sp)
{
	return fp345AO->SetB4LnaTempSetpoint(sp);
}
bool FeedPackageControl::SetB5aTempSetpoint(float sp)
{
	return fp345AO->SetB5aLnaTempSetpoint(sp);
}
bool FeedPackageControl::SetB5bTempSetpoint(float sp)
{
	return fp345AO->SetB5bLnaTempSetpoint(sp);
}
bool FeedPackageControl::SetB6TempSetpoint(float sp)
{
	return fp345AO->SetB6LnaTempSetpoint(sp);
}
bool FeedPackageControl::SetB3WpTempSetpoint(float sp)
{
	return fp345AO->SetB3WpTempSetpoint(sp);
}
bool FeedPackageControl::SetB4WpTempSetpoint(float sp)
{
	return fp345AO->SetB4WpTempSetpoint(sp);
}
bool FeedPackageControl::SetB5aWpTempSetpoint(float sp)
{
	return fp345AO->SetB5aWpTempSetpoint(sp);
}
bool FeedPackageControl::SetB5bWpTempSetpoint(float sp)
{
	return fp345AO->SetB5bWpTempSetpoint(sp);
}
bool FeedPackageControl::SetB6WpTempSetpoint(float sp)
{
	return fp345AO->SetB6WpTempSetpoint(sp);
}

bool FeedPackageControl::SetCalSourceTempSetpoint(float sp)
{
	return fp345AO->SetCalSourceTempSetpoint(sp);
}

bool FeedPackageControl::SetDefaultB3TempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b3", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b3 = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB4TempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b4", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b4 = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB5aTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b5a", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b5a = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB5bTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b5b", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b5b = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB6TempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b6", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b6 = sp;

	return retVal;
}
float FeedPackageControl::GetDefaultB3TempSetpoint()
{
	return settings->variables.TS_b3;
}
float FeedPackageControl::GetDefaultB4TempSetpoint()
{
	return settings->variables.TS_b4;
}
float FeedPackageControl::GetDefaultB5aTempSetpoint()
{
	return settings->variables.TS_b5a;
}
float FeedPackageControl::GetDefaultB5bTempSetpoint()
{
	return settings->variables.TS_b5b;
}
float FeedPackageControl::GetDefaultB6TempSetpoint()
{
	return settings->variables.TS_b6;
}

bool FeedPackageControl::SetDefaultB3WpTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b3_Warmplate", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b3Wp = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB4WpTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b4_Warmplate", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b4Wp = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB5aWpTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b5a_Warmplate", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b5aWp = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB5bWpTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b5b_Warmplate", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b5bWp = sp;

	return retVal;
}
bool FeedPackageControl::SetDefaultB6WpTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_b6_Warmplate", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_b6Wp = sp;

	return retVal;
}
float FeedPackageControl::GetDefaultB3WpTempSetpoint()
{
	return settings->variables.TS_b3Wp;
}
float FeedPackageControl::GetDefaultB4WpTempSetpoint()
{
	return settings->variables.TS_b4Wp;
}
float FeedPackageControl::GetDefaultB5aWpTempSetpoint()
{
	return settings->variables.TS_b5aWp;
}
float FeedPackageControl::GetDefaultB5bWpTempSetpoint()
{
	return settings->variables.TS_b5bWp;
}
float FeedPackageControl::GetDefaultB6WpTempSetpoint()
{
	return settings->variables.TS_b6Wp;
}
bool FeedPackageControl::SetDefaultCalSourceTempSetpoint(float sp)
{
	bool retVal = false;

	retVal  = settingsFile->SetFloat("TS_CalSource", sp, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_CalSource = sp;

	return retVal;
}
float FeedPackageControl::GetDefaultCalSourceTempSetpoint()
{
	return settings->variables.TS_CalSource;
}
float FeedPackageControl::GetTS_OffMax()
{
	return settings->constants.TS_OffMax;
}

bool FeedPackageControl::FeedDetected()
{
	return fp345AO->feedDetected;
}

enDeviceStatus FeedPackageControl::GetDeviceStatus()
{
	return fp345AO->GetDeviceStatus();
}

enCapabilityState FeedPackageControl::GetCapabilityState()
{
	return fp345AO->GetCapabilityState();
}

string FeedPackageControl::GetCurrentState()
{
	return fp345AO->GetCurrentState();
}

enDeviceMode FeedPackageControl::GetFeedMode()
{
	return fp345AO->GetFeedMode();
}

stFeedDataFrame FeedPackageControl::GetDataFrame()
{
	return fp345AO->GetDataFrame();
}

FP345::stFeedConfig FeedPackageControl::GetFeedConfig()
{
	return fp345AO->feedConfig;
}

stLnaBiasParameters FeedPackageControl::GetLnaHBiasParameters()
{
	return fp345AO->biasHParameters;
}

stLnaBiasParameters FeedPackageControl::GetLnaVBiasParameters()
{
	return fp345AO->biasVParameters;
}

void FeedPackageControl::SetLnaHBiasParameters(stLnaBiasParameters biasParams)
{
	fp345AO->biasHParameters = biasParams;
}

void FeedPackageControl::SetLnaVBiasParameters(stLnaBiasParameters biasParams)
{
	fp345AO->biasVParameters = biasParams;
}

bool FeedPackageControl::SetB345SerialNr(Tango::DevString serialNr)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.serial = serialNr;
		retVal = true;
//		retVal = fp345AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}


bool FeedPackageControl::SetLnaSerialNrs(const Tango::ConstDevString *serialNr)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.serialLna3H = serialNr[0];
		fp345AO->feedConfig.serialLna3V = serialNr[1];
		fp345AO->feedConfig.serialLna4H = serialNr[2];
		fp345AO->feedConfig.serialLna4V = serialNr[3];
		fp345AO->feedConfig.serialLna5aH = serialNr[4];
		fp345AO->feedConfig.serialLna5aV = serialNr[5];
		fp345AO->feedConfig.serialLna5bH = serialNr[6];
		fp345AO->feedConfig.serialLna5bV = serialNr[7];
		fp345AO->feedConfig.serialLna6H = serialNr[8];
		fp345AO->feedConfig.serialLna6V = serialNr[9];
		retVal = true;
//		retVal = fp345AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}
bool FeedPackageControl::SetLnaHSerialNr(Tango::DevString serialNr)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.serialLna3H = serialNr;
		retVal = true;
//		retVal = fp345AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaVSerialNr(Tango::DevString serialNr)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.serialLna3V = serialNr;
		retVal = true;
//		retVal = fp345AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaGain3H(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain3H = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain3V(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain3V = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain4H(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain4H = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain4V(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain4V = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain5aH(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain5aH = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain5aV(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain5aV = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain5bH(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain5bH = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain5bV(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain5bV = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain6H(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain6H = meanGain;
		retVal = true;
	}
	return retVal;
}
bool FeedPackageControl::SetLnaGain6V(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp345AO->GetCurrentState() == "Maintenance")
	{
		fp345AO->feedConfig.meanGain6V = meanGain;
		retVal = true;
	}
	return retVal;
}

bool FeedPackageControl::SetLnaHBiasParams(const Tango::DevFloat *params)
{
	bool retVal = false;
	if ((fp345AO->GetCurrentState() == "Maintenance") || (fp345AO->GetCurrentState() == "Offline"))
	{
		fp345AO->feedConfig.lna3HVd = params[0];
		fp345AO->feedConfig.lna3HId = params[1];
		fp345AO->feedConfig.lna4HVd = params[2];
		fp345AO->feedConfig.lna4HId = params[3];
		fp345AO->feedConfig.lna5aHVd = params[4];
		fp345AO->feedConfig.lna5aHId = params[5];
		fp345AO->feedConfig.lna5bHVd = params[6];
		fp345AO->feedConfig.lna5bHId = params[7];
		fp345AO->feedConfig.lna6HVd = params[8];
		fp345AO->feedConfig.lna6HId = params[9];
		retVal = true;
//		retVal = fp345AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaVBiasParams(const Tango::DevFloat *params)
{
	bool retVal = false;
	if ((fp345AO->GetCurrentState() == "Maintenance") || (fp345AO->GetCurrentState() == "Offline"))
	{
		fp345AO->feedConfig.lna3VVd = params[0];
		fp345AO->feedConfig.lna3VId = params[1];
		fp345AO->feedConfig.lna4VVd = params[2];
		fp345AO->feedConfig.lna4VId = params[3];
		fp345AO->feedConfig.lna5aVVd = params[4];
		fp345AO->feedConfig.lna5aVId = params[5];
		fp345AO->feedConfig.lna5bVVd = params[6];
		fp345AO->feedConfig.lna5bVId = params[7];
		fp345AO->feedConfig.lna6VVd = params[8];
		fp345AO->feedConfig.lna6VId = params[9];
		retVal = true;
//		retVal = fp345AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

string FeedPackageControl::GetFirmwareVersion()
{
	return fp345AO->fwVersion;
}

bool FeedPackageControl::SetB3LnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B3LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B3LNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB4LnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B4LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B4LNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB5aLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5aLNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5aLNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB5bLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5bLNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5bLNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB6LnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B6LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B6LNA_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, CS_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, CS_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB3PidLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B3PID_LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B3PID_LNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB4PidLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B4PID_LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B4PID_LNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB5aPidLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5aPID_LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5aPID_LNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB5bPidLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5bPID_LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B5bPID_LNA_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetB6PidLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B6PID_LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, B6PID_LNA_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetMotorState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, STEPPER_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, STEPPER_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetMotorSpeed(short rpm)
{
	return fp345AO->SetCryoMotorSpeed(rpm);
}

bool FeedPackageControl::SetTurboState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, TURBO_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, TURBO_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetNegPumpState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, NEG_ON_SIG), me);
	}
	else
	{
		retVal = fp345AO->POST(Q_NEW(QEvt, NEG_OFF_SIG), me);
	}

	return retVal;
}

int FeedPackageControl::IniKeyLoadOrCreateInt(std::string key, int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetInt(key, section);
	}
}

unsigned int FeedPackageControl::IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetUInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetUInt(key, section);
	}
}


double FeedPackageControl::IniKeyLoadOrCreateDouble(std::string key, double value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetDouble(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetDouble(key, section);
	}
}

float FeedPackageControl::IniKeyLoadOrCreateFloat(std::string key, float value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetFloat(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetFloat(key, section);
	}
}

bool FeedPackageControl::SendRawCommand(string command, string & response)
{
	return fp345AO->SendRawCommand(command, response);
}

bool FeedPackageControl::SetConfigFile(string & response)
{
	return fp345AO->SetConfigFile(response);
}

bool FeedPackageControl::SetModelFiles(string & response)
{
	return fp345AO->SetModelFiles(response);
}

bool FeedPackageControl::UpdateBias(string & response)
{
	return fp345AO->UpdateBias(response);
}

bool FeedPackageControl::UpdateConfigFile(string & response)
{
	return fp345AO->UpdateConfigFile(response);
}

bool FeedPackageControl::Test()
{
	return fp345AO->POST(Q_NEW(QEvt, TEST_SERVICE_SIG), me);
}



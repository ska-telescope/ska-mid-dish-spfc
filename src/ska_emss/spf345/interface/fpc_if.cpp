
/*
 * fpc_if.cpp
 *
 *  Created on: 09 Mar 2022
 *      Author: cvn
 */
#define _GLIBCXX_USE_NANOSLEEP 1

#define SERIAL_COMMS_TIMEOUT 1000 // milliseconds

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <map>
#include <chrono>
#include <thread>

#include "IniParser.h"
#include "fpc_if.h"
#include "system.h"
#include "utilities.h"

using namespace util;
using namespace std;
using namespace serial;

#ifdef SIMULATOR
extern string pPortName;
#endif

FpcProtocol::FpcProtocol()
{
#ifdef SIMULATOR
	portName = pPortName;
#else
	portName = "/dev/ttyS3";
#endif

	/* If no port name is given to serialport, it won't be opened automatically */
	serialPort = new Serial();
	serialTX = new Digital(TXD3, N_TXD3);
	serialRX = new Digital(RXD3, N_RXD3);

	ioOutLedOnline = new Digital(LED_B345_ONLINE, N_LED_B345_ONLINE);
}

FpcProtocol::FpcProtocol(const string PortName)
{
	portName = PortName;

	/* Initialise all the interfaces */
	serialPort = new Serial();
	serialTX = new Digital(TXD3, N_TXD3);
	serialRX = new Digital(RXD3, N_RXD3);

	ioOutLedOnline = new Digital(LED_B345_ONLINE, N_LED_B345_ONLINE);
}

FpcProtocol::~FpcProtocol()
{
	cout << "FP345 HW DESTRUCT" << endl;

	if (serialPort->isOpen())
	{
		serialPort->close();
	}

	delete serialPort;

	delete ioOutLedOnline;
}

// This function opens the digital channels and the serial port */
bool FpcProtocol::Init()
{
	bool retVal = true;

	if (ioOutLedOnline->SetDir(0) < 0)
	{
		cout << "Direction NOT set [ioOutLedOnline]" << endl;
	}

	if (serialTX->SetDir(0) < 0)
	{
		cout << "Direction NOT set [serialTX]" << endl;
	}
	if (serialTX->SetActiveLow(1) < 0)
	{
		cout << "ActiveLow could not be set for [serialTX]" << endl;
	}
	if (serialRX->SetActiveLow(1) < 0)
	{
		cout << "ActiveLow could not be set for [serialRX]" << endl;
	}

	try
	{
		//serialPort = new Serial(portName, 9600, Timeout::simpleTimeout(SERIAL_COMMS_TIMEOUT)); // Will open the serial port in this ctor
		serialPort = new Serial(portName, 19200, Timeout::simpleTimeout(SERIAL_COMMS_TIMEOUT)); // Will open the serial port in this ctor
	}
	catch(PortNotOpenedException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(IOException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(SerialException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(invalid_argument &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::SetOnlineLed()
{
	return !ioOutLedOnline->SetVal(1);
}

bool FpcProtocol::ClearOnlineLed()
{
	return !ioOutLedOnline->SetVal(0);
}


bool FpcProtocol::DownloadConfigFile()
{
	return ReadRecordNew(enMemoryRecord::CONFIG, SPF345_CONFIG);
}

/**
 * This function should be used to duplicate the Config file to the upload folder (after updating it)
 * in order to upload the changes to the FPC.
 */
bool FpcProtocol::DuplicateConfigFile()
{
	//fileCopy(SPF345_CONFIG -> SPF345_CONFIG_UL);
    std::ifstream  src(SPF345_CONFIG, std::ios::binary);
    std::ofstream  dst(SPF345_CONFIG_UL,   std::ios::binary);
    dst << src.rdbuf();
    return true;
}

bool FpcProtocol::UploadConfigFile(string & response)
{
	bool retVal = false;
	enFpcMode mode = enFpcMode::UNKNOWN;
	GetCurrentMode(mode);
	if (mode == enFpcMode::MAINTENANCE)
	{
		retVal = true;
	} else if (mode == enFpcMode::UNKNOWN)
	{
		response = "*QI -> Unknown state; File not uploaded";
		cout << response << endl;
		retVal = false;
	} else if (mode == enFpcMode::APPLICATION)
	{
		if ( (retVal = SendSimpleCommand("*JF\r")) )
		{
			usleep(500000);
			if ( !(retVal = SendSimpleCommand("*RU\r")) ) // stay in factory image mode for long enough to upload large files
			{
				response = "*RU -> Failed; File not uploaded";
				cout << response << endl;
			}
		} else
		{
			response = "*JF -> Failed; File not uploaded";
			cout << response << endl;
		}
	}
	if (retVal)
	{
		if (retVal &= WriteRecord(enMemoryRecord::CONFIG, SPF345_CONFIG_UL))
		{
			response = "[UL_FILES] SPF3_CONFIG upload finished";
			cout << response << endl;
		} else
		{
			response = "[UL_FILES] SPF3_CONFIG upload FAILED";
			cout << response << endl;
		}
//		SendSimpleCommand("*JA\r");
//		usleep(100000); // wait for 100 ms
	}
//	cout << "Mode [UL_FILES] completed" << endl;
	return retVal;
}

bool FpcProtocol::UploadLnaBias(stLnaBiasParameters biasH, stLnaBiasParameters biasV, string & response)
{
	bool retVal = false;
	string responseAdd;
	enFpcMode mode = enFpcMode::UNKNOWN;

	GetCurrentMode(mode);
	if (mode == enFpcMode::MAINTENANCE)
	{
		retVal = true;
	} else if (mode == enFpcMode::UNKNOWN)
	{
		response = "*QI -> Unknown state; File not uploaded";
		cout << response << endl;
		retVal = false;
	} else if (mode == enFpcMode::APPLICATION)
	{
		if ( (retVal = SendSimpleCommand("*JF\r")) )
		{
			usleep(500000);
			if ( !(retVal = SendSimpleCommand("*RU\r")) ) // stay in factory image mode for long enough to upload large files
			{
				response = "*RU -> Failed; bias values NOT updated";
				cout << response << endl;
			}
		} else
		{
			response = "*JF -> Failed; bias values NOT updated";
			cout << response << endl;
		}
	}
	if (retVal)
	{
		if ( (retVal &= SetLnaHBias(biasH)) )
		{
			response = "[UL_BIAS] LNA H-channel bias values Successfully updated";
			cout << response << endl;
		} else
		{
			response = "[UL_BIAS] LNA H-channel bias values update FAILED";
			cout << response << endl;
		}
		usleep(100000); // wait for 100 ms
		if ( (retVal &= SetLnaVBias(biasV)) )
		{
			responseAdd = "[UL_BIAS] LNA V-channel bias values Successfully updated";
			cout << responseAdd << endl;
		} else
		{
			responseAdd = "[UL_BIAS] LNA V-channel bias values update FAILED";
			cout << responseAdd << endl;
		}
		response += '\r' + responseAdd;

		SendSimpleCommand("*JA\r");
		usleep(100000); // wait for 100 ms
	}
//	cout << "Mode [UL_BIAS] completed" << endl;
	return retVal;
}

// Note this is only for Band 5a&b at this stage ToDo: need to clone this function to include bands 3,4 & 6 etc.
bool FpcProtocol::DownloadModelFiles5A()
{
	bool retVal = true;
	retVal &= ReadRecordNew(enMemoryRecord::REC_GAIN5A, SPF5A_REC_GAIN);
	cout << "[DL_FILES] SPF5A_REC_GAIN finished" << endl;
	retVal &= ReadRecordNew(enMemoryRecord::REC_NOISE5A, SPF5A_REC_NOISE);
	cout << "[DL_FILES] SPF5A_REC_NOISE finished" << endl;
	retVal &= ReadRecordNew(enMemoryRecord::CAL_NOISE5A, SPF5A_CAL_NOISE);
	cout << "[DL_FILES] SPF5A_CAL_NOISE finished" << endl;
	cout << "Mode [DL_FILES] completed" << endl;
	return retVal;
}
bool FpcProtocol::DownloadModelFiles5B()
{
	bool retVal = true;
	retVal &= ReadRecordNew(enMemoryRecord::REC_GAIN5B, SPF5B_REC_GAIN);
	cout << "[DL_FILES] SPF5B_REC_GAIN finished" << endl;
	retVal &= ReadRecordNew(enMemoryRecord::REC_NOISE5B, SPF5B_REC_NOISE);
	cout << "[DL_FILES] SPF5B_REC_NOISE finished" << endl;
	retVal &= ReadRecordNew(enMemoryRecord::CAL_NOISE5B, SPF5B_CAL_NOISE);
	cout << "[DL_FILES] SPF5B_CAL_NOISE finished" << endl;
	cout << "Mode [DL_FILES] completed" << endl;
	return retVal;
}

// Note this is only for Band 5a at this stage ToDo: need to clone this function to include bands 3,4,5b & 6 etc.
bool FpcProtocol::UploadModelFiles5A(string & response)
{
	bool retVal = false;
	string responseAdd;
	enFpcMode mode = enFpcMode::UNKNOWN;
	GetCurrentMode(mode);
	if (mode == enFpcMode::MAINTENANCE)
	{
		retVal = true;
	} else if (mode == enFpcMode::UNKNOWN)
	{
		response = "*QI -> Unknown state; File not uploaded";
		cout << response << endl;
		retVal = false;
	} else if (mode == enFpcMode::APPLICATION)
	{
		if ( (retVal = SendSimpleCommand("*JF\r")) )
		{
			usleep(500000);
			if ( !(retVal = SendSimpleCommand("*RU\r")) ) // stay in factory image mode for long enough to upload large files
			{
				response = "*RU -> Failed; bias values NOT updated";
				cout << response << endl;
			}
		} else
		{
			response = "*JF -> Failed; bias values NOT updated";
			cout << response << endl;
		}
	}
	if (retVal)
	{
			if (WriteRecord(enMemoryRecord::REC_GAIN5A, SPF5A_REC_GAIN_UL))
			{
				response = "[UL_FILES] SPF5A_REC_GAIN finished";
				cout << response << endl;
				retVal &= true;
			} else {
				response = "[UL_FILES] SPF5A_REC_GAIN upload failed";
				cout << response << endl;
				retVal &= false;
			}
			if (WriteRecord(enMemoryRecord::REC_NOISE5A, SPF5A_REC_NOISE_UL))
			{
				responseAdd = "[UL_FILES] SPF5A_REC_NOISE finished";
				cout << responseAdd << endl;
				retVal &= true;
			} else
			{
				responseAdd = "[UL_FILES] SPF5A_REC_NOISE upload failed";
				cout << responseAdd << endl;
				retVal &= false;
			}
			response += '\r' + responseAdd;
			if (WriteRecord(enMemoryRecord::CAL_NOISE5A, SPF5A_CAL_NOISE_UL))
			{
				responseAdd = "[UL_FILES] SPF5A_CAL_NOISE finished";
				cout << responseAdd << endl;
				retVal &= true;
			} else
			{
				responseAdd = "[UL_FILES] SPF5A_CAL_NOISE upload failed";
				cout << responseAdd << endl;
				retVal &= false;
			}
			response += '\r' + responseAdd;
	}
	SendSimpleCommand("*JA\r");
	responseAdd = "Mode [UL_FILES] completed";
	cout << responseAdd << endl;
	response += '\r' + responseAdd;
	return retVal;
}

bool FpcProtocol::LnasOn()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set all LNA  bits */
		cf.bits.LnaB3 = 1;
		cf.bits.LnaB4 = 1;
		cf.bits.LnaB5a = 1;
		cf.bits.LnaB5b = 1;
		cf.bits.LnaB6 = 1;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}

bool FpcProtocol::LnasOff()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set all LNA bits */
		cf.bits.LnaB3 = 0;
		cf.bits.LnaB4 = 0;
		cf.bits.LnaB5a = 0;
		cf.bits.LnaB5b = 0;
		cf.bits.LnaB6 = 0;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}

bool FpcProtocol::B3LnaOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_B3, 1);
}
bool FpcProtocol::B3LnaOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_B3, 0);
}
bool FpcProtocol::B4LnaOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_B4, 1);
}
bool FpcProtocol::B4LnaOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_B4, 0);
}
bool FpcProtocol::B5aLnaOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_B5A, 1);
}
bool FpcProtocol::B5aLnaOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_B5A, 0);
}
bool FpcProtocol::B5bLnaOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_B5B, 1);
}
bool FpcProtocol::B5bLnaOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_B5B, 0);
}
bool FpcProtocol::B6LnaOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_B6, 1);
}
bool FpcProtocol::B6LnaOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_B6, 0);
}


bool FpcProtocol::CalSourceOn()
{
	return ModifyControlBit(enControlBitNumber::CALSOURCE, 1);
}
bool FpcProtocol::CalSourceOff()
{
	return ModifyControlBit(enControlBitNumber::CALSOURCE, 0);
}

/* PID Control */
bool FpcProtocol::PidLnasOn()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set all LNA bits */
		cf.bits.PidLnaB3 = 1;
		cf.bits.PidLnaB4 = 1;
		cf.bits.PidLnaB5a = 1;
		cf.bits.PidLnaB5b = 1;
		cf.bits.PidLnaB6 = 1;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}
bool FpcProtocol::PidLnasOff()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set all LNA bits */
		cf.bits.PidLnaB3 = 0;
		cf.bits.PidLnaB4 = 0;
		cf.bits.PidLnaB5a = 0;
		cf.bits.PidLnaB5b = 0;
		cf.bits.PidLnaB6 = 0;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}
bool FpcProtocol::B3PidLnaOn()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B3, 1);
}
bool FpcProtocol::B3PidLnaOff()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B3, 0);
}
bool FpcProtocol::B4PidLnaOn()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B4, 1);
}
bool FpcProtocol::B4PidLnaOff()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B4, 0);
}
bool FpcProtocol::B5aPidLnaOn()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B5A, 1);
}
bool FpcProtocol::B5aPidLnaOff()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B5A, 0);
}
bool FpcProtocol::B5bPidLnaOn()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B5B, 1);
}
bool FpcProtocol::B5bPidLnaOff()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B5B, 0);
}
bool FpcProtocol::B6PidLnaOn()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B6, 1);
}
bool FpcProtocol::B6PidLnaOff()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA_B6, 0);
}

bool FpcProtocol::NegHeaterOn()
{
	return ModifyControlBit(enControlBitNumber::NEG_HEATER, 1);
}
bool FpcProtocol::NegHeaterOff()
{
	return ModifyControlBit(enControlBitNumber::NEG_HEATER, 0);
}
bool FpcProtocol::CryoMotorOn() // Same as using StartCryoMotor() command
{
	return ModifyControlBit(enControlBitNumber::CRYO_MOTOR, 1);
}
bool FpcProtocol::CryoMotorOff() // Same as using StopCryoMotor() command
{
	return ModifyControlBit(enControlBitNumber::CRYO_MOTOR, 0);
}
bool FpcProtocol::VaValveOpen() // Same as using OpenVacuumValve() command
{
	return ModifyControlBit(enControlBitNumber::VAC_VALVE, 1);
}
bool FpcProtocol::VaValveClose() // Same as using CloseVacuumValve() command
{
	return ModifyControlBit(enControlBitNumber::VAC_VALVE, 0);
}
bool FpcProtocol::TurboOn() // Same as using StartTurboPump() command
{
	return ModifyControlBit(enControlBitNumber::VAC_TURBO, 1);
}
bool FpcProtocol::TurboOff() // Same as using StopTurboPump() command
{
	return ModifyControlBit(enControlBitNumber::VAC_TURBO, 0);
}

/* True = Open, False = Closed */
bool FpcProtocol::GetValveStatus()
{
	bool valveStatus = false;
	if (GetVacuumValveStatus(valveStatus))
	{
		return valveStatus;
	}
	return false;
}

/* True = Running, False = OFF */
bool FpcProtocol::GetMotorStatus()
{
	short rpm;
	if (GetCryoMotorSpeed(rpm))
	{
		if (rpm == 0)
		{
			return false; // OFF
		}
		return true; // Running
	}
	return false; // No status => OFF
}

bool FpcProtocol::ClearAllErrors()
{
	unErrorFlags ef;
	ef.Word = 0;
	return ModifyErrorRegister(ef);
}

bool FpcProtocol::SetError(enErrorBits eb)
{
	return ModifyErrorBit(static_cast<int>(eb), 1);
}
bool FpcProtocol::ClearError(enErrorBits eb)
{
	return ModifyErrorBit(static_cast<int>(eb), 0);
}

// TODO: Check for malformed protocol responses
bool FpcProtocol::ValidateResponse(string response)
{
	bool retVal = false;

	if ((response != "") && (!response.empty()))
	{
		if ((response[0] == '*') && ((response[1] == 'S') || (response[1] == 's')))
		{
			retVal = true;
		}
	}

	return retVal;
}

bool FpcProtocol::SendSimpleCommand(string command)
{
	bool retVal = false;

	try
	{
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();
//		cout << command << " -> " << response << endl;

		/* Validate the response */
		retVal = ValidateResponse(response);

	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << "\n Command: " << command << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Sensor Monitoring  */
/* ================== */
// Sample all sensors
// test response 1: *S,4.99,513,35.6,21.5,8.5,29.3,22.9
// test response 2: *S,4.99,-513,35.6,-21.5,8.5,-29.3,22.9
// test response 3: *F
bool FpcProtocol::SampleAll(stFeedSensors & sensors)
{
	bool retVal = false;

	try
	{
		string command = "*SA\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			/* Parse message */
			retVal = ParseSampleAll(response, sensors);
		}
		else
		{
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseSampleAll(string response, stFeedSensors & sensors)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		//NOTE: Segmentation fault when trying to read more sensors than returned from feed.
		if (tokens.size() > 62) // "S" + 62 sensors = 63
		{
			// TODO: if we are modifying the variables here, some sort of lock should be put on it?
			sensors.b3LnaHDrainVoltage 			= stod(tokens[1]) / 100;
			sensors.b3LnaHGateVoltage 			= stod(tokens[2]) / 100;
			sensors.b3LnaVDrainVoltage 			= stod(tokens[3]) / 100;
			sensors.b3LnaVGateVoltage 			= stod(tokens[4]) / 100;
			sensors.b4LnaHDrainVoltage			= stod(tokens[5]) / 100;
			sensors.b4LnaHGateVoltage 			= stod(tokens[6]) / 100;
			sensors.b4LnaVDrainVoltage			= stod(tokens[7]) / 100;
			sensors.b4LnaVGateVoltage 			= stod(tokens[8]) / 100;
			sensors.b5aLnaHDrainVoltage			= stod(tokens[9]) / 100;
			sensors.b5aLnaHGateVoltage 			= stod(tokens[10]) / 100;
			sensors.b5aLnaVDrainVoltage			= stod(tokens[11]) / 100;
			sensors.b5aLnaVGateVoltage 			= stod(tokens[12]) / 100;
			sensors.b5bLnaHDrainVoltage			= stod(tokens[13]) / 100;
			sensors.b5bLnaHGateVoltage 			= stod(tokens[14]) / 100;
			sensors.b5bLnaVDrainVoltage			= stod(tokens[15]) / 100;
			sensors.b5bLnaVGateVoltage 			= stod(tokens[16]) / 100;
			sensors.b6LnaHDrainVoltage			= stod(tokens[17]) / 100;
			sensors.b6LnaHGateVoltage 			= stod(tokens[18]) / 100;
			sensors.b6LnaVDrainVoltage			= stod(tokens[19]) / 100;
			sensors.b6LnaVGateVoltage 			= stod(tokens[20]) / 100;
			sensors.b3LnaHDrainCurrent 			= stod(tokens[21]) / 10;
			sensors.b3LnaVDrainCurrent 			= stod(tokens[22]) / 10;
			sensors.b4LnaHDrainCurrent 			= stod(tokens[23]) / 10;
			sensors.b4LnaVDrainCurrent 			= stod(tokens[24]) / 10;
			sensors.b5aLnaHDrainCurrent 		= stod(tokens[25]) / 10;
			sensors.b5aLnaVDrainCurrent 		= stod(tokens[26]) / 10;
			sensors.b5bLnaHDrainCurrent 		= stod(tokens[27]) / 10;
			sensors.b5bLnaVDrainCurrent 		= stod(tokens[28]) / 10;
			sensors.b6LnaHDrainCurrent 			= stod(tokens[29]) / 10;
			sensors.b6LnaVDrainCurrent 			= stod(tokens[30]) / 10;

			sensors.b345CryoPressure 			= ParseMillibar(tokens[31]);
			sensors.b345ManifoldPressure	 	= ParseMillibar(tokens[32]);

			sensors.b3LnaTemp	 				= stod(tokens[33]) / 10;
			sensors.b4LnaTemp 					= stod(tokens[34]) / 10;
			sensors.b5aLnaTemp	 				= stod(tokens[35]) / 10;
			sensors.b5bLnaTemp 					= stod(tokens[36]) / 10;
			sensors.b6LnaTemp 					= stod(tokens[37]) / 10;

			sensors.b3OmtTemp	 				= stof(tokens[38]) / 10;
			sensors.b4OmtTemp 					= stod(tokens[39]) / 10;
			sensors.b5aOmtTemp	 				= stod(tokens[40]) / 10;
			sensors.b5bOmtTemp 					= stod(tokens[41]) / 10;
			sensors.b6OmtTemp 					= stod(tokens[42]) / 10;

			sensors.b5aHornTemp	 				= stod(tokens[43]) / 10;
			sensors.b5bHornTemp 				= stod(tokens[44]) / 10;
			sensors.b6HornTemp 					= stod(tokens[45]) / 10;

			sensors.b345BodyTemp 				= stod(tokens[46]) / 10;
			sensors.b345ColdheadStage1Temp 		= stod(tokens[47]) / 10;
			sensors.b345ColdheadStage2Temp 		= stod(tokens[48]) / 10;

			sensors.b3WarmPlateTemp				= stod(tokens[49]) / 10;
			sensors.b4WarmPlateTemp 			= stod(tokens[50]) / 10;
			sensors.b5aWarmPlateTemp			= stod(tokens[51]) / 10;
			sensors.b5bWarmPlateTemp 			= stod(tokens[52]) / 10;
			sensors.b6WarmPlateTemp				= stod(tokens[53]) / 10;

			sensors.b345CalSourceTemp			= stod(tokens[54]) / 10;
			sensors.b345AnalogueInterfaceTemp	= stod(tokens[55]) / 10;
			sensors.b345DigitalInterfaceTemp	= stod(tokens[56]) / 10;

			sensors.b345SpdHealth	            = stod(tokens[57]) / 100;

			sensors.b3LnaPidCurrent				= stod(tokens[58]) / 10;
			sensors.b4LnaPidCurrent 			= stod(tokens[59]) / 10;
			sensors.b5aLnaPidCurrent			= stod(tokens[60]) / 10;
			sensors.b5bLnaPidCurrent			= stod(tokens[61]) / 10;
			sensors.b6LnaPidCurrent				= stod(tokens[62]) / 10;


			retVal = true;
		} else
		{
			cout << "Could not parse *SA; sensors out of range" << endl;
			retVal = false;
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* string contains 4 characters:
 * 0-2 --> Bar value in notation x.xx
 * 3   --> negative exponent
 * Example:
 * 1234 -> 1.23e-4
 */
float FpcProtocol::ParseMillibar(std::string text)
{
	float retVal = 0.0;

	if (text.length() == 4)
	{
		std::stringstream ss;
		ss << text[0] << '.' << text[1] << text[2] << "e-" << text[3];
		retVal = stod(ss.str()) * 1000.0; // We have bar and want millibar
//		if (retVal > 100)
//		{	// Snap to atmosphere if P > 1e+2, eliminate temperature drift
//			retVal = 1000;
//		}
	}

	return retVal;
}

// Sample single sensor
// test response 1: *S,0A:23.5
// test response 2: *F
bool FpcProtocol::SampleSingleSensor(unsigned int channelNum, float& value)
{
	bool retVal = false;

	try
	{
		string messageId = "*SS,";
		string parameters = numberToHexString(channelNum, 2);
		string command = messageId + parameters + "\r";
		string response;


		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			unsigned int parsedChannel;
			float parsedValue;

			/* Parse message */
			retVal = ParseSampleSingle(response, parsedChannel, parsedValue);

			if (parsedChannel == channelNum)
			{
				/* Only assign the value if we have received the response for the specified channel */
				value = parsedValue;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseSampleSingle(string response, unsigned int & channel, float & value)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			vector<string> parameters = split(tokens[1], ':');

			if (parameters.size() == 2)
			{
				channel = stoul(parameters[0], nullptr, 16);
				value = stod(parameters[1]);

				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::SelectChannels(std::vector<string> channels)
{
	return false; //TODO and FIXME!!!
}

// Not all parameters are populated in struct
bool FpcProtocol::SampleSelectedChannels(stFeedSensors & sensors)
{
	bool retVal = false;

	try
	{
		string command = "*SG\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, float> returnData;

			/* Parse message */
			if (ParseGeneralMultipleFloat(response, returnData))
			{
				sensors.b3LnaHDrainVoltage 			= returnData[1] / 100;
				sensors.b3LnaHGateVoltage 			= returnData[2] / 100;
				sensors.b3LnaVDrainVoltage 			= returnData[3] / 100;
				sensors.b3LnaVGateVoltage 			= returnData[4] / 100;
				sensors.b4LnaHDrainVoltage			= returnData[5] / 100;
				sensors.b4LnaHGateVoltage 			= returnData[6] / 100;
				sensors.b4LnaVDrainVoltage			= returnData[7] / 100;
				sensors.b4LnaVGateVoltage 			= returnData[8] / 100;
				sensors.b5aLnaHDrainVoltage			= returnData[9] / 100;
				sensors.b5aLnaHGateVoltage 			= returnData[10] / 100;
				sensors.b5aLnaVDrainVoltage			= returnData[11] / 100;
				sensors.b5aLnaVGateVoltage 			= returnData[12] / 100;
				sensors.b5bLnaHDrainVoltage			= returnData[13] / 100;
				sensors.b5bLnaHGateVoltage 			= returnData[14] / 100;
				sensors.b5bLnaVDrainVoltage			= returnData[15] / 100;
				sensors.b5bLnaVGateVoltage 			= returnData[16] / 100;
				sensors.b6LnaHDrainVoltage			= returnData[17] / 100;
				sensors.b6LnaHGateVoltage 			= returnData[18] / 100;
				sensors.b6LnaVDrainVoltage			= returnData[19] / 100;
				sensors.b6LnaVGateVoltage 			= returnData[20] / 100;
				sensors.b3LnaHDrainCurrent 			= returnData[21] / 10;
				sensors.b3LnaVDrainCurrent 			= returnData[22] / 10;
				sensors.b4LnaHDrainCurrent 			= returnData[23] / 10;
				sensors.b4LnaVDrainCurrent 			= returnData[24] / 10;
				sensors.b5aLnaHDrainCurrent 		= returnData[25] / 10;
				sensors.b5aLnaVDrainCurrent 		= returnData[26] / 10;
				sensors.b5bLnaHDrainCurrent 		= returnData[27] / 10;
				sensors.b5bLnaVDrainCurrent 		= returnData[28] / 10;
				sensors.b6LnaHDrainCurrent 			= returnData[29] / 10;
				sensors.b6LnaVDrainCurrent 			= returnData[30] / 10;
// FixMe: Incompatible pressure values - require original string value
//				sensors.b345CryoPressure 			= ParseMillibar((string)returnData[31]);
//				sensors.b345ManifoldPressure	 	= ParseMillibar((string)returnData[32]);

				sensors.b3LnaTemp	 				= returnData[33] / 10;
				sensors.b4LnaTemp 					= returnData[34] / 10;
				sensors.b5aLnaTemp	 				= returnData[35] / 10;
				sensors.b5bLnaTemp 					= returnData[36] / 10;
				sensors.b6LnaTemp 					= returnData[37] / 10;

				sensors.b3OmtTemp	 				= returnData[38] / 10;
				sensors.b4OmtTemp 					= returnData[39] / 10;
				sensors.b5aOmtTemp	 				= returnData[40] / 10;
				sensors.b5bOmtTemp 					= returnData[41] / 10;
				sensors.b6OmtTemp 					= returnData[42] / 10;

				sensors.b5aHornTemp	 				= returnData[43] / 10;
				sensors.b5bHornTemp 				= returnData[44] / 10;
				sensors.b6HornTemp 					= returnData[45] / 10;

				sensors.b345BodyTemp 				= returnData[46] / 10;
				sensors.b345ColdheadStage1Temp 		= returnData[47] / 10;
				sensors.b345ColdheadStage2Temp 		= returnData[48] / 10;

				sensors.b3WarmPlateTemp				= returnData[49] / 10;
				sensors.b4WarmPlateTemp 			= returnData[50] / 10;
				sensors.b5aWarmPlateTemp			= returnData[51] / 10;
				sensors.b5bWarmPlateTemp 			= returnData[52] / 10;
				sensors.b6WarmPlateTemp				= returnData[53] / 10;

				sensors.b345CalSourceTemp			= returnData[54] / 10;
				sensors.b345AnalogueInterfaceTemp	= returnData[55] / 10;
				sensors.b345DigitalInterfaceTemp	= returnData[56] / 10;

				sensors.b345SpdHealth	            = returnData[57] / 100;

				sensors.b3LnaPidCurrent				= returnData[58] / 10;
				sensors.b4LnaPidCurrent 			= returnData[59] / 10;
				sensors.b5aLnaPidCurrent			= returnData[60] / 10;
				sensors.b5bLnaPidCurrent			= returnData[61] / 10;
				sensors.b6LnaPidCurrent				= returnData[62] / 10;

				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::SetAllChannels()
{
	return SendSimpleCommand("*AC\r");
}

bool FpcProtocol::EnableSensorAveraging()
{
	return SendSimpleCommand("*AE\r");
}

bool FpcProtocol::DisableSensorAveraging()
{
	return SendSimpleCommand("*AD\r");
}


/* ================== */
/* Modify the FP Control Register */
/* ================== */
bool FpcProtocol::ModifyControlRegister(unControlFlags controlFlags)
{
	bool retVal = false;

	try
	{
		string messageId = "*CR,";
		string parameters = numberToHexString(controlFlags.Word, 4);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the setRegister is equal to the actual register */
				if (stoul(parsedValue, nullptr, 16) == controlFlags.Word)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::ModifyControlBit(enControlBitNumber controlBit, unsigned int value)
{
	bool retVal = false;

	try
	{
		string messageId = "*CB,";
		string parameters = numberToHexString(static_cast<short>(controlBit), 2) + ':' + numberToHexString(value, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the command is equal to the echoed response */
				if (parameters == parsedValue)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			cout << response << endl;
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::GetControlRegister(unControlFlags & controlFlags)
{
	bool retVal = false;

	try
	{
		string command = "*GC\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();


		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				controlFlags.Word = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,00:01
// test response 1: *S,05:00
// test response 2: *F
bool FpcProtocol::GetControlBit(unsigned int controlBit, unsigned int & value)
{
	bool retVal = false;

	try
	{
		string messageId = "*GB,";
		string parameters = numberToHexString(controlBit, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedParameter;
			string parsedValue;

			/* Parse message */
			if (ParseGeneralDouble(response, parsedParameter, parsedValue))
			{
				/* We have strings, convert to input types and perform checks and variable assignments */
				if (numberToHexString(controlBit, 2) == parsedParameter)
				{
					value = stoul(parsedValue, nullptr, 16);
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ==================== */
/* Active Band Control */
/* ==================== */
bool FpcProtocol::SetActiveBand(short band)
{
	string command = "*AB," + util::numberToFixedString(band, 2) + "\r";
	return SendSimpleCommand(command);
}
bool FpcProtocol::GetActiveBand(enActiveBand & band)
{
	bool retVal = false;

	try
	{
		string command = "*RB\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				band = static_cast<enActiveBand>(stoi(parsedValue));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}



/* ==================== */
/* Cooler Motor Control */
/* ==================== */
bool FpcProtocol::StartCryoMotor()
{
	return SendSimpleCommand("*EM\r");
}
bool FpcProtocol::StopCryoMotor()
{
	return SendSimpleCommand("*DM\r");
}
bool FpcProtocol::SetCryoMotorSpeed(short rpm)
{
	string command = "*SF," + util::numberToFixedString(rpm, 2) + "\r";
	return SendSimpleCommand(command);
}

bool FpcProtocol::GetCryoMotorSpeed(short & rpm)
{
	bool retVal = false;

	try
	{
		string command = "*RF\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				rpm = stoi(parsedValue);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ==================== */
/* Vacuum Valve Control commands */
/* ==================== */
bool FpcProtocol::OpenVacuumValve()
{
	return SendSimpleCommand("*OV\r");
}
bool FpcProtocol::CloseVacuumValve()
{
	return SendSimpleCommand("*CV\r");
}

bool FpcProtocol::GetVacuumValveStatus(bool & isOpen)
{
	bool retVal = false;

	try
	{
		string command = "*VS\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				isOpen = static_cast<bool>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ==================== */
/* Vacuum Turbo Pump commands */
/* ==================== */
bool FpcProtocol::StartTurboPump()
{
	return SendSimpleCommand("*ET\r"); //EnableTurbo
}
bool FpcProtocol::StopTurboPump()
{
	return SendSimpleCommand("*DT\r"); //DisableTurbo
}

bool FpcProtocol::GetTurboStatus(bool & isOn)
{
	bool retVal = false;

	try
	{
		string command = "*TS\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				isOn = static_cast<bool>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* PID Controllers	  */
/* ================== */
bool FpcProtocol::SetPidPropGain(enPid pid, int gain)
{
	string messageId = "*PP";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(gain, 4);
	string command = messageId + "," + parameter + ":" + value + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::SetPidIntegral(enPid pid, float integral)
{
	string messageId = "*PI";
	string paramater = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(static_cast<int>(integral * 1000), 4);
	string command = messageId + "," + paramater + ":" + value + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::SetPidDerivative(enPid pid, float derivative)
{
	string messageId = "*PD";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(static_cast<int>(derivative * 1000), 4);
	string command = messageId + "," + parameter + ":" + value + "\r";

	return SendSimpleCommand(command);
}

bool FpcProtocol::SetPidTempSetPoint(enPid pid, float temp)
{
	string messageId = "*PT";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(static_cast<short>(temp*10), 4);
	string command = messageId + "," + parameter + ":" + value + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::GetPidTempSetPoint(enPid pid, float & temp)
{
	bool retVal = false;

	try
	{
		string messageId = "*PK";
		string parameter = numberToHexString(static_cast<short>(pid), 2);
		string command = messageId + "," + parameter + "\r";
		string response;

		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				temp = (stoi(parsedValue))/10.0;
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;

}
bool FpcProtocol::SavePidCoeffs(enPid pid)
{
	string messageId = "*PS";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string command = messageId + "," + parameter + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::GetPidCoeffs(enPid pid, stPidSettings & pidSettings)
{
	bool retVal = false;

	try
	{
		string messageId = "*PC";
		string parameter = numberToHexString(static_cast<short>(pid), 2);
		string command = messageId + "," + parameter + "\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, float> returnData;

			/* Parse message */
			if (ParseGeneralMultipleFloat(response, returnData))
			{
				pidSettings.proportional = static_cast<int>(returnData[1]); // index starts at 1
				pidSettings.integral = returnData[2] / 1000;
				pidSettings.derivative = returnData[3] / 1000;
				//pidSettings.temp = static_cast<int>(returnData[4]);
				retVal = true;
			}
			else
			{
				cerr << "Could not parse data." << endl;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* LNA Biasing	 	  */
/* ================== */
bool FpcProtocol::SetLnaHBias(stLnaBiasParameters biasParameters)
{
	string command = "*BH";
	command += string(",01:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt3 * 100), 4);
	command += string(",02:") + numberToFixedString(static_cast<int>(biasParameters.drainCur3 * 10), 4);
	command += string(",03:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt4 * 100), 4);
	command += string(",04:") + numberToFixedString(static_cast<int>(biasParameters.drainCur4 * 10), 4);
	command += string(",05:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt5a * 100), 4);
	command += string(",06:") + numberToFixedString(static_cast<int>(biasParameters.drainCur5a * 10), 4);
	command += string(",07:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt5b * 100), 4);
	command += string(",08:") + numberToFixedString(static_cast<int>(biasParameters.drainCur5b * 10), 4);
	command += string(",09:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt6 * 100), 4);
	command += string(",10:") + numberToFixedString(static_cast<int>(biasParameters.drainCur6 * 10), 4);
	command += "\r";
	cout << "[UL_BIAS] " << command << endl;

	return SendSimpleCommand(command);

}
bool FpcProtocol::SetLnaVBias(stLnaBiasParameters biasParameters)
{
	string command = "*BV";
	command += string(",01:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt3 * 100), 4);
	command += string(",02:") + numberToFixedString(static_cast<int>(biasParameters.drainCur3 * 10), 4);
	command += string(",03:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt4 * 100), 4);
	command += string(",04:") + numberToFixedString(static_cast<int>(biasParameters.drainCur4 * 10), 4);
	command += string(",05:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt5a * 100), 4);
	command += string(",06:") + numberToFixedString(static_cast<int>(biasParameters.drainCur5a * 10), 4);
	command += string(",07:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt5b * 100), 4);
	command += string(",08:") + numberToFixedString(static_cast<int>(biasParameters.drainCur5b * 10), 4);
	command += string(",09:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt6 * 100), 4);
	command += string(",10:") + numberToFixedString(static_cast<int>(biasParameters.drainCur6 * 10), 4);
	command += "\r";
	cout << "[UL_BIAS] " << command << endl;

	return SendSimpleCommand(command);
}
bool FpcProtocol::GetLnaHBias(stLnaBiasParameters & biasParameters)
{
	bool retVal = false;

	try
	{
		string command = "*HB\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, float> returnData;

			/* Parse message */
			if (ParseGeneralMultipleFloat(response, returnData))
			{
				biasParameters.gateVolt3 = returnData[0] / 100;
				biasParameters.drainVolt3 = returnData[1] / 100;
				biasParameters.drainCur3 = returnData[2] / 10;
				biasParameters.gateVolt4 = returnData[3] / 100;
				biasParameters.drainVolt4 = returnData[4] / 100;
				biasParameters.drainCur4 = returnData[5] / 10;
				biasParameters.gateVolt5a = returnData[6] / 100;
				biasParameters.drainVolt5a = returnData[7] / 100;
				biasParameters.drainCur5a = returnData[8] / 10;
				biasParameters.gateVolt5b = returnData[9] / 100;
				biasParameters.drainVolt5b = returnData[10] / 100;
				biasParameters.drainCur5b = returnData[11] / 10;
				biasParameters.gateVolt6 = returnData[12] / 100;
				biasParameters.drainVolt6 = returnData[13] / 100;
				biasParameters.drainCur6 = returnData[14] / 10;

				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::GetLnaVBias(stLnaBiasParameters & biasParameters)
{
	bool retVal = false;

	try
	{
		string command = "*VB\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, float> returnData;

			/* Parse message */
			if (ParseGeneralMultipleFloat(response, returnData))
			{
				biasParameters.gateVolt3 = returnData[0] / 100;
				biasParameters.drainVolt3 = returnData[1] / 100;
				biasParameters.drainCur3 = returnData[2] / 10;
				biasParameters.gateVolt4 = returnData[3] / 100;
				biasParameters.drainVolt4 = returnData[4] / 100;
				biasParameters.drainCur4 = returnData[5] / 10;
				biasParameters.gateVolt5a = returnData[6] / 100;
				biasParameters.drainVolt5a = returnData[7] / 100;
				biasParameters.drainCur5a = returnData[8] / 10;
				biasParameters.gateVolt5b = returnData[9] / 100;
				biasParameters.drainVolt5b = returnData[10] / 100;
				biasParameters.drainCur5b = returnData[11] / 10;
				biasParameters.gateVolt6 = returnData[12] / 100;
				biasParameters.drainVolt6 = returnData[13] / 100;
				biasParameters.drainCur6 = returnData[14] / 10;

				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* Firmware Version	  */
/* ================== */
// test response 1: *S,0123
// test response 2: *F
bool FpcProtocol::GetFirmwareVersion(string & version)
{
	bool retVal = false;

	try
	{
		string command = "*RV\r";
		string response;

		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				version = parsedValue;
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Serial Number	  */
/* ================== */
// test response 1: *S,3001
// test response 2: *F
bool FpcProtocol::GetSerialNumber(string & serial)
{
	bool retVal = false;

	try
	{
		string command = "*SN\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				serial = parsedValue;
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Memory Storage	  */
/* ================== */
bool FpcProtocol::EraseRecord(enMemoryRecord record)
{
	if (record == enMemoryRecord::NONE)
	{
		return false;
	}

	string messageId = "*EF,";
	string parameter = numberToHexString(static_cast<short>(record), 2);
	string command = messageId + parameter + "\r";

	return SendSimpleCommand(command);
}

/* This function will send the read record command and write all data to file until the <EOT> character is received. */
bool FpcProtocol::ReadRecordNew(enMemoryRecord record, string fileName)
{
	/* Pseudo:
	 * Open a new file
	 * send read command
	 * while returned bytes does not end with <EOT> do
	 * read chunk of data
	 * write string to file
	 * end [while]
	 * close file
	 */

	bool retVal = false;

	try
	{
		string buffer;
		string recordLocation = numberToHexString(static_cast<short>(record), 2);

		bool success = true;

		ofstream f (fileName.c_str());

		if (!f.is_open())
		{
			perror(("error while opening file " + fileName).c_str());
		} else
		{

			/* Send the read command */
			string messageId = "*RR,";
			string command = messageId + recordLocation + "\r";

			/* First, flush any remaining bytes in serial port input and output buffer */
			serialPort->flushInputOutput();

			/* Write the command string to the serial port */
			serialPort->write(command);

			/* Ensure all bytes are written with tcdrain()*/
			serialPort->flush();

			/* Read the first bytes to see if we have a success response *S,*/
			buffer = serialPort->read(6); //*S,01,<DATA>

			/* Validate the response */
			if (ValidateResponse(buffer))
			{
				/* If we get here, only the payload bytes are the next bytes to be read */
				while(serialPort->waitReadable())
				{
					uint8_t byte;

					if (serialPort->read(&byte, 1) == 1)
					{
						if (byte != 0x04) //EOT
						{
							f.write((char *)&byte,1);
						}
						else
						{
							/* If we get here, file has been written successfully */
							retVal = true;
							break;
						}
					}
				}
			}

			if (f.bad())
			{
				perror(("error while reading file " + fileName).c_str());
			}
		}
		f.close();
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* This function will send the read record command and write all data to file until the <EOT> character is received. */
bool FpcProtocol::ReadRecord(enMemoryRecord record, string fileName)
{
	/* Pseudo:
	 * Open a new file
	 * send read command
	 * while returned bytes does not end with <EOT> do
	 * read chunk of data
	 * write string to file
	 * end [while]
	 * close file
	 */

	bool retVal = false;

	try
	{
		string buffer;
		string recordLocation = numberToHexString(static_cast<short>(record), 2);

		bool success = true;

		ofstream f (fileName.c_str());

		if (!f.is_open())
		{
			perror(("error while opening file " + fileName).c_str());
		}

		/* Send the read command */
		string messageId = "*RR,";
		string command = messageId + recordLocation + "\r";

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Read the line */
		buffer = serialPort->readline(276484, "\x04"); // read until the EOT "\x04" character is received. Max file size is 270KB -- allow 4 extra bytes

		/* Validate the response */
		if (ValidateResponse(buffer))
		{
			/* Get the file payload from the buffer */
			buffer.erase(0,6); // Remove Message id
			buffer.erase(buffer.end()-1, buffer.end()); // Remove EOT character

			/* Write whole buffer to file */
			f << buffer;

			/* If we get here, file has bee written successfully */
			retVal = true;
		}

		if (f.bad())
		{
			perror(("error while reading file " + fileName).c_str());
		}
		f.close();
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* This function will continually send the write record command until the end of the string is received. */
bool FpcProtocol::WriteRecord(enMemoryRecord record, string fileName)
{
	/* Pseudo
	 * open existing file
	 * while not eof
	 * readline
	 * send line to fpga
	 * if success, continue, else break;
	 * end [while]
	 * close file
	 */
	bool retVal = false;

	try
	{
		ifstream fileToWrite (fileName.c_str());

		if (!fileToWrite.is_open() || fileToWrite.fail())
		{
			perror(("error while opening file " + fileName).c_str());
		} else
		{
			serial::Timeout newTout, oldTout;
			oldTout = serialPort->getTimeout();
			serialPort->setTimeout(0,60000,1,60000,1);

			string messageId = "*WR,";
			string parameter = numberToHexString(static_cast<short>(record), 2);
			string command = messageId + parameter + ",";
			cout << command << endl;

			/* First, flush any remaining bytes in serial port input and output buffer */
			serialPort->flushInputOutput();

			/* Write the command string to the serial port */
			serialPort->write(command);

			/* TJS - This could be faster, although more memory will be used. */
			if (fileToWrite)
			{
				std::string contents;
				fileToWrite.seekg(0, std::ios::end);				// go to the end
				contents.resize(fileToWrite.tellg());				// resize the string with the file's length
				fileToWrite.seekg(0, std::ios::beg);				// go back to the beginning
				fileToWrite.read(&contents[0], contents.size());	// read the whole file into the buffer
				fileToWrite.close();								// close file handle
				serialPort->write(contents);
	//			serialPort->flushOutput();
			}

			/*
			while (!fileToWrite.eof())
			{
				uint8_t byte;

				// Read a byte from the file
				if (fileToWrite.read((char *)&byte,1))
				{
					// Only write one byte to the serial device
					serialPort->write(&byte, 1);
				}

				if (fileToWrite.fail())
				{
					//error
					break;
				}
			}
			fileToWrite.close();
			serialPort->flushOutput();
			*/

			/* We are finished with the file */
			uint8_t eot = 0x04;
			serialPort->write(&eot, 1);
			cout << "[DATA]\\eot(0x04)" << endl;
	//		uint8_t cr = 0x0D;
	//		serialPort->write(&cr, 1); // FIXME: This should be removed
	//		serialPort->write("\r"); // FIXME: This should be removed

			/* Ensure all bytes are written with tcdrain()*/
			serialPort->flush();
			serialPort->setTimeout(oldTout);

			/* Read the line */
			string line = serialPort->readline();
			cout << line << endl;
			retVal = ValidateResponse(line);
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}
	return retVal;
}

/* ================== */
/* Error Flags		  */
/* ================== */
// test response 1: *S,ABCD
// test response 2: *S,0001
// test response 3: *F
bool FpcProtocol::ModifyErrorRegister(unErrorFlags errorFlags)
{
	bool retVal = false;

	try
	{
		string messageId = "*EC,";
		string parameters = numberToHexString(errorFlags.Word, 4);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the setRegister is equal to the actual register */
				if (stoul(parsedValue, nullptr, 16) == errorFlags.Word)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::ModifyErrorBit(unsigned int errorBit, unsigned int value)
{
	bool retVal = false;

	try
	{
		string messageId = "*EB,";
		string parameters = numberToHexString(errorBit, 2) + ':' + numberToHexString(value, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the command is equal to the echoed response */
				if (parameters == parsedValue)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::GetErrorRegister(unErrorFlags & errorFlags)
{
	bool retVal = false;

	try
	{
		string command = "*ER\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				errorFlags.Word = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,00:01
// test response 1: *S,05:00
// test response 2: *F
bool FpcProtocol::GetErrorBit(unsigned int errorBit, unsigned int & value)
{
	bool retVal = false;

	try
	{
		string messageId = "*BE,";
		string parameters = numberToHexString(errorBit, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedParameter;
			string parsedValue;

			/* Parse message */
			if (ParseGeneralDouble(response, parsedParameter, parsedValue))
			{
				/* We have strings, convert to input types and perform checks and variable assignments */
				if (numberToHexString(errorBit, 2) == parsedParameter)
				{
					value = stoul(parsedValue, nullptr, 16);
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* Real Time Clock	  */
/* ================== */
bool FpcProtocol::SetSystemTime(unsigned int systemTime)
{
	string messageId = "*TM,";
	string parameters = numberToHexString(systemTime, 8);
	string command = messageId + parameters + "\r";

	return SendSimpleCommand(command);
}

bool FpcProtocol::GetSystemTime(unsigned int & systemTime)
{
	bool retVal = false;

	try
	{
		string command = "*RM\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				systemTime = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ====================	*/
/* Elapsed Time Counter	*/
/* ==================== */
bool FpcProtocol::GetElapsedTime(enElapsedTimeCounter counterChannel, int & elapsedTime)
{
	bool retVal = false;

	try
	{
		string messageId = "*GT,";
		string parameters = numberToHexString(static_cast<short>(counterChannel), 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				elapsedTime = static_cast<int>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ResetElapsedTime(enElapsedTimeCounter counterChannel)
{
	string messageId = "*CT,";
	string parameters = numberToHexString(static_cast<short>(counterChannel), 2);
	string command = messageId + parameters + "\r";

	return SendSimpleCommand(command);
}

/* ====================== */
/* Active Band			  */
/* ====================== */
bool FpcProtocol::GetActiveBand(unsigned short & band)
{
	bool retVal = false;

	try
	{
		string command = "*BR\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				band = static_cast<unsigned short>(stoi(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::SetActiveBand(unsigned short band)
{
	string messageId = "*AB,";
	string parameters = numberToHexString(band, 2);
	string command = messageId + parameters + "\r";

	return SendSimpleCommand(command);
}

/* ====================== */
/* Cool down counter	  */
/* ====================== */
bool FpcProtocol::GetCoolDownCounter(unsigned short & count)
{
	bool retVal = false;

	try
	{
		string command = "*CG\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				count = static_cast<unsigned short>(stoi(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::IncrementCoolDownCounter()
{
	return SendSimpleCommand("*CI\r");
}
bool FpcProtocol::ClearCoolDownCounter()
{
	return SendSimpleCommand("*CW\r");
}

/* ====================== */
/* State and Mode Control */
/* ====================== */
bool FpcProtocol::RemainMaintenance()
{
	return SendSimpleCommand("*RU\r");
}
bool FpcProtocol::GoApplication()
{
	return SendSimpleCommand("*JA\r");
}
bool FpcProtocol::GoUpdate()
{
	return SendSimpleCommand("*FP\r");
}
bool FpcProtocol::GoMaintenance()
{
	return SendSimpleCommand("*JF\r");
}
bool FpcProtocol::GetCurrentMode(enFpcMode & mode)
{
	bool retVal = false;

	try
	{
		string command = "*QI\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				mode = static_cast<enFpcMode>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* This functions ends a raw serial command */
bool FpcProtocol::SendRawCommand(string command, string & response)
{
	bool retVal = false;

	try
	{
		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		retVal = true; // If we get here without exceptions, we accept the command was successful and return the received string to the caller.
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralSingle(string response, string & parsedValue)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			if ((tokens[1] != "") && (!tokens[1].empty()))
			{
				parsedValue = trim(tokens[1]);
				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralDouble(string response, string & parsedParameter, string & parsedValue)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			vector<string> parameters = split(tokens[1], ':');

			if (parameters.size() == 2)
			{
				parsedParameter = trim(parameters[0]);
				parsedValue = trim(parameters[1]);

				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralMultipleInt(string response, map<int, int> & returnData)
{
	bool retVal = true;

	try
	{
		vector<string> tokens = split(response, ',');

		//if (tokens.size() == 5) // FIXME: Why is this so if the function is 'General' ?
		{
			for (size_t i = 1; i < tokens.size(); i++)
			{
				vector<string> parameters = split(tokens[i], ':');

				if (parameters.size() == 2)
				{
					int index = stoi(trim(parameters[0]));
					int value = stoi(trim(parameters[1]));
					returnData[index] = value;
					//retVal &= true;
				}
				else
				{
					retVal &= false;
				}
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		retVal = false;
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralMultipleDouble(string response, map<int, double> & returnData)
{
	bool retVal = true;

	try
	{
		vector<string> tokens = split(response, ',');

		//if (tokens.size() == 5) // FIXME: Why is this so if the function is 'General' ?
		{
			for (size_t i = 1; i < tokens.size(); i++)
			{
				vector<string> parameters = split(tokens[i], ':');

				if (parameters.size() == 2)
				{
					int index = stod(trim(parameters[0]));
					int value = stod(trim(parameters[1]));
					returnData[index-1] = value;
				}
				else
				{
					retVal &= false;
				}
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		retVal = false;
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::ParseGeneralMultipleFloat(string response, map<int, float> & returnData)
{
	bool retVal = true;

	try
	{
		vector<string> tokens = split(response, ',');

		//if (tokens.size() == 5) // FIXME: Why is this so if the function is 'General' ?
		{
			for (size_t i = 1; i < tokens.size(); i++)
			{
				vector<string> parameters = split(tokens[i], ':');

				if (parameters.size() == 2)
				{
					int index = stof(trim(parameters[0]));
					int value = stof(trim(parameters[1]));
					returnData[index-1] = value;
				}
				else
				{
					retVal &= false;
				}
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		retVal = false;
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

string FpcProtocol::GetLineData(string line)
{
	return line.substr(11);
}

bool FpcProtocol::LineDataDone(string line)
{
	if (line.find("\0\0") != string::npos)
	{
		return true;
	}
	return false;
}



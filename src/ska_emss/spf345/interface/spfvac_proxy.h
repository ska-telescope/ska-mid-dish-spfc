/*
 * spfvac_proxy.h
 *
 *  Created on: 28 Mar 2017
 *      Author: theuns
 */

#ifndef SYSTEM_SPFVAC_PROXY_H_
#define SYSTEM_SPFVAC_PROXY_H_

#include <tango.h>
using namespace Tango;

#include <string>
using namespace std;

namespace SpfVac
{
	/* The Device Status is a global enum and should maybe be moved somewhere "global" */
	enum class enDeviceStatus : short
	{
	   UNKNOWN = 0,
	   NORMAL = 1,
	   DEGRADED = 2,
	   FAILED = 3,
	};

enum enVacPumpStatus : short
{
	OFFLINE = 0,
	ONLINE = 1, // Not ready
	READY = 2,	// Not running
	RUNNING = 3
};

enum class enDeviceMode : short
{
	OFF = 0,
	STARTUP,
	STANDBY_LP,
	OPERATE,
	MAINTENANCE,
	ERROR
};

enum class _enSetRequest : short
{
	USER = 0,
	B1,
	B2,
	B345,
	Bs,
	Bku
};

typedef struct _stVacuumPumpDataFrame
{
	enDeviceMode operatingState;
	enDeviceStatus healthState;
	enVacPumpStatus hwStatus;
	std::string controllerState;

	bool pumpState;
	bool purgeValveState;
	bool expectedOnline;
	std::string serialNumber;
	std::string swVersion;

	long currentOperationalTime;
	long totalOperationalTime;

} stVacuumPumpDataFrame;


class SpfVacProxy
{
public:
	SpfVacProxy(string name);
	~SpfVacProxy();

	bool Init();

	bool IsOnline();
	bool IsRunning();
	bool IsAvailable();

	bool Request(_enSetRequest reqDev);
	bool Cancel(_enSetRequest reqDev);
	bool RequestPurge();
	bool CancelPurge();

	bool SetMaintenance();
	bool SetStandby();
	bool Reset();

	stVacuumPumpDataFrame GetDataFrame();

	bool Test();

	bool SetState(enDeviceMode vacState);

private:
	DeviceProxy *spfVacDevice;
	string deviceName;
};

} // Namespace

#endif /* SYSTEM_SPFVAC_PROXY_H_ */

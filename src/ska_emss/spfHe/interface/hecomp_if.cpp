/*
 * hecomp_if.cpp
 *
 *  Created on: 30 Mar 2017
 *      Author: theuns
 */
#define _GLIBCXX_USE_NANOSLEEP 1

#define SERIAL_COMMS_TIMEOUT 500 // milliseconds

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <map>
#include <chrono>
#include <thread>

#include "hecomp_if.h"
#include "system.h"
#include "utilities.h"

#ifdef SIMULATOR
extern string pPortName;
#endif

using namespace util;
using namespace std;
using namespace serial;

HeliumCompressor::HeliumCompressor()
{
#ifdef SIMULATOR
	portName = pPortName;
#else
	portName = "/dev/ttyS5";
#endif

	/* Initialise all the interfaces */
	serialPort = new Serial();

	ioInRunning = new Digital(PIN_HCOMPR_RUNNING, N_PIN_HCOMPR_RUNNING);
#ifdef HCFCU
	ioOutWake = new Digital(PIN_HCOMPR_WAKE, N_PIN_HCOMPR_WAKE);
#else
	ioOutnReset = new Digital(PIN_HCOMPR_nRESET, N_PIN_HCOMPR_nRESET);
#endif

	ioOutLedError = new Digital(LED_HCOMPR_ERROR, N_LED_HCOMPR_ERROR);
	ioOutLedRunning = new Digital(LED_HCOMPR_RUNNING, N_LED_HCOMPR_RUNNING);
}

HeliumCompressor::HeliumCompressor(const string PortName)
{
	portName = PortName;

	/* Initialise all the interfaces */
	serialPort = new Serial();

	ioInRunning = new Digital(PIN_HCOMPR_RUNNING, N_PIN_HCOMPR_RUNNING);
#ifdef HCFCU
	ioOutWake = new Digital(PIN_HCOMPR_WAKE, N_PIN_HCOMPR_WAKE);
#else
	ioOutnReset = new Digital(PIN_HCOMPR_nRESET, N_PIN_HCOMPR_nRESET);
#endif

	ioOutLedError = new Digital(LED_HCOMPR_ERROR, N_LED_HCOMPR_ERROR);
	ioOutLedRunning = new Digital(LED_HCOMPR_RUNNING, N_LED_HCOMPR_RUNNING);
}

HeliumCompressor::~HeliumCompressor()
{
	cout << "HECOMP HW DESTRUCT" << endl;

	if (serialPort->isOpen())
	{
		serialPort->close();
	}

	delete serialPort;

	delete ioInRunning;
#ifdef HCFCU
	delete ioOutWake;
#else
	delete ioOutnReset;
#endif

	delete ioOutLedError;
	delete ioOutLedRunning;
}

// This function opens the digital channels and the serial port */
bool HeliumCompressor::Init()
{
	bool retVal = true;

#ifndef SIMULATOR
	char uBootVer[1024];
	FILE *fp;
	int fxTxFixed = 0;

	/* This code using a 'system' command 'strings' is not compatible when compiled into the latest c++14 code and does not use the correct ld-linux.so.3
//	fp = popen("strings /dev/mtd1 | grep U-Boot", "r");
//	fgets(uBootVer, sizeof(uBootVer), fp);
//	pclose(fp);

	/* That means that this will always be false, so the 'else' statement would always execute to the 'Fixed' version */
	if (strncmp("U-Boot 2013.01 (Sep 02 2015 - 12:46:02)", uBootVer, 38) == 0) // if original MK_factory_image
	{
		cout << "Original MK U-Boot version: " << uBootVer << endl;
	}
	else // the fibre transmitters are fixed on newer versions of U-Boot
	{
		fxTxFixed = 1;
		cout << "Fixed Fx U-Boot version: " << uBootVer << endl;
	}

	/* ioInRunning is active low */
	if (ioInRunning->SetDir(1) < 0)
	{
		cout << "Direction NOT set [ioInRunning]" << endl;
	}
#ifdef HCFCU
	if (ioInRunning->SetActiveLow(false) < 0)
#else
	if (ioInRunning->SetActiveLow(true) < 0)
#endif
	{
		cout << "Active Low NOT set [ioInRunning]" << endl;
	}

	/* Do not switch off compressor if it is running while initializing */
	/* ioOutnReset is inverted on previous faulty design */
	if (fxTxFixed ) // then it is reverted to active high
	{
#ifdef HCFCU
		if (ioOutWake->SetDir(0, 0) < 0) // set as output(0) with value '0'
		{
			cout << "Direction NOT set [ioOutWake]" << endl;
		}
		if (ioOutWake->SetActiveLow(false) < 0)
		{
			cout << "Active High NOT set [ioOutWake]" << endl;
		}
#else
		if (ioOutnReset->SetDir(0, 0) < 0)
		{
			cout << "Direction NOT set [ioOutReset]" << endl;
		}
		if (ioOutnReset->SetActiveLow(false) < 0)
		{
			cout << "Active High NOT set [ioOutnReset]" << endl;
		}
#endif
	} else
	{
#ifdef HCFCU
		if (ioOutWake->SetDir(0, 1) < 0)
		{
			cout << "Direction NOT set [ioOutWake]" << endl;
		}
		if (ioOutWake->SetActiveLow(true) < 0)
		{
			cout << "Active Low NOT set [ioOutWake]" << endl;
		}
#else
		if (ioOutnReset->SetDir(0, 1) < 0)
		{
			cout << "Direction NOT set [ioOutReset]" << endl;
		}
		if (ioOutnReset->SetActiveLow(true) < 0)
		{
			cout << "Active Low NOT set [ioOutnReset]" << endl;
		}
#endif
	}

	/* Status LEDS */
	if (ioOutLedError->SetDir(0) < 0)
	{
		cout << "Direction NOT set [ioOutLedError]" << endl;
	}
	if (ioOutLedRunning->SetDir(0) < 0)
	{
		cout << "Direction NOT set [ioOutRunning]" << endl;
	}
#endif

	try
	{
#ifdef HCFCU
		serialPort = new Serial(portName, 9600, Timeout::simpleTimeout(SERIAL_COMMS_TIMEOUT)); // Will open the serial port in this ctor
#else
		serialPort = new Serial(portName, 19200, Timeout::simpleTimeout(SERIAL_COMMS_TIMEOUT)); // Will open the serial port in this ctor
#endif
	}

	catch(PortNotOpenedException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(IOException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(SerialException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(invalid_argument &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
/* This function reads the running input pin from the Helium Compressor */
bool HeliumCompressor::IsRunning(bool & running)
{
	bool retVal = false;
	int line1 = -1;

	retVal =  !ioInRunning->GetVal(line1);

	/* Debug */
//	cout << "retVal: [" << retVal << "]" << endl;
//	cout << "running: [" << line1 << "]" << endl;

	/* Update the status LEDs if we could determine the status */
	if (retVal)
	{
		running = line1;
	}

	return retVal;
}

bool HeliumCompressor::SetLedStatus(enHeCompLeds led)
{
	bool retVal = false;
#ifndef SIMULATOR
	switch (led)
	{
		case enHeCompLeds::RUNNING:
			retVal  = !ioOutLedRunning->SetVal(1);
			break;

		case enHeCompLeds::ERROR:
			retVal  = !ioOutLedError->SetVal(1);
			break;

		default:
			ioOutLedError->SetVal(0);
			ioOutLedRunning->SetVal(0);
			retVal = false; /* This should never happen, but if it does - return false */
			break;
	}
#endif
	return retVal;
}

bool HeliumCompressor::ClearLedStatus(enHeCompLeds led)
{
	bool retVal = false;
#ifndef SIMULATOR
	switch (led)
	{
		case enHeCompLeds::RUNNING:
			retVal  = !ioOutLedRunning->SetVal(0);
			break;

		case enHeCompLeds::ERROR:
			retVal  = !ioOutLedError->SetVal(0);
			break;

		default:
			ioOutLedError->SetVal(0);
			ioOutLedRunning->SetVal(0);
			retVal = false; /* This should never happen, but if it does - return false */
			break;
	}
#endif
	return retVal;
}


// TODO: Check for malformed protocol responses
bool HeliumCompressor::ValidateResponse(string response)
{
	bool retVal = false;

	if ((response != "") && (!response.empty()))
	{
#ifdef HCFCU
		if ((response[0] == 'S') || (response[0] == 's'))
#else
		if ((response[0] == '*') && ((response[1] == 'S') || (response[1] == 's')))
#endif
		{
			retVal = true;
		}
	}

	return retVal;
}

bool HeliumCompressor::SendSimpleCommand(string command)
{
	bool retVal = false;

#ifdef HCFCU
	ioOutWake->SetVal(0);
	usleep(100 * 1000); // 100ms
	ioOutWake->SetVal(1);
#endif
	try
	{
		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Validate the response */
		retVal = ValidateResponse(serialPort->readline());
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << "\n Command: " << command << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Sensor Monitoring  */
/* ================== */
// Sample all sensors
// test response 1: *S,4.99,513,35.6,21.5,8.5,29.3,22.9
// test response 2: *S,4.99,-513,35.6,-21.5,8.5,-29.3,22.9
// test response 3: *F
bool HeliumCompressor::SampleAll(stHeliumCompressorSensors & sensors)
{
	bool retVal = false;

	try
	{
#ifdef HCFCU
		string command = "*HP\r"; // Helium Pressure (+ error bit values)
		string response2;

		/* First, wake up the device and wait 100ms */
		ioOutWake->SetVal(0);
		usleep(100 * 1000); // 100ms
		ioOutWake->SetVal(1);
		usleep(100 * 1000); // 100ms

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response2 = serialPort->readline();
//		std::cout << "Helium Pressure response: [" << response2 << "]" << std::endl;

		if (ValidateResponse(response2))
		{
			/* Parse message */
			retVal = ParseHeliumPressure(response2, sensors);
		}
		else
		{
			std::cout << "Helium Pressure response invalid: [" << response2 << "]" << std::endl;
			retVal = false;
		}
#else
		string command = "*SA\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			/* Parse message */
			retVal = ParseSampleAll(response, sensors);
		}
		else
		{
			std::cout << "Sample all response invalid: [" << response << "]" << std::endl;
			retVal = false;
		}
#endif
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ParseHeliumHealth(string response, stHeliumCompressorSensors & sensors)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 4)
		{
			sensors.controllerVoltage 			= stod(tokens[1]) / 100.0;
			sensors.controllerCurrent 			= stod(tokens[2]) / 10.0;
			sensors.controllerTemperature 		= stod(tokens[3]) / 10.0;

			/* Debug - Check if all values are 0 */
			if ((sensors.controllerVoltage 			== 0.0) &&
				(sensors.controllerCurrent 			== 0.0) &&
				(sensors.controllerTemperature 		== 0.0))
			{
				std::cout << "ParseHeliumHealth: All zeros!" << endl;
			}

			retVal = true;
		}
		else
		{
			std::cout << "ParseHeliumHealth: more than 4 tokens" << endl;
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ParseHeliumPressure(string response, stHeliumCompressorSensors & sensors)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 4)
		{
			sensors.compressorSupplyPressure 	= (double)strtol(tokens[1].c_str(), NULL, 16) / 1000.0;
			sensors.compressorReturnPressure 	= (double)strtol(tokens[2].c_str(), NULL, 16) / 1000.0;
//			sensors.compressorMotorTemperature 	= stod(tokens[3]) / 10.0;
//			sensors.compressorSupplyTemperature = stod(tokens[4]) / 10.0;

			/* Debug - Check if all values are 0 */
			if ((sensors.compressorSupplyPressure 	== 0.0) &&
				(sensors.compressorReturnPressure 	== 0.0)) // &&
//				(sensors.compressorMotorTemperature == 0.0) &&
//				(sensors.compressorSupplyTemperature== 0.0))
			{
				std::cout << "ParseSampleAll: All zeros!" << endl;
			}

			retVal = true;
		}
		else
		{
			std::cout << "ParseSampleAll: more than 4 tokens" << endl;
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ParseSampleAll(string response, stHeliumCompressorSensors & sensors)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 8)
		{
			sensors.controllerVoltage 			= stod(tokens[1]) / 100.0;
			sensors.controllerCurrent 			= stod(tokens[2]) / 10.0;
			sensors.controllerTemperature 		= stod(tokens[3]) / 10.0;

			sensors.compressorSupplyPressure 	= stod(tokens[4]) / 10.0;
			sensors.compressorReturnPressure 	= stod(tokens[5]) / 10.0;
			sensors.compressorMotorTemperature 	= stod(tokens[6]) / 10.0;
			sensors.compressorSupplyTemperature = stod(tokens[7]) / 10.0;

			/* Debug - Check if all values are 0 */
			if ((sensors.controllerVoltage 			== 0.0) &&
				(sensors.controllerCurrent 			== 0.0) &&
				(sensors.controllerTemperature 		== 0.0) &&
				(sensors.compressorSupplyPressure 	== 0.0) &&
				(sensors.compressorReturnPressure 	== 0.0) &&
				(sensors.compressorMotorTemperature == 0.0) &&
				(sensors.compressorSupplyTemperature== 0.0))
			{
				std::cout << "ParseSampleAll: All zeros!" << endl;
			}

			retVal = true;
		}
		else
		{
			std::cout << "ParseSampleAll: more than 8 tokens" << endl;
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// Sample single sensor
// test response 1: *S,0A:23.5
// test response 2: *F
bool HeliumCompressor::SampleSingleSensor(unsigned int channelNum, double& value)
{
	bool retVal = false;

	try
	{
		string messageId = "*SS,";
		string parameters = numberToHexString(channelNum, 2);
		string command = messageId + parameters + "\r";
		string response;


		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			unsigned int parsedChannel;
			double parsedValue;

			/* Parse message */
			retVal = ParseSampleSingle(response, parsedChannel, parsedValue);

			if (parsedChannel == channelNum)
			{
				/* Only assign the value if we have received the response for the specified channel */
				value = parsedValue;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ParseSampleSingle(string response, unsigned int & channel, double & value)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			vector<string> parameters = split(tokens[1], ':');

			if (parameters.size() == 2)
			{
				channel = stoul(parameters[0], nullptr, 16);
				value = stod(parameters[1]);

				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


bool HeliumCompressor::EnableSensorAveraging()
{
	return SendSimpleCommand("*AE\r");
}

bool HeliumCompressor::DisableSensorAveraging()
{
	return SendSimpleCommand("*AD\r");
}

void HeliumCompressor::Print(stHeliumCompressorSensors data)
{
	cout << "data.controllerVoltage: " << data.controllerVoltage << endl;
	cout << "data.controllerCurrent: " << data.controllerCurrent << endl;
	cout << "data.controllerTemperature: " << data.controllerTemperature << endl;

	cout << "data.compressorSupplyPressure: " << data.compressorSupplyPressure << endl;
	cout << "data.compressorReturnPressure: " << data.compressorReturnPressure << endl;
	cout << "data.compressorMotorTemperature: " << data.compressorMotorTemperature << endl;
	cout << "data.compressorSupplyTemperature: " << data.compressorSupplyTemperature << endl;

}


/* ================== */
/* Compressor Control */
/* ================== */
bool HeliumCompressor::Start()
{
	return SendSimpleCommand("*EH\r");
}
bool HeliumCompressor::Stop()
{
	return SendSimpleCommand("*DH\r");
}
bool HeliumCompressor::Reset(bool hard)
{
	bool retVal = false;

	if (hard)
	{
#ifndef SIMULATOR
	#ifdef HCFCU // do nothing if Hard reset is called in HCFCU

	#else // do Hard reset and wait for 100ms
		retVal  = !ioOutnReset->SetVal(1);

		/* Wait 100ms */
		this_thread::sleep_for(chrono::milliseconds(100));

		retVal &= !ioOutnReset->SetVal(0);
	#endif
#endif
	}
	else
	{
		retVal = SendSimpleCommand("*RH\r");
	}

	return retVal;
}

/* ================== */
/* Firmware Version	  */
/* ================== */
// test response 1: *S,0123
// test response 2: *F
bool HeliumCompressor::GetFirmwareVersion(string & version)
{
	bool retVal = false;

	try
	{
		string command = "*RV\r";
		string response;

#ifdef HCFCU
		/* First, wake up the device and wait 100ms */
		ioOutWake->SetVal(0);
		usleep(100 * 1000); // 100ms
		ioOutWake->SetVal(1);
#endif
		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();
		cout << "FirmwareVersion response: " << response << endl;

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				version = parsedValue;
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Serial Number	  */
/* ================== */
// test response 1: *S,3001
// test response 2: *F
bool HeliumCompressor::GetSerialNumber(string & serial)
{
	bool retVal = false;

	try
	{
		string command = "*SN\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				serial = parsedValue;
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}


	return retVal;
}


/* ================== */
/* Error Flags		  */
/* ================== */
// test response 1: *S,ABCD
// test response 2: *S,0001
// test response 3: *F
bool HeliumCompressor::ModifyErrorRegister(unErrorFlags errorFlags)
{
	bool retVal = false;

	try
	{
		string messageId = "*EC,";
		string parameters = numberToHexString(errorFlags.Word, 4);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the setRegister is equal to the actual register */
				if (stoul(parsedValue, nullptr, 16) == errorFlags.Word)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool HeliumCompressor::ModifyErrorBit(unsigned int errorBit, unsigned int value)
{
	bool retVal = false;

	try
	{
		string messageId = "*EB,";
		string parameters = numberToHexString(errorBit, 2) + ':' + numberToHexString(value, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the command is equal to the echoed response */
				if (parameters == parsedValue)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool HeliumCompressor::GetErrorRegister(unErrorFlags & errorFlags)
{
	bool retVal = false;

	try
	{
#ifdef HCFCU
		string command = "*HP\r"; // double command for Helium Health & Helium Pressure (+ error bit values)
		string response;

		/* First, wake up the device and wait 100ms */
		ioOutWake->SetVal(0);
		usleep(100 * 1000); // 100ms
		ioOutWake->SetVal(1);
		usleep(100 * 1000); // 100ms

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseSelectedParam(response, parsedValue, 3))
			{
				errorFlags.Word = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
#else
		string command = "*ER\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				errorFlags.Word = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
#endif
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,00:01
// test response 1: *S,05:00
// test response 2: *F
bool HeliumCompressor::GetErrorBit(unsigned int errorBit, unsigned int & value)
{
	bool retVal = false;

	try
	{
		string messageId = "*BE,";
		string parameters = numberToHexString(errorBit, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedParameter;
			string parsedValue;

			/* Parse message */
			if (ParseGeneralDouble(response, parsedParameter, parsedValue))
			{
				/* We have strings, convert to input types and perform checks and variable assignments */
				if (numberToHexString(errorBit, 2) == parsedParameter)
				{
					value = stoul(parsedValue, nullptr, 16);
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* Real Time Clock	  */
/* ================== */
bool HeliumCompressor::SetSystemTime(unsigned int systemTime)
{
	string messageId = "*TM,";
	string parameters = numberToHexString(systemTime, 8);
	string command = messageId + parameters + "\r";

	return SendSimpleCommand(command);
}

bool HeliumCompressor::GetSystemTime(unsigned int & systemTime)
{
	bool retVal = false;

	try
	{
		string command = "*RM\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				systemTime = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ====================	*/
/* Elapsed Time Counter	*/
/* ==================== */
bool HeliumCompressor::GetElapsedTime(enElapsedTimeCounter counterChannel, unsigned int & elapsedTime)
{
	bool retVal = false;

	try
	{
#ifdef HCFCU
		string command = "*HT\r"; // double command for Helium Health & Helium Pressure (+ error bit values)
		string response;

		/* First, wake up the device and wait 100ms */
		ioOutWake->SetVal(0);
		usleep(100 * 1000); // 100ms
		ioOutWake->SetVal(1);
		usleep(100 * 1000); // 100ms

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				elapsedTime = static_cast<int>(stoul(parsedValue, nullptr, 16) * 3600);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
#else
		string messageId = "*GT,";
		string parameters = numberToHexString(static_cast<short>(counterChannel), 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				elapsedTime = static_cast<unsigned int>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
#endif
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ResetElapsedTime(enElapsedTimeCounter counterChannel)
{
	string messageId = "*CT,";
	string parameters = numberToHexString(static_cast<short>(counterChannel), 2);
	string command = messageId + parameters + "\r";

	return SendSimpleCommand(command);
}

/* ====================== */
/* State and Mode Control */
/* ====================== */
bool HeliumCompressor::RemainMaintenance()
{
	return SendSimpleCommand("*RU\r");
}
bool HeliumCompressor::GoApplication()
{
	return SendSimpleCommand("*JA\r");
}
bool HeliumCompressor::GoUpdate()
{
	return SendSimpleCommand("*FP\r");
}
bool HeliumCompressor::GoMaintenance()
{
	return SendSimpleCommand("*JP\r");
}
bool HeliumCompressor::GetCurrentMode(enHeCompMode & mode)
{
	bool retVal = false;

	try
	{
		string command = "*QI\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				mode = static_cast<enHeCompMode>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* This functions ends a raw serial command */
bool HeliumCompressor::SendRawCommand(string command, string & response)
{
	bool retVal = false;

	try
	{
		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		retVal = true; // If we get here without exceptions, we accept the command was successful and return the received string to the caller.
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ParseSelectedParam(string response, string & parsedValue, unsigned int sel)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() > sel)
		{
			if ((tokens[sel] != "") && (!tokens[sel].empty()))
			{
				parsedValue = trim(tokens[sel]);
				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ParseGeneralSingle(string response, string & parsedValue)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			if ((tokens[1] != "") && (!tokens[1].empty()))
			{
				parsedValue = trim(tokens[1]);
				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool HeliumCompressor::ParseGeneralDouble(string response, string & parsedParameter, string & parsedValue)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			vector<string> parameters = split(tokens[1], ':');

			if (parameters.size() == 2)
			{
				parsedParameter = trim(parameters[0]);
				parsedValue = trim(parameters[1]);

				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}




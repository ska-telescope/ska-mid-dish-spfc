/*
 * hecomp_hw.h
 *
 *  Created on: 30 Mar 2017
 *      Author: theuns
 */

#ifndef HECOMP_HW_H_
#define HECOMP_HW_H_

#include <string.h>

#include "Digital.h"
#include "SerialPort.h"
#include "system.h"

using namespace std;
using namespace serial;

#define HIST_BUFFER_SIZE 60 // 60 seconds of historical data

struct stVariables
{
	bool expected_online;
	unsigned int he_req_user;
	unsigned int he_req_b1;
	unsigned int he_req_b2;
	unsigned int he_req_b345;
	unsigned int he_req_bS;
	unsigned int he_req_bKu;
};

struct stConstants
{
	double heliumSupplyOn;
	double heliumSupplyMax;
	double heliumSupplyDiffMin;
	double heliumSupplyDiffMax;
	double heliumReturnLimit;
	double heliumChargeLow;
	double heliumSupplyTempHigh;
	double heliumMotorTempLow;
};

struct stTimeouts
{
	unsigned int Timeout_OffWait;
	unsigned int Timeout_ReadyWait;
};

struct stSettings
{
	stVariables variables;
	stConstants constants;
	stTimeouts  timeouts;
};

enum class enDeviceStatus : short
{
   UNKNOWN = 0,
   NORMAL = 1,
   DEGRADED = 2,
   FAILED = 3,
};

enum class enDeviceMode : short
{
    OFF = 0,
    STARTUP,
    STANDBY_LP,
    OPERATE,
    MAINTENANCE,
	ERROR,
};

enum class enHeCompLeds : short
{
	RUNNING = 0,
	ERROR = 1
};

enum class enHeCompStatus : short
{
	OFFLINE = 0,
	RUNNING = 1,
	NOT_RUNNING = 2,
	ERROR = 3
};

typedef struct _stHeliumCompressorSensors
{
	double 	controllerVoltage;
	double 	controllerCurrent;
	double 	controllerTemperature;
	double 	compressorSupplyPressure;
	double 	compressorReturnPressure;
	double 	compressorMotorTemperature;
	double 	compressorSupplyTemperature;

} stHeliumCompressorSensors;

enum class enHeCompMode : short
{
	UNKNOWN = 0,
	APPLICATION = 1,
	MAINTENANCE = 2
};

typedef union _unErrorFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	Pressure	: 1;
		unsigned	Temperature	: 1;
		unsigned	Overload	: 1;
		unsigned	Surge		: 1;
		unsigned	SPARE		: 12;	// <- 16-bits
	}bits;
}unErrorFlags;

enum class enElapsedTimeCounter : short
{
	NONE = 0,
	OPERATIONAL = 1
};

enum class _enSetRequest : short
{
	USER = 0,
	B1,
	B2,
	B345,
	BS,
	BKu
};
typedef _enSetRequest _enSetRequest;

typedef union _unValidityFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	sensors				: 1;
		unsigned	errorRegister		: 1;
		unsigned 	rtc					: 1;
		unsigned	eltOperational		: 1;
		unsigned	SPARE				: 12;	// <- 16-bits
	}bits;
}unValidityFlags;

typedef struct _stHeliumCompressorDataFrame
{
	bool IsDataValid;
	bool expectedOnline;
	double timestamp;

	bool pressureOk;

	enHeCompStatus compStatus;

	stHeliumCompressorSensors sensors;
	unErrorFlags errorFlags;

	unsigned int elapsedOperational;

	enDeviceStatus deviceStatus;
	enHeCompMode compMode;
	enDeviceMode currentMode;

	unValidityFlags validities;

} stHeliumCompressorDataFrame;

class HeliumCompressor
{
public:
	HeliumCompressor();
	HeliumCompressor(const string PortName);
	~HeliumCompressor();

	bool Init();

	/* Hard lines */
	bool IsRunning(bool & running);
	bool SetLedStatus(enHeCompLeds status);
	bool ClearLedStatus(enHeCompLeds status);

	/* =============== */
	/* Serial commands */
	/* =============== */

	/* Sensor Monitoring */
	bool SampleAll(stHeliumCompressorSensors & sensors);
	bool SampleSingleSensor(unsigned int channelNumber, double& value);
	bool EnableSensorAveraging();
	bool DisableSensorAveraging();

	/* Control */
	bool Start();
	bool Stop();
	bool Reset(bool hard = false);

	/* Firmware */
	bool GetFirmwareVersion(string & version);
	/* Firmware */
	bool GetSerialNumber(string & serial);

	/* Error Flags */
	bool ModifyErrorRegister(unErrorFlags errorFlags);
	bool ModifyErrorBit(unsigned int errorBit, unsigned int value);
	bool GetErrorRegister(unErrorFlags & errorFlags);
	bool GetErrorBit(unsigned int errorBit, unsigned int & value);

	/* Real-time clock */
	bool SetSystemTime(unsigned int time);
	bool GetSystemTime(unsigned int & time);

	/* Elapsed time counter */
	bool GetElapsedTime(enElapsedTimeCounter counterChannel, unsigned int & time);
	bool ResetElapsedTime(enElapsedTimeCounter counterChannel);

	/* State and Mode Control */
	bool RemainMaintenance();
	bool GoApplication();
	bool GoUpdate();
	bool GoMaintenance();
	bool GetCurrentMode(enHeCompMode & mode);

	bool SendRawCommand(string command, string & response);

	void Print(stHeliumCompressorSensors data);

private:
	stHeliumCompressorSensors sensors;
	string portName;

	Serial* serialPort;

	Digital* ioInRunning;
#ifdef HCFCU
	Digital* ioOutWake;
#else
	Digital* ioOutnReset;
#endif

	Digital* ioOutLedError;
	Digital* ioOutLedRunning;

	bool SendSimpleCommand(string command);
	bool ValidateResponse(string response);
	bool ParseHeliumHealth(string response, stHeliumCompressorSensors & sensors);
	bool ParseHeliumPressure(string response, stHeliumCompressorSensors & sensors);
	bool ParseSampleAll(string response, stHeliumCompressorSensors & sensors);
	bool ParseSampleSingle(string response, unsigned int & channelNumber, double & value);
	bool ParseSelectedParam(string response, string & parsedValue, unsigned int sel);
	bool ParseGeneralSingle(string response, string & parsedValue);
	bool ParseGeneralDouble(string response, string & parsedParameter, string & parsedValue);
};

#endif /* VACPUMP_HW_H_ */

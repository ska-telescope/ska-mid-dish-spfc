/*
 * SensorLogger.cpp
 *
 *  Created on: 26 Jun 2017
 *      Author: theuns
 */

#include "SensorLogger.h"
#include "DataLogger.h"
#include "utilities.h"
#include "system.h"
#include "IniParser.h"
#include <iostream>
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string
#include <algorithm> // transform

SensorLogger::SensorLogger(std::string deviceLocation, std::string deviceId, std::string deviceSerial)
	:_deviceId(deviceId), _deviceSerial(deviceSerial)
{
	IniParser spfcConfig(SPFC_CONFIG); /* To get SPFC serial number */
    string spfcSerial = spfcConfig.GetString("SPFC", "Serial_Nr");

    if (spfcSerial == "")
    {
    	spfcSerial = "0000";
    }

    IniParser spfcSettings(SPFC_SETTINGS); /* To get logging location */
    bool logToSDCard = spfcSettings.GetBool("LogToSDCard", "Variables");

    if (logToSDCard && SDCardAvailable())
    {
    	_logPath = BASE_PATH_SD LOG_PATH;
    }
    else
    {
    	_logPath = BASE_PATH_LOCAL LOG_PATH;
    }


    _logPath = _logPath + "/" + _deviceId;

    /* If logpath does not exist, create it!
     * Using a system call is not the MOST efficient way, but this seems to be the easiest for now.
     * Permission to create directories are needed
     */
	//system(cmdLine.c_str()); // We can't do anything about it if the system call fails.
	std::filesystem::create_directories(_logPath.c_str());

    std::transform(_deviceId.begin(), _deviceId.end(), _deviceId.begin(), ::toupper);

    _controllerId = "SPFC";
    _controllerSerial = spfcSerial;
    _deviceLocation = deviceLocation;

    /* Try to identify the AP location from the device name
     * eg.: mid_dsh_0000/spf/spf2 -> M0000
     */
    if (deviceLocation.find("/") > 0)
    {
		if (deviceLocation.compare(0, 8, "mid_dsh_") == 0)
		{ // get the AP# between "mid_dsh_" and "/"
			_devLogLocation = "M" + deviceLocation.substr(8, deviceLocation.find("/")-8);
		} else
		{ // eg.: tent0003/spf/spf2 -> tent0003
			_devLogLocation = deviceLocation.substr(0, deviceLocation.find("/"));
		}
    } else
    { // eg.: tent3
    	_devLogLocation = deviceLocation;
    }
    std::replace(_devLogLocation.begin(), _devLogLocation.end(), '_', '-');

}

SensorLogger::~SensorLogger()
{
}

bool SensorLogger::Log(stHeliumCompressorDataFrame dataFrame)
{
	bool retVal = false;

	int sensorId;
	std::string line;

	/* Timestamp */
	line += util::NumberToString(dataFrame.timestamp);
	line += "\t";

	/* Sensors */
	sensorId = 1;
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.controllerVoltage, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.controllerCurrent, 2);			line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.controllerTemperature, 2);		line += "\t";

	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.compressorSupplyPressure, 2);	line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.compressorReturnPressure, 2);	line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.compressorMotorTemperature, 2);	line += "\t";
	line += std::to_string(sensorId++); line += ":"; line += util::NumberToString(dataFrame.sensors.compressorSupplyTemperature, 2);

	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	//ss << std::put_time(std::localtime(&in_time_t), "%Y%m%d");

	char timeBuf[24];
	std::strftime(timeBuf, sizeof(timeBuf), "%Y%m%d", std::localtime(&in_time_t));
	ss << timeBuf;


	std::string fileName = _logPath + "/" + _devLogLocation + "_" + _controllerId + _controllerSerial + "_" + _deviceId + _deviceSerial + "_" + ss.str() + ".log";

	DataLogger::AppendLine(fileName, line);

	return retVal;
}

bool SensorLogger::SDCardAvailable()
{
	bool retVal = false;

	std::ifstream mmcSizeFile ("/sys/block/mmcblk0/size");
	if (mmcSizeFile.is_open())
	{
	    unsigned long long size;
	    retVal = (mmcSizeFile >> size) && (size > 0);
	    mmcSizeFile.close();
	}

	return retVal; // Will return false if file could not be opened */
}




#ifndef hecomp_ctrl_h
#define hecomp_ctrl_h

#include <string.h>
#include "hecomp_if.h"
#include "hecomp_sm.h"
#include "IniParser.h"
#include "DataFrame.h"

#define DEFAULT_ONLINE		0
#define DEFAULT_HE_REQ		0

class HeCompControl
{
public:
	HeCompControl();
    ~HeCompControl();

    bool Run();
    bool ShutdownSM();
    bool RestartSM();

	std::string GetDeviceLocation();
	std::string GetDevLogLocation();
    void SetSpfHeLocation(std::string spfHeLocation);

    /* Mode commands commands */
    bool SetMode(std::string mode);

    /* Controller commands - called by Tango internals */
    bool Start();
    bool Stop();
    bool Reset();
    bool ClearErrors();

    /* Normal operational commands */
    bool RequestHelium(_enSetRequest reqDev);
    bool CancelHelium(_enSetRequest nReqDev);
    unsigned int GetHeRequest();
    bool CompressorOn();
    bool CompressorOff();

    enDeviceStatus GetDeviceStatus();
    enDeviceMode GetMode();
    enHeCompStatus GetHardwareStatus();
    std::string GetCurrentState();
    stHeliumCompressorDataFrame GetDataFrame();

    bool SetExpectedOnline(bool state);
    bool GetExpectedOnline();
    bool Detected();

    std::string GetFirmwareVersion();
    std::string GetSerial();

    bool* GetReqStatus(bool* requestArr);

    bool SendRawCommand(std::string command, std::string & response);

    int IniKeyLoadOrCreateInt(std::string key, int value, std::string section);
    unsigned int IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section);
    double IniKeyLoadOrCreateDouble(std::string key, double value, std::string section);

    /* To be defined */
    bool Test();

private:
    pthread_t sm_thread;

    IniParser * settingsFile;
    stSettings * settings;

	std::string deviceLocation; // = "mid_dsh_unkn/spf/spfhe";
	std::string devLogLocation; // = "unknown";

};

#endif /* hecomp_ctrl_h */

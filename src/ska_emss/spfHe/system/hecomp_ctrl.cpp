#include "qpcpp.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>      // for memcpy() and memset()
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "hecomp_if.h"
#include "hecomp_sm.h"
#include "hecomp_ctrl.h"

using namespace std;
using namespace QP;
using namespace HeComp;

extern "C" void Q_onAssert(char const * const module, int loc)
{
    cout << "Assertion failed in " << module
              << " location " << loc << endl;
    QS_ASSERTION(module, loc, static_cast<uint32_t>(10000U));
}

void QF::onStartup(void)
{
}
void QF::onCleanup(void)
{
    cout << endl << "Cleaning up" << endl;
}

void QP::QF_onClockTick(void)
{
	QF::TICK_X(0U, (void *)0);  // perform the QF clock tick processing
}

void *SM_main(void * arg)
{
	static QF_MPOOL_EL(QEvt) smlPoolSto[100];
    //static QSubscrList subscrSto[MAX_PUB_SIG];
    static QEvt const *eventQSto[100]; // Event queue storage for SpfVac

    QF::init(); // initialize the framework and the underlying RT kernel

    // init publish-subscribe
    //QF::psInit(subscrSto, Q_DIM(subscrSto)); // init publish-subscribe

    // dynamic event allocation not used, no call to
    // initialize event pools...
    QF::poolInit(smlPoolSto, sizeof(smlPoolSto), sizeof(smlPoolSto[0]));

    // instantiate and start the active objects...
    heCompAO->settings = (stSettings *)(arg); /* TODO: This passes the pointer to the AO, meaning it can edit the values */

    heCompAO->start(1U,                            // priority
                     eventQSto, Q_DIM(eventQSto),   // event queue
                     (void *)0, 0U);                // stack (unused

    QF::run(); // run the QF application - does not return
    cout << "QF::run() exited - QF framework halted" << endl;

    return (void*)0;
}

HeCompControl::HeCompControl()
{
	cout << "----------------- [HeCompControl()] -------------" << endl;
	sm_thread = 0;

	/* Read the config file */
	settingsFile = new IniParser(SPFHE_SETTINGS);

	settings = new stSettings();

    settings->variables.expected_online = IniKeyLoadOrCreateInt("expected_online", 0, "Variables");
	settings->variables.he_req_user = IniKeyLoadOrCreateUInt("he_req_user", 0, "Variables");
	settings->variables.he_req_b1 = IniKeyLoadOrCreateUInt("he_req_b1", 0, "Variables");
	settings->variables.he_req_b2 = IniKeyLoadOrCreateUInt("he_req_b2", 0, "Variables");
	settings->variables.he_req_b345 = IniKeyLoadOrCreateUInt("he_req_b345", 0, "Variables");
	settings->variables.he_req_bS = IniKeyLoadOrCreateUInt("he_req_bS", 0, "Variables");
	settings->variables.he_req_bKu = IniKeyLoadOrCreateUInt("he_req_bKu", 0, "Variables");

	settings->constants.heliumSupplyOn = IniKeyLoadOrCreateDouble("HeliumSupplyOn", 18.5, "Constants");
	settings->constants.heliumSupplyMax = IniKeyLoadOrCreateDouble("HeliumSupplyMax", 21.5, "Constants");
	settings->constants.heliumSupplyDiffMin = IniKeyLoadOrCreateDouble("HeliumDifferentialMin", 13, "Constants");
	settings->constants.heliumSupplyDiffMax = IniKeyLoadOrCreateDouble("HeliumDifferentialMax", 15, "Constants");
	settings->constants.heliumReturnLimit = IniKeyLoadOrCreateDouble("HeliumReturnLimit", 3, "Constants");
	settings->constants.heliumChargeLow = IniKeyLoadOrCreateDouble("HeliumChargeLow", 12, "Constants");
	settings->constants.heliumSupplyTempHigh = IniKeyLoadOrCreateDouble("HeliumSupplyTempHigh", 80, "Constants");
	settings->constants.heliumMotorTempLow = IniKeyLoadOrCreateDouble("HeliumMotorTempLow", 4, "Constants");
	settings->timeouts.Timeout_OffWait = IniKeyLoadOrCreateUInt("Timeout_OffWait", 20, "Timeouts");
	settings->timeouts.Timeout_ReadyWait = IniKeyLoadOrCreateUInt("Timeout_ReadyWait", 20, "Timeouts");
	settingsFile->Save();
}

HeCompControl::~HeCompControl()
{
	cout << "----------------- [~HeCompControl()] ------------" << endl;

	heCompAO->POST(Q_NEW(QEvt, SM_SHUTDOWN_SIG), me);

	heCompAO->stop();
	QF::stop(); /* stop QF and cleanup */

	delete settings;
	delete settingsFile; /* Will save any unsaved stuff */
}

/* This function starts the state machine thread */
bool HeCompControl::Run()
{
	cout << "STARTING SM THREAD" << endl;

	bool retVal = false;
    int res = pthread_create(&sm_thread, NULL, SM_main, this->settings);
    if (res != 0)
    {
        perror("SM_main() pthread create");
        // TODO: handle error here ...
    }
    else
    {
        retVal = true;
    }

    return retVal;
}

bool HeCompControl::Reset()
{
	return heCompAO->POST(Q_NEW(QEvt, M_HARD_RESET_SIG), me);
}

bool HeCompControl::ClearErrors()
{
	return heCompAO->POST(Q_NEW(QEvt, M_CLEAR_ERRORS_SIG), me);
}

bool HeCompControl::ShutdownSM()
{
    cout << "################## VIA TANGO ##################" << endl;
    cout << "Setting State to SHUTDOWN " << endl;

    return heCompAO->POST(Q_NEW(QEvt, SM_SHUTDOWN_SIG), me);
}

bool HeCompControl::RestartSM()
{
	return heCompAO->POST(Q_NEW(QEvt, SM_RESET_SIG), me);
}

void HeCompControl::SetSpfHeLocation(std::string spfHeLocation)
{
	heCompAO->spfHeLocation = spfHeLocation; // operator= copies data
}


bool HeCompControl::RequestHelium(_enSetRequest reqDev)
{
//	enControllerState currentState = heCompAO->GetState();
	bool retVal = false;
	if (heCompAO->compressorDetected) // only accept requests if compressor detected.
	{
		switch(reqDev)
		{
			case _enSetRequest::USER:
				if (!heCompAO->settings->variables.he_req_user)
				{
					heCompAO->settings->variables.he_req_user = 1;
					retVal  = settingsFile->SetUInt("he_req_user", heCompAO->settings->variables.he_req_user, "", "Variables");
					retVal &= heCompAO->POST(Q_NEW(QEvt, REQUEST_HELIUM_SIG), me);
				}
				break;
			case _enSetRequest::B1:
				// do nothing, band 1 should not request helium service at this stage
				break;
			case _enSetRequest::B2:
				if (!heCompAO->settings->variables.he_req_b2)
				{
					heCompAO->settings->variables.he_req_b2 = 1;
					retVal  = settingsFile->SetUInt("he_req_b2", heCompAO->settings->variables.he_req_b2, "", "Variables");
					retVal &= heCompAO->POST(Q_NEW(QEvt, REQUEST_HELIUM_SIG), me);
				}
				break;
			case _enSetRequest::B345:
				if (!heCompAO->settings->variables.he_req_b345)
				{
					heCompAO->settings->variables.he_req_b345 = 1;
					retVal  = settingsFile->SetUInt("he_req_b345", heCompAO->settings->variables.he_req_b345, "", "Variables");
					retVal &= heCompAO->POST(Q_NEW(QEvt, REQUEST_HELIUM_SIG), me);
				}
				break;
			case _enSetRequest::BS:
				if (!heCompAO->settings->variables.he_req_bS)
				{
					heCompAO->settings->variables.he_req_bS = 1;
					retVal  = settingsFile->SetUInt("he_req_bS", heCompAO->settings->variables.he_req_bS, "", "Variables");
					retVal &= heCompAO->POST(Q_NEW(QEvt, REQUEST_HELIUM_SIG), me);
				}
				break;
			case _enSetRequest::BKu:
				if (!heCompAO->settings->variables.he_req_bKu)
				{
					heCompAO->settings->variables.he_req_bKu = 1;
					retVal  = settingsFile->SetUInt("he_req_bKu", heCompAO->settings->variables.he_req_bKu, "", "Variables");
					retVal &= heCompAO->POST(Q_NEW(QEvt, REQUEST_HELIUM_SIG), me);
				}
				break;
		}
	}
	retVal &= settingsFile->Save();
	return retVal;
}

bool HeCompControl::CancelHelium(_enSetRequest nReqDev)
{
	bool retVal = false;
	switch(nReqDev)
	{
		case _enSetRequest::USER:
			if (heCompAO->settings->variables.he_req_user)
			{
				heCompAO->settings->variables.he_req_user = 0;
				retVal  = settingsFile->SetUInt("he_req_user", heCompAO->settings->variables.he_req_user, "", "Variables");
				retVal &= heCompAO->POST(Q_NEW(QEvt, CANCEL_HELIUM_SIG), me);
			}
			break;
		case _enSetRequest::B1:
			// do nothing, band 1 should not request helium service at this stage
			break;
		case _enSetRequest::B2:
			if (heCompAO->settings->variables.he_req_b2)
			{
				heCompAO->settings->variables.he_req_b2 = 0;
				retVal  = settingsFile->SetUInt("he_req_b2", heCompAO->settings->variables.he_req_b2, "", "Variables");
				retVal &= heCompAO->POST(Q_NEW(QEvt, CANCEL_HELIUM_SIG), me);
			}
			break;
		case _enSetRequest::B345:
			if (heCompAO->settings->variables.he_req_b345)
			{
				heCompAO->settings->variables.he_req_b345 = 0;
				retVal  = settingsFile->SetUInt("he_req_b345", heCompAO->settings->variables.he_req_b345, "", "Variables");
				retVal &= heCompAO->POST(Q_NEW(QEvt, CANCEL_HELIUM_SIG), me);
			}
			break;
		case _enSetRequest::BS:
			if (heCompAO->settings->variables.he_req_bS)
			{
				heCompAO->settings->variables.he_req_bS = 0;
				retVal  = settingsFile->SetUInt("he_req_bS", heCompAO->settings->variables.he_req_bS, "", "Variables");
				retVal &= heCompAO->POST(Q_NEW(QEvt, CANCEL_HELIUM_SIG), me);
			}
			break;
		case _enSetRequest::BKu:
			if (heCompAO->settings->variables.he_req_bKu)
			{
				heCompAO->settings->variables.he_req_bKu = 0;
				retVal  = settingsFile->SetUInt("he_req_bKu", heCompAO->settings->variables.he_req_bKu, "", "Variables");
				retVal &= heCompAO->POST(Q_NEW(QEvt, CANCEL_HELIUM_SIG), me);
			}
			break;
	}
	retVal &= settingsFile->Save();
	return retVal;
}

unsigned int HeCompControl::GetHeRequest()
{
	return settings->variables.he_req_user + settings->variables.he_req_b1 + settings->variables.he_req_b2 + settings->variables.he_req_b345 + settings->variables.he_req_bS + settings->variables.he_req_bKu;
}

bool HeCompControl::CompressorOn()
{
	if (!heCompAO->settings->variables.he_req_user)
	{
		heCompAO->settings->variables.he_req_user = 1;
		settingsFile->SetUInt("he_req_user", heCompAO->settings->variables.he_req_user, "", "Variables");
		settingsFile->Save();
	}
	heCompAO->POST(Q_NEW(QEvt, REQUEST_HELIUM_SIG), me);
	return heCompAO->POST(Q_NEW(QEvt, M_ON_SIG), me);

}
bool HeCompControl::CompressorOff()
{
	if (heCompAO->settings->variables.he_req_user)
	{
		heCompAO->settings->variables.he_req_user = 0;
		settingsFile->SetUInt("he_req_user", heCompAO->settings->variables.he_req_user, "", "Variables");
		settingsFile->Save();
	}
	heCompAO->POST(Q_NEW(QEvt, CANCEL_HELIUM_SIG), me);
	return heCompAO->POST(Q_NEW(QEvt, M_OFF_SIG), me);
}

bool HeCompControl::SetMode(std::string mode)
{
	bool retVal = false;

	cout << "Setting mode to: [" << mode << "]" << endl;

	if (mode == "MAINT")
	{
		retVal = heCompAO->POST(Q_NEW(QEvt, MAINTENANCE_SIG), me);
	}
	else if (mode == "STANDBY")
	{
		retVal = heCompAO->POST(Q_NEW(QEvt, STANDBY_SIG), me);
	}

	return retVal;
}

bool HeCompControl::SetExpectedOnline(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal  = settingsFile->SetInt("expected_online", 1, "", "Variables"); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		heCompAO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
	}
	else
	{
		retVal  = settingsFile->SetInt("expected_online", 0, "", "Variables"); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		heCompAO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
		retVal &= heCompAO->POST(Q_NEW(QEvt, OFFLINE_SIG), me);
	}

	return retVal;
}
bool HeCompControl::GetExpectedOnline()
{
	return settings->variables.expected_online;
}

bool HeCompControl::Test()
{
    return true; // FIXME
}

bool HeCompControl::Detected()
{
	return heCompAO->compressorDetected;
}

enDeviceStatus HeCompControl::GetDeviceStatus()
{
	return heCompAO->GetDeviceStatus();
}

std::string HeCompControl::GetCurrentState()
{
	return heCompAO->GetCurrentState();
}

enDeviceMode HeCompControl::GetMode()
{
	return heCompAO->GetMode();
}

stHeliumCompressorDataFrame HeCompControl::GetDataFrame()
{
	return heCompAO->GetDataFrame();
}

std::string HeCompControl::GetFirmwareVersion()
{
	return heCompAO->fwVersion;
}
std::string HeCompControl::GetSerial()
{
	return heCompAO->serial;
}

bool* HeCompControl::GetReqStatus(bool* requestArr)
{
	requestArr[0] = heCompAO->settings->variables.he_req_user;
	requestArr[1] = heCompAO->settings->variables.he_req_b1;
	requestArr[2] = heCompAO->settings->variables.he_req_b2;
	requestArr[3] = heCompAO->settings->variables.he_req_b345;
	requestArr[4] = heCompAO->settings->variables.he_req_bS;
	requestArr[5] = heCompAO->settings->variables.he_req_bKu;
	return requestArr;
}

bool HeCompControl::SendRawCommand(std::string command, std::string & response)
{
	return heCompAO->SendRawCommand(command, response);
}

int HeCompControl::IniKeyLoadOrCreateInt(std::string key, int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetInt(key, section);
	}
}

unsigned int HeCompControl::IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetUInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetUInt(key, section);
	}
}


double HeCompControl::IniKeyLoadOrCreateDouble(std::string key, double value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetDouble(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetDouble(key, section);
	}
}






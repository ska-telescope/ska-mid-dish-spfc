#ifndef CIRCULARFIFO_SEQUENTIAL_H_
#define CIRCULARFIFO_SEQUENTIAL_H_

#include <atomic>
#include <cstddef>

template<typename Element, size_t Size>
class CircularFifo{
public:
  enum { Capacity = Size+1 };

  CircularFifo() : _tail(0), _head(0), _filled(0){}
  virtual ~CircularFifo() {}

  bool push(const Element& item); // pushByMOve?
  bool pop(Element& item);
  bool peek(Element& item, size_t hist);
  bool clear();

  size_t getSequentialArray(Element seqArray[], size_t hLen);

  bool wasEmpty() const;
  bool wasFull() const;
  bool isLockFree() const;

private:
  size_t increment_modulo(size_t idx, size_t count = 1) const;
  size_t decrement_modulo(size_t idx, size_t count = 1) const;
  size_t increment_clipped(size_t idx) const;
  size_t decrement_clipped(size_t idx) const;

  std::atomic <size_t>  _tail;  // tail(input) index
  Element    _array[Capacity];
  std::atomic<size_t>   _head; // head(output) index

  size_t _filled;
};


// Here with memory_order_seq_cst for every operation. This is overkill but easy to reason about
//
// Push on tail. TailHead is only changed by producer and can be safely loaded using memory_order_relexed
//         head is updated by consumer and must be loaded using at least memory_order_acquire
template<typename Element, size_t Size>
bool CircularFifo<Element, Size>::push(const Element& item)
{
  const auto current_tail = _tail.load();
  const auto next_tail = increment_modulo(current_tail);

  /* TJS - Allow the buffer to wrap */
  //if(next_tail != _head.load())
  {
    _array[current_tail] = item;
    _tail.store(next_tail);
    _filled = increment_clipped(_filled);
    return true;
  }

  return false;  // full queue
}

// Pop by Consumer can only update the head
template<typename Element, size_t Size>
bool CircularFifo<Element, Size>::pop(Element& item)
{
	const auto current_head = _head.load();
	if(current_head == _tail.load())
		return false;   // empty queue

  item = _array[current_head];
  _head.store(increment_modulo(current_head));
  _filled = decrement_clipped(_filled);
  return true;
}

// Peek does not remove anything from the buffer
template<typename Element, size_t Size>
bool CircularFifo<Element, Size>::peek(Element& item, size_t hist)
{
	if (hist < _filled) // The number of history elements are within the number of elements filled
	{
		const auto previous_tail = decrement_modulo(_tail.load()); // the next open slot was saved during push, so we decrement by 1 to get the last position
		const auto index = decrement_modulo(previous_tail, hist);
		item = _array[index];
		return true;
	}

	return false; // Not enough data to return
}

// Clear the buffer - no data removed, just head and tail pointers manipulated
template<typename Element, size_t Size>
bool CircularFifo<Element, Size>::clear()
{
	_tail.store(0);
	_head.store(0);
	_filled = 0;

	return true;
}

// snapshot with acceptance of that this comparison function is not atomic
// (*) Used by clients or test, since pop() avoid double load overhead by not
// using wasEmpty()
template<typename Element, size_t Size>
bool CircularFifo<Element, Size>::wasEmpty() const
{
  return (_head.load() == _tail.load());
}

// snapshot with acceptance that this comparison is not atomic
// (*) Used by clients or test, since push() avoid double load overhead by not
// using wasFull()
template<typename Element, size_t Size>
bool CircularFifo<Element, Size>::wasFull() const
{
  const auto next_tail = increment_modulo(_tail.load());
  return (next_tail == _head.load());
}


template<typename Element, size_t Size>
bool CircularFifo<Element, Size>::isLockFree() const
{
  return (_tail.is_lock_free() && _head.is_lock_free());
}

template<typename Element, size_t Size>
size_t CircularFifo<Element, Size>::increment_modulo(size_t idx, size_t count) const
{
  return (idx + count) % Capacity;
}

template<typename Element, size_t Size>
size_t CircularFifo<Element, Size>::decrement_modulo(size_t idx, size_t count) const
{
	if (count < Capacity)
		return (idx + (Capacity - count)) % Capacity;
	else
		return idx;
}

template<typename Element, size_t Size>
size_t CircularFifo<Element, Size>::increment_clipped(size_t idx) const
{
	if (idx < Capacity)
		return (idx + 1);
	else
		return Capacity;
}

template<typename Element, size_t Size>
size_t CircularFifo<Element, Size>::decrement_clipped(size_t idx) const
{
	if (idx > 0)
		return (idx - 1);
	else
		return 0;
}

/* Will put the latest value in the front, position 0 */
template<typename Element, size_t Size>
size_t CircularFifo<Element, Size>::getSequentialArray(Element seqArray[], size_t count)
{
	size_t valuesFound = 0;
	for (size_t i = 0; i < count; i++)
	{
		//if (peek(seqArray[(count-1) - i], i)) //fill array from the back if valid
		if (peek(seqArray[i], i)) //fill array from the back if valid
		{
			valuesFound++;
		}
	}
	return valuesFound;
}


#endif /* CIRCULARFIFO_SEQUENTIAL_H_ */

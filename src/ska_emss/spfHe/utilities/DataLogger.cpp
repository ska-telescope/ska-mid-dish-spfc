/*
 * SensorLog.cpp
 *
 *  Created on: 26 Jun 2017
 *      Author: theuns
 */

#include "DataLogger.h"

#include <iostream>
#include <fstream>
#include <string>

DataLogger::DataLogger()
{
}

DataLogger::~DataLogger()
{
}

bool DataLogger::AppendLine(std::string fileName, std::string line)
{
    std::ofstream file;

    file.open(fileName, std::ios::out | std::ios::app);
    if (file.fail())
    {
        std::cout << "Could not open logfile: " << fileName << std::endl;
    	//throw std::ios_base::failure(std::strerror(errno));
        return false;
    }

    //make sure write fails with exception if something is wrong
    //file.exceptions(file.exceptions() | std::ios::failbit | std::ifstream::badbit);

    file << line << std::endl;

    return true;
}




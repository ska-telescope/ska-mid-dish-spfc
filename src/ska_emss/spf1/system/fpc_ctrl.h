#ifndef fpc_ctrl_h
#define fpc_ctrl_h

#include <string.h>

#include "fpc_if.h"
#include "fp1_sm.h"
#include "IniParser.h"
#include "DataFrame.h"

class FeedPackageControl
{
public:
	FeedPackageControl();
    ~FeedPackageControl();

    bool Run();
    bool Shutdown();

    /* Controller commands - called by Tango internals */
    bool Start();
    bool Stop();
    bool Reset();

    /* Normal operational commands */
    enDeviceStatus GetDeviceStatus();
    enDeviceMode GetFeedMode();
    enCapabilityState GetCapabilityState();
    string GetCurrentState();
    stFeedDataFrame GetDataFrame();
    FP1::stFeedConfig GetFeedConfig();

    string GetFirmwareVersion();

    int IniKeyLoadOrCreateInt(std::string key, int value, std::string section);
    unsigned int IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section);
    double IniKeyLoadOrCreateDouble(std::string key, double value, std::string section);

    bool SendRawCommand(string command, string & response);

    /* Mode commands commands */
    bool SetMode(std::string mode);

    bool ClearErrors();

    void SetSpf1Location(std::string spf1Location);
    void SetComPort(std::string ttyPort);
    bool SetExpectedOnline(bool state);
    bool GetExpectedOnline();
    bool SetStartupDefault(enStartupDefault state);
    enStartupDefault GetStartupDefault();

    bool SetLnaHTempSetpoint(int sp);
    bool SetLnaVTempSetpoint(int sp);
    bool SetCalSourceTempSetpoint(int sp);

    bool SetDefaultLnaHTempSetpoint(int sp);
    int  GetDefaultLnaHTempSetpoint();
    bool SetDefaultLnaVTempSetpoint(int sp);
    int  GetDefaultLnaVTempSetpoint();
    bool SetDefaultCalSourceTempSetpoint(int sp);
    int  GetDefaultCalSourceTempSetpoint();

    bool FeedDetected();

    bool SetLnaHPowerState(bool state);
    bool SetLnaVPowerState(bool state);
    bool SetAmp2HPowerState(bool state);
    bool SetAmp2VPowerState(bool state);
    bool SetCalSourcePowerState(bool state);
    bool SetPidCalSourcePowerState(bool state);
    bool SetPidLnaHPowerState(bool state);
    bool SetPidLnaVPowerState(bool state);

    /* To be defined */
    bool Test();

private:
    pthread_t sm_thread;

    IniParser * settingsFile;
    stSettings * settings;

};

#endif /* fpc_ctrl_h */

#include "fpc_ctrl.h"

#include "qpcpp.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>      // for memcpy() and memset()
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "fp1_sm.h"
#include "fpc_if.h"
#include "system.h"

using namespace std;
using namespace QP;
using namespace FP1;

static QP::QTicker l_ticker0(0); // ticker for tick rate 0
QP::QActive * the_Ticker0 = &l_ticker0;

extern "C" void Q_onAssert(char const * const module, int loc)
{
    cout << "Assertion failed in " << module
              << " location " << loc << endl;
    QS_ASSERTION(module, loc, static_cast<uint32_t>(10000U));
}

void QF::onStartup(void)
{
}
void QF::onCleanup(void)
{
    cout << endl << "Cleaning up" << endl;
}

void QP::QF_onClockTick(void)
{
	//QF::TICK_X(0U, (void *)0);  // perform the QF clock tick processing
	the_Ticker0->POST(0, 0); // post a don't-care event to Ticker0
}

void *SM_main(void * arg)
{
	static QF_MPOOL_EL(QEvt) smlPoolSto[100];
    //static QSubscrList subscrSto[MAX_PUB_SIG];
    static QEvt const *eventQSto[100]; // Event queue storage for SpfVac

    QF::init(); // initialize the framework and the underlying RT kernel

    // init publish-subscribe
    //QF::psInit(subscrSto, Q_DIM(subscrSto)); // init publish-subscribe

    // dynamic event allocation not used, no call to
    // initialize event pools...
    QF::poolInit(smlPoolSto, sizeof(smlPoolSto), sizeof(smlPoolSto[0]));

    // instantiate and start the active objects...
    //fp1AO->settings = (stSettings *)(arg); /* TODO: This passes the pointer to the AO, meaning it can edit the values */
    fp1AO->SetSettings((stSettings *)(arg));/* TODO: This passes the pointer to the AO, meaning it can edit the values */

    the_Ticker0->start(1U, // priority
                             0, 0, 0, 0);

    fp1AO->start(2U,                            // priority
                     eventQSto, Q_DIM(eventQSto),   // event queue
                     (void *)0, 0U);                // stack (unused

    QF::run(); // run the QF application - does not return
    cout << "QF::run() exited - QF framework halted" << endl;

    return (void*)0;
}

FeedPackageControl::FeedPackageControl()
{
	cout << "----------------- [FeedPackageControl()] -------------" << endl;
	sm_thread = 0;

	/* Read the settings file */
	settingsFile = new IniParser(SPF1_SETTINGS);
	settingsFile->Save();

	settings = new stSettings();

	settings->variables.expected_online = IniKeyLoadOrCreateInt(N_EXPECTED_ONLINE, VAL_EXPECTED_ONLINE, INI_VARS);
	settings->variables.startup_default = static_cast<enStartupDefault>(IniKeyLoadOrCreateInt(N_STARTUP_DEFAULT, VAL_STARTUP_DEFAULT, INI_VARS));
	settings->variables.TS_LnaH = IniKeyLoadOrCreateInt(N_TS_LNA_H, VAL_TS_LNA_H, INI_VARS);
	settings->variables.TS_LnaV = IniKeyLoadOrCreateInt(N_TS_LNA_V, VAL_TS_LNA_V, INI_VARS);
	settings->variables.TS_CalSource = IniKeyLoadOrCreateInt(N_TS_CALSOURCE, VAL_TS_CALSOURCE,INI_VARS);

	settings->constants.T_WarmOperational		= IniKeyLoadOrCreateDouble(N_T_WARM_OPERATIONAL, VAL_T_WARM_OPERATIONAL, INI_CONST);
	settings->constants.T_WarmOperationalError	= IniKeyLoadOrCreateDouble(N_T_WARM_OPERATIONAL_E, VAL_T_WARM_OPERATIONAL_E, INI_CONST);
	settings->constants.T_NonOperational		= IniKeyLoadOrCreateDouble(N_T_NON_OPERATIONAL, VAL_T_NON_OPERATIONAL, INI_CONST);
	settings->constants.TS_MaxOffset			= IniKeyLoadOrCreateDouble(N_TS_MAX_OFFSET, VAL_TS_MAX_OFFSET, INI_CONST);
	settings->constants.TS_StabilityDelta		= IniKeyLoadOrCreateDouble(N_TS_STABILITY_DELTA, VAL_TS_STABILITY_DELTA, INI_CONST);
	settings->constants.T_DeltaError			= IniKeyLoadOrCreateDouble(N_T_DELTA_E, VAL_T_DELTA_E, INI_CONST);
//	settings->constants.T_Offset_H				= settingsFile->GetInt("T_Offset_H", INI_CONST);
//	settings->constants.T_Offset_V				= settingsFile->GetInt("T_Offset_V", INI_CONST);

	settings->timeouts.Timeout_LnaTemperatureIncrease	= IniKeyLoadOrCreateUInt(N_TO_LNA_TEMP_INC, VAL_TO_LNA_TEMP_INC, INI_TIMEOUTS);
	settings->timeouts.Timeout_WarmOpsTemp				= IniKeyLoadOrCreateUInt(N_TO_WARM_OPS_TEMP, VAL_TO_WARM_OPS_TEMP, INI_TIMEOUTS);

	settingsFile->Save(); // if new default values were created
}

FeedPackageControl::~FeedPackageControl()
{
	cout << "----------------- [~FeedPackageControl()] ------------" << endl;

	fp1AO->POST(Q_NEW(QEvt, SHUTDOWN_SIG), me);

	fp1AO->stop();
	QF::stop(); /* stop QF and cleanup */

	delete settings;
	delete settingsFile; /* Will save any unsaved stuff */
}

/* This function starts the state machine thread */
bool FeedPackageControl::Run()
{
	cout << "STARTING SM THREAD" << endl;

	bool retVal = false;

	pthread_attr_t attr;
	pthread_attr_init(&attr);

	// SCHED_FIFO corresponds to real-time preemptive priority-based scheduler
	// NOTE: This scheduling policy requires the superuser privileges
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);

	struct sched_param param;
	param.sched_priority = 1 + (sched_get_priority_max(SCHED_FIFO) - 60);

	pthread_attr_setschedparam(&attr, &param);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    int res = pthread_create(&sm_thread, &attr, SM_main, this->settings);
    if (res != 0)
    {
        perror("SM_main() pthread create");
        // TODO: handle error here ...
    }
    else
    {
        retVal = true;
    }

    return retVal;
}


bool FeedPackageControl::Reset()
{
	return fp1AO->POST(Q_NEW(QEvt, RESET_SIG), me);
}

bool FeedPackageControl::Shutdown()
{
    cout << "################## VIA TANGO ##################" << endl;
    cout << "Setting State to SHUTDOWN " << endl;

    return fp1AO->POST(Q_NEW(QEvt, SHUTDOWN_SIG), me);
}

bool FeedPackageControl::SetMode(std::string mode)
{
	bool retVal = false;

	cout << "Setting mode to: [" << mode << "]" << endl;

	if (mode == "MAINT")
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, ITC_SIG), me);
	}
	else if (mode == "STANDBY")
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, SOFT_OFF_SIG), me);
	}
	else if (mode == "OPER")
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, OPERATIONAL_SIG), me);
	}


	return retVal;
}

bool FeedPackageControl::ClearErrors()
{
	return fp1AO->POST(Q_NEW(QEvt, ERRORS_CLEARED_SIG), me);
}

void FeedPackageControl::SetSpf1Location(std::string spf1Location)
{
	fp1AO->spf1Location = spf1Location; // operator= copies data
}
void FeedPackageControl::SetComPort(std::string ttyPort)
{
	fp1AO->ttyPort = ttyPort; // operator= copies data
}


bool FeedPackageControl::SetExpectedOnline(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal  = settingsFile->SetInt(N_EXPECTED_ONLINE, 1, "", INI_VARS); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		fp1AO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
	}
	else
	{
		retVal  = settingsFile->SetInt(N_EXPECTED_ONLINE, 0, "", INI_VARS); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		fp1AO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
		retVal &= fp1AO->POST(Q_NEW(QEvt, OFFLINE_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::GetExpectedOnline()
{
	return settings->variables.expected_online;
}

bool FeedPackageControl::SetStartupDefault(enStartupDefault state)
{
	bool retVal = false;

	retVal  = settingsFile->SetInt(N_STARTUP_DEFAULT, static_cast<short>(state), "", INI_VARS);
	retVal &= settingsFile->Save();
	settings->variables.startup_default = state;

	return retVal;
}
enStartupDefault FeedPackageControl::GetStartupDefault()
{
	return settings->variables.startup_default;
}

bool FeedPackageControl::SetLnaHTempSetpoint(int sp)
{
	int tempValue = 0;
				switch(sp)
				{
				case 0:
					tempValue = 0;
					break;
				case 1:
					tempValue = 283;
					break;
				case 2:
					tempValue = 288;
					break;
				case 3:
					tempValue = 293;
					break;
				case 4:
					tempValue = 298;
					break;
				case 5:
					tempValue = 303;
					break;
				case 6:
					tempValue = 308;
					break;
				case 7:
					tempValue = 313;
					break;
				default:
					tempValue = sp;
					break;
				}
	return fp1AO->SetLnaHTempSetpoint(tempValue);
}
bool FeedPackageControl::SetLnaVTempSetpoint(int sp)
{
	int tempValue = 0;
				switch(sp)
				{
				case 0:
					tempValue = 0;
					break;
				case 1:
					tempValue = 283;
					break;
				case 2:
					tempValue = 288;
					break;
				case 3:
					tempValue = 293;
					break;
				case 4:
					tempValue = 298;
					break;
				case 5:
					tempValue = 303;
					break;
				case 6:
					tempValue = 308;
					break;
				case 7:
					tempValue = 313;
					break;
				default:
					tempValue = sp;
					break;
				}
	return fp1AO->SetLnaVTempSetpoint(tempValue);
}
bool FeedPackageControl::SetCalSourceTempSetpoint(int sp)
{
	int tempValue = 0;
				switch(sp)
				{
				case 0:
					tempValue = 0;
					break;
				case 1:
					tempValue = 325;
					break;
				case 2:
					tempValue = 330;
					break;
				case 3:
					tempValue = 335;
					break;
				case 4:
					tempValue = 340;
					break;
				case 5:
					tempValue = 345;
					break;
				case 6:
					tempValue = 350;
					break;
				case 7:
					tempValue = 355;
					break;
				default:
					tempValue = sp;
					break;
				}
	return fp1AO->SetCalSourceTempSetpoint(tempValue);
}

bool FeedPackageControl::SetDefaultLnaHTempSetpoint(int sp)
{
	bool retVal = false;
	int tempValue = 0;
				switch(sp)
				{
				case 0:
					tempValue = 0;
					break;
				case 1:
					tempValue = 283;
					break;
				case 2:
					tempValue = 288;
					break;
				case 3:
					tempValue = 293;
					break;
				case 4:
					tempValue = 298;
					break;
				case 5:
					tempValue = 303;
					break;
				case 6:
					tempValue = 308;
					break;
				default:
					tempValue = sp;
					break;
				}

	retVal  = settingsFile->SetInt(N_TS_LNA_H, tempValue, "", INI_VARS);
	retVal &= settingsFile->Save();
	settings->variables.TS_LnaH = tempValue;

	return retVal;
}
int FeedPackageControl::GetDefaultLnaHTempSetpoint()
{
	return settings->variables.TS_LnaH;
}
bool FeedPackageControl::SetDefaultLnaVTempSetpoint(int sp)
{
	bool retVal = false;
	int tempValue = 0;
				switch(sp)
				{
				case 0:
					tempValue = 0;
					break;
				case 1:
					tempValue = 283;
					break;
				case 2:
					tempValue = 288;
					break;
				case 3:
					tempValue = 293;
					break;
				case 4:
					tempValue = 298;
					break;
				case 5:
					tempValue = 303;
					break;
				case 6:
					tempValue = 308;
					break;
				default:
					tempValue = sp;
					break;
				}

	retVal  = settingsFile->SetInt(N_TS_LNA_V, tempValue, "", INI_VARS);
	retVal &= settingsFile->Save();
	settings->variables.TS_LnaV = tempValue;

	return retVal;
}
int FeedPackageControl::GetDefaultLnaVTempSetpoint()
{
	return settings->variables.TS_LnaV;
}
bool FeedPackageControl::SetDefaultCalSourceTempSetpoint(int sp)
{
	bool retVal = false;
	int tempValue = 0;
				switch(sp)
				{
				case 0:
					tempValue = 0;
					break;
				case 1:
					tempValue = 325;
					break;
				case 2:
					tempValue = 330;
					break;
				case 3:
					tempValue = 335;
					break;
				case 4:
					tempValue = 340;
					break;
				case 5:
					tempValue = 345;
					break;
				case 6:
					tempValue = 350;
					break;
				default:
					tempValue = sp;
					break;
				}

	retVal  = settingsFile->SetInt(N_TS_CALSOURCE, tempValue, "", INI_VARS);
	retVal &= settingsFile->Save();
	settings->variables.TS_CalSource = tempValue;

	return retVal;
}
int FeedPackageControl::GetDefaultCalSourceTempSetpoint()
{
	return settings->variables.TS_CalSource;
}

bool FeedPackageControl::FeedDetected()
{
	return fp1AO->feedDetected;
}

enDeviceStatus FeedPackageControl::GetDeviceStatus()
{
	return fp1AO->GetDeviceStatus();
}

enCapabilityState FeedPackageControl::GetCapabilityState()
{
	return fp1AO->GetCapabilityState();
}

string FeedPackageControl::GetCurrentState()
{
	return fp1AO->GetCurrentState();
}

enDeviceMode FeedPackageControl::GetFeedMode()
{
	return fp1AO->GetFeedMode();
}

stFeedDataFrame FeedPackageControl::GetDataFrame()
{
	return fp1AO->GetDataFrame();
}

FP1::stFeedConfig FeedPackageControl::GetFeedConfig()
{
	return fp1AO->feedConfig;
}

string FeedPackageControl::GetFirmwareVersion()
{
	return fp1AO->fwVersion;
}

bool FeedPackageControl::SetLnaHPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA_H_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA_H_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetLnaVPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA_V_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA_V_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetAmp2HPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA2_H_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA2_H_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetAmp2VPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA2_V_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, LNA2_V_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, CAL_SOURCE_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, CAL_SOURCE_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetPidCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, PID_CAL_SOURCE_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, PID_CAL_SOURCE_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetPidLnaHPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, PID_LNA_H_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, PID_LNA_H_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetPidLnaVPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, PID_LNA_V_ON_SIG), me);
	}
	else
	{
		retVal = fp1AO->POST(Q_NEW(QEvt, PID_LNA_V_OFF_SIG), me);
	}

	return retVal;
}

int FeedPackageControl::IniKeyLoadOrCreateInt(std::string key, int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetInt(key, section);
	}
}

unsigned int FeedPackageControl::IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetUInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetUInt(key, section);
	}
}


double FeedPackageControl::IniKeyLoadOrCreateDouble(std::string key, double value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetDouble(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetDouble(key, section);
	}
}

bool FeedPackageControl::SendRawCommand(string command, string & response)
{
	return fp1AO->SendRawCommand(command, response);
}

bool FeedPackageControl::Test()
{
	return fp1AO->POST(Q_NEW(QEvt, TEST_SERVICE_SIG), me);
}



/*
 * check_ctrl.cpp
 *
 *  Created on: 23 May 2017
 *      Author: theuns
 */

#include "check_ctrl.h"
#include "utilities.h"
#include <cmath>
#include <iostream>

using namespace util;

namespace FP1
{
CheckController::CheckController(stSettings settings, CircularFifo<stFeedDataFrame, HIST_BUFFER_SIZE> * buffPtr)
{
	_settings = settings;
	_buffPtr = buffPtr; // This is only a reference
}

CheckController::~CheckController()
{
}

/* Return true if there are more or equal then @sampleCount samples in the circular buffer */
bool CheckController::ActiveSamples(int sampleCount)
{
	return (_buffPtr->elementCount() >= sampleCount ? true : false);
}


int CheckController::GetLnaHTempHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.lnaHTemp;
	}

	return actualSamples;
}
int CheckController::GetLnaVTempHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.lnaVTemp;
	}

	return actualSamples;
}
int CheckController::GetSpdHistoryArray(double array[], int count)
{
	stFeedDataFrame histFrames[STABILITY_WINDOW] = {}; // Initialise all struct vars to their type-default

	int actualSamples = _buffPtr->getSequentialArray(histFrames, count);

	for (int i = 0; i < actualSamples; i++)
	{
		array[i] = histFrames[i].sensors.spdHealth;
	}

	return actualSamples;
}

/**
 * AverageLnaArrays() average the values in two arrays. The first array contains the averaged values upon return.
 */
void CheckController::AverageLnaArrays(double array1[], double array2[], int count)
{
	for (int i = 0; i < count; i++)
	{
		array1[i] = (array1[i] + array2[i])/2;
	}
}

bool CheckController::CheckSpdHealth()
{
	int count = MIN_COMPARED;
	double spdArray[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default

	int vote = 0;

	count = GetSpdHistoryArray(spdArray, count);

	if ((count > 0) && ActiveSamples(2)) // if the amount requested is the amount returned
	{
		for (int i = 0; i < count; i++)
		{
			if ( (spdArray[i] > 1.0)) // alarm if spfHealth below 1V
			{
				vote++;
			}
		}
		return ((vote == count) ? true : false);
	}
	return true; // Not enough data, so return true until enough data gives correct value;
}

/**
 * IsLnaHTempSafe() returns true if LNA-H temperature is within the available limits
 */
bool CheckController::IsLnaHTempSafe()
{
	return true; // ToDo:
}
/**
 * IsLnaVTempSafe() returns true if LNA-V temperature is within the available limits
 */
bool CheckController::IsLnaVTempSafe()
{
	return true; // ToDo:
}
/**
 * IsCalSourceTempSafe() returns true if Cal Source temperature is within the available limits
 */
bool CheckController::IsCalSourceTempSafe()
{
	return true; // ToDo:
}

bool CheckController::TempDiffMax()
{
	int countH = MIN_COMPARED;
	int countV = MIN_COMPARED;
	double lnaHArray[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default
	double lnaVArray[MIN_COMPARED] = {0.0}; // Initialise all struct vars to their type-default

	int vote = 0;

	countH = GetLnaHTempHistoryArray(lnaHArray, countH);
	countV = GetLnaVTempHistoryArray(lnaVArray, countV);

	if ( (countH == countV) && (countH > 0)) // if the amount requested is the amount returned
	{
		for (int i = 0; i < countH; i++)
		{
			if ( (lnaHArray[i] > 0) && (lnaVArray[i] > 0) && (abs(lnaHArray[i] - lnaVArray[i]) < _settings.constants.T_DeltaError) )
			{
				vote++;
			}
		}
		return ((vote == countH) ? true : false);
	}
	return false; // Not enough data, so return false;
}

/**
 *  isLnaTempControllable() returns true if the LNA temperatures are low enough to control.
 */
bool CheckController::isLnaTempControllable()
{
	int vote = 0;
	int countH = MIN_COMPARED;
	int countV = MIN_COMPARED;
	double histLnaHTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default
	double histLnaVTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default

	countH = GetLnaHTempHistoryArray(histLnaHTemp, countH);
	countV = GetLnaVTempHistoryArray(histLnaVTemp, countV);

	if ( (countH == countV) && (countH > 0)) // check equal lengths and if the amount requested is the amount returned
	{
		AverageLnaArrays(histLnaHTemp, histLnaVTemp, countH);
		for (int i = 0; i < countH; i++)
		{
			if ( histLnaHTemp[i] <= _settings.constants.T_WarmOperational - _settings.constants.TS_MaxOffset)
			{
				vote++;
			}
		}
		return ((vote == countH) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  LnaTempIncrease() returns true if the average RFE1 temperature time derivative
 *  is more than 0
 */
bool CheckController::LnaTempIncrease()
{
	int countH = MIN_COMPARED;
	int countV = MIN_COMPARED;
	double histLnaHTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default
	double histLnaVTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default

	countH = GetLnaHTempHistoryArray(histLnaHTemp, countH);
	countV = GetLnaVTempHistoryArray(histLnaVTemp, countV);

	if ( (countH == countV) && (countH > 0)) // check equal lengths and if the amount requested is the amount returned
	{
		AverageLnaArrays(histLnaHTemp, histLnaVTemp, countH);
		return (tempDt(histLnaHTemp, countH) > 0 ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  WarmTransStabilityCheck() returns true if the LNA temperatures are stable enough
 *  Depricated; not in use because it is in essence the same test as WOTempCheck()
 */
bool CheckController::WarmTransStabilityCheck()
{
	int vote = 0;
	int countH = MIN_COMPARED;
	int countV = MIN_COMPARED;
	double histLnaHTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default
	double histLnaVTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default

	countH = GetLnaHTempHistoryArray(histLnaHTemp, countH);
	countV = GetLnaVTempHistoryArray(histLnaVTemp, countV);

	if ( (countH == countV) && (countH > 0)) // check equal lengths and if the amount requested is the amount returned
	{
		AverageLnaArrays(histLnaHTemp, histLnaVTemp, countH);
		for (int i = 0; i < countH; i++)
		{
			if ( (histLnaHTemp[i] >  _settings.constants.TS_StabilityDelta) && (histLnaHTemp[i] > _settings.constants.T_WarmOperational - _settings.constants.TS_MaxOffset) && (histLnaHTemp[i] < _settings.constants.T_WarmOperational + _settings.constants.TS_MaxOffset) )
			{
				vote++;
			}
		}
		return ((vote == countH) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  WOTempCheck() returns true if the LNAs temperatures are within the offset ranges
 */
bool CheckController::WOTempCheck(bool lnaPidOn, int setpoint)
{
	bool warmOps = false;
	int countH = GRADIENT_CHECK_WINDOW;
	int countV = GRADIENT_CHECK_WINDOW;
	double histLnaHTemp[countH]; // = {0.0}; // Initialise all struct vars to their type-default
	double histLnaVTemp[countV]; //  = {0.0}; // Initialise all struct vars to their type-default

	countH = GetLnaHTempHistoryArray(histLnaHTemp, countH);
	countV = GetLnaVTempHistoryArray(histLnaVTemp, countV);

	if ( (countH == countV) && (countH > 0)) // check equal lengths and if the amount requested is the amount returned
	{
		AverageLnaArrays(histLnaHTemp, histLnaVTemp, countH);
		// test if latest value is within offset values
		if ( (histLnaHTemp[countH-1] < setpoint + _settings.constants.TS_MaxOffset) && (histLnaHTemp[countH-1] > setpoint - _settings.constants.TS_MaxOffset) )
		{	// test that min & max values are within the stability range
			if ( max_array(histLnaHTemp, countH) - min_array(histLnaHTemp, countH) <= _settings.constants.TS_StabilityDelta )
			{
				warmOps = true;
			}
		}
		return warmOps;
	}
	return false; // Not enough data, so return false
}

/**
 *  WOErrorTempCheck() returns true if the LNA temperatures are larger than the Warm Operational Error temperature
 */
bool CheckController::WOErrorTempCheck()
{
	int vote = 0;
	int countH = MIN_COMPARED;
	int countV = MIN_COMPARED;
	double histLnaHTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default
	double histLnaVTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default

	countH = GetLnaHTempHistoryArray(histLnaHTemp, countH);
	countV = GetLnaVTempHistoryArray(histLnaVTemp, countV);

//	std::cout << "T_WarmOperationalError: " << _settings.constants.T_WarmOperationalError << std::endl;

	if ( (countH == countV) && (countH > 0)) // check equal lengths and if the amount requested is the amount returned
	{
		AverageLnaArrays(histLnaHTemp, histLnaVTemp, countH);
		for (int i = 0; i < countH; i++)
		{
			if (histLnaHTemp[i] > _settings.constants.T_WarmOperationalError)
			{
//				std::cout << "histLnaHTemp: " << histLnaHTemp[i] << std::endl;
				vote++;
			}
		}
		return ((vote == countH) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  WOfloatTempCheck() returns true if the LNA temperatures are within the Warm Operational floating temperature range
 */
bool CheckController::WOfloatTempCheck()
{
	int vote = 0;
	int countH = MIN_COMPARED;
	int countV = MIN_COMPARED;
	double histLnaHTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default
	double histLnaVTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default

	countH = GetLnaHTempHistoryArray(histLnaHTemp, countH);
	countV = GetLnaVTempHistoryArray(histLnaVTemp, countV);

	if ( (countH == countV) && (countH > 0)) // check equal lengths and if the amount requested is the amount returned
	{
		AverageLnaArrays(histLnaHTemp, histLnaVTemp, countH);
		for (int i = 0; i < countH; i++)
		{
			if ( (histLnaHTemp[i] > _settings.constants.T_WarmOperational - _settings.constants.TS_MaxOffset) && (histLnaHTemp[i] <= _settings.constants.T_WarmOperationalError) )
			{
				vote++;
			}
		}
		return ((vote == countH) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  LnaTempNonOperational() returns true if the LNA temperatures are larger than the maximum Operational temperature
 */
bool CheckController::LnaTempNonOperational()
{
	int vote = 0;
	int countH = MIN_COMPARED;
	int countV = MIN_COMPARED;
	double histLnaHTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default
	double histLnaVTemp[GRADIENT_CHECK_WINDOW]  = {0.0}; // Initialise all struct vars to their type-default

	countH = GetLnaHTempHistoryArray(histLnaHTemp, countH);
	countV = GetLnaVTempHistoryArray(histLnaVTemp, countV);

	if ( (countH == countV) && (countH > 0)) // check equal lengths and if the amount requested is the amount returned
	{
		AverageLnaArrays(histLnaHTemp, histLnaVTemp, countH);
		for (int i = 0; i < countH; i++)
		{
			if (histLnaHTemp[i] > _settings.constants.T_NonOperational)
			{
				vote++;
			}
		}
		return ((vote == countH) ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  cryoPressureIncrease() returns 1 if the average cryostat pressure time
 *  derivative is more than 0
 */
bool CheckController::CryoPressureIncrease()
{
	int count = GRADIENT_CHECK_WINDOW;
	double histCryoP[GRADIENT_CHECK_WINDOW];

//	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		return (logPressureDt(histCryoP, count) > 0.0 ? true : false);
	}
	return false; // Not enough data, so return false
}
/**
 *  cryoPressureDecrease() returns 1 if the cryostat pressure time derivative
 *  is less than 0
 */
bool CheckController::CryoPressureDecrease()
{
	int count = GRADIENT_CHECK_WINDOW;
	double histCryoP[GRADIENT_CHECK_WINDOW];

//	count = GetCryostatPressureHistoryArray(histCryoP, count);

	if (count > 0)
	{
		return (logPressureDt(histCryoP, count) < 0.0 ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 *  manifPressureDecrease() returns true if the manifold pressure time derivative
 *  is less than 0
 */
bool CheckController::ManifoldPressureDecrease()
{
	int count = GRADIENT_CHECK_WINDOW;
	double histManifP[GRADIENT_CHECK_WINDOW];

//	count = GetManifoldPressureHistoryArray(histManifP, count);

	if (count > 0) // if the amount requested is the amount returned
	{
		return (logPressureDt(histManifP, count) < 0.0 ? true : false);
	}
	return false; // Not enough data, so return false
}

/**
 * logPressureDt() determines the time derivative of the log values of the
 * pressures in the PArray, averaged over aLen samples.
 */
double CheckController::logPressureDt (double PArray[], int count)
{
	double avgPDt = 0;
	for (int i = 1; i < count; i++)
	{
		avgPDt += log10(PArray[i]) - log10(PArray[i - 1]);
	}
	return avgPDt/(count - 1);
}

/**
 * tempDt() determines the time derivative of the temperature values
 * in the TArray, averaged over aLen samples.
 */
double CheckController::tempDt (double TArray[], int count)
{
	double avgTDt = 0;
	for (int i = 1; i < count; i++)
	{
		avgTDt += TArray[i] - TArray[i - 1];
	}
	return avgTDt/(count - 1);
}

}


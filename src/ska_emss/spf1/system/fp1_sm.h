//****************************************************************************
// Model: Band1Model.qm
// File:  ./fp1_sm.h
//
// This code has been generated by QM tool (see state-machine.com/qm).
// DO NOT EDIT THIS FILE MANUALLY. All your changes will be lost.
//
// This program is open source software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//****************************************************************************
//${.::fp1_sm.h} .............................................................
#ifndef fp1_sm_h
#define fp1_sm_h

#include <bitset>
#include "qpcpp.h"
#include "fpc_if.h"
#include "IniParser.h"
#include "DataFrame.h"
#include "check_ctrl.h"
#include "SensorLogger.h"
#include "CircularFifo.h"

#include "system.h"

#include <string>
#include <unistd.h>
#include <chrono>
#include <ctime>
using namespace std::chrono;

using namespace util;
using namespace std;

namespace FP1
{
    struct stFeedConfig
    {
        string serial;
        string serialLnaH;
        string serialLnaV;

        double meanGainH;
        double meanGainV;

        double lnaHVd[3];
        double lnaHId[3];
        double lnaVVd[3];
        double lnaVId[3];
    };

    enum class enControllerState : int
    {
        None = 0,
        Off,
        Standby,
        Operate,
        Unavailable,
        Available,
        WarmOperational,
        WarmOpsFloat,
        Degraded,
        Maintenance,
        Error,
    };

    enum FP1Signals
    {
        OPERATIONAL_SIG = QP::Q_USER_SIG,
        /* Timer events */
        PING_FEED_SIG,
        VALVE_OPEN_CHECK_SIG,
        CHECK_SIG,
        DEC_CHECK_SIG,
        CT1_ERROR_SIG,
        CT2_ERROR_SIG,
        AMB_ERROR_SIG,
        WATCHDOG_SIG,
        VAC_CHECK_SIG,
        ERROR_SIG,
        SAMPLE_SENSORS_SIG,

        NEXT_SIG,

        WARM_OPS_SIG,
        ITC_SIG,
        SHUTDOWN_SIG,
        CRYOSTAT_VACUUM_SIG,
        CRYO_VAC_READY_SIG,

        PID_LNA_H_ON_SIG,
        PID_LNA_H_OFF_SIG,
        PID_LNA_V_ON_SIG,
        PID_LNA_V_OFF_SIG,
        PID_LNA_SP_WARM_SIG,
        PID_CAL_SOURCE_ON_SIG,
        PID_CAL_SOURCE_OFF_SIG,
        CAL_SOURCE_ON_SIG,
        CAL_SOURCE_OFF_SIG,

        RFE2_ON_SIG,
        RFE2_OFF_SIG,
        CRYO_PUMP_SIG,
        CRYO_PUMP_READY_SIG,
        REGENERATE_SIG,
        RGN_TEST_SIG,
        RGN_FINISHED_SIG,
        ERROR_CORRECTED_SIG,
        TEMP_RISE_SIG,
        VACUUM_LOST_SIG,
        RETRY_VAC_SIG,
        SOFT_OFF_SIG,
        SELF_CHECK_SIG,
        ERRORS_CLEARED_SIG,
        LNA_H_ON_SIG,
        LNA_H_OFF_SIG,
        LNA_V_ON_SIG,
        LNA_V_OFF_SIG,
        LNA2_H_ON_SIG,
        LNA2_H_OFF_SIG,
        LNA2_V_ON_SIG,
        LNA2_V_OFF_SIG,
        OFFLINE_SIG,
        VAC_FAIL_SIG,
        HE_COMPR_FAIL_SIG,
        RESET_SIG,

        TEST_SERVICE_SIG,

        MAX_PUB_SIG,
        IGNORE_SIG,
        MAX_SIG
    };

    // BSP functions to dispaly a message and exit
    void BSP_Display(char_t const *msg);
}

namespace FP1 {


#if ((QP_VERSION < 580) || (QP_VERSION != ((QP_RELEASE^4294967295) % 0x3E8)))
#error qpcpp version 5.8.0 or higher required
#endif

//${AOs::FP1AO} ..............................................................
class FP1AO : public QP::QActive {
private:
    QP::QTimeEvt pingFeedTimer;
    QP::QTimeEvt valveOpenCheckTimer;
    QP::QTimeEvt checkTimer;
    QP::QTimeEvt decrementCheckTimer;
    QP::QTimeEvt cryopumpErrorTimer;
    QP::QTimeEvt coldOperationalErrorTimer;
    QP::QTimeEvt ambientErrorTimer;
    QP::QTimeEvt watchdogTimer;
    QP::QTimeEvt vacCheckTimer;
    FpcProtocol * fpInterface;
    IniParser * configFile;
    string configFilePath;
    QP::QTimeEvt errorTimer;
    CheckController* checkControl;
    static CircularFifo<stFeedDataFrame, HIST_BUFFER_SIZE> systemData;
    bool coldTransitionFlag;
    int16_t degradedReason;
    enDeviceStatus deviceStatus;
    enControllerState state;

public:
    stFeedConfig feedConfig;

private:
    uint16_t warnCnt;
    QP::QTimeEvt sampleSensorsTimer;

public:
    stSettings* settings;

private:
    uint16_t setpointLnaH;
    bool lnaPidHOn;

public:
    bool feedDetected;

private:
    enDeviceMode deviceMode;

public:
    string fwVersion;

private:
    enCapabilityState capabilityState;
    SensorLogger* sensorLog;
    int8_t offlineThreshold;
    string currentState;
    unErrorFlags errorFlags;

public:
    int lnaHPidTempSetpoint;
    int calSourcePidTempSetpoint;

private:
    bool lnaPidVOn;

public:
    int lnaVPidTempSetpoint;

private:
    uint16_t setpointLnaV;
    IniParser * settingsFile;

public:
    string spf1Location;
    string ttyPort;
    short lastError;

public:
    FP1AO();
    enDeviceStatus GetDeviceStatus();
    enControllerState GetState();
    stFeedDataFrame GetDataFrame();
    bool SendRawCommand(string command, string & response);

private:
    void SwitchAllOff();

public:
    enDeviceMode GetFeedMode();
    bool SetLnaHTempSetpoint(int kelvin);
    bool SetLnaVTempSetpoint(int kelvin);
    enCapabilityState GetCapabilityState();
    string GetCurrentState();
    bool SetCalSourceTempSetpoint(int kelvin);
    void SetSettings(stSettings* setSettings);

protected:
    static QP::QState initial(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState Active(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState Operational_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState Degraded_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState WarmOps_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState WarmTransient_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState WarmOperational_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState TempDiff_check(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState OffLine_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState Diagnostic_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState IntegrationTestControl_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState SelfCheck_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState CheckStartUp_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState SoftOff_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState Error_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState DefinedErrors_state(FP1AO * const me, QP::QEvt const * const e);
    static QP::QState Destruct(FP1AO * const me, QP::QEvt const * const e);
};

} // namespace FP1

namespace FP1
{
    extern FP1AO * const fp1AO;
}


#endif // fp1_sm_h

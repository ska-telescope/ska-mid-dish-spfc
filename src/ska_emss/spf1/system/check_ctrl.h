/*
 * check_ctrl.h
 *
 *  Created on: 23 May 2017
 *      Author: theuns
 */

#ifndef SYSTEM_CHECK_CTRL_H
#define SYSTEM_CHECK_CTRL_H

#include "CircularFifo.h"
#include "fpc_if.h"

#define MIN_COMPARED 2
#define STABILITY_WINDOW	30		// time frame in seconds for RFE1 stability check
#define GRADIENT_CHECK_WINDOW 20

// TODO: This needs to be defined elsewhere?
#define TEMP_SP_WARM 313

/* user defined variable names & default values */
// Services
#define N_SD_CARD					"LogToSDcard"
#define VAL_SD_CARD					1
// Variables
#define INI_VARS					"Variables"
#define N_EXPECTED_ONLINE			"expected_online"
#define VAL_EXPECTED_ONLINE			1
#define N_STARTUP_DEFAULT			"startup_default"
#define VAL_STARTUP_DEFAULT			0
#define N_TS_LNA_H					"TS_LnaH"
#define VAL_TS_LNA_H				283
#define N_TS_LNA_V					"TS_LnaV"
#define VAL_TS_LNA_V				283
#define N_TS_CALSOURCE				"TS_CalSource"
#define VAL_TS_CALSOURCE			330
// Constants
#define INI_CONST					"Constants"
#define N_T_WARM_OPERATIONAL		"T_WarmOperational"
#define VAL_T_WARM_OPERATIONAL		303
#define N_T_WARM_OPERATIONAL_E		"T_WarmOperationalError"
#define VAL_T_WARM_OPERATIONAL_E	315
#define N_T_NON_OPERATIONAL			"T_NonOperational"
#define VAL_T_NON_OPERATIONAL		320
#define N_TS_MAX_OFFSET				"TS_MaxOffset"
#define VAL_TS_MAX_OFFSET			1.0
#define N_TS_STABILITY_DELTA		"TS_StabilityDelta"
#define VAL_TS_STABILITY_DELTA		0.2
#define N_T_DELTA_E					"T_DeltaError"
#define VAL_T_DELTA_E				2.0
// Timeouts
#define INI_TIMEOUTS				"Timeouts"
#define N_TO_LNA_TEMP_INC			"Timeout_LnaTemperatureIncrease"
#define VAL_TO_LNA_TEMP_INC			180
#define N_TO_WARM_OPS_TEMP			"Timeout_WarmOpsTemp"
#define VAL_TO_WARM_OPS_TEMP		7200



struct stVariables
    {
        bool expected_online;
        enStartupDefault startup_default;
        int TS_LnaH; // Temperature setpoint in Kelvin
        int TS_LnaV; // Temperature setpoint in Kelvin
        int TS_CalSource; // Temperature setpoint in Kelvin
    };

    struct stConstants
    {
        double T_WarmOperational;
        double T_WarmOperationalError;
        double T_NonOperational;
        double TS_MaxOffset;
        double TS_StabilityDelta;
        double T_DeltaError;
        double T_Offset_H;
        double T_Offset_V;
    };

    struct stTimeouts
    {
        unsigned int Timeout_LnaTemperatureIncrease;
        unsigned int Timeout_WarmOpsTemp;
    };

    struct stSettings
    {
        stVariables variables;
        stConstants constants;
        stTimeouts  timeouts;
    };


namespace FP1
{
class CheckController
{
public:
	CheckController(stSettings settings, CircularFifo<stFeedDataFrame, 60> * buffPtr);
	virtual ~CheckController();
	bool ActiveSamples(int sampleCount);

	stSettings _settings;

	bool CheckSpdHealth();
	bool IsLnaHTempSafe();
	bool IsLnaVTempSafe();
	bool IsCalSourceTempSafe();

	bool TempDiffMax();
	bool isLnaTempControllable();
	bool LnaTempIncrease();
	bool WarmTransStabilityCheck();
	bool WOTempCheck(bool lnaPidOn, int setpoint);
	bool WOErrorTempCheck();
	bool WOfloatTempCheck();
	bool LnaTempNonOperational();
	bool CryoPressureIncrease();
	bool CryoPressureDecrease();
	bool ManifoldPressureDecrease();

	int GetLnaHTempHistoryArray(double array[], int count);
	int GetLnaVTempHistoryArray(double array[], int count);
	int GetSpdHistoryArray(double array[], int count);
	void AverageLnaArrays(double array1[], double array2[], int count);

private:
	double logPressureDt(double PArray[], int count);
	double tempDt(double TArray[], int count);

	CircularFifo<stFeedDataFrame, 60> * _buffPtr; // Reference to a circular buffer //FIXME

};
}

#endif /* SYSTEM_CHECK_CTRL_H */

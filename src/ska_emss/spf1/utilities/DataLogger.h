/*
 * DataLogger.h
 *
 *  Created on: 26 Jun 2017
 *      Author: theuns
 */

#ifndef UTILITIES_DATALOGGER_H_
#define UTILITIES_DATALOGGER_H_

#include <string>

class DataLogger
{
public:
	~DataLogger();

	static bool AppendLine(std::string fileName, std::string line);

private:
	DataLogger();
};

#endif /* UTILITIES_DATALOGGER_H_ */

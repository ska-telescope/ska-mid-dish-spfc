/*
 * utilities.h
 *
 *  Created on: 14 Mar 2017
 *      Author: theuns
 */

#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <string.h>
#include <sstream>
#include <vector>
#include <iterator>
#include <iomanip>

namespace util
{
	std::vector<std::string> split(const std::string &s, char delim);
	std::string trim(const std::string &s);

	/* Templates are defines in the header file */
	template<typename T>
	void split(const std::string &s, char delim, T result)
	{
		std::stringstream ss;
		ss.str(s);
		std::string item;

		while (std::getline(ss, item, delim))
		{
			if (!item.empty()) /* Ignore empty items */
			{
				*(result++) = item;
			}
		}
	}

	template<typename T> std::string numberToHexString( T i, int numChars )
	{
	  std::stringstream stream;
	  stream << std::setfill ('0') << std::setw(numChars) << std::uppercase << std::hex << i;
	  return stream.str();
	}

	template<typename T> std::string numberToFixedString( T i, int numChars )
	{
	  std::stringstream stream;
	  stream << std::setfill ('0') << std::setw(numChars) << i;
	  return stream.str();
	}

	template<typename T> std::string NumberToString(T number, int decimals = 0)
	{
		std::stringstream stream;
		stream << std::fixed << std::setprecision(decimals) << number;
		return stream.str();
	}

	template<typename T> std::string NumberToSciString(T number, int decimals = 0)
	{
		std::stringstream stream;
		stream << std::fixed << std::setprecision(decimals) << std::scientific << number;
		return stream.str();
	}

	template<typename T>
	T max_array(T a[], int count)
	{
	   int i;
	   T max = a[0];
	   for (i = 1; i < count; i++)
	   {
		 if (a[i] > max)
		 {
			max = a[i];
		 }
	   }
	   return(max);
	}

	template<typename T>
	T min_array(T a[], int count)
	{
	   int i;
	   T min = a[0];
	   for (i = 1; i < count; i++)
	   {
		 if (a[i] < min)
		 {
			min = a[i];
		 }
	   }
	   return(min);
	}

} /* namespace util */

#endif /* UTILITIES_H_ */

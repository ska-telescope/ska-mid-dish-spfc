/*
 * fpc_if.h
 *
 *  Created on: 15 May 2017
 *      Author: theuns
 */

#ifndef FPC_IF_H_
#define FPC_IF_H_

#include <map>
#include <string.h>

#include "Digital.h"
#include "SerialPort.h"

using namespace std;
using namespace serial;

#define HIST_BUFFER_SIZE 60 // 60 seconds of historical data
#define CRYO_MOTOR_DEFAULT_SPEED 60 //60 RPM

enum class enStartupDefault : short
{
	STANDBY = 0,
	OPERATE,
	MAINTENANCE
};

enum class enCapabilityState : short
{
	UNAVAILABLE = 0,
	STANDBY,
	OPERATE_DEGRADED,
	OPERATE_FULL
};

enum class enDeviceStatus : short
{
   UNKNOWN = 0,
   NORMAL = 1,
   DEGRADED = 2,
   FAILED = 3,
};

enum class enDeviceMode : short
{
    OFF = 0,
    STARTUP,
    STANDBY_LP,
    OPERATE,
    MAINTENANCE,
	ERROR,
};

typedef struct _stFeedSensors
{
	double 	controllerVoltage;
	double 	controllerCurrent;
	double 	controllerTemperature;
	double 	psu9VRectifiedVoltage;
	double 	psuAnalogue6vRegulatorTemp;
	double 	psuAnalogue15vRegulatorTemp;
	double 	psuDigital6vRegulatorTemp;
	double 	lnaHDrainVoltage;
	double 	lnaHDrainCurrent;
	double 	lnaHGateVoltage;
	double 	lnaVDrainVoltage;
	double 	lnaVDrainCurrent;
	double 	lnaVGateVoltage;
	double 	amp2HVoltage;
	double 	amp2HCurrent;
	double 	amp2VVoltage;
	double 	amp2VCurrent;
	double 	calsourceVoltage;
	double 	calsourceCurrent;
	double 	calsourceTemp;
	double 	pidCalsourceCurrent;
	double 	pidLnaHCurrent;
	double 	pidLnaVCurrent;
	double 	chamberBodyTemp;
	double 	b1AnalogueInterfaceTemp;
	double 	lnaHTemp;
	double 	lnaVTemp;
	double 	spdHealth;
} stFeedSensors;

enum class enFpcMode : short
{
	UNKNOWN = 0,
	APPLICATION = 1,
	MAINTENANCE = 2
};

enum class enPid : short
{
	NONE = 0,
	CALSOURCE = 1,
	LNA_H = 2,
	LNA_V = 3
};

enum class enMemoryRecord : short
{
	NONE = 0,
	CONFIG = 1,
	REC_GAIN = 2,
	REC_NOISE = 3,
	CAL_NOISE = 4
};

enum class enElapsedTimeCounter : short
{
	NONE = 0,
	OPERATIONAL = 1,
};

enum class enErrorBits : short
{
	INITIALISE_ERROR		= 0,
	SELF_CHECK				= 1,
	DELTA_T_LNA				= 2,
	NON_OPS_TEMP			= 3,
	TIMEOUT_LNA_TEMP_INC	= 4,
	TIMEOUT_WARM_OPS_TEMP	= 5,
	SPD_TRIGGERED			= 6
};

typedef union _unErrorFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	InitialiseError				: 1;
		unsigned	SelfCheck					: 1;
		unsigned	DeltaT_LNA					: 1;
		unsigned	NonOpsTemp					: 1;
		unsigned	TimeoutLnaTempIncrease		: 1;
		unsigned	TimeoutWarmOpsTemp			: 1;
		unsigned	SPARE						: 10;	// <- 16-bits
	}bits;
}unErrorFlags;

#define MAX_ERRORS 16

struct error_enumtypes
{
	enErrorBits eNum;
	char* eName;
	char* eDescr;
};

static const struct error_enumtypes rxErrorNames[MAX_ERRORS] = {
		{enErrorBits::INITIALISE_ERROR, 		(char*)"E_Intitialise", 			(char*)"Initialisation error"},
		{enErrorBits::SELF_CHECK,				(char*)"E_Selfcheck", 				(char*)"Detected error while sensors were self-checke"},
		{enErrorBits::DELTA_T_LNA,				(char*)"E_DeltaT_LNA",				(char*)"Delta T between LNAs too large"},
		{enErrorBits::NON_OPS_TEMP,				(char*)"E_NonOpsTemp",				(char*)"Operational temperature exceeded"},
		{enErrorBits::TIMEOUT_LNA_TEMP_INC,		(char*)"E_Timeout_LNATempIncrease",	(char*)"Timeout: LNA temperature not controlled as expected"},
		{enErrorBits::TIMEOUT_WARM_OPS_TEMP,	(char*)"E_Timeout_WarmOpsTemp", 	(char*)"Timeout: Warm operational temperature not reached"},
		{enErrorBits::SPD_TRIGGERED,			(char*)"E_SurgeProtectionDevice",	(char*)"Surge protection device failure triggered"},
};

enum class enControlBitNumber : short
{
	LNA_H				= 0,
	LNA_V				= 1,
	AMP2_H				= 2,
	AMP2_V				= 3,
	SPARE1				= 4,
	SPARE2				= 5,
	CALSOURCE			= 6,
	PID_CALSOURCE		= 7,
	PID_LNA_H			= 8,
	PID_LNA_V			= 9
};

typedef union _unControlFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	LnaH				: 1;
		unsigned	LnaV				: 1;
		unsigned	Amp2H				: 1;
		unsigned	Amp2V				: 1;
		unsigned	Spare1				: 1;
		unsigned	Spare2				: 1;
		unsigned	CalSource			: 1;
		unsigned	PidCalSource		: 1;
		unsigned	PidLnaH				: 1;
		unsigned	PidLnaV				: 1;
		unsigned	SPARE				: 6;	// <- 16-bits
	}bits;
}unControlFlags;

typedef struct _stPidSettings
{
	int 	proportional;
	double 	integral;
	double  derivative;
	//int 	temp;
} stPidSettings;

// TODO: Better to do this in array?
typedef struct _stLnaBiasParameters
{
	int drainVolt;
	int drainCurrent;
} stLnaBiasParameters;

typedef union _unValidityFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	sensors				: 1;
		unsigned	vacuumValve			: 1;
		unsigned	cryoMotor			: 1;
		unsigned	controlRegister		: 1;
		unsigned	errorRegister		: 1;
		unsigned	lnaHBias			: 1;
		unsigned	lnaVBias			: 1;
		unsigned	pidCalSource		: 1;
		unsigned	pidLnaH				: 1;
		unsigned	pidLnaV				: 1;
		unsigned 	rtc					: 1;
		unsigned	eltOperational		: 1;
		unsigned	SPARE				: 4;	// <- 16-bits
	}bits;
}unValidityFlags;

typedef struct _stFeedDataFrame
{
	bool enabled;
	bool expectedOnline;

	double timestamp;
	stFeedSensors sensors;
	unControlFlags controlFlags;
	unErrorFlags errorFlags;
	short lastError;

	int elapsedOperational;

	enDeviceStatus deviceStatus;
	enFpcMode fpcMode;
	enDeviceMode currentMode;

	short pidLnaHTempSetPoint;
	short pidLnaVTempSetPoint;
	short pidCalSourceTempSetPoint;

	unValidityFlags validities;

} stFeedDataFrame;

class FpcProtocol
{
public:
	FpcProtocol();
	FpcProtocol(const string PortName);
	~FpcProtocol();

	bool Init();
	bool Reset();

	/* LED */
	bool SetOnlineLed();
	bool ClearOnlineLed();

	/* =================== */
	/* Functional commands */
	/* =================== */
	bool DownloadConfigFile();

	bool LnasOn();
	bool LnaOff();
	bool LnaHOn();
	bool LnaHOff();
	bool LnaVOn();
	bool LnaVOff();

	bool Amp2On();
	bool Amp2Off();
	bool Amp2HOn();
	bool Amp2HOff();
	bool Amp2VOn();
	bool Amp2VOff();

	bool CalSourceOn();
	bool CalSourceOff();

	bool PidLnaHOn();
	bool PidLnaHOff();
	bool PidLnaVOn();
	bool PidLnaVOff();
	bool PidCalSourceOn();
	bool PidCalSourceOff();

	bool ClearAllErrors();
	bool SetError(enErrorBits eb);
	bool ClearError(enErrorBits eb);


	/* =============== */
	/* Serial commands */
	/* =============== */

	/* Sensor Monitoring */
	bool SampleAll(stFeedSensors & sensors);
	bool SampleSingleSensor(unsigned int channelNumber, double& value);
	bool SelectChannels(std::vector<string> channels);
	bool SampleSelectedChannels(stFeedSensors & sensors); // Not all parameters are populated in struct
	bool SetAllChannels();
	bool EnableSensorAveraging();
	bool DisableSensorAveraging();

	/* Control */
	bool ModifyControlRegister(unControlFlags controlFlags);
	bool ModifyControlBit(enControlBitNumber controlBit, unsigned int value);
	bool GetControlRegister(unControlFlags & controlFlags);
	bool GetControlBit(unsigned int controlBit, unsigned int & value);

	/* PID Controllers */
	bool SetPidPropGain(enPid pid, int gain);
	bool SetPidIntegral(enPid pid, double integral);
	bool SetPidDerivative(enPid pid, double derivative);
	bool SetPidTempSetPoint(enPid pid, short temp);
	bool GetPidTempSetPoint(enPid pid, short & temp);
	bool SavePidCoeffs(enPid pid);
	bool GetPidCoeffs(enPid pid, stPidSettings & pidSettings);

	/* LNA Biasing */
	bool SetLnaHBias(stLnaBiasParameters biasParameters);
	bool SetLnaVBias(stLnaBiasParameters biasParameters);
	bool GetLnaHBias(stLnaBiasParameters & biasParameters);
	bool GetLnaVBias(stLnaBiasParameters & biasParameters);

	/* Firmware */
	bool GetFirmwareVersion(string & version);
	/* Serial */
	bool GetSerialNumber(string & serial);

	/* Memory Storage */
	bool EraseRecord(enMemoryRecord record);
	bool ReadRecord(enMemoryRecord record, string fileName);
	bool ReadRecordNew(enMemoryRecord record, string fileName);
	bool WriteRecord(enMemoryRecord record, string fileName);

	/* Error Flags */
	bool ModifyErrorRegister(unErrorFlags errorFlags);
	bool ModifyErrorBit(unsigned int errorBit, unsigned int value);
	bool GetErrorRegister(unErrorFlags & errorFlags);
	bool GetErrorBit(unsigned int errorBit, unsigned int & value);

	/* Real-time clock */
	bool SetSystemTime(unsigned int time);
	bool GetSystemTime(unsigned int & time);

	/* Elapsed time counter */
	bool GetElapsedTime(enElapsedTimeCounter counterChannel, int & time);
	bool ResetElapsedTime(enElapsedTimeCounter counterChannel);

	/* State and Mode Control */
	bool RemainMaintenance();
	bool GoApplication();
	bool GoUpdate();
	bool GoMaintenance();
	bool GetCurrentMode(enFpcMode & mode);

	bool SendRawCommand(string command, string & response);

private:
	string portName;
	Digital* serialTX;
	Digital* serialRX;
	Serial* serialPort;
	Digital * ioOutLedOnline;

	bool SendSimpleCommand(string command);
	bool ValidateResponse(string response);
	bool ParseSampleAll(string response, stFeedSensors & sensors);
	bool ParseSampleSingle(string response, unsigned int & channelNumber, double & value);
	bool ParseGeneralSingle(string response, string & parsedValue);
	bool ParseGeneralDouble(string response, string & parsedParameter, string & parsedValue);
	bool ParseGeneralMultipleInt(string response, map<int, int> & returnData);
	bool ParseGeneralMultipleDouble(string response, map<int, double> & returnData);
	string GetLineData(string line);
	bool LineDataDone(string line);
};

#endif /* FPC_IF_H_ */

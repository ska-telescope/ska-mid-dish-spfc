/*
 * Digital.h
 *
 *  Created on: 14 Dec 2016
 *      Author: theuns
 */

#ifndef BSP_DIGITAL_H_
#define BSP_DIGITAL_H_

#include <string>

class Digital
{
public:
	Digital(int gpio, std::string pinName);
	~Digital();

	/* Operations */
	int SetDir(int dir, int initialVal = 0); // Set GPIO Direction
	int SetActiveLow(bool state);
	int SetVal(int val); // Set GPIO Value (putput pins)
	int GetVal(int& val); // Get GPIO Value (input/ output pins)
	int GetPinNumber(); // return the GPIO number associated with the instance of an object

private:
	int Export(); // exports GPIO
	int Unexport(); // unexport GPIO
	int gpionum; // GPIO number associated with the instance of an object
	std::string pinName;

	int valuefd;
	int directionfd;
	int activelowfd;
	int exportfd;
	int unexportfd;

};

#endif /* BSP_DIGITAL_H_ */

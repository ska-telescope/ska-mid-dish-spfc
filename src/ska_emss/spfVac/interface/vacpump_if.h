/*
 * VacuumPump.h
 *
 *  Created on: 21 Feb 2017
 *      Author: theuns
 */

#ifndef VACPUMP_IF_H_
#define VACPUMP_IF_H_

#include "Digital.h"

struct stVariables
{
	bool expected_online;
	unsigned short vac_req_user;
	unsigned short vac_req_b1;
	unsigned short vac_req_b2;
	unsigned short vac_req_b345;
	unsigned short vac_req_bS;
	unsigned short vac_req_bKu;
};

struct stTimeouts
{
	unsigned int Timeout_PurgeDuration;
	unsigned int Timeout_OverLoadRetry;
	unsigned int Timeout_MaxOnDuration;
};

struct stSettings
{
	stVariables variables;
	stTimeouts  timeouts;
};

enum class enDeviceStatus : short
{
   UNKNOWN = 0,
   NORMAL = 1,
   DEGRADED = 2,
   FAILED = 3,
};

enum class enDeviceMode : short
{
    OFF = 0,
    STARTUP,
    STANDBY_LP,
    OPERATE,
    MAINTENANCE,
	ERROR
};

enum enVacPumpStatus : int
{
	OFFLINE = 0,
	ONLINE = 1, // Not ready
	READY = 2,	// Not running
	RUNNING = 3
};

enum enVacPumpControl : int
{
	NONE = 0,
	ON = 1,
	OFF = 2,
	PURGE_ON = 3,
	PURGE_OFF = 4
};

enum class enVacPumpLeds : short
{
	RUNNING = 0,
	READY = 1
};

enum class _enSetRequest : short
{
	USER = 0,
	B1,
	B2,
	B345,
	Bs,
	Bku
};
typedef _enSetRequest _enSetRequest;

class VacuumPump
{
public:
	VacuumPump();
	~VacuumPump();

	bool Init();

	bool SetLedStatus(enVacPumpLeds led);
	bool ClearLedStatus(enVacPumpLeds led);

	/* Status */
	bool GetStatus(enVacPumpStatus & status);
	bool IsPurging();

	/* Control functions */
	bool On();
	bool Off();
	bool PurgeOn();
	bool PurgeOff();
	bool None();

private:
	Digital* ioInLine1; // MPO_10
	Digital* ioInLine2; // MPO_12

	Digital* ioOutValve; // MPO_9
	Digital* ioOutOnOff; // MPO_11

	Digital* ioOutLedReady;
	Digital* ioOutLedRunning;

	bool Control(enVacPumpControl ctrl);

};

#endif /* VACPUMP_IF_H_ */

/*
 * VacuumPump.cpp
 *
 *  Created on: 21 Feb 2017
 *      Author: theuns
 */
#define _GLIBCXX_USE_NANOSLEEP 1

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <chrono>
#include <thread>
#include <stdio.h>
#include <string.h>

#include "vacpump_if.h"
#include "system.h"

/* PIN_VACPUMP_ON
 * PIN_VACPUMP_OFF
 * PIN_VACPUMP_READY
 * PIN_VACPUMP_RUNNING
 */

using namespace std;

VacuumPump::VacuumPump()
{
	ioInLine1 = new Digital(PIN_VACPUMP_READY, N_PIN_VACPUMP_READY); // MPO_10
	ioInLine2 = new Digital(PIN_VACPUMP_RUNNING, N_PIN_VACPUMP_RUNNING); // MPO_12

	ioOutValve = new Digital(PIN_VACPUMP_VALVE, N_PIN_VACPUMP_VALVE); // MPO_9 (fxTxFixed => MPO_11)
	ioOutOnOff = new Digital(PIN_VACPUMP_ON_OFF, N_PIN_VACPUMP_ON_OFF); // MPO_11 (fxTxFixed => MPO_9)

	ioOutLedReady = new Digital(LED_VACPUMP_READY, N_LED_VACPUMP_READY);
	ioOutLedRunning = new Digital(LED_VACPUMP_RUNNING, N_LED_VACPUMP_RUNNING);
}

VacuumPump::~VacuumPump()
{
	cout << "VACPUMP HW DESTRUCT" << endl;

	delete ioInLine1;
	delete ioInLine2;
	delete ioOutValve;
	delete ioOutOnOff;

	delete ioOutLedReady;
	delete ioOutLedRunning;
}

bool VacuumPump::Init()
{

#ifndef SIMULATOR

	char uBootVer[1024];
	FILE *fp;
	int fxTxFixed = 0;

	/* This code using a 'system' command 'strings' is not compatible when compiled into the latest c++14 code and does not use the correct ld-linux.so.3 linker */
//	fp = popen("strings /dev/mtd1 | grep U-Boot", "r");
//	fgets(uBootVer, sizeof(uBootVer), fp);
//	pclose(fp);

	/* That means that this will always be false, so the 'else' statement would always execute to the 'Fixed' version */
	if (strncmp("U-Boot 2013.01 (Sep 02 2015 - 12:46:02)", uBootVer, 38) == 0) // if original MK_factory_image
	{
		cout << "Original MK U-Boot version: " << uBootVer << endl;
	}
	else  // the fibre transmitters are fixed on newer versions of U-Boot
	{
		fxTxFixed = 1;
		cout << "Fixed Fx U-Boot version: " << uBootVer << endl;
	}

	if (ioInLine1->SetDir(1, 1) < 0)
	{
		cout << "Direction NOT set [ioInLine1]" << endl;
	}
	if (ioInLine1->SetActiveLow(true) < 0)
	{
		cout << "Active Low NOT set [ioInLine1]" << endl;
	}

	if (ioInLine2->SetDir(1, 1) < 0)
	{
		cout << "Direction NOT set [ioInLine2]" << endl;
	}
	if (ioInLine2->SetActiveLow(true) < 0)
	{
		cout << "Active Low NOT set [ioInLine2]" << endl;
	}

	/* set the purge valve properties */
	if (fxTxFixed ) // then it is reverted to active high
	{
		if (ioOutValve->SetDir(0, 0) < 0)
		{
			cout << "Direction NOT set [ioOutValve]" << endl;
		}
		if (ioOutValve->SetActiveLow(false) < 0)
		{
			cout << "Active High NOT set [ioOutValve]" << endl;
		}
	} else
	{
		if (ioOutValve->SetDir(0, 1) < 0)
		{
			cout << "Direction NOT set [ioOutValve]" << endl;
		}
		if (ioOutValve->SetActiveLow(true) < 0)
		{
			cout << "Active Low NOT set [ioOutValve]" << endl;
		}
	}

	/* set the ON/OFF line properties */
	if (fxTxFixed ) // then it is reverted to active high
	{
		if (ioOutOnOff->SetDir(0, 0) < 0)
		{
			cout << "Direction NOT set [ioOutOnOff]" << endl;
		}
		if (ioOutOnOff->SetActiveLow(false) < 0)
		{
			cout << "Active High NOT set [ioOutOnOff]" << endl;
		}
	} else
	{
		if (ioOutOnOff->SetDir(0, 1) < 0)
		{
			cout << "Direction NOT set [ioOutOnOff]" << endl;
		}
		if (ioOutOnOff->SetActiveLow(true) < 0)
		{
			cout << "Active Low NOT set [ioOutOnOff]" << endl;
		}
	}

	/* Status LEDS */
	if (ioOutLedReady->SetDir(0) < 0)
	{
		cout << "Direction NOT set [ioOutReady]" << endl;
	}
	if (ioOutLedRunning->SetDir(0) < 0)
	{
		cout << "Direction NOT set [ioOutRunning]" << endl;
	}
#endif
	return true;
}

/* Start */
bool VacuumPump::On()
{
#ifdef SIMULATOR
	ioInLine2->SetVal(1);
#endif
	return Control(enVacPumpControl::ON);
}
/* Stop */
bool VacuumPump::Off()
{
#ifdef SIMULATOR
	ioInLine2->SetVal(0);
#endif
	return Control(enVacPumpControl::OFF);
}

bool VacuumPump::PurgeOn()
{
	return Control(enVacPumpControl::PURGE_ON);
}
bool VacuumPump::PurgeOff()
{
	return Control(enVacPumpControl::PURGE_OFF);
}
bool VacuumPump::None()
{
	return Control(enVacPumpControl::NONE);
}
bool VacuumPump::IsPurging()
{
	int isPurging = -1;
	if (!ioOutValve->GetVal(isPurging))
	{
		return isPurging;
	}
	return false;
}


/* Private low level functions with bit manipulations */
bool VacuumPump::Control(enVacPumpControl ctrl)
{
	bool retVal = false;

	switch (ctrl)
	{
		case enVacPumpControl::NONE:
			retVal  = !ioOutOnOff->SetVal(0);
			retVal &= !ioOutValve->SetVal(0);
			break;

		case enVacPumpControl::OFF:
			retVal = !ioOutOnOff->SetVal(0);
			break;

		case enVacPumpControl::ON:
			retVal = !ioOutOnOff->SetVal(1);
			break;

		case enVacPumpControl::PURGE_OFF:
			retVal = !ioOutValve->SetVal(0);
			break;

		case enVacPumpControl::PURGE_ON:
			retVal = !ioOutValve->SetVal(1);
			break;

		default:
			ioOutOnOff->SetVal(0);
			ioOutValve->SetVal(0);
			retVal = false; /* This should never happen, but if it does - return false */
			break;
	}

	return retVal;
}

bool VacuumPump::GetStatus(enVacPumpStatus& status)
{
	bool retVal = false;
	int line1 = -1;
	int line2 = -1;

	retVal =  !ioInLine1->GetVal(line1);
	retVal &= !ioInLine2->GetVal(line2);

	status = (enVacPumpStatus)((line1 << 1) | (line2 & 0x1));

	/* Debug */
	///cout << "status: [" << status << "]" << endl;

	return retVal;
}

bool VacuumPump::SetLedStatus(enVacPumpLeds led)
{
	bool retVal = false;

#ifndef SIMULATOR
	switch (led)
	{
		case enVacPumpLeds::RUNNING:
			retVal  = !ioOutLedRunning->SetVal(1);
			break;

		case enVacPumpLeds::READY:
			retVal  = !ioOutLedReady->SetVal(1);
			break;

		default:
			ioOutLedReady->SetVal(0);
			ioOutLedRunning->SetVal(0);
			retVal = false; /* This should never happen, but if it does - return false */
			break;
	}
#endif
	return retVal;
}

bool VacuumPump::ClearLedStatus(enVacPumpLeds led)
{
	bool retVal = false;

#ifndef SIMULATOR
	switch (led)
	{
		case enVacPumpLeds::RUNNING:
			retVal  = !ioOutLedRunning->SetVal(0);
			break;

		case enVacPumpLeds::READY:
			retVal  = !ioOutLedReady->SetVal(0);
			break;

		default:
			ioOutLedReady->SetVal(0);
			ioOutLedRunning->SetVal(0);
			retVal = false; /* This should never happen, but if it does - return false */
			break;
	}
#endif
	return retVal;
}





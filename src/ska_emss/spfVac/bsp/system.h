/*
 * system.h
 *
 *  Created on: 15 Dec 2016
 *      Author: theuns
 */

#ifndef BSP_SYSTEM_H_
#define BSP_SYSTEM_H_

/* Pin definitions */
#define PIN_A_0 		(0 + (32 * 1))
#define PIN_A_1 		(1 + (32 * 1))
#define PIN_A_2 		(2 + (32 * 1))
#define PIN_A_3 		(3 + (32 * 1))
#define PIN_A_4 		(4 + (32 * 1))
#define PIN_A_5 		(5 + (32 * 1))
#define PIN_A_24 		(24 + (32 * 1))
#define PIN_A_30 		(30 + (32 * 1))
#define PIN_A_31 		(31 + (32 * 1))

#define PIN_B_0 		(0 + (32 * 2))
#define PIN_B_1 		(1 + (32 * 2))
#define PIN_B_2 		(2 + (32 * 2))
#define PIN_B_3 		(3 + (32 * 2))
#define PIN_B_4 		(4 + (32 * 2))
#define PIN_B_5 		(5 + (32 * 2))
#define PIN_B_6 		(6 + (32 * 2))
#define PIN_B_7 		(7 + (32 * 2))
#define PIN_B_8 		(8 + (32 * 2))
#define PIN_B_9 		(9 + (32 * 2))
#define PIN_B_10 		(10 + (32 * 2))
#define PIN_B_11 		(11 + (32 * 2))
#define PIN_B_12 		(12 + (32 * 2))
#define PIN_B_13 		(13 + (32 * 2))
#define PIN_B_14 		(14 + (32 * 2))
#define PIN_B_15 		(15 + (32 * 2))
#define PIN_B_16 		(16 + (32 * 2))
#define PIN_B_17 		(17 + (32 * 2))
#define PIN_B_18 		(18 + (32 * 2))
#define PIN_B_19 		(19 + (32 * 2))
#define PIN_B_20 		(20 + (32 * 2))
#define PIN_B_21 		(21 + (32 * 2))
#define PIN_B_22 		(22 + (32 * 2))
#define PIN_B_23 		(23 + (32 * 2))
#define PIN_B_24 		(24 + (32 * 2))
#define PIN_B_25 		(25 + (32 * 2))
#define PIN_B_26 		(26 + (32 * 2))
#define PIN_B_27 		(27 + (32 * 2))
#define PIN_B_28 		(28 + (32 * 2))
#define PIN_B_29 		(29 + (32 * 2))
#define PIN_B_30 		(30 + (32 * 2))
#define PIN_B_31 		(31 + (32 * 2))

#if 1 // fxTxFixed (post "U-Boot 2013.01 (Sep 02 2015 - 12:46:02)")
#define PIN_VACPUMP_VALVE		PIN_A_3		// swapped valve with on_off pin because of startup pulse on A3 with every SPFC reboot
#define N_PIN_VACPUMP_VALVE		"GPIOA3:"
#define PIN_VACPUMP_ON_OFF		PIN_B_0
#define N_PIN_VACPUMP_ON_OFF	"GPIOB0:"
#else
#define PIN_VACPUMP_VALVE		PIN_B_0
#define N_PIN_VACPUMP_VALVE		"GPIOB0:"
#define PIN_VACPUMP_ON_OFF		PIN_A_3
#define N_PIN_VACPUMP_ON_OFF	"GPIOA3:"
#endif

#define PIN_VACPUMP_READY		PIN_A_1
#define N_PIN_VACPUMP_READY		"GPIOA1:"
#define PIN_VACPUMP_RUNNING		PIN_A_2
#define N_PIN_VACPUMP_RUNNING	"GPIOA2:"


#define PIN_HCOMPR_nRESET		PIN_B_30
#define N_PIN_HCOMPR_nRESET		"GPIOB30:"
#define PIN_HCOMPR_RUNNING		PIN_B_31
#define N_PIN_HCOMPR_RUNNING	"GPIOB31:"



#define LED_B1_ONLINE				PIN_B_22
#define N_LED_B1_ONLINE			"GPIOB22:"
#define LED_B2_ONLINE				PIN_B_20
#define N_LED_B2_ONLINE			"GPIOB20:"
#define LED_B345_ONLINE				PIN_B_26
#define N_LED_B345_ONLINE			"GPIOB26:"
#define LED_AUX_ONLINE				PIN_B_24
#define N_LED_AUX_ONLINE			"GPIOB24:"

#define LED_VACPUMP_READY			PIN_B_13
#define N_LED_VACPUMP_READY			"GPIOB13:"
#define LED_VACPUMP_RUNNING			PIN_B_12
#define N_LED_VACPUMP_RUNNING		"GPIOB12:"
#define LED_HCOMPR_ERROR			PIN_B_21
#define N_LED_HCOMPR_ERROR			"GPIOB21:"
#define LED_HCOMPR_RUNNING			PIN_B_25
#define N_LED_HCOMPR_RUNNING		"GPIOB25:"

/* ADC Channels */
#define ADC_PCB_VOLT_CH 		0
#define ADC_PCB_CURRENT_CH 		1
#define ADC_PCB_TEMP_CH 		2

#define TIME_PATH 					"/sys/class/i2c-adapter/i2c-0/0-006b/elapsed_time"


#define BASE_PATH_SD		"/media/card"
#define BASE_PATH_LOCAL		"/var/log"

#define LOG_PATH			"/spfc/logs"


#define BASE_LIB_PATH		"/var/lib/spfc"

#define SPFC_SETTINGS_PATH		BASE_LIB_PATH "/spfc"
#define SPFC_SETTINGS			SPFC_SETTINGS_PATH "/spfc_settings.ini" /* Variables and constants for software. Variable */
#define SPFC_CONFIG				SPFC_SETTINGS_PATH "/spfc_config.ini"	/* Serial number and hardware specific settings. Fixed */

/* SPF 1 paths */
#define SPF1_SETTINGS_PATH		BASE_LIB_PATH "/spf1"
#define SPF1_SETTINGS			SPF1_SETTINGS_PATH "/spf1_settings.ini" /* Variables and constants for software. Variable */
#define SPF1_CONFIG				SPF1_SETTINGS_PATH "/spf1_config.ini"   /* Serial number and hardware specific settings. Fixed */
#define SPF1_LOG_SD				BASE_PATH_SD LOG_PATH "/spf1"			/* Logging path when SD card is available */
#define SPF1_LOG_LOCAL			BASE_PATH_LOCAL LOG_PATH "/spf1"		/* Logging path when SD card is NOT available */
/* SPF 2 paths */
#define SPF2_SETTINGS_PATH		BASE_LIB_PATH "/spf2"
#define SPF2_SETTINGS			SPF2_SETTINGS_PATH "/spf2_settings.ini" /* Variables and constants for software. Variable */
#define SPF2_CONFIG				SPF2_SETTINGS_PATH "/spf2_config.ini"   /* Serial number and hardware specific settings. Fixed */
#define SPF2_LOG_SD				BASE_PATH_SD LOG_PATH "/spf2"			/* Logging path when SD card is available */
#define SPF2_LOG_LOCAL			BASE_PATH_LOCAL LOG_PATH "/spf2"		/* Logging path when SD card is NOT available */
/* SPF 345 paths */
#define SPF345_SETTINGS_PATH	BASE_LIB_PATH "/spf345"
#define SPF345_SETTINGS			SPF345_SETTINGS_PATH "/spf345_settings.ini" /* Variables and constants for software. Variable */
#define SPF345_CONFIG			SPF345_SETTINGS_PATH "/spf345_config.ini"   /* Serial number and hardware specific settings. Fixed */
#define SPF345_LOG_SD			BASE_PATH_SD LOG_PATH "/spf345"				/* Logging path when SD card is available */
#define SPF345_LOG_LOCAL		BASE_PATH_LOCAL LOG_PATH "/spf345"			/* Logging path when SD card is NOT available */

#define SPFHE_SETTINGS_PATH		BASE_LIB_PATH "/spfhe"
#define SPFHE_SETTINGS			SPFHE_SETTINGS_PATH "/spfhe_settings.ini"	/* Variables and constants for software. Variable */
#define SPFHE_CONFIG			SPFHE_SETTINGS_PATH "/spfhe_config.ini"   	/* Serial number and hardware specific settings. Fixed */
#define SPFHE_LOG_SD			BASE_PATH_SD LOG_PATH "/spfhe"				/* Logging path when SD card is available */
#define SPFHE_LOG_LOCAL			BASE_PATH_LOCAL LOG_PATH "/spfhe"			/* Logging path when SD card is NOT available */

#define SPFVAC_SETTINGS_PATH	BASE_LIB_PATH "/spfvac"
#define SPFVAC_SETTINGS			SPFVAC_SETTINGS_PATH "/spfvac_settings.ini"	/* Variables and constants for software. Variable */
#define SPFVAC_CONFIG			SPFVAC_SETTINGS_PATH "/spfvac_config.ini"   /* Serial number and hardware specific settings. Fixed */

#define UPLOADS_PATH			BASE_LIB_PATH "/uploads"
#define RSC_SERIAL				BASE_LIB_PATH "/spfcSerial.ini"




#endif /* BSP_SYSTEM_H_ */

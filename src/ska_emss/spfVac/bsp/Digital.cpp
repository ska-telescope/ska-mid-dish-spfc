/*
 * Digital.cpp
 *
 *  Created on: 14 Dec 2016
 *      Author: theuns
 */

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "Digital.h"

using namespace std;

Digital::Digital(int gnum, std::string name):gpionum(gnum),pinName(name),valuefd(-1),directionfd(-1),activelowfd(-1),exportfd(-1),unexportfd(-1)
{
	//Instantiate GPIOClass object for GPIO pin number "gnum"
	this->Export();
}

Digital::~Digital()
{
	this->Unexport();
}

int Digital::Export()
{
	bool alreadyExported = false;
	int statusVal = -1;
	string exportStr = "/sys/class/gpio/export";

	/* Check if the pin is already enabled */
    ifstream  kernelFile("/sys/kernel/debug/gpio");
    string line;
    while( std::getline( kernelFile, line ) )
    {
        if(line.find(this->pinName) != string::npos)
        {
            alreadyExported = true;
        	cout << "SYSFS GPIO device ["<< this->pinName << "] already enabled" << endl;
            break;
        }
    }
    kernelFile.close();

    if (!alreadyExported)
    {
		this->exportfd = statusVal = open(exportStr.c_str(),  O_WRONLY|O_SYNC);
		if (statusVal < 0)
		{
			perror("could not open SYSFS GPIO export device");
			return statusVal;
		}

		string numStr = to_string(this->gpionum);

		statusVal = write(this->exportfd, numStr.c_str(), numStr.length());
		if (statusVal < 0)
		{
			perror("could not write to SYSFS GPIO export device");
			return statusVal;
		}

		statusVal = close(this->exportfd);
		if (statusVal < 0)
		{
			perror("could not close SYSFS GPIO export device");
			return statusVal;
		}
    }
    else
    {
    	statusVal = 0; //success
    }

	return statusVal;
}

int Digital::Unexport()
{
	int statusVal = -1;
	string unexportStr = "/sys/class/gpio/unexport";
	this->unexportfd = statusVal = open(unexportStr.c_str(),  O_WRONLY|O_SYNC);
	if (statusVal < 0)
	{
		perror("could not open SYSFS GPIO unexport device");
		return statusVal;
	}

	string numStr = to_string(this->gpionum);
	statusVal = write(this->unexportfd, numStr.c_str(), numStr.length());
	if (statusVal < 0)
	{
		perror("could not write to SYSFS GPIO unexport device");
		return statusVal;
	}

	statusVal = close(this->unexportfd);
	if (statusVal < 0)
	{
		perror("could not close SYSFS GPIO unexport device");
		return statusVal;
	}

	return statusVal;
}

int Digital::SetDir(int dir, int initialVal)
{
	int statusVal = -1;
	string dirStr;
	string setdirStr ="/sys/class/gpio/gpio" + to_string(this->gpionum) + "/direction";


	this->directionfd = statusVal = open(setdirStr.c_str(), O_WRONLY|O_SYNC); // open direction file for gpio
	if (statusVal < 0)
	{
		perror("could not open SYSFS GPIO direction device");
		return statusVal;
	}

	if (dir == 0)
	{
		if (initialVal == 1)
		{
			dirStr = "high";
		}
		else
		{
			dirStr = "low";
		}
	}
	else if (dir == 1)
	{
		dirStr = "in";
	}
	else
	{
		perror("Invalid direction value. Should be 1 for \"in\" or 0 for \"out\". \n");
		return statusVal;
	}

	statusVal = write(this->directionfd, dirStr.c_str(), dirStr.length());
	if (statusVal < 0)
	{
		perror("could not write to SYSFS GPIO direction device");
		return statusVal;
	}

	statusVal = close(this->directionfd);
	if (statusVal < 0)
	{
		perror("could not close SYSFS GPIO direction device");
		return statusVal;
	}

	return statusVal;
}

int Digital::SetActiveLow(bool state)
{
	int statusVal = -1;
	string setStr;
	string setActiveLowStr ="/sys/class/gpio/gpio" + to_string(this->gpionum) + "/active_low";

	this->activelowfd = statusVal = open(setActiveLowStr.c_str(), O_WRONLY|O_SYNC); // open active_low file for gpio
	if (statusVal < 0)
	{
		perror("could not open SYSFS GPIO active_low device");
		return statusVal;
	}

	if (state)
	{
		setStr = "1";
	}
	else
	{
		setStr = "0";
	}

	statusVal = write(this->activelowfd, setStr.c_str(), setStr.length());
	if (statusVal < 0)
	{
		perror("could not write to SYSFS GPIO active_low device");
		return statusVal;
	}

	statusVal = close(this->activelowfd);
	if (statusVal < 0)
	{
		perror("could not close SYSFS GPIO active_low device");
		return statusVal;
	}

	return statusVal;
}



// Function returns 0 if successful
int Digital::SetVal(int val)
{
	int statusVal = -1;
	string valStr;
#ifdef SIMULATOR
	string setValStr = "/var/lib/spfc/spfvac/sim/gpio" + to_string(this->gpionum);
//	const char* sendStr = "touch " + setValStr;
//	system(sendStr);
#else
	string setValStr = "/sys/class/gpio/gpio" + to_string(this->gpionum) + "/value";
#endif
	this->valuefd = statusVal = open(setValStr.c_str(), O_WRONLY|O_SYNC);
	if (statusVal < 0)
	{
		perror("could not open SYSFS GPIO value device");
		return statusVal;
	}

	if (val != 1 && val != 0 )
	{
		fprintf(stderr, "Invalid  value. Should be \"1\" or \"0\". \n");
		return statusVal;
	}

	valStr = to_string(val);

	statusVal = write(this->valuefd, valStr.c_str(), valStr.length());
	if (statusVal < 0)
	{
		perror("could not write to SYSFS GPIO value device");
		return statusVal;
	}

	statusVal = close(this->valuefd);
	if (statusVal < 0)
	{
		perror("could not close SYSFS GPIO value device");
		return statusVal;
	}

	return statusVal;
}

int Digital::GetVal(int& val)
{
	string tempval;
#ifdef SIMULATOR
	string getValStr = "/var/lib/spfc/spfvac/sim/gpio" + to_string(this->gpionum);
#else
	string getValStr = "/sys/class/gpio/gpio" + to_string(this->gpionum) + "/value";
#endif
	char buff[2];
	int statusVal = -1;
	this->valuefd = statusVal = open(getValStr.c_str(), O_RDONLY|O_SYNC);
	if (statusVal < 0)
	{
		perror("could not open SYSFS GPIO value device");
		return statusVal;
	}

	statusVal = read(this->valuefd, &buff, 1);
	if (statusVal < 0)
	{
		perror("could not read SYSFS GPIO value device");
		return statusVal;
	}

	buff[1]='\0';
	tempval = string(buff);

	if (tempval.compare("1") != 0 && tempval.compare("0") != 0 )
	{
		perror("Invalid  value read. Should be \"1\" or \"0\". \n");
		return statusVal;
	}

	try
	{
	    val = std::stoi(tempval) & 0x01; // Ensure we only have a bit

	    //string::size_type sz;   // alias of size_t
	    //val = stoi(tempval, &sz);
	}
	catch (exception &e)
	{
		statusVal = -1;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	statusVal = close(this->valuefd);
	if (statusVal < 0)
	{
		perror("could not close SYSFS GPIO value device");
		return statusVal;
	}

	/* Status of 0 indicates success */
	return statusVal;
}

int Digital::GetPinNumber()
{
	return this->gpionum;
}

/*
 * utilities.cpp
 *
 *  Created on: 14 Mar 2017
 *      Author: theuns
 */

#include <math.h>
#include "utilities.h"
#include "lookupTables.h"

namespace util
{
	/* Helper functions */

	std::vector<std::string> split(const std::string &s, char delim)
	{
		std::vector<std::string> elems;
		split(s, delim, std::back_inserter(elems));
		return elems;
	}

	std::string trim(const std::string &s)
	{
		std::string::const_iterator it = s.begin();
		while (it != s.end() && isspace(*it))
			it++;

		std::string::const_reverse_iterator rit = s.rbegin();
		while (rit.base() != it && isspace(*rit))
			rit++;

		return std::string(it, rit.base());
	}

	double InterpolateLinear(double x0, double y0, double x1, double y1, double y){
		double tempval;
		tempval = (x0 + (y-y0)*(x1-x0)/(y1-y0));
		return tempval;
	}

	double InterpolateLog(double x0, double y0, double x1, double y1, double y){
		double tempval;
		y  = log10(y);
		y0 = log10(y0);
		y1 = log10(y1);
		tempval = (x0 + (y-y0)*(x1-x0)/(y1-y0));
		return tempval;
	}

/**
 * tempTimeLookup() looks up and returns the time left (in seconds) until feed is available (at 15K)
 */
	double tempTimeLookup(double temp){
		int i;
		double x0, y0, x1, y1;
		x1 = tempTimeTable[0].timeLeft;
		y1 = tempTimeTable[0].lnaTemp;
		if (temp >= y1){
			return x1;
		}
		else if (temp < y1){
			for (i = 1; i < TTT_SIZE; i++){
				x0 = tempTimeTable[i].timeLeft;
				y0 = tempTimeTable[i].lnaTemp;
				if (temp == y0){ return x0; }
				else if (temp > y0){ return InterpolateLinear(x0, y0, x1, y1, temp); }
				else {
					x1 = x0;
					y1 = y0;
				}
			}
		}
		return tempTimeTable[TTT_SIZE-1].timeLeft; // return last value in table
	}

/**
* tempTimeLookup45() looks up and returns the time left (in seconds) until feed is available at the set point
*/
double tempTimeLookup45(double temp, double tempSP){
	int i;
	double x0, y0, x1, y1, xSP;

	x0 = tempTimeTable45[0].tempTime;
	y0 = tempTimeTable45[0].lnaTemp;
	xSP = x0;
	i = 1;
	// First loop determines the time value of the setpoint (xSP)
	while ((tempSP < y0) && (i < TTT45_SIZE))
	{
		x1 = tempTimeTable45[i].tempTime;
		y1 = tempTimeTable45[i].lnaTemp;
		if (tempSP > y1){ xSP = InterpolateLinear(x0, y0, x1, y1, tempSP); }
		else { xSP = x1;}
		x0 = x1;
		y0 = y1;
		i++;
	}

	// 2nd loop determines the time value of the LNA temperature (temp) and subtract the two values
	x0 = tempTimeTable45[0].tempTime;
	y0 = tempTimeTable45[0].lnaTemp;
	if (temp >= y0){
		return xSP - x0;
	}
	else if (temp < y0){
		for (i = 1; i < TTT45_SIZE; i++){
			x1 = tempTimeTable45[i].tempTime;
			y1 = tempTimeTable45[i].lnaTemp;
			if (temp == y1){
				return xSP - x1;
			}
			else if (temp > y1){ return xSP - InterpolateLinear(x0, y0, x1, y1, temp); }
			else {
				x0 = x1;
				y0 = y1;
			}
		}
	}
	return xSP - tempTimeTable45[TTT45_SIZE-1].tempTime; // return last value in table
}

/**
 * tempTimeLookup60() looks up and returns the time left (in seconds) until feed is available at the set point
 */
double tempTimeLookup60(double temp, double tempSP){
	int i;
	double x0, y0, x1, y1, xSP;

	x0 = tempTimeTable60[0].tempTime;
	y0 = tempTimeTable60[0].lnaTemp;
	xSP = x0;
	i = 1;
	// First loop determines the time value of the setpoint (xSP)
	while ((tempSP < y0) && (i < TTT60_SIZE))
	{
		x1 = tempTimeTable60[i].tempTime;
		y1 = tempTimeTable60[i].lnaTemp;
		if (tempSP > y1){ xSP = InterpolateLinear(x0, y0, x1, y1, tempSP); }
		else
		{
			xSP = x1;
		}
		x0 = x1;
		y0 = y1;
		i++;
	}

	// 2nd loop determines the time value of the LNA temperature (temp) and subtract the two values
	x0 = tempTimeTable60[0].tempTime;
	y0 = tempTimeTable60[0].lnaTemp;
	if (temp >= y0){
		return xSP - x0;
	}
	else if (temp < y0){
		for (i = 1; i < TTT60_SIZE; i++){
			x1 = tempTimeTable60[i].tempTime;
			y1 = tempTimeTable60[i].lnaTemp;
			if (temp == y1){
				return xSP - x1;
			}
			else if (temp > y1){ return xSP - InterpolateLinear(x0, y0, x1, y1, temp); }
			else {
				x0 = x1;
				y0 = y1;
			}
		}
	}
	return xSP - tempTimeTable60[TTT60_SIZE-1].tempTime; // return last value in table
}

/**
 * tempTimeLookup75() looks up and returns the time left (in seconds) until feed is available at the set point
 */
double tempTimeLookup75(double temp, double tempSP){
	int i;
	double x0, y0, x1, y1, xSP;

	x0 = tempTimeTable75[0].tempTime;
	y0 = tempTimeTable75[0].lnaTemp;
	xSP = x0;
	i = 1;
	// First loop determines the time value of the setpoint (xSP)
	while ((tempSP < y0) && (i < TTT75_SIZE))
	{
		x1 = tempTimeTable75[i].tempTime;
		y1 = tempTimeTable75[i].lnaTemp;
		if (tempSP > y1){ xSP = InterpolateLinear(x0, y0, x1, y1, tempSP); }
		else
		{
			xSP = x1;
		}
		x0 = x1;
		y0 = y1;
		i++;
	}

	// 2nd loop determines the time value of the LNA temperature (temp) and subtract the two values
	x0 = tempTimeTable75[0].tempTime;
	y0 = tempTimeTable75[0].lnaTemp;
	if (temp >= y0){
		return xSP - x0;
	}
	else if (temp < y0){
		for (i = 1; i < TTT75_SIZE; i++){
			x1 = tempTimeTable75[i].tempTime;
			y1 = tempTimeTable75[i].lnaTemp;
			if (temp == y1){
				return xSP - x1;
			}
			else if (temp > y1){ return xSP - InterpolateLinear(x0, y0, x1, y1, temp); }
			else {
				x0 = x1;
				y0 = y1;
			}
		}
	}
	return xSP - tempTimeTable75[TTT75_SIZE-1].tempTime; // return last value in table
}

	/**
	 * pTimeLookup() looks up and returns the time left (in seconds) until manifold is available (at 5e-2 mbar)
	 */
	double pTimeLookup(double pCheck){
		int i;
		double x0, y0, x1, y1, tempval;
		x1 = pTimeTable[0].timeLeft;
		y1 = pTimeTable[0].cryoPressure;
		if (pCheck >= y1){
			return (x1);
		}
		else if (pCheck < y1){
			for (i = 1; i < PTT_SIZE; i++){
				x0 = pTimeTable[i].timeLeft;
				y0 = pTimeTable[i].cryoPressure;
				if (pCheck == y0){return x0; }
				else if (pCheck > y0){
	//				printd("[SSS] Interpolate:\nx1:%0.3E; y1:%f; \nx0:%0.3E; y0:%f \n\r", x1, y1,x0, y0);
					tempval = InterpolateLog(x0, y0,x1, y1, pCheck);
					return (tempval);
				}
				else {
					x1 = x0;
					y1 = y0;
				}
			}
		}
		return (pTimeTable[PTT_SIZE-1].timeLeft); // return last value in table
	}

	/*
	std::string& trim_left_in_place(std::string& str) {
		size_t i = 0;
		while(i < str.size() && isspace(str[i])) { ++i; };
		return str.erase(0, i);
	}

	std::string& trim_right_in_place(std::string& str) {
		size_t i = str.size();
		while(i > 0 && isspace(str[i - 1])) { --i; };
		return str.erase(i, str.size());
	}

	std::string& trim_in_place(std::string& str) {
		return trim_left_in_place(trim_right_in_place(str));
	}

	// returns newly created strings


	std::string trim(std::string str) {
		return trim_left_in_place(trim_right_in_place(str));
	}
	*/
} /* namespace util */

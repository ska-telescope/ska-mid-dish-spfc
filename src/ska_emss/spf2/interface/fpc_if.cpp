
/*
 * hecomp_if.cpp
 *
 *  Created on: 30 Mar 2017
 *      Author: theuns
 */
#define _GLIBCXX_USE_NANOSLEEP 1

#define SERIAL_COMMS_TIMEOUT 5900 // milliseconds

#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <map>
#include <chrono>
#include <thread>

#include "IniParser.h"
#include "fpc_if.h"
#include "system.h"
#include "utilities.h"

using namespace util;
using namespace std;
using namespace serial;

#ifdef SIMULATOR
extern string pPortName;
#endif

FpcProtocol::FpcProtocol()
{
#ifdef SIMULATOR
	portName = pPortName;
#else
	portName = "/dev/ttyS2";
//	portName = "/dev/ttyS3"; // Test B2 on B345 serial port for CDR qual tests
#endif

	/* If no port name is given to serialport, it won't be opened automatically */
	serialPort = new Serial();
	serialTX = new Digital(PIN_B_8, "GPIOB8:");
	serialRX = new Digital(PIN_B_9, "GPIOB9:");

	ioOutLedOnline = new Digital(LED_B2_ONLINE, N_LED_B2_ONLINE);
//	ioOutLedOnline = new Digital(LED_B345_ONLINE, N_LED_B345_ONLINE); // Test B2 on B345 serial port for CDR qual tests
}

FpcProtocol::FpcProtocol(const string PortName)
{
	portName = PortName;

	/* Initialise all the interfaces */
	serialPort = new Serial();
	serialTX = new Digital(PIN_B_8, "GPIOB8:");
	serialRX = new Digital(PIN_B_9, "GPIOB9:");

	ioOutLedOnline = new Digital(LED_B2_ONLINE, N_LED_B2_ONLINE);
}

FpcProtocol::~FpcProtocol()
{
	cout << "FP2 HW DESTRUCT" << endl;

	if (serialPort->isOpen())
	{
		serialPort->close();
	}

	delete serialPort;

	delete ioOutLedOnline;
}

// This function opens the digital channels and the serial port */
bool FpcProtocol::Init()
{
	bool retVal = true;

#ifndef SIMULATOR
	if (ioOutLedOnline->SetDir(0) < 0)
	{
		cout << "Direction NOT set [ioOutLedOnline]" << endl;
	}

	if (serialTX->SetDir(0) < 0)
	{
		cout << "Direction NOT set [serialTX]" << endl;
	}
	if (serialTX->SetActiveLow(1) < 0)
	{
		cout << "ActiveLow could not be set for [serialTX]" << endl;
	}
	if (serialRX->SetActiveLow(1) < 0)
	{
		cout << "ActiveLow could not be set for [serialRX]" << endl;
	}
#endif
	try
	{
		//serialPort = new Serial(portName, 9600, Timeout::simpleTimeout(SERIAL_COMMS_TIMEOUT)); // Will open the serial port in this ctor
		serialPort = new Serial(portName, 19200, Timeout::simpleTimeout(SERIAL_COMMS_TIMEOUT)); // Will open the serial port in this ctor
	}
	catch(PortNotOpenedException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(IOException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(SerialException &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch(invalid_argument &e)
	{
		retVal = false;
		cerr << e.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::SetOnlineLed()
{
	return !ioOutLedOnline->SetVal(1);
}

bool FpcProtocol::ClearOnlineLed()
{
	return !ioOutLedOnline->SetVal(0);
}


bool FpcProtocol::DownloadConfigFile()
{
	return ReadRecordNew(enMemoryRecord::CONFIG, SPF2x_SETTINGS_PATH + devName + SPF2x_CONFIG);
}

/**
 * This function should be used to duplicate the Config file to the upload folder (after updating it)
 * in order to upload the changes to the FPC.
 */
bool FpcProtocol::DuplicateConfigFile()
{
	//fileCopy(SPF2_CONFIG -> SPF2_CONFIG_UL);
    std::ifstream  src(SPF2x_SETTINGS_PATH + devName + SPF2x_CONFIG, std::ios::binary);
    std::ofstream  dst(SPF2_CONFIG_UL,   std::ios::binary);
    dst << src.rdbuf();
    return true;
}

bool FpcProtocol::UploadConfigFile(string & response)
{
	bool retVal = false;
	short rpm = 0;
	enFpcMode mode = enFpcMode::UNKNOWN;
	GetCurrentMode(mode);
	if (mode == enFpcMode::MAINTENANCE)
	{
		retVal = true;
	} else if (mode == enFpcMode::UNKNOWN)
	{
		response = "*QI -> Unknown state; File not uploaded";
		cout << response << endl;
		retVal = false;
	} else if (mode == enFpcMode::APPLICATION)
	{
		GetCryoMotorSpeed(rpm);
		if ( (retVal = SendSimpleCommand("*JF\r")) )
		{
			usleep(500000);
			if ( !(retVal = SendSimpleCommand("*RU\r")) ) // stay in factory image mode for long enough to upload large files
			{
				response = "*RU -> Failed; File not uploaded";
				cout << response << endl;
			}
		} else
		{
			response = "*JF -> Failed; File not uploaded";
			cout << response << endl;
		}
		if (rpm) // restart stepper motor if it was running before Factory jump
		{
			StartCryoMotor();
			cout << "Stepper motor restarted" << endl;
		}
	}
	if (retVal)
	{
		if (retVal &= WriteRecord(enMemoryRecord::CONFIG, SPF2_CONFIG_UL))
		{
			response = "[UL_FILES] SPF2_CONFIG upload finished";
			cout << response << endl;
		} else
		{
			response = "[UL_FILES] SPF2_CONFIG upload FAILED";
			cout << response << endl;
		}
		SendSimpleCommand("*JA\r");
		usleep(500000); // wait for 500 ms
		if (rpm) // restart stepper motor if it was running before Factory jump
		{
			StartCryoMotor();
			cout << "Stepper motor restarted" << endl;
		}
	}
//	cout << "Mode [UL_FILES] completed" << endl;
	return retVal;
}

bool FpcProtocol::UploadLnaBias(stLnaBiasParameters biasH, stLnaBiasParameters biasV, string & response)
{
	bool retVal = false;
	short rpm = 0;
	string responseAdd;
	enFpcMode mode = enFpcMode::UNKNOWN;

	GetCurrentMode(mode);
	if (mode == enFpcMode::MAINTENANCE)
	{
		retVal = true;
	} else if (mode == enFpcMode::UNKNOWN)
	{
		response = "*QI -> Unknown state; File not uploaded";
		cout << response << endl;
		retVal = false;
	} else if (mode == enFpcMode::APPLICATION)
	{
		GetCryoMotorSpeed(rpm);
		if ( (retVal = SendSimpleCommand("*JF\r")) )
		{
			usleep(500000);
			if ( !(retVal = SendSimpleCommand("*RU\r")) ) // stay in factory image mode for long enough to upload large files
			{
				response = "*RU -> Failed; bias values NOT updated";
				cout << response << endl;
			}
		} else
		{
			response = "*JF -> Failed; bias values NOT updated";
			cout << response << endl;
		}
		if (rpm) // restart stepper motor if it was running before Factory jump
		{
			StartCryoMotor();
			cout << "Stepper motor restarted" << endl;
		}
	}
	if (retVal)
	{
		if ( (retVal &= SetLnaHBias(biasH)) )
		{
			response = "[UL_BIAS] LNA H-channel bias values Successfully updated";
			cout << response << endl;
		} else
		{
			response = "[UL_BIAS] LNA H-channel bias values update FAILED";
			cout << response << endl;
		}
		usleep(100000); // wait for 100 ms
		if ( (retVal &= SetLnaVBias(biasV)) )
		{
			responseAdd = "[UL_BIAS] LNA V-channel bias values Successfully updated";
			cout << responseAdd << endl;
		} else
		{
			responseAdd = "[UL_BIAS] LNA V-channel bias values update FAILED";
			cout << responseAdd << endl;
		}
		response += '\r' + responseAdd;

		SendSimpleCommand("*JA\r");
		usleep(500000); // wait for 500 ms
		if (rpm) // restart stepper motor if it was running before Factory jump
		{
			StartCryoMotor();
			cout << "Stepper motor restarted" << endl;
		}
	}
//	cout << "Mode [UL_BIAS] completed" << endl;
	return retVal;
}

bool FpcProtocol::DownloadModelFiles()
{
	bool retVal = true;
	retVal &= ReadRecordNew(enMemoryRecord::REC_GAIN, SPF2x_SETTINGS_PATH + devName + SPF2x_REC_GAIN);
	cout << "[DL_FILES] SPF2_REC_GAIN finished" << endl;
	retVal &= ReadRecordNew(enMemoryRecord::REC_NOISE, SPF2x_SETTINGS_PATH + devName + SPF2x_REC_NOISE);
	cout << "[DL_FILES] SPF2_REC_NOISE finished" << endl;
	retVal &= ReadRecordNew(enMemoryRecord::CAL_NOISE, SPF2x_SETTINGS_PATH + devName + SPF2x_CAL_NOISE);
	cout << "[DL_FILES] SPF2_CAL_NOISE finished" << endl;
	cout << "Mode [DL_FILES] completed" << endl;
	return retVal;
}

bool FpcProtocol::UploadModelFiles(string & response)
{
	bool retVal = false;
	short rpm = 0;
	string responseAdd;
	enFpcMode mode = enFpcMode::UNKNOWN;
	GetCurrentMode(mode);
	if (mode == enFpcMode::MAINTENANCE)
	{
		retVal = true;
	} else if (mode == enFpcMode::UNKNOWN)
	{
		response = "*QI -> Unknown state; File not uploaded";
		cout << response << endl;
		retVal = false;
	} else if (mode == enFpcMode::APPLICATION)
	{
		GetCryoMotorSpeed(rpm);
		if ( (retVal = SendSimpleCommand("*JF\r")) )
		{
			usleep(500000);
			if ( !(retVal = SendSimpleCommand("*RU\r")) ) // stay in factory image mode for long enough to upload large files
			{
				response = "*RU -> Failed; bias values NOT updated";
				cout << response << endl;
			}
		} else
		{
			response = "*JF -> Failed; bias values NOT updated";
			cout << response << endl;
		}
		if (rpm) // restart stepper motor if it was running before Factory jump
		{
			StartCryoMotor();
			cout << "Stepper motor restarted" << endl;
		}
	}
	if (retVal)
	{
//			retVal &= WriteRecord(enMemoryRecord::CONFIG, SPF2_CONFIG_UL);
//			cout << "[UL_FILES] SPF2_CONFIG finished" << endl;
			if (WriteRecord(enMemoryRecord::REC_GAIN, SPF2_REC_GAIN_UL))
			{
				response = "[UL_FILES] SPF2_REC_GAIN finished";
				cout << response << endl;
				retVal &= true;
			} else {
				response = "[UL_FILES] SPF2_REC_GAIN upload failed";
				cout << response << endl;
				retVal &= false;
			}
			if (WriteRecord(enMemoryRecord::REC_NOISE, SPF2_REC_NOISE_UL))
			{
				responseAdd = "[UL_FILES] SPF2_REC_NOISE finished";
				cout << responseAdd << endl;
				retVal &= true;
			} else
			{
				responseAdd = "[UL_FILES] SPF2_REC_NOISE upload failed";
				cout << responseAdd << endl;
				retVal &= false;
			}
			response += '\r' + responseAdd;
			if (WriteRecord(enMemoryRecord::CAL_NOISE, SPF2_CAL_NOISE_UL))
			{
				responseAdd = "[UL_FILES] SPF2_CAL_NOISE finished";
				cout << responseAdd << endl;
				retVal &= true;
			} else
			{
				responseAdd = "[UL_FILES] SPF2_CAL_NOISE upload failed";
				cout << responseAdd << endl;
				retVal &= false;
			}
			response += '\r' + responseAdd;
	}
	SendSimpleCommand("*JA\r");
	usleep(500000); // wait for 500 ms
	if (rpm) // restart stepper motor if it was running before Factory jump
	{
		StartCryoMotor();
		cout << "Stepper motor restarted" << endl;
	}
	responseAdd = "Mode [UL_FILES] completed";
	cout << responseAdd << endl;
	response += '\r' + responseAdd;
	return retVal;
}

bool FpcProtocol::LnaOn()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set the LNA H and V bits */
		cf.bits.LnaH = 1;
		cf.bits.LnaV = 1;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}
bool FpcProtocol::LnaOff()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set the LNA H and V bits */
		cf.bits.LnaH = 0;
		cf.bits.LnaV = 0;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}

bool FpcProtocol::LnaHOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_H, 1);
}
bool FpcProtocol::LnaHOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_H, 0);
}
bool FpcProtocol::LnaVOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_V, 1);
}
bool FpcProtocol::LnaVOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_V, 0);
}

bool FpcProtocol::Amp2On()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set the LNA H and V bits */
		cf.bits.Amp2H = 1;
		cf.bits.Amp2V = 1;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}
bool FpcProtocol::Amp2Off()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set the LNA H and V bits */
		cf.bits.Amp2H = 0;
		cf.bits.Amp2V = 0;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}
bool FpcProtocol::Amp2HOn()
{
	return ModifyControlBit(enControlBitNumber::AMP2_H, 1);
}
bool FpcProtocol::Amp2HOff()
{
	return ModifyControlBit(enControlBitNumber::AMP2_H, 0);
}
bool FpcProtocol::Amp2VOn()
{
	return ModifyControlBit(enControlBitNumber::AMP2_V, 1);
}
bool FpcProtocol::Amp2VOff()
{
	return ModifyControlBit(enControlBitNumber::AMP2_V, 0);
}

bool FpcProtocol::LnaIlluminationOn()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set the LNA H and V bits */
		cf.bits.LnaHIllumination = 1;
		cf.bits.LnaVIllumination = 1;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}
bool FpcProtocol::LnaIlluminationOff()
{
	bool retVal = true;

	unControlFlags cf;
	if (GetControlRegister(cf))
	{
		/* Set the LNA H and V bits */
		cf.bits.LnaHIllumination = 0;
		cf.bits.LnaVIllumination = 0;

		/* Now we send the new control register to the FPC */
		if (ModifyControlRegister(cf))
		{
			retVal = true;
		}
	}

	return retVal;
}
bool FpcProtocol::LnaHIlluminationOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_H_ILLUMINATION, 1);
}
bool FpcProtocol::LnaHIlluminationOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_H_ILLUMINATION, 0);
}
bool FpcProtocol::LnaVIlluminationOn()
{
	return ModifyControlBit(enControlBitNumber::LNA_V_ILLUMINATION, 1);
}
bool FpcProtocol::LnaVIlluminationOff()
{
	return ModifyControlBit(enControlBitNumber::LNA_V_ILLUMINATION, 0);
}

bool FpcProtocol::CalSourceOn()
{
	return ModifyControlBit(enControlBitNumber::CALSOURCE, 1);
}
bool FpcProtocol::CalSourceOff()
{
	return ModifyControlBit(enControlBitNumber::CALSOURCE, 0);
}

/* PID Control */
bool FpcProtocol::PidLnaOn()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA, 1);
}
bool FpcProtocol::PidLnaOff()
{
	return ModifyControlBit(enControlBitNumber::PID_LNA, 0);
}

bool FpcProtocol::PidCalSourceOn()
{
	return ModifyControlBit(enControlBitNumber::PID_CALSOURCE, 1);
}
bool FpcProtocol::PidCalSourceOff()
{
	return ModifyControlBit(enControlBitNumber::PID_CALSOURCE, 0);
}

/* True = Open, False = Closed */
bool FpcProtocol::GetValveStatus()
{
	bool valveStatus = false;
	if (GetVacuumValveStatus(valveStatus))
	{
		return valveStatus;
	}
	return false;
}

/* True = Running, False = OFF */
bool FpcProtocol::GetMotorStatus()
{
	short rpm;
	if (GetCryoMotorSpeed(rpm))
	{
		if (rpm == 0)
		{
			return false; // OFF
		}
		return true; // Running
	}
	return false; // No status => OFF
}

bool FpcProtocol::ClearAllErrors()
{
	unErrorFlags ef;
	ef.Word = 0;
	return ModifyErrorRegister(ef);
}

bool FpcProtocol::SetError(enErrorBits eb)
{
	return ModifyErrorBit(static_cast<int>(eb), 1);
}
bool FpcProtocol::ClearError(enErrorBits eb)
{
	return ModifyErrorBit(static_cast<int>(eb), 0);
}

// TODO: Check for malformed protocol responses
bool FpcProtocol::ValidateResponse(string response)
{
	bool retVal = false;

	if ((response != "") && (!response.empty()))
	{
		if ((response[0] == '*') && ((response[1] == 'S') || (response[1] == 's')))
		{
			retVal = true;
		}
	}

	return retVal;
}

bool FpcProtocol::SendSimpleCommand(string command)
{
	bool retVal = false;

	try
	{
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();
//		cout << command << " -> " << response << endl;

		/* Validate the response */
		retVal = ValidateResponse(response);

	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << "\n Command: " << command << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Sensor Monitoring  */
/* ================== */
// Sample all sensors
// test response 1: *S,4.99,513,35.6,21.5,8.5,29.3,22.9
// test response 2: *S,4.99,-513,35.6,-21.5,8.5,-29.3,22.9
// test response 3: *F
bool FpcProtocol::SampleAll(stFeedSensors & sensors)
{
	bool retVal = false;

	try
	{
		string command = "*SA\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			/* Parse message */
			retVal = ParseSampleAll(response, sensors);
		}
		else
		{
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseSampleAll(string response, stFeedSensors & sensors)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		//NOTE: Segmentation fault when trying to read more sensors than returned from feed.
		if (tokens.size() > 45) // "S" + 45 sensors = 46
		{
			// TODO: if we are modifying the variables here, some sort of lock should be put on it?
			sensors.controllerVoltage 			= stod(tokens[1]) / 100;
			sensors.controllerCurrent 			= stod(tokens[2]) /  1; // Changed to 1
			sensors.controllerTemperature 		= stod(tokens[3]) / 10;
			sensors.psu9VRectifiedVoltage 		= stod(tokens[4]) / 100;
			sensors.psuDigital6vRegulatorTemp	= stod(tokens[5]) / 10; // this switched with channel 7; order is still switched in spfc
			sensors.psuAnalogue15vRegulatorTemp = stod(tokens[6]) / 10;
			sensors.psuAnalogue6vRegulatorTemp	= stod(tokens[7]) / 10; // this switched with channel 5; the order is still switched in spfc, but doesn't matter because it searches the correct name.

			sensors.lnaHDrainVoltage1 			= stod(tokens[8]) / 100;
			sensors.lnaHDrainVoltage2 			= stod(tokens[9]) / 100;
			sensors.lnaHDrainVoltage3 			= stod(tokens[10]) / 100;
			sensors.lnaHDrainCurrent1 			= stod(tokens[11]) / 10;
			sensors.lnaHDrainCurrent2 			= stod(tokens[12]) / 10;
			sensors.lnaHDrainCurrent3 			= stod(tokens[13]) / 10;
			sensors.lnaHGateVoltage1 			= stod(tokens[14]) / 100;
			sensors.lnaHGateVoltage2 			= stod(tokens[15]) / 100;
			sensors.lnaHGateVoltage3 			= stod(tokens[16]) / 100;
			sensors.lnaVDrainVoltage1 			= stod(tokens[17]) / 100;
			sensors.lnaVDrainVoltage2 			= stod(tokens[18]) / 100;
			sensors.lnaVDrainVoltage3 			= stod(tokens[19]) / 100;
			sensors.lnaVDrainCurrent1 			= stod(tokens[20]) / 10;
			sensors.lnaVDrainCurrent2 			= stod(tokens[21]) / 10;
			sensors.lnaVDrainCurrent3 			= stod(tokens[22]) / 10;
			sensors.lnaVGateVoltage1 			= stod(tokens[23]) / 100;
			sensors.lnaVGateVoltage2 			= stod(tokens[24]) / 100;
			sensors.lnaVGateVoltage3 			= stod(tokens[25]) / 100;
			sensors.amp2HVoltage 				= stod(tokens[26]) / 100;
			sensors.amp2HCurrent 				= stod(tokens[27]) / 10;
			sensors.amp2VVoltage 				= stod(tokens[28]) / 100;
			sensors.amp2VCurrent 				= stod(tokens[29]) / 10;
			sensors.calsourceVoltage 			= stod(tokens[30]) / 10;
			sensors.calsourceCurrent 			= stod(tokens[31]) / 10;
			sensors.calsourceTemp		 		= stod(tokens[32]) / 10;
			sensors.pid1Current 				= stod(tokens[33]) / 1; // changed to 1
			sensors.pid2Current 				= stod(tokens[34]) / 1; // changed to 1

			/* New way */
			sensors.cryostatPressure 			= ParseMillibar(tokens[35]);
			sensors.manifoldPressure 			= ParseMillibar(tokens[36]);

			/* Old way */
			//sensors.cryostatPressure 			= stod(tokens[35]) * 0.0013332237; //mTorr -> mBar
			//sensors.manifoldPressure 			= stod(tokens[36]) * 0.0013332237; //mTorr -> mBar

			sensors.lnaTemp 					= stod(tokens[37]) / 100; // more sensitive after firmware version 2.00
			sensors.omtTemp		 				= stod(tokens[38]) / 100; // more sensitive after firmware version 2.00
			sensors.coldheadTemp		 		= stod(tokens[39]) / 100; // more sensitive after firmware version 2.00
			sensors.cryostatBodyTemp		 	= stod(tokens[40]) / 10;
			sensors.analogueInterfaceTemp		= stod(tokens[41]) / 10;

			sensors.psuMotorPos15VRegCurrent    = stod(tokens[42]) / 1;
			sensors.psuMotorNeg15VRegCurrent    = stod(tokens[43]) / 1;
			sensors.psuMotor15VRegTemp          = stod(tokens[44]) / 10;
			sensors.spdHealth		            = stod(tokens[45]) / 100;

			retVal = true;
		} else
		{
			cout << "Could not parse *SA; sensors out of range" << endl;
			retVal = false;
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* string contains 4 characters:
 * 0-2 --> Bar value in notation x.xx
 * 3   --> negative exponent
 * Example:
 * 1234 -> 1.23e-4
 */
double FpcProtocol::ParseMillibar(std::string text)
{
	double retVal = 0.0;

	if (text.length() == 4)
	{
		std::stringstream ss;
		ss << text[0] << '.' << text[1] << text[2] << "e-" << text[3];
		retVal = stod(ss.str()) * 1000.0; // We have bar and want millibar
		if (retVal > 1000) // indicates sensor connection issue...
		{
			retVal = 2000;
		}
		else if (retVal > 100)
		{	// Snap to atmosphere if P > 1e+2, eliminate temperature drift
			retVal = 1000;
		}
	}

	return retVal;
}

// Sample single sensor
// test response 1: *S,0A:23.5
// test response 2: *F
bool FpcProtocol::SampleSingleSensor(unsigned int channelNum, double& value)
{
	bool retVal = false;

	try
	{
		string messageId = "*SS,";
		string parameters = numberToHexString(channelNum, 2);
		string command = messageId + parameters + "\r";
		string response;


		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			unsigned int parsedChannel;
			double parsedValue;

			/* Parse message */
			retVal = ParseSampleSingle(response, parsedChannel, parsedValue);

			if (parsedChannel == channelNum)
			{
				/* Only assign the value if we have received the response for the specified channel */
				value = parsedValue;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseSampleSingle(string response, unsigned int & channel, double & value)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			vector<string> parameters = split(tokens[1], ':');

			if (parameters.size() == 2)
			{
				channel = stoul(parameters[0], nullptr, 16);
				value = stod(parameters[1]);

				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::SelectChannels(std::vector<string> channels)
{
	return false; //TODO and FIXME!!!
}

// Not all parameters are populated in struct
bool FpcProtocol::SampleSelectedChannels(stFeedSensors & sensors)
{
	bool retVal = false;

	try
	{
		string command = "*SG\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, double> returnData;

			/* Parse message */
			if (ParseGeneralMultipleDouble(response, returnData))
			{
				sensors.controllerVoltage 			= returnData[1] / 100;
				sensors.controllerCurrent 			= returnData[2] /  1; // Changed to 1
				sensors.controllerTemperature 		= returnData[3] / 10;
				sensors.psu9VRectifiedVoltage 	= returnData[4] / 100;
				sensors.psuAnalogue6vRegulatorTemp	= returnData[5] / 10;
				sensors.psuAnalogue15vRegulatorTemp = returnData[6] / 10;
				sensors.psuDigital6vRegulatorTemp	= returnData[7] / 10;

				sensors.lnaHDrainVoltage1 			= returnData[8] / 100;
				sensors.lnaHDrainVoltage2 			= returnData[9] / 100;
				sensors.lnaHDrainVoltage3 			= returnData[10] / 100;
				sensors.lnaHDrainCurrent1 			= returnData[11] / 10;
				sensors.lnaHDrainCurrent2 			= returnData[12] / 10;
				sensors.lnaHDrainCurrent3 			= returnData[13] / 10;
				sensors.lnaHGateVoltage1 			= returnData[14] / 100;
				sensors.lnaHGateVoltage2 			= returnData[15] / 100;
				sensors.lnaHGateVoltage3 			= returnData[16] / 100;
				sensors.lnaVDrainVoltage1 			= returnData[17] / 100;
				sensors.lnaVDrainVoltage2 			= returnData[18] / 100;
				sensors.lnaVDrainVoltage3 			= returnData[19] / 100;
				sensors.lnaVDrainCurrent1 			= returnData[20] / 10;
				sensors.lnaVDrainCurrent2 			= returnData[21] / 10;
				sensors.lnaVDrainCurrent3 			= returnData[22] / 10;
				sensors.lnaVGateVoltage1 			= returnData[23] / 100;
				sensors.lnaVGateVoltage2 			= returnData[24] / 100;
				sensors.lnaVGateVoltage3 			= returnData[25] / 100;
				sensors.amp2HVoltage 				= returnData[26] / 100;
				sensors.amp2HCurrent 				= returnData[27] / 10;
				sensors.amp2VVoltage 				= returnData[28] / 100;
				sensors.amp2VCurrent 				= returnData[29] / 10;
				sensors.calsourceVoltage 			= returnData[30] / 10;
				sensors.calsourceCurrent 			= returnData[31] / 10;
				sensors.calsourceTemp		 		= returnData[32] / 10;
				sensors.pid1Current 				= returnData[33] / 1; // changed to 1
				sensors.pid2Current 				= returnData[34] / 1; // changed to 1
				sensors.cryostatPressure 			= returnData[35] * 0.0013332237; //mTorr -> mBar
				sensors.manifoldPressure 			= returnData[36] * 0.0013332237; //mTorr -> mBar
				sensors.lnaTemp 					= returnData[37] / 10;
				sensors.omtTemp		 				= returnData[38] / 10;
				sensors.coldheadTemp		 		= returnData[39] / 10;
				sensors.cryostatBodyTemp		 	= returnData[40] / 10;
				sensors.analogueInterfaceTemp		= returnData[41] / 10;

				sensors.psuMotorPos15VRegCurrent    = returnData[42] / 1;
				sensors.psuMotorNeg15VRegCurrent    = returnData[43] / 1;
				sensors.psuMotor15VRegTemp          = returnData[44] / 10;
				sensors.spdHealth		            = returnData[45] / 100;

				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::SetAllChannels()
{
	return SendSimpleCommand("*AC\r");
}

bool FpcProtocol::EnableSensorAveraging()
{
	return SendSimpleCommand("*AE\r");
}

bool FpcProtocol::DisableSensorAveraging()
{
	return SendSimpleCommand("*AD\r");
}


/* ================== */
/* Compressor Control */
/* ================== */
// test response 1: *S,ABCD
// test response 2: *S,0001
// test response 3: *F
bool FpcProtocol::ModifyControlRegister(unControlFlags controlFlags)
{
	bool retVal = false;

	try
	{
		string messageId = "*CR,";
		string parameters = numberToHexString(controlFlags.Word, 4);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the setRegister is equal to the actual register */
				if (stoul(parsedValue, nullptr, 16) == controlFlags.Word)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::ModifyControlBit(enControlBitNumber controlBit, unsigned int value)
{
	bool retVal = false;

	try
	{
		string messageId = "*CB,";
		string parameters = numberToHexString(static_cast<short>(controlBit), 2) + ':' + numberToHexString(value, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the command is equal to the echoed response */
				if (parameters == parsedValue)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			cout << response << endl;
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::GetControlRegister(unControlFlags & controlFlags)
{
	bool retVal = false;

	try
	{
		string command = "*GC\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();


		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				controlFlags.Word = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,00:01
// test response 1: *S,05:00
// test response 2: *F
bool FpcProtocol::GetControlBit(unsigned int controlBit, unsigned int & value)
{
	bool retVal = false;

	try
	{
		string messageId = "*GB";
		string parameters = numberToHexString(controlBit, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedParameter;
			string parsedValue;

			/* Parse message */
			if (ParseGeneralDouble(response, parsedParameter, parsedValue))
			{
				/* We have strings, convert to input types and perform checks and variable assignments */
				if (numberToHexString(controlBit, 2) == parsedParameter)
				{
					value = stoul(parsedValue, nullptr, 16);
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ==================== */
/* Cooler Motor Control */
/* ==================== */
bool FpcProtocol::StartCryoMotor()
{
	return SendSimpleCommand("*EM\r");
}
bool FpcProtocol::StopCryoMotor()
{
	return SendSimpleCommand("*DM\r");
}
bool FpcProtocol::SetCryoMotorSpeed(short rpm)
{
	string command = "*SF," + util::numberToFixedString(rpm, 2) + "\r";
	return SendSimpleCommand(command);
}

bool FpcProtocol::GetCryoMotorSpeed(short & rpm)
{
	bool retVal = false;

	try
	{
		string command = "*RF\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				rpm = stoi(parsedValue);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ==================== */
/* Vacuum Valve Control */
/* ==================== */
bool FpcProtocol::OpenVacuumValve()
{
	return SendSimpleCommand("*OV\r");
}
bool FpcProtocol::CloseVacuumValve()
{
	return SendSimpleCommand("*CV\r");
}

bool FpcProtocol::GetVacuumValveStatus(bool & isOpen)
{
	bool retVal = false;

	try
	{
		string command = "*VS\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				isOpen = static_cast<bool>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* PID Controllers	  */
/* ================== */
bool FpcProtocol::SetPidPropGain(enPid pid, int gain)
{
	string messageId = "*PP";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(gain, 4);
	string command = messageId + "," + parameter + ":" + value + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::SetPidIntegral(enPid pid, double integral)
{
	string messageId = "*PI";
	string paramater = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(static_cast<int>(integral * 1000), 4);
	string command = messageId + "," + paramater + ":" + value + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::SetPidDerivative(enPid pid, double derivative)
{
	string messageId = "*PD";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(static_cast<int>(derivative * 1000), 4);
	string command = messageId + "," + parameter + ":" + value + "\r";

	return SendSimpleCommand(command);
}

bool FpcProtocol::SetPidTempSetPoint(enPid pid, short temp)
{
	string messageId = "*PT";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string value = numberToFixedString(temp, 4);
	string command = messageId + "," + parameter + ":" + value + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::GetPidTempSetPoint(enPid pid, short & temp)
{
	bool retVal = false;

	try
	{
		string messageId = "*PK";
		string parameter = numberToHexString(static_cast<short>(pid), 2);
		string command = messageId + "," + parameter + "\r";
		string response;

		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				temp = stoi(parsedValue);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;

}
bool FpcProtocol::SavePidCoeffs(enPid pid)
{
	string messageId = "*PS";
	string parameter = numberToHexString(static_cast<short>(pid), 2);
	string command = messageId + "," + parameter + "\r";

	return SendSimpleCommand(command);
}
bool FpcProtocol::GetPidCoeffs(enPid pid, stPidSettings & pidSettings)
{
	bool retVal = false;

	try
	{
		string messageId = "*PC";
		string parameter = numberToHexString(static_cast<short>(pid), 2);
		string command = messageId + "," + parameter + "\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, double> returnData;

			/* Parse message */
			if (ParseGeneralMultipleDouble(response, returnData))
			{
				pidSettings.proportional = static_cast<int>(returnData[1]); // index starts at 1
				pidSettings.integral = returnData[2] / 1000;
				pidSettings.derivative = returnData[3] / 1000;
				//pidSettings.temp = static_cast<int>(returnData[4]);
				retVal = true;
			}
			else
			{
				cerr << "Could not parse data." << endl;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* LNA Biasing	 	  */
/* ================== */
bool FpcProtocol::SetLnaHBias(stLnaBiasParameters biasParameters)
{
	string command = "*BH";
	command += string(",01:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt1 * 1000), 4);
	command += string(",02:") + numberToFixedString(static_cast<int>(biasParameters.drainCurrent1 * 100), 4);
	command += string(",03:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt2 * 1000), 4);
	command += string(",04:") + numberToFixedString(static_cast<int>(biasParameters.drainCurrent2 * 100), 4);
	command += string(",05:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt3 * 1000), 4);
	command += string(",06:") + numberToFixedString(static_cast<int>(biasParameters.drainCurrent3 * 100), 4);
	command += "\r";
	cout << "[UL_BIAS] " << command << endl;

	return SendSimpleCommand(command);

}
bool FpcProtocol::SetLnaVBias(stLnaBiasParameters biasParameters)
{
	string command = "*BV";
	command += string(",01:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt1 * 1000), 4);
	command += string(",02:") + numberToFixedString(static_cast<int>(biasParameters.drainCurrent1 * 100), 4);
	command += string(",03:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt2 * 1000), 4);
	command += string(",04:") + numberToFixedString(static_cast<int>(biasParameters.drainCurrent2 * 100), 4);
	command += string(",05:") + numberToFixedString(static_cast<int>(biasParameters.drainVolt3 * 1000), 4);
	command += string(",06:") + numberToFixedString(static_cast<int>(biasParameters.drainCurrent3 * 100), 4);
	command += "\r";
	cout << "[UL_BIAS] " << command << endl;

	return SendSimpleCommand(command);
}
bool FpcProtocol::GetLnaHBias(stLnaBiasParameters & biasParameters)
{
	bool retVal = false;

	try
	{
		string command = "*HB\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, double> returnData;

			/* Parse message */
			if (ParseGeneralMultipleDouble(response, returnData))
			{
				biasParameters.drainVolt1 = returnData[0] / 1000;
				biasParameters.drainCurrent1 = returnData[1] / 100;
				biasParameters.drainVolt2 = returnData[2] / 1000;
				biasParameters.drainCurrent2 = returnData[3] / 100;
				biasParameters.drainVolt3 = returnData[4] / 1000;
				biasParameters.drainCurrent3 = returnData[5] / 100;

				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::GetLnaVBias(stLnaBiasParameters & biasParameters)
{
	bool retVal = false;

	try
	{
		string command = "*VB\r";

		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			map<int, double> returnData;

			/* Parse message */
			if (ParseGeneralMultipleDouble(response, returnData))
			{
				biasParameters.drainVolt1 = returnData[0] / 1000;
				biasParameters.drainCurrent1 = returnData[1] / 100;
				biasParameters.drainVolt2 = returnData[2] / 1000;
				biasParameters.drainCurrent2 = returnData[3] / 100;
				biasParameters.drainVolt3 = returnData[4] / 1000;
				biasParameters.drainCurrent3 = returnData[5] / 100;

				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* Firmware Version	  */
/* ================== */
// test response 1: *S,0123
// test response 2: *F
bool FpcProtocol::GetFirmwareVersion(string & version)
{
	bool retVal = false;

	try
	{
		string command = "*RV\r";
		string response;

		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				version = parsedValue;
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Serial Number	  */
/* ================== */
// test response 1: *S,3001
// test response 2: *F
bool FpcProtocol::GetSerialNumber(string & serial)
{
	bool retVal = false;

	try
	{
		string command = "*SN\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				serial = parsedValue;
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ================== */
/* Memory Storage	  */
/* ================== */
bool FpcProtocol::EraseRecord(enMemoryRecord record)
{
	if (record == enMemoryRecord::NONE)
	{
		return false;
	}

	string messageId = "*EF,";
	string parameter = numberToHexString(static_cast<short>(record), 2);
	string command = messageId + parameter + "\r";

	return SendSimpleCommand(command);
}

/* This function will send the read record command and write all data to file until the <EOT> character is received. */
bool FpcProtocol::ReadRecordNew(enMemoryRecord record, string fileName)
{
	/* Pseudo:
	 * Open a new file
	 * send read command
	 * while returned bytes does not end with <EOT> do
	 * read chunk of data
	 * write string to file
	 * end [while]
	 * close file
	 */

	bool retVal = false;

	try
	{
		string buffer;
		string recordLocation = numberToHexString(static_cast<short>(record), 2);

		bool success = true;

		ofstream f (fileName.c_str());

		if (!f.is_open())
		{
			perror(("error while opening file " + fileName).c_str());
		} else
		{

			/* Send the read command */
			string messageId = "*RR,";
			string command = messageId + recordLocation + "\r";

			/* First, flush any remaining bytes in serial port input and output buffer */
			serialPort->flushInputOutput();

			/* Write the command string to the serial port */
			serialPort->write(command);

			/* Ensure all bytes are written with tcdrain()*/
			serialPort->flush();

			/* Read the first bytes to see if we have a success response *S,*/
			buffer = serialPort->read(6); //*S,01,<DATA>

			/* Validate the response */
			if (ValidateResponse(buffer))
			{
				/* If we get here, only the payload bytes are the next bytes to be read */
				while(serialPort->waitReadable())
				{
					uint8_t byte;

					if (serialPort->read(&byte, 1) == 1)
					{
						if (byte != 0x04) //EOT
						{
							f.write((char *)&byte,1);
						}
						else
						{
							/* If we get here, file has been written successfully */
							retVal = true;
							break;
						}
					}
				}
			}

			if (f.bad())
			{
				perror(("error while reading file " + fileName).c_str());
			}
		}
		f.close();
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* This function will send the read record command and write all data to file until the <EOT> character is received. */
bool FpcProtocol::ReadRecord(enMemoryRecord record, string fileName)
{
	/* Pseudo:
	 * Open a new file
	 * send read command
	 * while returned bytes does not end with <EOT> do
	 * read chunk of data
	 * write string to file
	 * end [while]
	 * close file
	 */

	bool retVal = false;

	try
	{
		string buffer;
		string recordLocation = numberToHexString(static_cast<short>(record), 2);

		bool success = true;

		ofstream f (fileName.c_str());

		if (!f.is_open())
		{
			perror(("error while opening file " + fileName).c_str());
		}

		/* Send the read command */
		string messageId = "*RR,";
		string command = messageId + recordLocation + "\r";

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Read the line */
		buffer = serialPort->readline(276484, "\x04"); // read until the EOT "\x04" character is received. Max file size is 270KB -- allow 4 extra bytes

		/* Validate the response */
		if (ValidateResponse(buffer))
		{
			/* Get the file payload from the buffer */
			buffer.erase(0,6); // Remove Message id
			buffer.erase(buffer.end()-1, buffer.end()); // Remove EOT character

			/* Write whole buffer to file */
			f << buffer;

			/* If we get here, file has bee written successfully */
			retVal = true;
		}

		if (f.bad())
		{
			perror(("error while reading file " + fileName).c_str());
		}
		f.close();
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* This function will continually send the write record command until the end of the string is received. */
bool FpcProtocol::WriteRecord(enMemoryRecord record, string fileName)
{
	/* Pseudo
	 * open existing file
	 * while not eof
	 * readline
	 * send line to fpga
	 * if success, continue, else break;
	 * end [while]
	 * close file
	 */
	bool retVal = false;

	try
	{
		ifstream fileToWrite (fileName.c_str());

		if (!fileToWrite.is_open() || fileToWrite.fail())
		{
			perror(("error while opening file " + fileName).c_str());
		} else
		{
			serial::Timeout newTout, oldTout;
			oldTout = serialPort->getTimeout();
			serialPort->setTimeout(0,60000,1,60000,1);

			string messageId = "*WR,";
			string parameter = numberToHexString(static_cast<short>(record), 2);
			string command = messageId + parameter + ",";
			cout << command << endl;

			/* First, flush any remaining bytes in serial port input and output buffer */
			serialPort->flushInputOutput();

			/* Write the command string to the serial port */
			serialPort->write(command);

			/* TJS - This could be faster, although more memory will be used. */
			if (fileToWrite)
			{
				std::string contents;
				fileToWrite.seekg(0, std::ios::end);				// go to the end
				contents.resize(fileToWrite.tellg());				// resize the string with the file's length
				fileToWrite.seekg(0, std::ios::beg);				// go back to the beginning
				fileToWrite.read(&contents[0], contents.size());	// read the whole file into the buffer
				fileToWrite.close();								// close file handle
				serialPort->write(contents);
	//			serialPort->flushOutput();
			}

			/*
			while (!fileToWrite.eof())
			{
				uint8_t byte;

				// Read a byte from the file
				if (fileToWrite.read((char *)&byte,1))
				{
					// Only write one byte to the serial device
					serialPort->write(&byte, 1);
				}

				if (fileToWrite.fail())
				{
					//error
					break;
				}
			}
			fileToWrite.close();
			serialPort->flushOutput();
			*/

			/* We are finished with the file */
			uint8_t eot = 0x04;
			serialPort->write(&eot, 1);
			cout << "[DATA]\\eot(0x04)" << endl;
	//		uint8_t cr = 0x0D;
	//		serialPort->write(&cr, 1); // FIXME: This should be removed
	//		serialPort->write("\r"); // FIXME: This should be removed

			/* Ensure all bytes are written with tcdrain()*/
			serialPort->flush();
			serialPort->setTimeout(oldTout);

			/* Read the line */
			string line = serialPort->readline();
			cout << line << endl;
			retVal = ValidateResponse(line);
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}
	return retVal;
}

/* ================== */
/* Error Flags		  */
/* ================== */
// test response 1: *S,ABCD
// test response 2: *S,0001
// test response 3: *F
bool FpcProtocol::ModifyErrorRegister(unErrorFlags errorFlags)
{
	bool retVal = false;

	try
	{
		string messageId = "*EC,";
		string parameters = numberToHexString(errorFlags.Word, 4);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the setRegister is equal to the actual register */
				if (stoul(parsedValue, nullptr, 16) == errorFlags.Word)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::ModifyErrorBit(unsigned int errorBit, unsigned int value)
{
	bool retVal = false;

	try
	{
		string messageId = "*EB,";
		string parameters = numberToHexString(errorBit, 2) + ':' + numberToHexString(value, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				/* Check if the command is equal to the echoed response */
				if (parameters == parsedValue)
				{
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,08:01
// test response 2: *F
bool FpcProtocol::GetErrorRegister(unErrorFlags & errorFlags)
{
	bool retVal = false;

	try
	{
		string command = "*ER\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				errorFlags.Word = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

// test response 1: *S,00:01
// test response 1: *S,05:00
// test response 2: *F
bool FpcProtocol::GetErrorBit(unsigned int errorBit, unsigned int & value)
{
	bool retVal = false;

	try
	{
		string messageId = "*BE,";
		string parameters = numberToHexString(errorBit, 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedParameter;
			string parsedValue;

			/* Parse message */
			if (ParseGeneralDouble(response, parsedParameter, parsedValue))
			{
				/* We have strings, convert to input types and perform checks and variable assignments */
				if (numberToHexString(errorBit, 2) == parsedParameter)
				{
					value = stoul(parsedValue, nullptr, 16);
					retVal = true;
				}
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}


/* ================== */
/* Real Time Clock	  */
/* ================== */
bool FpcProtocol::SetSystemTime(unsigned int systemTime)
{
	string messageId = "*TM,";
	string parameters = numberToHexString(systemTime, 8);
	string command = messageId + parameters + "\r";

	return SendSimpleCommand(command);
}

bool FpcProtocol::GetSystemTime(unsigned int & systemTime)
{
	bool retVal = false;

	try
	{
		string command = "*RM\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				systemTime = stoul(parsedValue, nullptr, 16);
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* ====================	*/
/* Elapsed Time Counter	*/
/* ==================== */
bool FpcProtocol::GetElapsedTime(enElapsedTimeCounter counterChannel, int & elapsedTime)
{
	bool retVal = false;

	try
	{
		string messageId = "*GT,";
		string parameters = numberToHexString(static_cast<short>(counterChannel), 2);
		string command = messageId + parameters + "\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				elapsedTime = static_cast<int>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ResetElapsedTime(enElapsedTimeCounter counterChannel)
{
	string messageId = "*CT,";
	string parameters = numberToHexString(static_cast<short>(counterChannel), 2);
	string command = messageId + parameters + "\r";

	return SendSimpleCommand(command);
}

/* ====================== */
/* Cool down counter	  */
/* ====================== */
bool FpcProtocol::GetCoolDownCounter(unsigned short & count)
{
	bool retVal = false;

	try
	{
		string command = "*CG\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				count = static_cast<unsigned short>(stoi(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}
bool FpcProtocol::IncrementCoolDownCounter()
{
	return SendSimpleCommand("*CI\r");
}
bool FpcProtocol::ClearCoolDownCounter()
{
	return SendSimpleCommand("*CW\r");
}

/* ====================== */
/* State and Mode Control */
/* ====================== */
bool FpcProtocol::RemainMaintenance()
{
	return SendSimpleCommand("*RU\r");
}
bool FpcProtocol::GoApplication()
{
	return SendSimpleCommand("*JA\r");
}
bool FpcProtocol::GoUpdate()
{
	return SendSimpleCommand("*FP\r");
}
bool FpcProtocol::GoMaintenance()
{
	return SendSimpleCommand("*JF\r");
}
bool FpcProtocol::GetCurrentMode(enFpcMode & mode)
{
	bool retVal = false;

	try
	{
		string command = "*QI\r";
		string response;

		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		/* Validate the response */
		if (ValidateResponse(response))
		{
			string parsedValue;

			/* Parse message */
			if (ParseGeneralSingle(response, parsedValue))
			{
				mode = static_cast<enFpcMode>(stoul(parsedValue, nullptr, 16));
				retVal = true;
			}
		}
		else
		{
			// TODO: Indicate a protocol error has occurred
			retVal = false;
		}
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* This functions ends a raw serial command */
bool FpcProtocol::SendRawCommand(string command, string & response)
{
	bool retVal = false;

	try
	{
		/* First, flush any remaining bytes in serial port input and output buffer */
		serialPort->flushInputOutput();

		/* Write the command string to the serial port */
		serialPort->write(command);

		/* Ensure all bytes are written with tcdrain()*/
		serialPort->flush();

		/* Get response (with timeout) */
		response = serialPort->readline();

		retVal = true; // If we get here without exceptions, we accept the command was successful and return the received string to the caller.
	}
	catch (PortNotOpenedException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] not open" << endl;
	}
	catch (TimeOutException &e)
	{
		retVal = false;
		cerr << "Serial port [ "<< portName << " ] Read Timeout: " << __PRETTY_FUNCTION__ << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralSingle(string response, string & parsedValue)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			if ((tokens[1] != "") && (!tokens[1].empty()))
			{
				parsedValue = trim(tokens[1]);
				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralDouble(string response, string & parsedParameter, string & parsedValue)
{
	bool retVal = false;

	try
	{
		vector<string> tokens = split(response, ',');

		if (tokens.size() == 2)
		{
			vector<string> parameters = split(tokens[1], ':');

			if (parameters.size() == 2)
			{
				parsedParameter = trim(parameters[0]);
				parsedValue = trim(parameters[1]);

				retVal = true;
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralMultipleInt(string response, map<int, int> & returnData)
{
	bool retVal = true;

	try
	{
		vector<string> tokens = split(response, ',');

		//if (tokens.size() == 5) // FIXME: Why is this so if the function is 'General' ?
		{
			for (size_t i = 1; i < tokens.size(); i++)
			{
				vector<string> parameters = split(tokens[i], ':');

				if (parameters.size() == 2)
				{
					int index = stoi(trim(parameters[0]));
					int value = stoi(trim(parameters[1]));
					returnData[index] = value;
					//retVal &= true;
				}
				else
				{
					retVal &= false;
				}
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		retVal = false;
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool FpcProtocol::ParseGeneralMultipleDouble(string response, map<int, double> & returnData)
{
	bool retVal = true;

	try
	{
		vector<string> tokens = split(response, ',');

		//if (tokens.size() == 5) // FIXME: Why is this so if the function is 'General' ?
		{
			for (size_t i = 1; i < tokens.size(); i++)
			{
				vector<string> parameters = split(tokens[i], ':');

				if (parameters.size() == 2)
				{
					int index = stod(trim(parameters[0]));
					int value = stod(trim(parameters[1]));
					returnData[index-1] = value;
				}
				else
				{
					retVal &= false;
				}
			}
		}
	}
	catch (const invalid_argument &ia)
	{
		retVal = false;
		cerr << "Could not parse response message: " << ia.what() << endl;
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

string FpcProtocol::GetLineData(string line)
{
	return line.substr(11);
}

bool FpcProtocol::LineDataDone(string line)
{
	if (line.find("\0\0") != string::npos)
	{
		return true;
	}
	return false;
}



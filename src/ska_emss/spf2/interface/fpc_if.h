/*
 * fpc_if.h
 *
 *  Created on: 15 May 2017
 *      Author: theuns
 */

#ifndef FPC_IF_H_
#define FPC_IF_H_

#include <map>
#include <string.h>

#include "Digital.h"
#include "SerialPort.h"

using namespace std;
using namespace serial;

#define HIST_BUFFER_SIZE 60 // 60 seconds of historical data
#define CRYO_MOTOR_DEFAULT_SPEED 60 //60 RPM

enum class enStartupDefault : short
{
	STANDBY = 0,
	OPERATE,
	MAINTENANCE
};

enum class enCapabilityState : short
{
	UNAVAILABLE = 0,
	STANDBY,
	OPERATE_DEGRADED,
	OPERATE_FULL
};

enum class enDeviceStatus : short
{
   UNKNOWN = 0,
   NORMAL = 1,
   DEGRADED = 2,
   FAILED = 3,
};

enum class enDeviceMode : short
{
    OFF = 0,
    STARTUP,
    STANDBY_LP,
    OPERATE,
    MAINTENANCE,
	ERROR,
    REGENERATION,
	PRE_VAC
};

typedef struct _stFeedSensors
{
	double 	controllerVoltage;
	double 	controllerCurrent;
	double 	controllerTemperature;
	double 	psu9VRectifiedVoltage;
	double 	psuAnalogue6vRegulatorTemp;
	double 	psuAnalogue15vRegulatorTemp;
	double 	psuDigital6vRegulatorTemp;
	double 	lnaHDrainVoltage1;
	double 	lnaHDrainVoltage2;
	double 	lnaHDrainVoltage3;
	double 	lnaHDrainCurrent1;
	double 	lnaHDrainCurrent2;
	double 	lnaHDrainCurrent3;
	double 	lnaHGateVoltage1;
	double 	lnaHGateVoltage2;
	double 	lnaHGateVoltage3;
	double 	lnaVDrainVoltage1;
	double 	lnaVDrainVoltage2;
	double 	lnaVDrainVoltage3;
	double 	lnaVDrainCurrent1;
	double 	lnaVDrainCurrent2;
	double 	lnaVDrainCurrent3;
	double 	lnaVGateVoltage1;
	double 	lnaVGateVoltage2;
	double 	lnaVGateVoltage3;
	double 	amp2HVoltage;
	double 	amp2HCurrent;
	double 	amp2VVoltage;
	double 	amp2VCurrent;
	double 	calsourceVoltage;
	double 	calsourceCurrent;
	double 	calsourceTemp;
	double 	pid1Current;
	double 	pid2Current;
	double 	cryostatPressure;
	double 	manifoldPressure;
	double 	lnaTemp;
	double 	omtTemp;
	double 	coldheadTemp;
	double 	cryostatBodyTemp;
	double 	analogueInterfaceTemp;
	double	psuMotorPos15VRegCurrent;
	double 	psuMotorNeg15VRegCurrent;
	double 	psuMotor15VRegTemp;
	double	spdHealth;

} stFeedSensors;

enum class enFpcMode : short
{
	UNKNOWN = 0,
	APPLICATION = 1,
	MAINTENANCE = 2
};

enum class enPid : short
{
	NONE = 0,
	CALSOURCE = 1,
	LNA = 2
};

enum class enMemoryRecord : short
{
	NONE = 0,
	CONFIG = 1,
	REC_GAIN = 2,
	REC_NOISE = 3,
	CAL_NOISE = 4
};

enum class enElapsedTimeCounter : short
{
	NONE = 0,
	OPERATIONAL = 1,
	MOTOR = 2,
	VALVE = 3
};

enum class enErrorBits : short
{
	AUTO_VALVE_CLOSE		= 0,
	TIMEOUT_VSC				= 1,
	TIMEOUT_CRYO_PRESS_DEC	= 2,
	TIMEOUT_CRYO_ON_PRESS	= 3,
	TIMEOUT_RFE1_TEMP_DEC	= 4,
	TIMEOUT_CRYO_PUMP_PRESS	= 5,
	TIMEOUT_COLD_OPS_TEMP	= 6,
	TIMEOUT_AMBIENT_TEMP	= 7,
	CRYOPUMP_RETRY_LIMIT	= 8,
	REGEN_RETRY_LIMIT 		= 9,
	TEMP_DEGRADED			= 10,
	PRESS_DEGRADED 			= 11,
	SPD_TRIGGERED			= 12,
	E_14					= 13,
	E_15					= 14,
	E_16					= 15
};

enum class enErrorServ : short
{
	P_SUDDEN_RISE			= 16,
	VAC_PUMP_ON				= 17,
	VAC_PUMP_PRESSURE		= 18,
	HE_COMPR_DISABLED		= 19,
	HE_COMPR_DIFF_PRESSURE	= 20,
	HE_COMPR_PRESSURE		= 21,
	HE_COMPR_TEMPERATURE	= 22,
	HE_COMPR_OVERLOAD		= 23,
	HE_COMPR_SURGE			= 24,
	HE_COMPR_SOFT_TRIGGER	= 25,
	VAC_PUMP_TIMEOUT		= 26,
	VSC_S_BAND	 			= 27,
	S_BAND_VALVE			= 28
};

typedef union _unErrorFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	AutoValveClose				: 1;
		unsigned	TimeoutVsc					: 1;
		unsigned	TimeoutCryoPressureDec		: 1;
		unsigned	TimeoutOnPressure			: 1;
		unsigned	TimeoutCryoOnPressure		: 1;
		unsigned	TimeoutCryoPumpPressure		: 1;
		unsigned	TimeoutColdOpsTemperature	: 1;
		unsigned	TimeoutAmbientTemperature	: 1;
		unsigned	CryoPumpRetryLimit			: 1;
		unsigned	RegenRetryLimit				: 1;
		unsigned	TxTempDegraded				: 1;
		unsigned	PressDegraded				: 1;
		unsigned	SurgeProtectionDevice		: 1;
		unsigned	SPARE						: 3;	// <- 16-bits
	}bits;
}unErrorFlags;

#define MAX_ERRORS 16

struct error_enumtypes
{
	enErrorBits eNum;
	char* eName;
	char* eDescr;
};
struct error_enumStypes
{
	enErrorServ eNum;
	char* eName;
	char* eDescr;
};


static const struct error_enumtypes rxErrorNames[MAX_ERRORS] = {
		{enErrorBits::AUTO_VALVE_CLOSE, 		(char*)"E_AutoValveClose", 			(char*)"Receiver firmware safety:Automatic valve close"},
		{enErrorBits::TIMEOUT_VSC,				(char*)"E_TimeoutVSC", 				(char*)"Timeout:Valve Safety Check not passed"},
		{enErrorBits::TIMEOUT_CRYO_PRESS_DEC,	(char*)"E_TimeoutCryoPressureDec",	(char*)"Timeout:Cryostat pressure not decreasing over time"},
		{enErrorBits::TIMEOUT_CRYO_ON_PRESS,	(char*)"E_TimeoutCryoOnPressure",	(char*)"Timeout:Cryocooler ON pressure not reached"},
		{enErrorBits::TIMEOUT_RFE1_TEMP_DEC,	(char*)"E_TimeoutLnaTempDec",		(char*)"Timeout:LNA temperature not decreasing over time"},
		{enErrorBits::TIMEOUT_CRYO_PUMP_PRESS,	(char*)"E_TimeoutCryoPumpPressure", (char*)"Timeout:Cryo-pumping temperature or pressure not reached"},
		{enErrorBits::TIMEOUT_COLD_OPS_TEMP,	(char*)"E_TimeoutColdOpsTemp",		(char*)"Timeout:Cold operational temperature not reached"},
		{enErrorBits::TIMEOUT_AMBIENT_TEMP,		(char*)"E_TimeoutAmbientTemp",		(char*)"Timeout:1st stage RFE ambient temperature not reached"},
		{enErrorBits::CRYOPUMP_RETRY_LIMIT,		(char*)"E_CryoPumpRetryLimit",		(char*)"Retry limit:Cryo-pumping retries limit reached"},
		{enErrorBits::REGEN_RETRY_LIMIT,		(char*)"E_RegenRetryLimit",			(char*)"Retry limit:Automatic regeneration retries limit reached"},
		{enErrorBits::TEMP_DEGRADED,			(char*)"E_RxTempDegraded",			(char*)"Temperature limit:1st stage RFE temperature above error limit"},
		{enErrorBits::PRESS_DEGRADED,			(char*)"E_RxPressDegraded",			(char*)"Pressure limit:Cryostat pressure above error limit"},
		{enErrorBits::SPD_TRIGGERED,			(char*)"E_SurgeProtectionDevice",	(char*)"Surge protection device failure triggered"},
		{enErrorBits::E_14,						(char*)"E_14",					(char*)"NON-ERROR - ALLOCATED FOR FUTURE USE"},
		{enErrorBits::E_15,						(char*)"E_15",					(char*)"NON-ERROR - ALLOCATED FOR FUTURE USE"},
		{enErrorBits::E_16,						(char*)"E_16",					(char*)"NON-ERROR - ALLOCATED FOR FUTURE USE"},
};

static const struct error_enumStypes servErrorNames[13] = {
		/* Non RX errors */
		{enErrorServ::P_SUDDEN_RISE, 			(char*)"E_PSuddenRise",			(char*)"Services:Manifold pressure sudden increase while vacuum pump required"},
		{enErrorServ::VAC_PUMP_ON,				(char*)"E_VacPumpOn",			(char*)"Services:Vacuum pump not working, unavailable or disabled from ITC state"},
		{enErrorServ::VAC_PUMP_PRESSURE,		(char*)"E_VacPumpPressure",	 	(char*)"Services:Vacuum pump not decreasing manifold pressure"},
		{enErrorServ::HE_COMPR_DISABLED,		(char*)"E_HeComprDisabled",	 	(char*)"Services:Helium compressor not working, disabled or in ITC state"},
		{enErrorServ::HE_COMPR_DIFF_PRESSURE,	(char*)"E_HeComprDiffPressure", (char*)"Services:Helium compressor supply or differential pressure out of range"},
		{enErrorServ::HE_COMPR_PRESSURE,		(char*)"E_HeComprPressure",	 	(char*)"Services:Helium compressor pressure fault"},
		{enErrorServ::HE_COMPR_TEMPERATURE,		(char*)"E_HeComprTemperature",	(char*)"Services:Helium compressor temperature fault"},
		{enErrorServ::HE_COMPR_OVERLOAD,		(char*)"E_HeComprOverload",	 	(char*)"Services:Helium compressor overload fault"},
		{enErrorServ::HE_COMPR_SURGE,			(char*)"E_HeComprSurge",		(char*)"Services:Helium compressor surge fault"},
		{enErrorServ::HE_COMPR_SOFT_TRIGGER,	(char*)"E_HeComprSoftTrigger",	(char*)"Services:Software triggered error:low return pressure"},
		{enErrorServ::VAC_PUMP_TIMEOUT,			(char*)"E_VacPumpTimeout",		(char*)"Services:Vacuum pump timeout reached"},
		{enErrorServ::VSC_S_BAND,				(char*)"E_VSCwithSband",		(char*)"Services:Valve Safety Check not passed OR S-band valve open"},
		{enErrorServ::S_BAND_VALVE,				(char*)"E_SbandValve",			(char*)"S-band valve open, not closing on command"},
};


enum class enControlBitNumber : short
{
	LNA_H				= 0,
	LNA_V				= 1,
	AMP2_H				= 2,
	AMP2_V				= 3,
	LNA_H_ILLUMINATION	= 4,
	LNA_V_ILLUMINATION	= 5,
	CALSOURCE			= 6,
	PID_CALSOURCE		= 7,
	PID_LNA				= 8
};

typedef union _unControlFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	LnaH				: 1;
		unsigned	LnaV				: 1;
		unsigned	Amp2H				: 1;
		unsigned	Amp2V				: 1;
		unsigned	LnaHIllumination	: 1;
		unsigned	LnaVIllumination	: 1;
		unsigned	CalSource			: 1;
		unsigned	PidCalSource		: 1;
		unsigned	PidLna				: 1;
		unsigned	SPARE				: 7;	// <- 16-bits
	}bits;
}unControlFlags;

typedef struct _stPidSettings
{
	int 	proportional;
	double 	integral;
	double  derivative;
	//int 	temp;
} stPidSettings;

// TODO: Better to do this in array?
typedef struct _stLnaBiasParameters
{
	double drainVolt1;
	double drainCurrent1;
	double drainVolt2;
	double drainCurrent2;
	double drainVolt3;
	double drainCurrent3;
} stLnaBiasParameters;

typedef union _unValidityFlags
{
	unsigned short	Word; // 16 bits
	struct
	{
		unsigned	sensors				: 1;
		unsigned	vacuumValve			: 1;
		unsigned	cryoMotor			: 1;
		unsigned	controlRegister		: 1;
		unsigned	errorRegister		: 1;
		unsigned	lnaHBias			: 1;
		unsigned	lnaVBias			: 1;
		unsigned	pidCalSource		: 1;
		unsigned	pidLna				: 1;
		unsigned 	rtc					: 1;
		unsigned	eltOperational		: 1;
		unsigned 	eltMotor			: 1;
		unsigned 	eltValve			: 1;
		unsigned	SPARE				: 3;	// <- 16-bits
	}bits;
}unValidityFlags;

typedef struct _stFeedDataFrame
{
	bool enabled;
	bool expectedOnline;

	double timestamp;
	bool isValveOpen;
	short cryoMotorSpeed;
	stFeedSensors sensors;
	unControlFlags controlFlags;
	unErrorFlags errorFlags;
	short lastError;
	short lastServError;

	int elapsedOperational;
	int elapsedMotor;
	int elapsedValve;

	enDeviceStatus deviceStatus;
	enFpcMode fpcMode;
	enDeviceMode currentMode;

	short pidLnaTempSetPoint;
	short pidCalSourceTempSetPoint;
	unsigned short coolDownCounter;

	unValidityFlags validities;

	// TODO: Is this really needed to add in every data frame?
	//string fwVersion;
	//string serial;
} stFeedDataFrame;

class FpcProtocol
{
public:
	FpcProtocol();
	FpcProtocol(const string PortName);
	~FpcProtocol();

	bool Init();
	bool Reset();

	/* LED */
	bool SetOnlineLed();
	bool ClearOnlineLed();

	/* =================== */
	/* Functional commands */
	/* =================== */
	bool DownloadConfigFile();
	bool DuplicateConfigFile();
	bool UploadConfigFile(string & response);
	bool UploadLnaBias(stLnaBiasParameters biasH, stLnaBiasParameters biasV, string & response);
	bool DownloadModelFiles();
	bool UploadModelFiles(string & response);

	bool LnaOn();
	bool LnaOff();
	bool LnaHOn();
	bool LnaHOff();
	bool LnaVOn();
	bool LnaVOff();

	bool Amp2On();
	bool Amp2Off();
	bool Amp2HOn();
	bool Amp2HOff();
	bool Amp2VOn();
	bool Amp2VOff();

	bool LnaIlluminationOn();
	bool LnaIlluminationOff();
	bool LnaHIlluminationOn();
	bool LnaHIlluminationOff();
	bool LnaVIlluminationOn();
	bool LnaVIlluminationOff();

	bool CalSourceOn();
	bool CalSourceOff();

	bool PidLnaOn();
	bool PidLnaOff();
	bool PidCalSourceOn();
	bool PidCalSourceOff();

	bool GetValveStatus();
	bool GetMotorStatus();

	bool ClearAllErrors();
	bool SetError(enErrorBits eb);
	bool ClearError(enErrorBits eb);


	/* =============== */
	/* Serial commands */
	/* =============== */

	/* Sensor Monitoring */
	bool SampleAll(stFeedSensors & sensors);
	bool SampleSingleSensor(unsigned int channelNumber, double& value);
	bool SelectChannels(std::vector<string> channels);
	bool SampleSelectedChannels(stFeedSensors & sensors); // Not all parameters are populated in struct
	bool SetAllChannels();
	bool EnableSensorAveraging();
	bool DisableSensorAveraging();

	/* Control */
	bool ModifyControlRegister(unControlFlags controlFlags);
	bool ModifyControlBit(enControlBitNumber controlBit, unsigned int value);
	bool GetControlRegister(unControlFlags & controlFlags);
	bool GetControlBit(unsigned int controlBit, unsigned int & value);

	/* Cooler Motor Control */
	bool StartCryoMotor();
	bool StopCryoMotor();
	bool SetCryoMotorSpeed(short rpm);
	bool GetCryoMotorSpeed(short & rpm);

	/* Vacuum Valve */
	bool OpenVacuumValve();
	bool CloseVacuumValve();
	bool GetVacuumValveStatus(bool  & isOpen);

	/* PID Controllers */
	bool SetPidPropGain(enPid pid, int gain);
	bool SetPidIntegral(enPid pid, double integral);
	bool SetPidDerivative(enPid pid, double derivative);
	bool SetPidTempSetPoint(enPid pid, short temp);
	bool GetPidTempSetPoint(enPid pid, short & temp);
	bool SavePidCoeffs(enPid pid);
	bool GetPidCoeffs(enPid pid, stPidSettings & pidSettings);

	/* LNA Biasing */
	bool SetLnaHBias(stLnaBiasParameters biasParameters);
	bool SetLnaVBias(stLnaBiasParameters biasParameters);
	bool GetLnaHBias(stLnaBiasParameters & biasParameters);
	bool GetLnaVBias(stLnaBiasParameters & biasParameters);

	/* Firmware */
	bool GetFirmwareVersion(string & version);
	/* Serial */
	bool GetSerialNumber(string & serial);

	/* Memory Storage */
	bool EraseRecord(enMemoryRecord record);
	bool ReadRecord(enMemoryRecord record, string fileName);
	bool ReadRecordNew(enMemoryRecord record, string fileName);
	bool WriteRecord(enMemoryRecord record, string fileName);

	/* Error Flags */
	bool ModifyErrorRegister(unErrorFlags errorFlags);
	bool ModifyErrorBit(unsigned int errorBit, unsigned int value);
	bool GetErrorRegister(unErrorFlags & errorFlags);
	bool GetErrorBit(unsigned int errorBit, unsigned int & value);

	/* Real-time clock */
	bool SetSystemTime(unsigned int time);
	bool GetSystemTime(unsigned int & time);

	/* Elapsed time counter */
	bool GetElapsedTime(enElapsedTimeCounter counterChannel, int & time);
	bool ResetElapsedTime(enElapsedTimeCounter counterChannel);

	/* Cool down counter */
	bool GetCoolDownCounter(unsigned short & count);
	bool IncrementCoolDownCounter();
	bool ClearCoolDownCounter();

	/* State and Mode Control */
	bool RemainMaintenance();
	bool GoApplication();
	bool GoUpdate();
	bool GoMaintenance();
	bool GetCurrentMode(enFpcMode & mode);

	bool SendRawCommand(string command, string & response);

private:
	string portName;
	Serial* serialPort;
	Digital* serialTX;
	Digital* serialRX;
	Digital * ioOutLedOnline;

	bool SendSimpleCommand(string command);
	bool ValidateResponse(string response);
	double ParseMillibar(std::string text);
	bool ParseSampleAll(string response, stFeedSensors & sensors);
	bool ParseSampleSingle(string response, unsigned int & channelNumber, double & value);
	bool ParseGeneralSingle(string response, string & parsedValue);
	bool ParseGeneralDouble(string response, string & parsedParameter, string & parsedValue);
	bool ParseGeneralMultipleInt(string response, map<int, int> & returnData);
	bool ParseGeneralMultipleDouble(string response, map<int, double> & returnData);
	string GetLineData(string line);
	bool LineDataDone(string line);
};

#endif /* FPC_IF_H_ */

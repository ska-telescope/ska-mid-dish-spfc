//****************************************************************************
// Model: Band2Model.qm
// File:  ./fp2_sm.h
//
// This code has been generated by QM tool (see state-machine.com/qm).
// DO NOT EDIT THIS FILE MANUALLY. All your changes will be lost.
//
// This program is open source software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//****************************************************************************
//${.::fp2_sm.h} .............................................................
#ifndef fp2_sm_h
#define fp2_sm_h

#include <bitset>
#include "qpcpp.h"
#include "fpc_if.h"
#include "IniParser.h"
#include "DataFrame.h"
#include "check_ctrl.h"
#include "SensorLogger.h"
#include "CircularFifo.h"
#include "tango.h"
//#include "log4tango.h"
#include "utilities.h"

#include "spfhe_proxy.h"
#include "spfvac_proxy.h"
#include "system.h"

#include <string>
#include <unistd.h>
#include <chrono>
#include <ctime>
using namespace std::chrono;

using namespace util;
using namespace std;
using namespace Tango;

namespace FP2
{
    struct stFeedConfig
    {
        string serial;
        string serialLnaH;
        string serialLnaV;

        double meanGainH;
        double meanGainV;

        double lnaHVd[3];
        double lnaHId[3];
        double lnaVVd[3];
        double lnaVId[3];
    };

    enum class enControllerState : int
    {
        None = 0,
        Off,
        Standby,
        Operate,
        Unavailable,
        Available,
        Transitional,
        Regeneration,
        RegenPending,
        Degraded,
        Maintenance,
        Error,
    };

    enum FP2Signals
    {
        OPERATIONAL_SIG = QP::Q_USER_SIG,
        /* Timer events */
        PING_FEED_SIG,
        VALVE_OPEN_CHECK_SIG,
        CHECK_SIG,
        DEC_CHECK_SIG,
        CT1_ERROR_SIG,
        CT2_ERROR_SIG,
        AMB_ERROR_SIG,
        WATCHDOG_SIG,
        VAC_CHECK_SIG,
        ERROR_SIG,
        SAMPLE_SENSORS_SIG,

        NEXT_SIG,

        WARM_OPS_SIG,
        ITC_SIG,
        SHUTDOWN_SIG,
        OPEN_VALVE_SIG,
        VALVE_OPENED_SIG,    // Valve already open
        VALVE_CLOSED_SIG,
        CLOSE_VALVE_SIG,
        CRYOSTAT_VACUUM_SIG,
        CRYO_VAC_READY_SIG,
        STEPPER_ON_SIG,
        STEPPER_OFF_SIG,
        VAC_ERROR_SIG,

        PID_LNA_ON_SIG,
        PID_LNA_OFF_SIG,
        PID_LNA_SP_WARM_SIG,
        PID_CAL_SOURCE_ON_SIG,
        PID_CAL_SOURCE_OFF_SIG,
        CAL_SOURCE_ON_SIG,
        CAL_SOURCE_OFF_SIG,

        RFE2_ON_SIG,
        RFE2_OFF_SIG,
        CRYO_PUMP_SIG,
        CRYO_PUMP_READY_SIG,
        REGENERATE_SIG,
        RGN_TEST_SIG,
        RGN_FINISHED_SIG,
        ERROR_CORRECTED_SIG,
        TEMP_RISE_SIG,
        VACUUM_LOST_SIG,
        RETRY_VAC_SIG,
        SOFT_OFF_SIG,
        SELF_CHECK_SIG,
        ERRORS_CLEARED_SIG,
        LNA_H_ON_SIG,
        LNA_H_OFF_SIG,
        LNA_V_ON_SIG,
        LNA_V_OFF_SIG,
        LNA2_H_ON_SIG,
        LNA2_H_OFF_SIG,
        LNA2_V_ON_SIG,
        LNA2_V_OFF_SIG,
        LNA_ILLUM_OFF_SIG,
        LNA_ILLUM_ON_SIG,
        LNA_H_ILLUM_ON_SIG,
        LNA_H_ILLUM_OFF_SIG,
        LNA_V_ILLUM_ON_SIG,
        LNA_V_ILLUM_OFF_SIG,
        OFFLINE_SIG,
        VAC_FAIL_SIG,
        HE_COMPR_FAIL_SIG,
        RESET_SIG,
        UL_CONFIG_SIG,
        UL_BIAS_SIG,
        DL_FILES_SIG,
        UL_FILES_SIG,
        UPDATE_CONFIG_SIG,

        TEST_SERVICE_SIG,

        MAX_PUB_SIG,
        IGNORE_SIG,
        MAX_SIG
    };

    // BSP functions to dispaly a message and exit
    void BSP_Display(char_t const *msg);
}

namespace FP2 {


#if ((QP_VERSION < 580) || (QP_VERSION != ((QP_RELEASE^4294967295) % 0x3E8)))
#error qpcpp version 5.8.0 or higher required
#endif

//${AOs::FP2AO} ..............................................................
class FP2AO : public QP::QActive {
private:
    QP::QTimeEvt pingFeedTimer;
    QP::QTimeEvt valveOpenCheckTimer;
    QP::QTimeEvt checkTimer;
    QP::QTimeEvt decrementCheckTimer;
    QP::QTimeEvt cryopumpErrorTimer;
    QP::QTimeEvt coldOperationalErrorTimer;
    QP::QTimeEvt ambientErrorTimer;
    QP::QTimeEvt watchdogTimer;
    QP::QTimeEvt vacCheckTimer;
    FpcProtocol * fpInterface;
    IniParser * configFile;
    string configFilePath;
    QP::QTimeEvt errorTimer;
    CheckController* checkControl;
    static CircularFifo<stFeedDataFrame, HIST_BUFFER_SIZE> systemData;
    uint8_t vacRetries;
    uint8_t cryopumpRetries;
    bool regenerate;
    double cryoPressure;
    double manifoldPressure;
    bool coldTransitionFlag;
    int16_t degradedReason;
    enDeviceStatus deviceStatus;
    enControllerState state;

public:
    stFeedConfig feedConfig;

private:
    bool vacuumRequested;
    bool heliumRequested;
    uint16_t warnCnt;
    QP::QTimeEvt sampleSensorsTimer;

public:
    stSettings* settings;

private:
    SpfVac::SpfVacProxy* spfVacProxy;
    SpfHe::SpfHeProxy* spfHeProxy;
    uint16_t setpoint;
    bool lnaPidOn;

public:
    bool feedDetected;

private:
    enDeviceMode deviceMode;

public:
    string fwVersion;

private:
    enCapabilityState capabilityState;
    SensorLogger* sensorLog;
    int8_t offlineThreshold;
    string currentState;
    unErrorFlags errorFlags;

public:
    string spfHeLocation;
    string spfVacLocation;
    int lnaPidTempSetpoint;
    int calSourcePidTempSetpoint;
    string spf2Location;
    string ttyPort;
    QP::QTimeEvt vacErrorTimer;
    bool preVacMode;
    stLnaBiasParameters biasHParameters;
    stLnaBiasParameters biasVParameters;
    short lastError;
    short lastServError;

public:
    FP2AO();
    enDeviceStatus GetDeviceStatus();
    enControllerState GetState();
    stFeedDataFrame GetDataFrame();
    bool SendRawCommand(string command, string & response);

private:
    void SwitchAllOff();
    bool RequestVacuum();
    bool CancelVacuum();
    void RequestHelium();
    void CancelHelium();

public:
    enDeviceMode GetFeedMode();
    bool SetLnaTempSetpoint(int kelvin);
    enCapabilityState GetCapabilityState();
    string GetCurrentState();
    bool SetCryoMotorSpeed(short rpm);
    bool SetCalSourceTempSetpoint(int kelvin);
    void SetSettings(stSettings* setSettings);
    bool SetConfigFile(string & response);
    bool UpdateConfigFile(string & response);
    bool UpdateBias(string & response);
    bool SetModelFiles(string & response);
    bool ErrTest(short ErrBit);

protected:
    static QP::QState initial(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Active(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Operational_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Transitional_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState coldTransition_check(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState pumpCurCheck_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState manifoldPressure_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState VSC_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState cryoPressure_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState WarmTransient_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState rfe1Ambient_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Cold_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState rfe1TempDecrease_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState ColdTransient1_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState correctSupplyP_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState compressorOn_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState cryoPumping_timeout(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState ColdTransient2_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState cryoPminCheck_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Degraded_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState ColdVacuum_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Regeneration_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState OffLine_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Diagnostic_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState IntegrationTestControl_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState SelfCheck_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState CheckStartUp_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState SoftOff_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Error_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState DefinedErrors_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState VaccumErrorRetry_state(FP2AO * const me, QP::QEvt const * const e);
    static QP::QState Destruct(FP2AO * const me, QP::QEvt const * const e);
};

} // namespace FP2

namespace FP2
{
    extern FP2AO * const fp2AO;
}


#endif // fp2_sm_h

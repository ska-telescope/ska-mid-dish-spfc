#ifndef fpc_ctrl_h
#define fpc_ctrl_h

#include <string.h>

#include "fpc_if.h"
#include "fp2_sm.h"
#include "IniParser.h"
#include "DataFrame.h"

class FeedPackageControl
{
public:
	FeedPackageControl();
    ~FeedPackageControl();
    void LoadIniSettings();
    void ReloadSettings();

    bool Run();
    bool Shutdown();

    /* Controller commands - called by Tango internals */
    bool Start();
    bool Stop();
    bool Reset();

    void SetSpfHeLocation(std::string spfHeLocation);
    void SetSpfVacLocation(std::string spfVacLocation);
    void SetSpf2Location(std::string spf2Location);
    void SetComPort(std::string ttyPort);

    /* Normal operational commands */
    enDeviceStatus GetDeviceStatus();
    enDeviceMode GetFeedMode();
    enCapabilityState GetCapabilityState();
    string GetCurrentState();
    stFeedDataFrame GetDataFrame();
    FP2::stFeedConfig GetFeedConfig();
    stLnaBiasParameters GetLnaHBiasParameters();
    stLnaBiasParameters GetLnaVBiasParameters();
    void SetLnaHBiasParameters(stLnaBiasParameters biasParams);
    void SetLnaVBiasParameters(stLnaBiasParameters biasParams);

    bool SetB2SerialNr(Tango::DevString serialNr);
    bool SetLnaHSerialNr(Tango::DevString serialNr);
    bool SetLnaVSerialNr(Tango::DevString serialNr);
    bool SetLnaHGain(const Tango::DevFloat meanGain);
    bool SetLnaVGain(const Tango::DevFloat meanGain);
    bool SetLnaHBiasVoltParams(const Tango::DevFloat *params);
    bool SetLnaHBiasCurrentParams(const Tango::DevFloat *params);
    bool SetLnaVBiasVoltParams(const Tango::DevFloat *params);
    bool SetLnaVBiasCurrentParams(const Tango::DevFloat *params);
    string GetFirmwareVersion();

    bool SendRawCommand(string command, string & response);
    bool SetConfigFile(string & response);
    bool SetModelFiles(string & response);
    bool UpdateBias(string & response);
    bool UpdateConfigFile(string & response);

    /* Mode commands commands */
    bool SetMode(std::string mode);

    bool ClearErrors();

    bool SetExpectedOnline(bool state);
    bool GetExpectedOnline();
    bool SetStartupDefault(enStartupDefault state);
    enStartupDefault GetStartupDefault();

    bool SetTempSetpoint(int sp);
    bool SetCalSourceTempSetpoint(int sp);

    bool SetDefaultTempSetpoint(int sp);
    int  GetDefaultTempSetpoint();
    bool SetDefaultCalSourceTempSetpoint(int sp);
    int  GetDefaultCalSourceTempSetpoint();
    double GetTS_OffMax();

    bool FeedDetected();

    bool SetLnaHPowerState(bool state);
    bool SetLnaVPowerState(bool state);
    bool SetAmp2HPowerState(bool state);
    bool SetAmp2VPowerState(bool state);
    bool SetLnaIllumination(bool state);
    bool SetLnaHIllumination(bool state);
    bool SetLnaVIllumination(bool state);
    bool SetCalSourcePowerState(bool state);
    bool SetPidCalSourcePowerState(bool state);
    bool SetPidLnaPowerState(bool state);

    bool SetMotorState(bool state);
    bool SetMotorSpeed(short rpm);

    bool SetVacuumValve(bool state);

    int IniKeyLoadOrCreateInt(std::string key, int value, std::string section);
    unsigned int IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section);
    double IniKeyLoadOrCreateDouble(std::string key, double value, std::string section);

    /* To be defined */
    bool Test();
    bool ErrTest(short ErrBit);
private:
    pthread_t sm_thread;

    IniParser * settingsFile;
    stSettings * settings;

};

#endif /* fpc_ctrl_h */

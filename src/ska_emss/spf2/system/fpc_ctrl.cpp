#include "fpc_ctrl.h"

#include "qpcpp.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>      // for memcpy() and memset()
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "fp2_sm.h"
#include "fpc_if.h"
#include "system.h"

using namespace std;
using namespace QP;
using namespace FP2;

static QP::QTicker l_ticker0(0); // ticker for tick rate 0
QP::QActive * the_Ticker0 = &l_ticker0;

extern "C" void Q_onAssert(char const * const module, int loc)
{
    cout << "Assertion failed in " << module
              << " location " << loc << endl;
    QS_ASSERTION(module, loc, static_cast<uint32_t>(10000U));
}

void QF::onStartup(void)
{
}
void QF::onCleanup(void)
{
    cout << endl << "Cleaning up" << endl;
}

void QP::QF_onClockTick(void)
{
	//QF::TICK_X(0U, (void *)0);  // perform the QF clock tick processing
	the_Ticker0->POST(0, 0); // post a don't-care event to Ticker0
}

void *SM_main(void * arg)
{
	static QF_MPOOL_EL(QEvt) smlPoolSto[100];
    //static QSubscrList subscrSto[MAX_PUB_SIG];
    static QEvt const *eventQSto[100]; // Event queue storage for SpfVac

    QF::init(); // initialize the framework and the underlying RT kernel

    // init publish-subscribe
    //QF::psInit(subscrSto, Q_DIM(subscrSto)); // init publish-subscribe

    // dynamic event allocation not used, no call to
    // initialize event pools...
    QF::poolInit(smlPoolSto, sizeof(smlPoolSto), sizeof(smlPoolSto[0]));

    // instantiate and start the active objects...
    //fp2AO->settings = (stSettings *)(arg); /* TODO: This passes the pointer to the AO, meaning it can edit the values */
    fp2AO->SetSettings((stSettings *)(arg));/* TODO: This passes the pointer to the AO, meaning it can edit the values */

    the_Ticker0->start(1U, // priority
                             0, 0, 0, 0);

    fp2AO->start(2U,                            // priority
                     eventQSto, Q_DIM(eventQSto),   // event queue
                     (void *)0, 0U);                // stack (unused

    QF::run(); // run the QF application - does not return
    cout << "QF::run() exited - QF framework halted" << endl;

    return (void*)0;
}


FeedPackageControl::FeedPackageControl()
{
	cout << "----------------- [FeedPackageControl()] -------------" << endl;
	sm_thread = 0;

	/* Read the config file */
	settingsFile = new IniParser(SPF2x_SETTINGS_PATH + devName + SPF2x_SETTINGS);

	settings = new stSettings();

	LoadIniSettings();

}

FeedPackageControl::~FeedPackageControl()
{
	cout << "----------------- [~FeedPackageControl()] ------------" << endl;

	fp2AO->POST(Q_NEW(QEvt, SHUTDOWN_SIG), me);

	fp2AO->stop();
	QF::stop(); /* stop QF and cleanup */

	delete settings;
	delete settingsFile; /* Will save any unsaved stuff */
}

void FeedPackageControl::LoadIniSettings()
{
	cout << "----------------- [LoadIniSettings()] -------------" << endl;

	/* Read the config file */

	settings->variables.expected_online = IniKeyLoadOrCreateInt(N_EXPECTED_ONLINE, VAL_EXPECTED_ONLINE, INI_VARS);
	settings->variables.startup_default = static_cast<enStartupDefault>(IniKeyLoadOrCreateInt(N_STARTUP_DEFAULT, VAL_STARTUP_DEFAULT, INI_VARS));
	settings->variables.TS_Rfe1 = IniKeyLoadOrCreateInt(N_TS_RFE1, VAL_TS_RFE1, INI_VARS);
	settings->variables.TS_CalSource = IniKeyLoadOrCreateInt(N_TS_CAL_SOURCE, VAL_TS_CAL_SOURCE, INI_VARS);

	settings->constants.P_SuddenRiseValveClose = IniKeyLoadOrCreateDouble(N_P_RISE_VALVE_CLOSE, VAL_P_RISE_VALVE_CLOSE, INI_CONST);

	settings->constants.P_PreVac = IniKeyLoadOrCreateDouble(N_P_PRE_VAC, VAL_P_PRE_VAC, INI_CONST);
	settings->constants.P_CryocoolerOn = IniKeyLoadOrCreateDouble(N_P_CRYO_ON, VAL_P_CRYO_ON, INI_CONST);
	settings->constants.P_CryoPump = IniKeyLoadOrCreateDouble(N_P_CRYO_PUMP, VAL_P_CRYO_PUMP, INI_CONST);
	settings->constants.P_CryoPumpRising = IniKeyLoadOrCreateDouble(N_P_CRYO_PUMP_RISING, VAL_P_CRYO_PUMP_RISING, INI_CONST);
	settings->constants.P_CryoPumpSatisfy = IniKeyLoadOrCreateDouble(N_P_CRYO_PUMP_SATISFY, VAL_P_CRYO_PUMP_SATISFY, INI_CONST);
	settings->constants.PE_CryoPump = IniKeyLoadOrCreateDouble(N_PE_CRYO_PUMP, VAL_PE_CRYO_PUMP, INI_CONST);
	settings->constants.P_ColdOperationalDegraded = IniKeyLoadOrCreateDouble(N_P_COLD_OPERATIONAL_DEG, VAL_P_COLD_OPERATIONAL_DEG, INI_CONST);
	settings->constants.T_CryoPumping = IniKeyLoadOrCreateDouble(N_T_CRYO_PUMPING, VAL_T_CRYO_PUMPING, INI_CONST);
	settings->constants.T_CryoRecovery = IniKeyLoadOrCreateDouble(N_T_CRYO_RECOVERY, VAL_T_CRYO_RECOVERY, INI_CONST);
	settings->constants.T_ColdOperational = IniKeyLoadOrCreateDouble(N_T_COLD_OPERATIONAL, VAL_T_COLD_OPERATIONAL, INI_CONST);
	settings->constants.TS_MaxOffset = IniKeyLoadOrCreateDouble(N_TS_MAX_OFFSET, VAL_TS_MAX_OFFSET, INI_CONST);
	settings->constants.TS_StabilityDelta = IniKeyLoadOrCreateDouble(N_TS_STABILITY_DELTA, VAL_TS_STABILITY_DELTA, INI_CONST);
	settings->constants.TS_OffMax = IniKeyLoadOrCreateDouble(N_TS_OFF_MAX, VAL_TS_OFF_MAX, INI_CONST);
	settings->constants.TS_OffStabilityDelta = IniKeyLoadOrCreateDouble(N_TS_OFF_STABILITY_DELTA, VAL_TS_OFF_STABILITY_DELTA, INI_CONST);
	settings->constants.TE_ColdOperational = IniKeyLoadOrCreateDouble(N_TE_COLD_OPERATIONAL, VAL_TE_COLD_OPERATIONAL, INI_CONST);
	settings->constants.T_RegenerationAmbient = IniKeyLoadOrCreateDouble(N_T_REGEN_AMB, VAL_T_REGEN_AMB, INI_CONST);
	settings->constants.Max_VacuumErrorRetry = IniKeyLoadOrCreateUInt(N_MAX_VACUUM_E_RETRY, VAL_MAX_VACUUM_E_RETRY, INI_CONST);
	settings->constants.Max_CryoPumpingRetry = IniKeyLoadOrCreateUInt(N_MAX_CRYOPUMPING_RETRY, VAL_MAX_CRYOPUMPING_RETRY, INI_CONST);
	settings->constants.TE_Rfe1PidMaxTemp = IniKeyLoadOrCreateDouble(N_TMAX_RFE1_PID, VAL_TMAX_RFE1_PID, INI_CONST);
	settings->constants.TE_CalSourcePidMaxTemp = IniKeyLoadOrCreateDouble(N_TMAX_CAL_PID, VAL_TMAX_CAL_PID, INI_CONST);

    settings->timeouts.Timeout_VacPumpOn = IniKeyLoadOrCreateUInt(N_TO_VAC_PUMP_ON, VAL_TO_VAC_PUMP_ON, INI_TIMEOUTS);
    settings->timeouts.Timeout_CryoPressureDecrease = IniKeyLoadOrCreateUInt(N_TO_CRYO_PRESS_DEC, VAL_TO_CRYO_PRESS_DEC, INI_TIMEOUTS);
    settings->timeouts.Timeout_Rfe1TemperatureDecrease = IniKeyLoadOrCreateUInt(N_TO_CRYO_TEMP_DEC, VAL_TO_CRYO_TEMP_DEC, INI_TIMEOUTS);
    settings->timeouts.Timeout_CryoOnPressure = IniKeyLoadOrCreateUInt(N_TO_CRYO_ON_PRESS, VAL_TO_CRYO_ON_PRESS, INI_TIMEOUTS);
    settings->timeouts.Timeout_VSC = IniKeyLoadOrCreateUInt(N_TO_VSC, VAL_TO_VSC, INI_TIMEOUTS);
    settings->timeouts.Timeout_CompressorOn = IniKeyLoadOrCreateUInt(N_TO_COMPRESSOR_ON, VAL_TO_COMPRESSOR_ON, INI_TIMEOUTS);
    settings->timeouts.Timeout_CryoPumpPressure = IniKeyLoadOrCreateUInt(N_TO_CRYO_PUMP_PRESS, VAL_TO_CRYO_PUMP_PRESS, INI_TIMEOUTS);
    settings->timeouts.Timeout_ColdOpsTemp = IniKeyLoadOrCreateUInt(N_TO_COLD_OPS_TEMP, VAL_TO_COLD_OPS_TEMP, INI_TIMEOUTS);
    settings->timeouts.Timeout_AmbientTemp = IniKeyLoadOrCreateUInt(N_TO_AMBIENT_TEMP, VAL_TO_AMBIENT_TEMP, INI_TIMEOUTS);

    settingsFile->Save();
}

void FeedPackageControl::ReloadSettings()
{
	settingsFile->Reload();
	LoadIniSettings();
    fp2AO->settings = settings;
}

/* This function starts the state machine thread */
bool FeedPackageControl::Run()
{
	cout << "STARTING SM THREAD" << endl;

	bool retVal = false;

	pthread_attr_t attr;
	pthread_attr_init(&attr);

	// SCHED_FIFO corresponds to real-time preemptive priority-based scheduler
	// NOTE: This scheduling policy requires the superuser privileges
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);

	struct sched_param param;
	param.sched_priority = 1 + (sched_get_priority_max(SCHED_FIFO) - 60);

	pthread_attr_setschedparam(&attr, &param);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

    int res = pthread_create(&sm_thread, &attr, SM_main, this->settings);
    if (res != 0)
    {
        perror("SM_main() pthread create");
        // TODO: handle error here ...
    }
    else
    {
        retVal = true;
    }

    return retVal;
}


bool FeedPackageControl::Reset()
{
	return fp2AO->POST(Q_NEW(QEvt, RESET_SIG), me);
}

bool FeedPackageControl::Shutdown()
{
    cout << "################## VIA TANGO ##################" << endl;
    cout << "Setting State to SHUTDOWN " << endl;

    return fp2AO->POST(Q_NEW(QEvt, SHUTDOWN_SIG), me);
}

bool FeedPackageControl::SetMode(std::string mode)
{
	bool retVal = false;

	cout << "Setting mode to: [" << mode << "]" << endl;

	if (mode == "MAINT")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, ITC_SIG), me);
	}
	else if (mode == "STANDBY")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, SOFT_OFF_SIG), me);
	}
	else if (mode == "OPER")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, OPERATIONAL_SIG), me);
	}
	else if (mode == "REGEN")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, REGENERATE_SIG), me);
	}
	else if (mode == "DL_FILES")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, DL_FILES_SIG), me);
	}
	else if (mode == "UL_FILES")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, UL_FILES_SIG), me);
	}
	else if (mode == "UL_CONFIG")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, UL_CONFIG_SIG), me);
	}
	else if (mode == "UD_CONFIG")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);
	}
	else if (mode == "UL_BIAS")
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, UL_BIAS_SIG), me);
	}
	return retVal;
}

bool FeedPackageControl::ClearErrors()
{
	return fp2AO->POST(Q_NEW(QEvt, ERRORS_CLEARED_SIG), me);
}

bool FeedPackageControl::SetVacuumValve(bool state)
{
	bool retVal = false;

	if (state) // open vacuum valve
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, OPEN_VALVE_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, CLOSE_VALVE_SIG), me);
	}

	return retVal;
}

void FeedPackageControl::SetSpfHeLocation(std::string spfHeLocation)
{
	fp2AO->spfHeLocation = spfHeLocation; // operator= copies data

}
void FeedPackageControl::SetSpfVacLocation(std::string spfVacLocation)
{
	fp2AO->spfVacLocation = spfVacLocation; // operator= copies data
}
void FeedPackageControl::SetSpf2Location(std::string spf2Location)
{
	fp2AO->spf2Location = spf2Location; // operator= copies data
}
void FeedPackageControl::SetComPort(std::string ttyPort)
{
	fp2AO->ttyPort = ttyPort; // operator= copies data
}



bool FeedPackageControl::SetExpectedOnline(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal  = settingsFile->SetInt("expected_online", 1, "", "Variables"); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		fp2AO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
	}
	else
	{
		retVal  = settingsFile->SetInt("expected_online", 0, "", "Variables"); // We use SetInt here as we are saving a 0 or 1
		retVal &= settingsFile->Save();
		fp2AO->settings->variables.expected_online = state; // TODO: Should this be thread safe?
		retVal &= fp2AO->POST(Q_NEW(QEvt, OFFLINE_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::GetExpectedOnline()
{
	return settings->variables.expected_online;
}

bool FeedPackageControl::SetStartupDefault(enStartupDefault state)
{
	bool retVal = false;

	retVal  = settingsFile->SetInt("startup_default", static_cast<short>(state), "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.startup_default = state;

	return retVal;
}
enStartupDefault FeedPackageControl::GetStartupDefault()
{
	return settings->variables.startup_default;
}

bool FeedPackageControl::SetTempSetpoint(int sp)
{
	int tempValue = 0;
	switch(sp)
	{
	case 0:
		tempValue = 0;
		break;
	case 1:
		tempValue = 14;
		break;
	case 2:
		tempValue = 16;
		break;
	case 3:
		tempValue = 18;
		break;
	case 4:
		tempValue = 20;
		break;
	case 5:
		tempValue = 22;
		break;
	case 6:
		tempValue = 24;
		break;
	case 7:
		tempValue = 295;
		break;
	default:
		tempValue = sp;
		break;
	}

	return fp2AO->SetLnaTempSetpoint(tempValue);
}

bool FeedPackageControl::SetCalSourceTempSetpoint(int sp)
{
	int tempValue = 0;
	switch(sp)
	{
	case 0:
		tempValue = 0;
		break;
	case 1:
		tempValue = 280;
		break;
	case 2:
		tempValue = 285;
		break;
	case 3:
		tempValue = 290;
		break;
	case 4:
		tempValue = 296;
		break;
	case 5:
		tempValue = 300;
		break;
	case 6:
		tempValue = 305;
		break;
	case 7:
		tempValue = 310;
		break;
	default:
		tempValue = sp;
		break;
	}
	return fp2AO->SetCalSourceTempSetpoint(tempValue);
}

bool FeedPackageControl::SetDefaultTempSetpoint(int sp)
{
	bool retVal = false;
	int tempValue = 0;
	switch(sp)
	{
	case 0:
		tempValue = 0;
		break;
	case 1:
		tempValue = 14;
		break;
	case 2:
		tempValue = 16;
		break;
	case 3:
		tempValue = 18;
		break;
	case 4:
		tempValue = 20;
		break;
	case 5:
		tempValue = 22;
		break;
	case 6:
		tempValue = 24;
		break;
	default:
		tempValue = sp;
		break;
	}

	retVal  = settingsFile->SetInt("TS_Rfe1", tempValue, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_Rfe1 = tempValue;

	return retVal;
}
int FeedPackageControl::GetDefaultTempSetpoint()
{
	return settings->variables.TS_Rfe1;
}
bool FeedPackageControl::SetDefaultCalSourceTempSetpoint(int sp)
{
	bool retVal = false;
	int tempValue = 0;
	switch(sp)
	{
	case 0:
		tempValue = 0;
		break;
	case 1:
		tempValue = 280;
		break;
	case 2:
		tempValue = 285;
		break;
	case 3:
		tempValue = 290;
		break;
	case 4:
		tempValue = 296;
		break;
	case 5:
		tempValue = 300;
		break;
	case 6:
		tempValue = 305;
		break;
	default:
		tempValue = sp;
		break;
	}

	retVal  = settingsFile->SetInt("TS_CalSource", tempValue, "", "Variables");
	retVal &= settingsFile->Save();
	settings->variables.TS_CalSource = tempValue;

	return retVal;
}
double FeedPackageControl::GetTS_OffMax()
{
	return settings->constants.TS_OffMax;
}
int FeedPackageControl::GetDefaultCalSourceTempSetpoint()
{
	return settings->variables.TS_CalSource;
}

bool FeedPackageControl::FeedDetected()
{
	return fp2AO->feedDetected;
}

enDeviceStatus FeedPackageControl::GetDeviceStatus()
{
	return fp2AO->GetDeviceStatus();
}

enCapabilityState FeedPackageControl::GetCapabilityState()
{
	return fp2AO->GetCapabilityState();
}

string FeedPackageControl::GetCurrentState()
{
	return fp2AO->GetCurrentState();
}

enDeviceMode FeedPackageControl::GetFeedMode()
{
	return fp2AO->GetFeedMode();
}

stFeedDataFrame FeedPackageControl::GetDataFrame()
{
	return fp2AO->GetDataFrame();
}

FP2::stFeedConfig FeedPackageControl::GetFeedConfig()
{
	return fp2AO->feedConfig;
}

stLnaBiasParameters FeedPackageControl::GetLnaHBiasParameters()
{
	return fp2AO->biasHParameters;
}

stLnaBiasParameters FeedPackageControl::GetLnaVBiasParameters()
{
	return fp2AO->biasVParameters;
}

void FeedPackageControl::SetLnaHBiasParameters(stLnaBiasParameters biasParams)
{
	fp2AO->biasHParameters = biasParams;
}

void FeedPackageControl::SetLnaVBiasParameters(stLnaBiasParameters biasParams)
{
	fp2AO->biasVParameters = biasParams;
}

bool FeedPackageControl::SetB2SerialNr(Tango::DevString serialNr)
{
	bool retVal = false;
	if (fp2AO->GetCurrentState() == "Maintenance")
	{
		fp2AO->feedConfig.serial = serialNr;
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaHSerialNr(Tango::DevString serialNr)
{
	bool retVal = false;
	if (fp2AO->GetCurrentState() == "Maintenance")
	{
		fp2AO->feedConfig.serialLnaH = serialNr;
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaVSerialNr(Tango::DevString serialNr)
{
	bool retVal = false;
	if (fp2AO->GetCurrentState() == "Maintenance")
	{
		fp2AO->feedConfig.serialLnaV = serialNr;
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaHGain(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp2AO->GetCurrentState() == "Maintenance")
	{
		fp2AO->feedConfig.meanGainH = meanGain;
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaVGain(const Tango::DevFloat meanGain)
{
	bool retVal = false;
	if (fp2AO->GetCurrentState() == "Maintenance")
	{
		fp2AO->feedConfig.meanGainV = meanGain;
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaHBiasVoltParams(const Tango::DevFloat *params)
{
	bool retVal = false;
	if ((fp2AO->GetCurrentState() == "Maintenance") || (fp2AO->GetCurrentState() == "Offline"))
	{
		fp2AO->feedConfig.lnaHVd[0] = params[0];
		fp2AO->feedConfig.lnaHVd[1] = params[1];
		fp2AO->feedConfig.lnaHVd[2] = params[2];
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaHBiasCurrentParams(const Tango::DevFloat *params)
{
	bool retVal = false;
	if ((fp2AO->GetCurrentState() == "Maintenance") || (fp2AO->GetCurrentState() == "Offline"))
	{
		fp2AO->feedConfig.lnaHId[0] = params[0];
		fp2AO->feedConfig.lnaHId[1] = params[1];
		fp2AO->feedConfig.lnaHId[2] = params[2];
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaVBiasVoltParams(const Tango::DevFloat *params)
{
	bool retVal = false;
	if ((fp2AO->GetCurrentState() == "Maintenance") || (fp2AO->GetCurrentState() == "Offline"))
	{
		fp2AO->feedConfig.lnaVVd[0] = params[0];
		fp2AO->feedConfig.lnaVVd[1] = params[1];
		fp2AO->feedConfig.lnaVVd[2] = params[2];
		retVal = true;
//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

bool FeedPackageControl::SetLnaVBiasCurrentParams(const Tango::DevFloat *params)
{
	bool retVal = false;
	if ((fp2AO->GetCurrentState() == "Maintenance") || (fp2AO->GetCurrentState() == "Offline"))
	{
		fp2AO->feedConfig.lnaVId[0] = params[0];
		fp2AO->feedConfig.lnaVId[1] = params[1];
		fp2AO->feedConfig.lnaVId[2] = params[2];

//		retVal = fp2AO->POST(Q_NEW(QEvt, UPDATE_CONFIG_SIG), me);

	}
	return retVal;
}

string FeedPackageControl::GetFirmwareVersion()
{
	return fp2AO->fwVersion;
}

bool FeedPackageControl::SetLnaHPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_H_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_H_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetLnaVPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_V_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_V_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetAmp2HPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA2_H_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA2_H_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetAmp2VPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA2_V_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA2_V_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetLnaIllumination(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_ILLUM_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_ILLUM_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetLnaHIllumination(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_H_ILLUM_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_H_ILLUM_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetLnaVIllumination(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_V_ILLUM_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, LNA_V_ILLUM_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, CAL_SOURCE_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, CAL_SOURCE_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetPidCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, PID_CAL_SOURCE_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, PID_CAL_SOURCE_OFF_SIG), me);
	}

	return retVal;
}
bool FeedPackageControl::SetPidLnaPowerState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, PID_LNA_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, PID_LNA_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetMotorState(bool state)
{
	bool retVal = false;

	if (state)
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, STEPPER_ON_SIG), me);
	}
	else
	{
		retVal = fp2AO->POST(Q_NEW(QEvt, STEPPER_OFF_SIG), me);
	}

	return retVal;
}

bool FeedPackageControl::SetMotorSpeed(short rpm)
{
	return fp2AO->SetCryoMotorSpeed(rpm);
}

int FeedPackageControl::IniKeyLoadOrCreateInt(std::string key, int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetInt(key, section);
	}
}

unsigned int FeedPackageControl::IniKeyLoadOrCreateUInt(std::string key, unsigned int value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetUInt(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetUInt(key, section);
	}
}


double FeedPackageControl::IniKeyLoadOrCreateDouble(std::string key, double value, std::string section)
{
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
	{
		settingsFile->SetDouble(key, value, "", section);
		return value;
	} else
	{
		return settingsFile->GetDouble(key, section);
	}
}

bool FeedPackageControl::SendRawCommand(string command, string & response)
{
	return fp2AO->SendRawCommand(command, response);
}

bool FeedPackageControl::SetConfigFile(string & response)
{
	return fp2AO->SetConfigFile(response);
}

bool FeedPackageControl::SetModelFiles(string & response)
{
	return fp2AO->SetModelFiles(response);
}

bool FeedPackageControl::UpdateBias(string & response)
{
	return fp2AO->UpdateBias(response);
}

bool FeedPackageControl::UpdateConfigFile(string & response)
{
	return fp2AO->UpdateConfigFile(response);
}

bool FeedPackageControl::Test()
{
	return fp2AO->POST(Q_NEW(QEvt, TEST_SERVICE_SIG), me);
}
bool FeedPackageControl::ErrTest(short ErrBit)
{
	return fp2AO->ErrTest(ErrBit);
}




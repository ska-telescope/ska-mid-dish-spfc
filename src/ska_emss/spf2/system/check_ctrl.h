/*
 * check_ctrl.h
 *
 *  Created on: 23 May 2017
 *      Author: theuns
 */

#ifndef SYSTEM_CHECK_CTRL_H
#define SYSTEM_CHECK_CTRL_H

#include "CircularFifo.h"
#include "fpc_if.h"

#define MIN_COMPARED 2
#define STABILITY_WINDOW	30		// time frame in seconds for RFE1 stability check
#define GRADIENT_CHECK_WINDOW 20

// TODO: This needs to be defined elsewhere
#define TEMP_SP_WARM 296

/* user defined variable names & default values */
// Services
#define N_SD_CARD					"LogToSDcard"
#define VAL_SD_CARD					1
#define N_VACPUMP_COMMS				"VacPump_commsAvailable"
#define VAL_VACPUMP_COMMS			1
#define N_VACPUMP_ENABLED			"VacPump_enabled"
#define VAL_VACPUMP_ENABLED			1
#define N_HCOMPR_COMMS				"HeCompressor_commsAvailable"
#define VAL_HCOMPR_COMMS			1
#define N_HCOMPR_ENABLED			"HeCompressor_enabled"
#define VAL_HCOMPR_ENABLED			1
// Variables
#define INI_VARS					"Variables"
#define N_EXPECTED_ONLINE			"expected_online"
#define VAL_EXPECTED_ONLINE			0
#define N_STARTUP_DEFAULT			"startup_default"
#define VAL_STARTUP_DEFAULT			0
#define N_TS_RFE1					"TS_Rfe1"
#define VAL_TS_RFE1					0
#define N_TS_CAL_SOURCE				"TS_CalSource"
#define VAL_TS_CAL_SOURCE			296
// Constants
#define INI_CONST					"Constants"
#define N_P_RISE_VALVE_CLOSE		"P_SuddenRiseValveClose"
#define VAL_P_RISE_VALVE_CLOSE		5e-3
#define N_P_PRE_VAC					"P_PreVac"
#define VAL_P_PRE_VAC				10
#define N_P_CRYO_ON					"P_CryocoolerOn"
#define VAL_P_CRYO_ON				5e-2
#define N_P_CRYO_PUMP				"P_CryoPump"
#define VAL_P_CRYO_PUMP				1e-2
#define N_P_CRYO_PUMP_RISING		"P_CryoPumpRising"
#define VAL_P_CRYO_PUMP_RISING		3e-2
#define N_P_CRYO_PUMP_SATISFY		"P_CryoPumpSatisfy"
#define VAL_P_CRYO_PUMP_SATISFY		2e-2
#define N_PE_CRYO_PUMP				"PE_CryoPump"
#define VAL_PE_CRYO_PUMP			1e-1
#define N_P_COLD_OPERATIONAL_DEG	"P_ColdOperationalDegraded"
#define VAL_P_COLD_OPERATIONAL_DEG	1e-2
#define N_P_HELIUM_SUPPLY_ON		"P_HeliumSupplyOn"
#define VAL_P_HELIUM_SUPPLY_ON		17.5
#define N_P_HELIUM_SUPPLY_MAX		"P_HeliumSupplyMax"
#define VAL_P_HELIUM_SUPPLY_MAX		21.5
#define N_P_HELIUM_DIFF_MIN			"P_HeliumDifferentialMin"
#define VAL_P_HELIUM_DIFF_MIN		13
#define N_P_HELIUM_DIFF_MAX			"P_HeliumDifferentialMax"
#define VAL_P_HELIUM_DIFF_MAX		15
#define N_P_HELIUM_RETURN_MIN		"P_HeliumReturnLimit"
#define VAL_P_HELIUM_RETURN_MIN		3
#define N_T_CRYO_PUMPING			"T_CryoPumping"
#define VAL_T_CRYO_PUMPING			200
#define N_T_CRYO_RECOVERY			"T_CryoRecovery"
#define VAL_T_CRYO_RECOVERY			80
#define N_T_COLD_OPERATIONAL		"T_ColdOperational"
#define VAL_T_COLD_OPERATIONAL		30
#define N_TS_MAX_OFFSET				"TS_MaxOffset"
#define VAL_TS_MAX_OFFSET			1.0
#define N_TS_STABILITY_DELTA		"TS_StabilityDelta"
#define VAL_TS_STABILITY_DELTA		0.2
#define N_TS_OFF_MAX				"TS_OffMax"
#define VAL_TS_OFF_MAX				25
#define N_TS_OFF_STABILITY_DELTA	"TS_OffStabilityDelta"
#define VAL_TS_OFF_STABILITY_DELTA	1
#define N_TE_COLD_OPERATIONAL		"TE_ColdOperational"
#define VAL_TE_COLD_OPERATIONAL		100
#define N_T_REGEN_AMB				"T_RegenerationAmbient"
#define VAL_T_REGEN_AMB				290
#define N_MAX_VACUUM_E_RETRY		"Max_VacuumErrorRetry"
#define VAL_MAX_VACUUM_E_RETRY		10
#define N_MAX_CRYOPUMPING_RETRY		"Max_CryoPumpingRetry"
#define VAL_MAX_CRYOPUMPING_RETRY	6
#define N_TMAX_RFE1_PID				"TE_Rfe1PidMaxTemp"
#define VAL_TMAX_RFE1_PID			320
#define N_TMAX_CAL_PID				"TE_CalSourcePidMaxTemp"
#define VAL_TMAX_CAL_PID			320
// Timeouts
#define INI_TIMEOUTS				"Timeouts"
#define N_TO_VAC_PUMP_ON			"Timeout_VacPumpOn"
#define VAL_TO_VAC_PUMP_ON			300
#define N_TO_CRYO_PRESS_DEC			"Timeout_CryoPressureDecrease"
#define VAL_TO_CRYO_PRESS_DEC		120
#define N_TO_CRYO_TEMP_DEC			"Timeout_Rfe1TemperatureDecrease"
#define VAL_TO_CRYO_TEMP_DEC		120
#define N_TO_CRYO_ON_PRESS			"Timeout_CryoOnPressure"
#define VAL_TO_CRYO_ON_PRESS		7200
#define N_TO_VSC					"Timeout_VSC"
#define VAL_TO_VSC					600
#define N_TO_COMPRESSOR_ON			"Timeout_CompressorOn"
#define VAL_TO_COMPRESSOR_ON		60
#define N_TO_CRYO_PUMP_PRESS		"Timeout_CryoPumpPressure"
#define VAL_TO_CRYO_PUMP_PRESS		7200
#define N_TO_COLD_OPS_TEMP			"Timeout_ColdOpsTemp"
#define VAL_TO_COLD_OPS_TEMP		28800
#define N_TO_AMBIENT_TEMP			"Timeout_AmbientTemp"
#define VAL_TO_AMBIENT_TEMP			86400


struct stVariables
    {
        bool expected_online;
        enStartupDefault startup_default;
        int TS_Rfe1; // Temperature setpoint in Kelvin
        int TS_CalSource; // Temperature setpoint in Kelvin
    };

    struct stConstants
    {
        double P_SuddenRiseValveClose;
        double P_PreVac;
        double P_CryocoolerOn;
        double P_CryoPump;
        double P_CryoPumpRising;
        double P_CryoPumpSatisfy;
        double PE_CryoPump;
        double P_ColdOperationalDegraded;
        double T_CryoPumping;
        double T_CryoRecovery;
        double T_ColdOperational;
        double TS_MaxOffset;
        double TS_StabilityDelta;
        double TS_OffMax;
        double TS_OffStabilityDelta;
        double TE_ColdOperational;
        double T_RegenerationAmbient;
        unsigned int Max_VacuumErrorRetry;
        unsigned int Max_CryoPumpingRetry;
        double TE_Rfe1PidMaxTemp;
        double TE_CalSourcePidMaxTemp;
    };

    struct stTimeouts
    {
        unsigned int Timeout_VacPumpOn;
        unsigned int Timeout_CryoPressureDecrease;
        unsigned int Timeout_Rfe1TemperatureDecrease;
        unsigned int Timeout_CryoOnPressure;
        unsigned int Timeout_VSC;
        unsigned int Timeout_CompressorOn;
        unsigned int Timeout_CryoPumpPressure;
        unsigned int Timeout_ColdOpsTemp;
        unsigned int Timeout_AmbientTemp;
    };

    struct stSettings
    {
        stVariables variables;
        stConstants constants;
        stTimeouts  timeouts;
    };


namespace FP2
{
class CheckController
{
public:
	CheckController(stSettings settings, CircularFifo<stFeedDataFrame, 60> * buffPtr);
	virtual ~CheckController();

	stSettings _settings;

	void ReloadSettings(stSettings settings);
	bool ActiveSamples(int sampleCount);

	bool IsPressureSafe();
	bool PreVacuumed();
	bool ValveSafetyCheck();
	bool CheckSpdHealth();
	bool IsRfe1TempSafe();
	bool IsCalSourceTempSafe();

	bool ColdOpsTemp();
	bool ColdOpsAvailable(bool lnaPidOn, int setpoint);
	int  NotColdOperational(bool lnaPidOn, int setpoint);
	bool PressureCryoOn();
	bool ManifoldPressureCryoOn();
	bool AmbientPressures();
	bool TempCryoRecovery();
	int  ColdOpsError();
	bool RegenTemp();
	bool CryoPumpRecovered();
	bool CryoPumpRiseVal();
	bool CryoPressureMoreThanManifoldPressure();
	bool ManifoldPressureMoreThan(double pressure);
	bool CryoPumpSatisfactory();

	bool CryoPumpTReached();
	bool CryoPumpReached();

	bool Rfe1TempDecrease();
	bool CryoPressureIncrease();
	bool CryoPressureDecrease();
	bool ManifoldPressureDecrease();

	int GetColdHeadTempHistoryArray(double array[], int count);
	int GetSpdHistoryArray(double array[], int count);
	int GetLnaTempHistoryArray(double array[], int count);
	int GetCalSourceTempHistoryArray(double array[], int count);
	int GetOmtTempHistoryArray(double array[], int count);
	int GetCryostatPressureHistoryArray(double array[], int count);
	int GetManifoldPressureHistoryArray(double array[], int count);

private:
	double logPressureDt(double PArray[], int count);
	double tempDt(double TArray[], int count);

	CircularFifo<stFeedDataFrame, 60> * _buffPtr; // Reference to a circular buffer //FIXME

};
}

#endif /* SYSTEM_CHECK_CTRL_H */

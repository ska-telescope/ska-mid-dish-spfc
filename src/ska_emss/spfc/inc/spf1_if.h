/*
 * spf1_if.h
 *
 *  Created on: 18 August 2017
 *      Author: theuns
 */

#ifndef INC_SPF1_IF_H_
#define INC_SPF1_IF_H_

namespace Spf1Interface
{
	/* The Device Status is a global enum and should maybe be moved somewhere "global" */
	enum class enDeviceStatus : short
	{
	   UNKNOWN = 0,
	   NORMAL = 1,
	   DEGRADED = 2,
	   FAILED = 3,
	};

	typedef struct _stFeedSensors
	{
//		float 	controllerVoltage;
//		float 	controllerCurrent;
//		float 	controllerTemperature;
//		float 	psu9VRectifiedVoltage;
//		float 	psuAnalogue6vRegulatorTemp;
//		float 	psuAnalogue15vRegulatorTemp;
//		float 	psuDigital6vRegulatorTemp;
		float 	lnaHDrainVoltage;
		float 	lnaHDrainCurrent;
		float 	lnaHGateVoltage;
		float 	lnaVDrainVoltage;
		float 	lnaVDrainCurrent;
		float 	lnaVGateVoltage;
		float 	amp2HVoltage;
		float 	amp2HCurrent;
		float 	amp2VVoltage;
		float 	amp2VCurrent;
		float 	calsourceVoltage;
		float 	calsourceCurrent;
		float 	calsourceTemp;
		float 	pidCalsourceCurrent;
		float 	pidLnaHCurrent;
		float 	pidLnaVCurrent;
		float 	chamberBodyTemp;
		float 	analogueInterfaceTemp;
		float 	lnaHTemp;
		float 	lnaVTemp;
		float 	spdHealth;

	} stFeedSensors;

	enum class enDeviceMode : short
	{
	    OFF = 0,
	    STARTUP,
	    STANDBY_LP,
	    OPERATE,
	    MAINTENANCE,
		ERROR
	};

	enum class enFpcMode : short
	{
		UNKNOWN = 0,
		APPLICATION = 1,
		MAINTENANCE = 2
	};

	enum class enPid : short
	{
		NONE = 0,
		CALSOURCE = 1,
		LNA_H = 2,
		LNA_V = 3
	};

	enum class enMemoryRecord : short
	{
		NONE = 0,
		CONFIG = 1,
		REC_GAIN = 2,
		REC_NOISE = 3,
		CAL_NOISE = 4
	};

	enum class enElapsedTimeCounter : short
	{
		NONE = 0,
		OPERATIONAL = 1
	};

	enum class enErrorBits : short
	{
		INITIALISE_ERROR		= 0,
		SELF_CHECK				= 1,
		DELTA_T_LNA				= 2,
		NON_OPS_TEMP			= 3,
		TIMEOUT_LNA_TEMP_INC	= 4,
		TIMEOUT_WARM_OPS_TEMP	= 5
	};

	typedef union _unErrorFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	InitialiseError				: 1;
			unsigned	SelfCheck					: 1;
			unsigned	DeltaT_LNA					: 1;
			unsigned	NonOpsTemp					: 1;
			unsigned	TimeoutLnaTempIncrease		: 1;
			unsigned	TimeoutWarmOpsTemp			: 1;
			unsigned	SPARE						: 10;	// <- 16-bits
		}bits;
	}unErrorFlags;

	enum class enControlBitNumber : short
	{
		LNA_H				= 0,
		LNA_V				= 1,
		AMP2_H				= 2,
		AMP2_V				= 3,
		SPARE1				= 4,
		SPARE2				= 5,
		CALSOURCE			= 6,
		PID_CALSOURCE		= 7,
		PID_LNA_H			= 8,
		PID_LNA_V			= 9
	};

	typedef union _unControlFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	LnaH				: 1;
			unsigned	LnaV				: 1;
			unsigned	Amp2H				: 1;
			unsigned	Amp2V				: 1;
			unsigned	Spare1				: 1;
			unsigned	Spare2				: 1;
			unsigned	CalSource			: 1;
			unsigned	PidCalSource		: 1;
			unsigned	PidLnaH				: 1;
			unsigned	PidLnaV				: 1;
			unsigned	SPARE				: 6;	// <- 16-bits
		}bits;
	}unControlFlags;

	typedef struct _stPidSettings
	{
		int 	proportional;
		double 	integral;
		double  derivative;
		int 	temp;
	} stPidSettings;

	// TODO: Better to do this in array?
	typedef struct _stLnaBiasParameters
	{
		int drainVolt1;
		int drainCurrent1;
		int drainVolt2;
		int drainCurrent2;
		int drainVolt3;
		int drainCurrent3;
	} stLnaBiasParameters;

	typedef union _unValidityFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	sensors				: 1;
			unsigned	vacuumValve			: 1;
			unsigned	cryoMotor			: 1;
			unsigned	controlRegister		: 1;
			unsigned	errorRegister		: 1;
			unsigned	lnaHBias			: 1;
			unsigned	lnaVBias			: 1;
			unsigned	pidCalSource		: 1;
			unsigned	pidLnaH				: 1;
			unsigned	pidLnaV				: 1;
			unsigned 	rtc					: 1;
			unsigned	eltOperational		: 1;
			unsigned	SPARE				: 4;	// <- 16-bits
		}bits;
	}unValidityFlags;

	typedef struct _stConfiguration
	{
		string serial;
		string serialLnaH;
		string serialLnaV;
		string swVersion;
		string fwVersion;
		float lnaHMeanGain;
		float lnaVMeanGain;
	} stConfiguration;

	enum class _enCapabilityState : short
	{
		UNKNOWN,
		UNAVAILABLE,
		STANDBY,
		OPERATE_DEGRADED,
		OPERATE_FULL,
	} ;
	typedef _enCapabilityState enCapabilityState;


	enum class _b1DefaultStartStateEnum : short
	{
		_STANDBYminusLP,
		_OPERATE,
		_MAINTENANCE,
	} ;
	typedef _b1DefaultStartStateEnum b1DefaultStartStateEnum;

	enum class _b1InterfaceTypeEnum : short
	{
		_UNKNOWN,
		_REAL,
		_EMULATED,
		_HYBRID
	} ;
	typedef _b1InterfaceTypeEnum b1InterfaceTypeEnum;

	typedef struct _stFeedDataFrame
	{
		bool enabled;
		bool expectedOnline;
		//bool detected;

		double timestamp;

		stFeedSensors sensors;
		unControlFlags controlFlags;
		unErrorFlags errorFlags;

		long elapsedCurrentOperational;
		long elapsedOperational;

		long timeToFullCapability;
		enCapabilityState capabilityState;
		enDeviceStatus deviceStatus;
		enFpcMode fpcMode;
		enDeviceMode currentMode;

		unValidityFlags validities;
		string errorCode[3];

		stConfiguration config;
		short pidLnaHTempSetPoint;
		short pidLnaVTempSetPoint;
		short pidCalSourceTempSetPoint;
		short pidLnaHTempSetPointDefault;
		short pidLnaVTempSetPointDefault;
		short pidCalSourceTempSetPointDefault;
		b1DefaultStartStateEnum startupDefault;
		b1InterfaceTypeEnum interfaceType;

		// TODO: Is this really needed to add in every data frame?
		//string fwVersion;
		//string serial;
		std::string stateMachineState;
	} stFeedDataFrame;

	enum class enSetFeedMode : short
	{
		STANDBY_LP = 0,
		OPERATE,
		MAINTENANCE
	};

	enum class _enLnaPidSetPoint : short
	{
		OFF,
		SP1,
		SP2,
		SP3,
		SP4,
		SP5,
		SP6,
		CUSTOM
	} ;
	typedef _enLnaPidSetPoint enLnaPidSetPoint;

	enum class _enCalSourcePidSetPoint : short
	{
		OFF,
		SP1,
		SP2,
		SP3,
		SP4,
		SP5,
		SP6,
		CUSTOM
	} ;
	typedef _enCalSourcePidSetPoint enCalSourcePidSetPoint;

}

#endif /* INC_SPF1_IF_H_ */

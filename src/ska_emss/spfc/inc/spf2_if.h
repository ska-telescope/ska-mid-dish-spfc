/*
 * spf2_if.h
 *
 *  Created on: 27 May 2017
 *      Author: theuns
 */

#ifndef INC_SPF2_IF_H_
#define INC_SPF2_IF_H_

namespace Spf2Interface
{
	/* The Device Status is a global enum and should maybe be moved somewhere "global" */
	enum class enDeviceStatus : short
	{
	   UNKNOWN = 0,
	   NORMAL = 1,
	   DEGRADED = 2,
	   FAILED = 3,
	};

	typedef struct _stFeedSensors
	{
//		double 	controllerVolt;
//		double 	controllerCurrent;
//		double 	controllerTemp;
//		double 	psuAnalogue6vRectifiedVolt;
//		double 	psuAnalogue6vRegulatorTemp;
//		double 	psuAnalogue15vRegulatorTemp;
//		double 	psuDigital6vRegulatorTemp;
		float 	lnaHDrainVoltage;
//		double 	lnaHDrainVoltage1;
//		double 	lnaHDrainVoltage2;
//		double 	lnaHDrainVoltage3;
		float 	lnaHDrainCurrent;
//		double 	lnaHDrainCurrent1;
//		double 	lnaHDrainCurrent2;
//		double 	lnaHDrainCurrent3;
		float 	lnaHGateVoltage;
//		double 	lnaHGateVoltage1;
//		double 	lnaHGateVoltage2;
//		double 	lnaHGateVoltage3;
		float 	lnaVDrainVoltage;
//		double 	lnaVDrainVoltage1;
//		double 	lnaVDrainVoltage2;
//		double 	lnaVDrainVoltage3;
		float 	lnaVDrainCurrent;
//		double 	lnaVDrainCurrent1;
//		double 	lnaVDrainCurrent2;
//		double 	lnaVDrainCurrent3;
		float 	lnaVGateVoltage;
//		double 	lnaVGateVoltage1;
//		double 	lnaVGateVoltage2;
//		double 	lnaVGateVoltage3;
		float 	amp2HVoltage;
		float 	amp2HCurrent;
		float 	amp2VVoltage;
		float 	amp2VCurrent;
		float 	calsourceVoltage;
		float 	calsourceCurrent;
		float 	calsourceTemp;
		float 	CalSourcePidCurrent;
		float 	LnaPidCurrent;
		float 	cryostatPressure;
		float 	manifoldPressure;
		float 	lnaTemp;
		float 	omtTemp;
		float 	coldheadTemp;
		float 	cryostatBodyTemp;
		float 	analogueInterfaceTemp;
		float	psuMotorPosSupplyCurrent;
		float 	psuMotorNegSupplyCurrent;
		float 	psuMotorDriverTemp;
		float	spdHealth;

	} stFeedSensors;

	enum class enDeviceMode : short
	{
	    OFF = 0,
	    STARTUP,
	    STANDBY_LP,
	    OPERATE,
	    MAINTENANCE,
		ERROR,
	    REGENERATION,
		PRE_VAC
	};

	enum class enFpcMode : short
	{
		UNKNOWN = 0,
		APPLICATION = 1,
		MAINTENANCE = 2
	};

	enum class enPid : short
	{
		CALSOURCE = 0,
		LNA = 1
	};

	enum class enMemoryRecord : short
	{
		NONE = 0,
		CONFIG = 1,
		REC_GAIN = 2,
		REC_NOISE = 3,
		CAL_NOISE = 4
	};

	enum class enElapsedTimeCounter : short
	{
		NONE = 0,
		OPERATIONAL = 1,
		MOTOR = 2,
		VALVE = 3
	};

	enum class enErrorBits : short
	{
		AUTO_VALVE_CLOSE		= 0,
		TIMEOUT_VSC				= 1,
		TIMEOUT_CRYO_PRESS_DEC	= 2,
		TIMEOUT_CRYO_ON_PRESS	= 3,
		TIMEOUT_RFE1_TEMP_DEC	= 4,
		TIMEOUT_CRYO_PUMP_PRESS	= 5,
		TIMEOUT_COLD_OPS_TEMP	= 6,
		TIMEOUT_AMBIENT_TEMP	= 7,
		CRYOPUMP_RETRY_LIMIT	= 8,
		REGEN_RETRY_LIMIT 		= 9,
		TEMP_DEGRADED			= 10,
		PRESS_DEGRADED 			= 11
	};

	typedef union _unErrorFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	AutoValveClose				: 1;
			unsigned	TimeoutVsc					: 1;
			unsigned	TimeoutCryoPressureDec		: 1;
			unsigned	TimeoutOnPressure			: 1;
			unsigned	TimeoutCryoOnPressure		: 1;
			unsigned	TimeoutCryoPumpPressure		: 1;
			unsigned	TimeoutColdOpsTemperature	: 1;
			unsigned	TimeoutAmbientTemperature	: 1;
			unsigned	CryoPumpRetryLimit			: 1;
			unsigned	RegenRetryLimit				: 1;
			unsigned	TxTempDegraded				: 1;
			unsigned	PressDegraded				: 1;
			unsigned	SPARE						: 5;	// <- 16-bits
		}bits;
	}unErrorFlags;

	enum class enControlBitNumber : short
	{
		LNA_H				= 0,
		LNA_V				= 1,
		AMP2_H				= 2,
		AMP2_V				= 3,
		LNA_H_ILLUMINATION	= 4,
		LNA_V_ILLUMINATION	= 5,
		CALSOURCE			= 6,
		PID_CALSOURCE		= 7,
		PID_LNA				= 8
	};

	typedef union _unControlFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	LnaH				: 1;
			unsigned	LnaV				: 1;
			unsigned	Amp2H				: 1;
			unsigned	Amp2V				: 1;
			unsigned	LnaHIllumination	: 1;
			unsigned	LnaVIllumination	: 1;
			unsigned	CalSource			: 1;
			unsigned	PidCalSource		: 1;
			unsigned	PidLna				: 1;
			unsigned	SPARE				: 7;	// <- 16-bits
		}bits;
	}unControlFlags;

	typedef struct _stPidSettings
	{
		int 	proportional;
		double 	integral;
		double  derivative;
		int 	temp;
	} stPidSettings;

	// TODO: Better to do this in array?
	typedef struct _stLnaBiasParameters
	{
		int drainVolt1;
		int drainCurrent1;
		int drainVolt2;
		int drainCurrent2;
		int drainVolt3;
		int drainCurrent3;
	} stLnaBiasParameters;

	typedef union _unValidityFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	sensors				: 1;
			unsigned	vacuumValve			: 1;
			unsigned	cryoMotor			: 1;
			unsigned	controlRegister		: 1;
			unsigned	errorRegister		: 1;
			unsigned	lnaHBias			: 1;
			unsigned	lnaVBias			: 1;
			unsigned	pidCalSource		: 1;
			unsigned	pidLna				: 1;
			unsigned 	rtc					: 1;
			unsigned	eltOperational		: 1;
			unsigned 	eltMotor			: 1;
			unsigned 	eltValve			: 1;
			unsigned	SPARE				: 3;	// <- 16-bits
		}bits;
	}unValidityFlags;

	typedef struct _stConfiguration
	{
		string serial;
		string serialLnaH;
		string serialLnaV;
		string swVersion;
		string fwVersion;
		float lnaHMeanGain;
		float lnaVMeanGain;
	} stConfiguration;

	enum class _b2CapabilityStateEnum : short
	{
		_UNAVAILABLE,
		_STANDBY,
		_OPERATEminusDEGRADED,
		_OPERATEminusFULL,
	} ;
	typedef _b2CapabilityStateEnum b2CapabilityStateEnum;


	enum class _b2DefaultStartStateEnum : short
	{
		_STANDBYminusLP,
		_OPERATE,
		_MAINTENANCE,
	} ;
	typedef _b2DefaultStartStateEnum b2DefaultStartStateEnum;

	enum class _b2InterfaceTypeEnum : short
	{
		_UNKNOWN,
		_REAL,
		_EMULATED,
		_HYBRID
	} ;
	typedef _b2InterfaceTypeEnum b2InterfaceTypeEnum;

	typedef struct _stFeedDataFrame
	{
		bool enabled;
		bool expectedOnline;
		//bool detected;

		double timestamp;
		bool isValveOpen;

		stFeedSensors sensors;
		unControlFlags controlFlags;
		unErrorFlags errorFlags;

		DevLong elapsedCurrentOperational;
		DevLong elapsedOperational;
		DevLong elapsedMotor;
		DevLong elapsedValve;

		short cryoMotorSpeed;
		bool cryoMotorState;
		unsigned short b2CoolDownCounter;

		DevLong timeToFullCapability;
		b2CapabilityStateEnum capabilityState;
		enDeviceStatus deviceStatus;
		enFpcMode fpcMode;
		enDeviceMode currentMode;

		unValidityFlags validities;
		string errorCode[3];

		stConfiguration config;
		short pidLnaTempSetPoint;
		short pidCalSourceTempSetPoint;
		short pidLnaTempSetPointDefault;
		short pidCalSourceTempSetPointDefault;
		b2DefaultStartStateEnum startupDefault;
		b2InterfaceTypeEnum interfaceType;

		// TODO: Is this really needed to add in every data frame?
		//string fwVersion;
		//string serial;
		string stateMachineState;
	} stFeedDataFrame;

	enum class enSetFeedMode : short
	{
		STANDBY_LP = 0,
		OPERATE,
		MAINTENANCE
	};

	enum class _enLnaPidSetPoint : short
	{
		OFF,
		SP1,
		SP2,
		SP3,
		SP4,
		SP5,
		SP6,
		CUSTOM
	} ;
	typedef _enLnaPidSetPoint enLnaPidSetPoint;

	enum class _enCalSourcePidSetPoint : short
	{
		OFF,
		SP1,
		SP2,
		SP3,
		SP4,
		SP5,
		SP6,
		CUSTOM
	} ;
	typedef _enCalSourcePidSetPoint enCalSourcePidSetPoint;

}

#endif /* INC_SPF345_IF_H_ */

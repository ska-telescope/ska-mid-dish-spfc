/*
 * spf345_if.h
 *
 *  Created on: 3 Aug 2022
 *      Author: cvn
 */

#ifndef INC_SPF345_IF_H_
#define INC_SPF345_IF_H_

namespace Spf345Interface
{
	/* The Device Status is a global enum and should maybe be moved somewhere "global" */

enum class enCapabilityState : short
{
	UNAVAILABLE = 0,
	STANDBY,
	OPERATE_DEGRADED,
	OPERATE_FULL
};

enum class enDeviceStatus : short
{
   UNKNOWN = 0,
   NORMAL = 1,
   DEGRADED = 2,
   FAILED = 3,
};

enum class enDeviceMode : short
{
    OFF = 0,
    STARTUP,
    STANDBY_LP,
    OPERATE,
    MAINTENANCE,
	ERROR,
    REGENERATION,
	PRE_VAC
};

	typedef struct _stFeedSensors
	{
		float 	b3LnaHDrainVoltage;
		float 	b3LnaHGateVoltage;
		float 	b3LnaVDrainVoltage;
		float 	b3LnaVGateVoltage;
		float 	b4LnaHDrainVoltage;
		float 	b4LnaHGateVoltage;
		float 	b4LnaVDrainVoltage;
		float 	b4LnaVGateVoltage;
		float 	b5aLnaHDrainVoltage;
		float 	b5aLnaHGateVoltage;
		float 	b5aLnaVDrainVoltage;
		float 	b5aLnaVGateVoltage;
		float 	b5bLnaHDrainVoltage;
		float 	b5bLnaHGateVoltage;
		float 	b5bLnaVDrainVoltage;
		float 	b5bLnaVGateVoltage;
		float 	b6LnaHDrainVoltage;
		float 	b6LnaHGateVoltage;
		float 	b6LnaVDrainVoltage;
		float 	b6LnaVGateVoltage;
		float 	b3LnaHDrainCurrent;
		float 	b3LnaVDrainCurrent;
		float 	b4LnaHDrainCurrent;
		float 	b4LnaVDrainCurrent;
		float 	b5aLnaHDrainCurrent;
		float 	b5aLnaVDrainCurrent;
		float 	b5bLnaHDrainCurrent;
		float 	b5bLnaVDrainCurrent;
		float 	b6LnaHDrainCurrent;
		float 	b6LnaVDrainCurrent;
		float 	b345CryoPressure;
		float 	b345ManifoldPressure;
		float 	b3LnaTemp;
		float 	b4LnaTemp;
		float 	b5aLnaTemp;
		float 	b5bLnaTemp;
		float 	b6LnaTemp;
		float 	b3OmtTemp;
		float 	b4OmtTemp;
		float 	b5aOmtTemp;
		float 	b5bOmtTemp;
		float 	b6OmtTemp;
		float 	b5aHornTemp;
		float 	b5bHornTemp;
		float 	b6HornTemp;
		float 	b345BodyTemp;
		float 	b345ColdheadStage1Temp;
		float 	b345ColdheadStage2Temp;
		float 	b3WarmPlateTemp;
		float 	b4WarmPlateTemp;
		float 	b5aWarmPlateTemp;
		float 	b5bWarmPlateTemp;
		float 	b6WarmPlateTemp;
		float 	b345CalSourceTemp;
		float 	b345AnalogueInterfaceTemp;
		float 	b345DigitalInterfaceTemp;
		float	b345SpdHealth;
		float 	b3LnaPidCurrent;
		float 	b4LnaPidCurrent;
		float 	b5aLnaPidCurrent;
		float 	b5bLnaPidCurrent;
		float 	b6LnaPidCurrent;

	} stFeedSensors;

	enum class enFpcMode : short
	{
		UNKNOWN = 0,
		APPLICATION = 1,
		MAINTENANCE = 2
	};

	enum class enPid : short
	{
		NONE 	= 0,
		LNA_B3 	= 1,
		LNA_B4 	= 2,
		LNA_B5A = 3,
		LNA_B5B = 4,
		LNA_B6 	= 5,
		WARMPLATE_B3 	= 6,
		WARMPLATE_B4 	= 7,
		WARMPLATE_B5A	= 8,
		WARMPLATE_B5B 	= 9,
		WARMPLATE_B6 	= 10,
		CALSOURCE	 	= 11
	};

	enum class enMemoryRecord : short
	{
		NONE = 0,
		CONFIG = 	1,
		REC_GAIN = 	2,
		REC_NOISE = 3,
		CAL_NOISE = 4,
		REC_GAIN3 =		2,
		REC_NOISE3 = 	3,
		CAL_NOISE3 = 	4,
		REC_GAIN4 = 	5,
		REC_NOISE4 = 	6,
		CAL_NOISE4 = 	7,
		REC_GAIN5A = 	8,
		REC_NOISE5A = 	9,
		CAL_NOISE5A = 	10,
		REC_GAIN5B = 	11,
		REC_NOISE5B = 	12,
		CAL_NOISE5B = 	13,
		REC_GAIN6 = 	14,
		REC_NOISE6 = 	15,
		CAL_NOISE56 = 	16,
		TEMP_CAL_CURVES =	17,
		PRESS_CAL_CURVES =	18
	};

	enum class enActiveBand : short
	{
		NONE = 0,
		B3 = 1,
		B4 = 2,
		B5A = 3,
		B5B = 4,
		B6 = 5
	};

	enum class enElapsedTimeCounter : short
	{
		NONE = 0,
		OPERATIONAL = 1,
		MOTOR = 2,
		VALVE = 3,
		COLDHEADCYCLES = 4
	};


	enum class enErrorBits : short
	{
		AUTO_VALVE_CLOSE		= 0,
		TIMEOUT_VSC				= 1,
		TIMEOUT_CRYO_PRESS_DEC	= 2,
		TIMEOUT_CRYO_ON_PRESS	= 3,
		TIMEOUT_RFE1_TEMP_DEC	= 4,
		TIMEOUT_CRYO_PUMP_PRESS	= 5,
		TIMEOUT_COLD_OPS_TEMP	= 6,
		TIMEOUT_AMBIENT_TEMP	= 7,
		CRYOPUMP_RETRY_LIMIT	= 8,
		REGEN_RETRY_LIMIT 		= 9,
		TEMP_DEGRADED			= 10,
		PRESS_DEGRADED 			= 11,
		SPD_TRIGGERED			= 12,
		E_14					= 13,
		E_15					= 14,
		E_16					= 15
	};

	typedef union _unErrorFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	AutoValveClose				: 1;
			unsigned	TimeoutVsc					: 1;
			unsigned	TimeoutCryoPressureDec		: 1;
			unsigned	TimeoutOnPressure			: 1;
			unsigned	TimeoutCryoOnPressure		: 1;
			unsigned	TimeoutCryoPumpPressure		: 1;
			unsigned	TimeoutColdOpsTemperature	: 1;
			unsigned	TimeoutAmbientTemperature	: 1;
			unsigned	CryoPumpRetryLimit			: 1;
			unsigned	RegenRetryLimit				: 1;
			unsigned	TxTempDegraded				: 1;
			unsigned	PressDegraded				: 1;
			unsigned	SurgeProtectionDevice		: 1;
			unsigned	SPARE						: 3;	// <- 16-bits
		}bits;
	}unErrorFlags;

	enum class enControlBitNumber : short
	{
		LNA_B3				= 0,
		LNA_B4				= 1,
		LNA_B5A				= 2,
		LNA_B5B				= 3,
		LNA_B6				= 4,
		CALSOURCE			= 5,
		PID_LNA_B3			= 6,
		PID_LNA_B4			= 7,
		PID_LNA_B5A			= 8,
		PID_LNA_B5B			= 9,
		PID_LNA_B6			= 10,
		NEG_HEATER			= 11,
		CRYO_MOTOR			= 12,
		VAC_VALVE			= 13,
		VAC_TURBO			= 14,

	};

	typedef union _unControlFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	LnaB3				: 1;
			unsigned	LnaB4				: 1;
			unsigned	LnaB5a				: 1;
			unsigned	LnaB5b				: 1;
			unsigned	LnaB6				: 1;
			unsigned	CalSource			: 1;
			unsigned	PidLnaB3			: 1;
			unsigned	PidLnaB4			: 1;
			unsigned	PidLnaB5a			: 1;
			unsigned	PidLnaB5b			: 1;
			unsigned	PidLnaB6			: 1;
			unsigned	NegHeater			: 1;
			unsigned	CryoMotor			: 1;
			unsigned	VaValve				: 1;
			unsigned	VaTurbo				: 1;
			unsigned	SPARE				: 1;	// <- 16-bits
		}bits;
	}unControlFlags;

	typedef struct _stPidSettings
	{
		int 	proportional;
		double 	integral;
		double  derivative;
		int 	temp;
	} stPidSettings;

	// TODO: Better to do this in array?
	typedef struct _stLnaBiasParameters
	{
		float gateVolt3;
		float drainVolt3;
		float drainCur3;
		float gateVolt4;
		float drainVolt4;
		float drainCur4;
		float gateVolt5a;
		float drainVolt5a;
		float drainCur5a;
		float gateVolt5b;
		float drainVolt5b;
		float drainCur5b;
		float gateVolt6;
		float drainVolt6;
		float drainCur6;
	} stLnaBiasParameters;

	typedef union _unValidityFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	sensors				: 1;
			unsigned	vacuumValve			: 1;
			unsigned	cryoMotor			: 1;
			unsigned	controlRegister		: 1;
			unsigned	errorRegister		: 1;
			unsigned	lnaHBias			: 1;
			unsigned	lnaVBias			: 1;
			unsigned	pidCalSource		: 1;
			unsigned	pidLna				: 1;
			unsigned 	rtc					: 1;
			unsigned	eltOperational		: 1;
			unsigned 	eltMotor			: 1;
			unsigned 	eltValve			: 1;
			unsigned	cntCycles			: 1;
			unsigned	SPARE				: 2;	// <- 16-bits
		}bits;
	}unValidityFlags;

	typedef struct _stConfiguration
	{
		string swVersion;
		string fwVersion;
		string serial;
		string serialDigBoard;
		string serialAnaBoard;
		string serialTSens1;
		string serialTSens2;
		string serialTSens3;
		string serialTSens4;
		string serialTSens5;
		string serialTSens6;
		string serialTSens7;
		string serialTSens8;
		string serialTSens9;
		string serialTSens10;
		string serialTSens11;
		string serialTSens12;
		string serialTSens13;
		string serialTSens14;
		string serialTSens15;
		string serialTSens16;
		string serialTSens17;
		string serialTSens18;
		string serialTSens19;
		string serialTSens20;
		string serialTSens21;
		string serialTSens22;
		string serialTSens23;
		string serialPSens1;
		string serialPSens2;
		string serialLna3H;
		string serialLna3V;
		string serialLna4H;
		string serialLna4V;
		string serialLna5aH;
		string serialLna5aV;
		string serialLna5bH;
		string serialLna5bV;
		string serialLna6H;
		string serialLna6V;
		std::string tempSerialNrs[23];
		std::string pressSerialNrs[2];
		std::string lnaSerialNrs[10];

		float meanGain3H;
		float meanGain3V;
		float meanGain4H;
		float meanGain4V;
		float meanGain5aH;
		float meanGain5aV;
		float meanGain5bH;
		float meanGain5bV;
		float meanGain6H;
		float meanGain6V;

		float lna3HVd;
		float lna3HId;
		float lna3HVg;
		float lna3VVd;
		float lna3VId;
		float lna3VVg;
		float lna4HVd;
		float lna4HId;
		float lna4HVg;
		float lna4VVd;
		float lna4VId;
		float lna4VVg;
		float lna5aHVd;
		float lna5aHId;
		float lna5aHVg;
		float lna5aVVd;
		float lna5aVId;
		float lna5aVVg;
		float lna5bHVd;
		float lna5bHId;
		float lna5bHVg;
		float lna5bVVd;
		float lna5bVId;
		float lna5bVVg;
		float lna6HVd;
		float lna6HId;
		float lna6HVg;
		float lna6VVd;
		float lna6VId;
		float lna6VVg;
	} stConfiguration;

	enum class _b345CapabilityStateEnum : short
	{
		_UNAVAILABLE,
		_STANDBY,
		_OPERATEminusDEGRADED,
		_OPERATEminusFULL,
	} ;
	typedef _b345CapabilityStateEnum b345CapabilityStateEnum;


	enum class _b345DefaultStartStateEnum : short {
		_STANDBYminusLP,
		_OPERATE,
		_MAINTENANCE,
	} ;
	typedef _b345DefaultStartStateEnum b345DefaultStartStateEnum;

	enum class _b345InterfaceTypeEnum : short
	{
		_UNKNOWN,
		_REAL,
		_EMULATED,
		_HYBRID
	} ;
	typedef _b345InterfaceTypeEnum b345InterfaceTypeEnum;

	typedef struct _stFeedDataFrame
	{
		bool enabled;
		bool expectedOnline;

		double timestamp;
		bool isValveOpen;
		bool isTurboOn;
		short cryoMotorSpeed;
		stFeedSensors sensors;
		unControlFlags controlFlags;
		unErrorFlags errorFlags;
		short lastError;
		short lastServError;

		Tango::DevLong elapsedOperational;
		Tango::DevLong elapsedTotOp;
		Tango::DevLong elapsedMotor;
		Tango::DevLong elapsedValve;
		Tango::DevLong coldHeadCycles;

		long b3CapTime2OpFull;
		long b4CapTime2OpFull;
		long b5aCapTime2OpFull;
		long b5bCapTime2OpFull;
		long b6CapTime2OpFull;
		enCapabilityState b3CapabilityState;
		enCapabilityState b4CapabilityState;
		enCapabilityState b5aCapabilityState;
		enCapabilityState b5bCapabilityState;
		enCapabilityState b6CapabilityState;
		enDeviceStatus deviceStatus;
		enFpcMode fpcMode;
		enDeviceMode currentMode;
		enActiveBand activeBand;

		float b3LnaPidTempSetPoint;
		float b4LnaPidTempSetPoint;
		float b5aLnaPidTempSetPoint;
		float b5bLnaPidTempSetPoint;
		float b6LnaPidTempSetPoint;
		float b3WarmPlateTempSetPoint;
		float b4WarmPlateTempSetPoint;
		float b5aWarmPlateTempSetPoint;
		float b5bWarmPlateTempSetPoint;
		float b6WarmPlateTempSetPoint;
		float b345CalSourceTempSetPoint;

		float b3DefaultLnaPidTempSetPoint;
		float b4DefaultLnaPidTempSetPoint;
		float b5aDefaultLnaPidTempSetPoint;
		float b5bDefaultLnaPidTempSetPoint;
		float b6DefaultLnaPidTempSetPoint;
		float b3DefaultWarmPlateTempSetPoint;
		float b4DefaultWarmPlateTempSetPoint;
		float b5aDefaultWarmPlateTempSetPoint;
		float b5bDefaultWarmPlateTempSetPoint;
		float b6DefaultWarmPlateTempSetPoint;
		float b345DefaultCalSourceTempSetPoint;
		unsigned short b345CoolDownCounter;

		b345DefaultStartStateEnum defaultStartState;
		b345InterfaceTypeEnum interfaceType;


		unValidityFlags validities;
		stConfiguration config;
		string errorCode[3];

		// TODO: Is this really needed to add in every data frame?
		//string fwVersion;
		//string serial;
	} stFeedDataFrame;

	enum class enSetFeedMode : short
	{
		STANDBY_LP = 0,
		OPERATE,
		MAINTENANCE
	};

	enum class _enBandSelect : short
	{
		NONE,
		B3,
		B4,
		B5A,
		B5B,
		B6,
	} ;
	typedef _enBandSelect enBandSelect;

	enum class _enLnaPidSetPoint : short
	{
		OFF,
		SP1,
		SP2,
		SP3,
		SP4,
		SP5,
		SP6,
		CUSTOM
	} ;
	typedef _enLnaPidSetPoint enLnaPidSetPoint;

	enum class _enWarmPlateSetPoint : short
	{
		OFF,
		SP1,
		SP2,
		SP3,
		SP4,
		SP5,
		SP6,
		CUSTOM
	} ;
	typedef _enWarmPlateSetPoint enWarmPlateSetPoint;

	enum class _enCalSourcePidSetPoint : short
	{
		OFF,
		SP1,
		SP2,
		SP3,
		SP4,
		SP5,
		SP6,
		CUSTOM
	} ;
	typedef _enCalSourcePidSetPoint enCalSourcePidSetPoint;

}

#endif /* INC_SPF345_IF_H_ */

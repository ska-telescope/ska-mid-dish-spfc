/*
 * spfhe_proxy.h
 *
 *  Created on: 23 May 2017
 *      Author: theuns
 */

#ifndef INTERFACE_SPFHE_PROXY_H_
#define INTERFACE_SPFHE_PROXY_H_

#include <tango.h>
using namespace Tango;

#include <string>
using namespace std;

namespace SpfHe
{
	/* The Device Status is a global enum and should maybe be moved somewhere "global" */
	enum class enDeviceStatus : short
	{
	   UNKNOWN = 0,
	   NORMAL = 1,
	   DEGRADED = 2,
	   FAILED = 3,
	};

	enum class enHeCompStatus : short
	{
		NONE = 0,
		RUNNING = 1,
		NOT_RUNNING = 2,
		ERROR = 3
	};

	typedef struct _stHeliumCompressorSensors
	{
		double 	controllerVoltage;
		double 	controllerCurrent;
		double 	controllerTemperature;
		double 	compressorSupplyPressure;
		double 	compressorReturnPressure;
		double 	compressorMotorTemperature;
		double 	compressorSupplyTemperature;

	} stHeliumCompressorSensors;

	enum class enDeviceMode : short
	{
		OFF = 0,
		STARTUP,
		STANDBY_LP,
		OPERATE,
		MAINTENANCE,
		ERROR
	};

	enum class enHeCompMode : short
	{
		UNKNOWN = 0,
		APPLICATION = 1,
		MAINTENANCE = 2
	};

	enum class enElapsedTimeCounter : short
	{
		NONE = 0,
		OPERATIONAL = 1
	};

	enum class enErrorBits : short
	{
		PRESSURE	= 0,
		TEMPERATURE	= 1,
		OVERLOAD	= 2,
		SURGE		= 3
	};

	typedef union _unErrorFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	Pressure	: 1;
			unsigned	Temperature	: 1;
			unsigned	Overload	: 1;
			unsigned	Surge		: 1;
			unsigned	SPARE		: 12;	// <- 16-bits
		}bits;
	}unErrorFlags;

	typedef union _unValidityFlags
	{
		unsigned short	Word; // 16 bits
		struct
		{
			unsigned	sensors				: 1;
			unsigned	errorRegister		: 1;
			unsigned 	rtc					: 1;
			unsigned	eltOperational		: 1;
			unsigned	SPARE				: 12;	// <- 16-bits
		}bits;
	}unValidityFlags;

	typedef struct _stHeliumCompressorDataFrame
	{
		bool IsDataValid;
		bool expectedOnline;
		double timestamp;

		bool pressureOk;
		bool compressorState;
		Tango::DevVarBooleanArray *requestStatus;

		enHeCompStatus compStatus;

		stHeliumCompressorSensors sensors;
		unErrorFlags errorFlags;

		long elapsedCurrentOperational;
		long elapsedOperational;

		enDeviceStatus deviceStatus;
		enHeCompMode compMode;
		enDeviceMode currentMode;

		unValidityFlags validities;

		std::string serial;
		std::string swVersion;
		std::string fwVersion;
		std::string stateMachineState;

	} stHeliumCompressorDataFrame;

	class SpfHeProxy
	{
	public:
		SpfHeProxy(string name);
		~SpfHeProxy();

		bool Init();

		/* Raw commands */
		Tango::DevString SendRawCommand(string commandString);

		bool IsOnline();
		bool IsRunning();
		bool IsAvailable();

		bool SetExpectedOnline(bool state);

		enDeviceStatus GetHealthState();
		stHeliumCompressorDataFrame GetDataFrame();

		bool attrPressureOk();

		bool Request();
		bool Cancel();

		bool SetCompressorPowerState(bool state);

		bool SetMaintenance();
		bool SetStandby();
		bool Reset();
		bool ResetSM();
		bool Restart();

		bool ClearErrors();

		bool Test();

	private:
		DeviceProxy *spfHeDevice;
		string deviceName;
	};
}

#endif /* INTERFACE_SPFHE_PROXY_H_ */

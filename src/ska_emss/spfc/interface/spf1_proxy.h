/*
 * spf1_proxy.h
 *
 *  Created on: 18 August 2017
 *      Author: theuns
 */

#ifndef INTERFACE_SPF1_PROXY_H_
#define INTERFACE_SPF1_PROXY_H_

#include <tango.h>
using namespace Tango;

#include <string>
using namespace std;

#include "spf1_if.h"
using namespace Spf1Interface;


class Spf1Proxy
{
public:
	Spf1Proxy(string name);
	~Spf1Proxy();

	bool Init();
	bool Reset();
	bool Restart();

	/* Raw commands */
	Tango::DevString SendRawCommand(string commandString);

	/* Monitoring */
	bool IsOnline();
	enDeviceStatus GetHealthState();
	stFeedDataFrame GetDataFrame();

	/* Control */
	bool Control(enSetFeedMode feedCommand);
	bool Regenerate();
	bool ClearErrors();

	/* Write attributes */
	bool SetLnaHPowerState(bool state);
	bool SetLnaVPowerState(bool state);
	bool SetAmp2HPowerState(bool state);
	bool SetAmp2VPowerState(bool state);
	bool SetCalSourcePowerState(bool state);

	bool SetLnaHTempSetPoint(enLnaPidSetPoint sp);
	bool SetLnaHTempSetPointDefault(enLnaPidSetPoint sp);
	bool SetLnaVTempSetPoint(enLnaPidSetPoint sp);
	bool SetLnaVTempSetPointDefault(enLnaPidSetPoint sp);
	bool SetCalSourceTempSetPoint(enCalSourcePidSetPoint sp);
	bool SetCalSourceTempSetPointDefault(enCalSourcePidSetPoint sp);


	bool SetLnaHTempCtrlState(bool state);
	bool SetLnaVTempCtrlState(bool state);
	bool SetCalSourceTempCtrlState(bool state);

	bool SetExpectedOnline(bool state);
	bool SetStartupDefault(b1DefaultStartStateEnum state);

	enLnaPidSetPoint Convertb1LnaHTempSetPointEnum(short setpoint);
	enLnaPidSetPoint Convertb1LnaVTempSetPointEnum(short setpoint);
	enCalSourcePidSetPoint Convertb1CalSourceTempSetPointEnum(short setpoint);

	bool Test();

private:
	DeviceProxy *spf1Device;
	string deviceName;
};

#endif /* INTERFACE_SPF1_PROXY_H_ */

/*
 * spf345_proxy.cpp
 *
 *  Created on: 03 Aug 2022
 *      Author: cvn
 */

#include "spf345_proxy.h"
#include "spf345_if.h"
using namespace Spf345Interface;

#include <tango.h>
using namespace Tango;

//extern bool set_b345ExpectedOnline;

class DoubleEventCallBack3 : public Tango::CallBack
{
	void push_event(Tango::EventData*);
};
void DoubleEventCallBack3::push_event(Tango::EventData *myevent)
{
	Tango::DevVarDoubleArray *double_value;
	try
	{
		cout << "DoubleEventCallBack3::push_event(): called attribute "
		<< myevent->attr_name
		<< " event "
		<< myevent->event
		<< " (err="
		<< myevent->err
		<< ")" << endl;
		if (!myevent->err)
		{
			*(myevent->attr_value) >> double_value;
			cout << "double value "	<< (*double_value)[0] << endl;
			delete double_value;
		}
	}
	catch (...)
	{
		cout << "DoubleEventCallBack3::push_event(): could not extract data !\n";
	}
}

class EnumEventCallBack3 : public Tango::CallBack
{
	void push_event(Tango::EventData*);
};
void EnumEventCallBack3::push_event(Tango::EventData *myevent)
{
	Tango::DevEnum enum_value;
	try
	{
		cout << "EnumEventCallBack3::push_event(): called attribute "
		<< myevent->attr_name
		<< " event "
		<< myevent->event
		<< " (err="
		<< myevent->err
		<< ")" << endl;
		if (!myevent->err)
		{
			//(*devattr)[52] >> spf345DataFrame.capabilityState;
			*(myevent->attr_value) >> enum_value;
			cout << "enum value "	<< enum_value << endl;
			//delete enum_value;
		}
	}
	catch (...)
	{
		cout << "EnumEventCallBack3::push_event(): could not extract data !\n";
	}
}

Spf345Proxy::Spf345Proxy(string name)
{
	spf345Device = nullptr;
	deviceName = name;
}

Spf345Proxy::~Spf345Proxy()
{
	delete spf345Device;
	spf345Device = nullptr;
}

bool Spf345Proxy::Init()
{
	bool retVal = false;

	try
	{
		//DoubleEventCallBack *double_callback = new DoubleEventCallBack;
		//EnumEventCallBack *enum_callback = new EnumEventCallBack;

		// create a connection to a TANGO device
		spf345Device = new DeviceProxy(deviceName);

		if (spf345Device)
		{
			// Ping the device
			spf345Device->ping();

			/*
			int event_id;
			const string attr_name("lnaHDrainVoltage1");
			event_id = spf345Device->subscribe_event(attr_name, Tango::CHANGE_EVENT, double_callback);
			cout << "event_client() id = " << event_id << endl;

			int event_id2;
			const string attr_name2("lnaHDrainVoltage2");
			event_id2 = spf345Device->subscribe_event(attr_name2, Tango::CHANGE_EVENT, double_callback);
			cout << "event_client() id2 = " << event_id2 << endl;*/

			/*int event_id2;
			const string attr_name2("operatingState");
			event_id2 = spf345Device->subscribe_event(attr_name2, Tango::CHANGE_EVENT, enum_callback);
			cout << "event_client() id2 = " << event_id2 << endl;
			*/


			// Read a device attribute (string data type)
			DeviceAttribute att_reply;
			att_reply = spf345Device->read_attribute("b345HealthState");
			retVal = true;
		}
	}
	catch (ConnectionFailed &e)
	{
		Except::print_exception(e);
	}
	catch (CommunicationFailed &e)
	{
		Except::print_exception(e);
	}
	catch (DevFailed &e)
	{
		Except::print_exception(e);
	}
	catch (exception &e)
	{
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool Spf345Proxy::Reset()
{
	bool retVal = false;

	if (spf345Device) // Check that device is not null
	{
		try
		{
			spf345Device->command_inout("Reset");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::Restart()
{
	bool retVal = false;

	if (spf345Device) // Check that device is not null
	{
		try
		{
			spf345Device->command_inout("Restart");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::Shutdown()
{
	bool retVal = false;

	if (spf345Device) // Check that device is not null
	{
		try
		{
			spf345Device->command_inout("Shutdown");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

/* Device considered to be online if the Tango Device server has comms and the SM reports hardware detected. */
bool Spf345Proxy::IsOnline()
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			// Ping the device
			int pingTime = spf345Device->ping();

			//if (pingTime < 3000) /* FIXME: Removed during integration tests because of high ARM CPU load and high ping times.
			{
				DeviceAttribute att_reply = spf345Device->read_attribute("b345HwDetected");
				//att_reply >> retVal;

				if (! att_reply.is_empty())
				{
					att_reply >> retVal;
				}
				else
				{
					cout <<  "no hardware status defined!" << endl;
				}
			}
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

Tango::DevString Spf345Proxy::SendRawCommand(string commandString)
{
	Tango::DevString retVal;

	if (spf345Device)
	{
		try
		{
			DeviceData din, dout;

			din << commandString;

			dout = spf345Device->command_inout("SendRawCommand", din); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			if (!dout.is_empty())
			{
				string s;
				dout >> s;
				retVal = new char[s.size() + 1];
				strcpy(retVal, s.c_str());
			}
			else
			{
				retVal = Tango::string_dup("No data received from feed.");
			}
		}
		catch (ConnectionFailed &e)
		{
			retVal = Tango::string_dup("Exception: Connection failed.");
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = Tango::string_dup("Exception: Communication failed.");
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = Tango::string_dup("Exception: Device unlocked.");
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = Tango::string_dup("Exception: Device failed.");
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = Tango::string_dup("Exception: Unhandled.");
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}


/* TODO: Think about changing this to call a pipe function with the dataframe serialised and deserialised */
stFeedDataFrame Spf345Proxy::GetDataFrame()
{
	stFeedDataFrame spf345DataFrame = {};

	if (spf345Device) // Check that device is not null
	{
		try
		{
			vector<DeviceAttribute> *devattr;
			vector<string> attr_names;

			/* Setup attributes to get */
			attr_names.push_back("b3LnaHDrainVoltage");					// 0
			attr_names.push_back("b3LnaHDrainCurrent");					// 1
			attr_names.push_back("b3LnaHGateVoltage");					// 2
			attr_names.push_back("b3LnaVDrainVoltage");					// 3
			attr_names.push_back("b3LnaVDrainCurrent");					// 4
			attr_names.push_back("b3LnaVGateVoltage");					// 5
			attr_names.push_back("b4LnaHDrainVoltage");					// 6
			attr_names.push_back("b4LnaHDrainCurrent");					// 7
			attr_names.push_back("b4LnaHGateVoltage");					// 8
			attr_names.push_back("b4LnaVDrainVoltage");					// 9
			attr_names.push_back("b4LnaVDrainCurrent");					// 10
			attr_names.push_back("b4LnaVGateVoltage");					// 11
			attr_names.push_back("b5aLnaHDrainVoltage");				// 12
			attr_names.push_back("b5aLnaHDrainCurrent");				// 13
			attr_names.push_back("b5aLnaHGateVoltage");					// 14
			attr_names.push_back("b5aLnaVDrainVoltage");				// 15
			attr_names.push_back("b5aLnaVDrainCurrent");				// 16
			attr_names.push_back("b5aLnaVGateVoltage");					// 17
			attr_names.push_back("b5bLnaHDrainVoltage");				// 18
			attr_names.push_back("b5bLnaHDrainCurrent");				// 19
			attr_names.push_back("b5bLnaHGateVoltage");					// 20
			attr_names.push_back("b5bLnaVDrainVoltage");				// 21
			attr_names.push_back("b5bLnaVDrainCurrent");				// 22
			attr_names.push_back("b5bLnaVGateVoltage");					// 23
			attr_names.push_back("b6LnaHDrainVoltage");					// 24
			attr_names.push_back("b6LnaHDrainCurrent");					// 25
			attr_names.push_back("b6LnaHGateVoltage");					// 26
			attr_names.push_back("b6LnaVDrainVoltage");					// 27
			attr_names.push_back("b6LnaVDrainCurrent");					// 28
			attr_names.push_back("b6LnaVGateVoltage");					// 29

			attr_names.push_back("b345CryoPressure");				// 30
			attr_names.push_back("b345ManifoldPressure");			// 31
			attr_names.push_back("b3LnaTemp");						// 32
			attr_names.push_back("b4LnaTemp");						// 33
			attr_names.push_back("b5aLnaTemp");						// 34
			attr_names.push_back("b5bLnaTemp");						// 35
			attr_names.push_back("b6LnaTemp");						// 36
			attr_names.push_back("b3OmtTemp");						// 37
			attr_names.push_back("b4OmtTemp");						// 38
			attr_names.push_back("b5aOmtTemp");						// 39
			attr_names.push_back("b5bOmtTemp");						// 40
			attr_names.push_back("b6OmtTemp");						// 41
			attr_names.push_back("b5aHornTemp");					// 42
			attr_names.push_back("b5bHornTemp");					// 43
			attr_names.push_back("b6HornTemp");						// 44
			attr_names.push_back("b345BodyTemp");					// 45
			attr_names.push_back("b345ColdHeadStage1Temp");			// 46
			attr_names.push_back("b345ColdHeadStage2Temp");			// 47
			attr_names.push_back("b3WarmPlateTemp");				// 48
			attr_names.push_back("b4WarmPlateTemp");				// 49
			attr_names.push_back("b5aWarmPlateTemp");				// 50
			attr_names.push_back("b5bWarmPlateTemp");				// 51
			attr_names.push_back("b6WarmPlateTemp");				// 52
			attr_names.push_back("b345CalSourceTemp");				// 53
			attr_names.push_back("b345AnalogueInterfaceTemp");		// 54
			attr_names.push_back("b345DigitalInterfaceTemp");		// 55
			attr_names.push_back("b345SpdHealth");					// 56
			attr_names.push_back("b3LnaPidCurrent");				// 57
			attr_names.push_back("b4LnaPidCurrent");				// 58
			attr_names.push_back("b5aLnaPidCurrent");				// 59
			attr_names.push_back("b5bLnaPidCurrent");				// 60
			attr_names.push_back("b6LnaPidCurrent");				// 61

			attr_names.push_back("b345OperatingState");				// 62
//			attr_names.push_back("b345VaValveState");				//
			attr_names.push_back("b345ExpectedOnline");				// 63
			attr_names.push_back("b345HealthState");				// 64
//			attr_names.push_back("b4HealthState");					//
//			attr_names.push_back("b5aHealthState");					//
//			attr_names.push_back("b5bHealthState");					//
//			attr_names.push_back("b6HealthState");					//

			attr_names.push_back("b3LnaHMeanGain");					// 65
			attr_names.push_back("b3LnaVMeanGain");					// 66
			attr_names.push_back("b4LnaHMeanGain");					// 67
			attr_names.push_back("b4LnaVMeanGain");					// 68
			attr_names.push_back("b5aLnaHMeanGain");				// 69
			attr_names.push_back("b5aLnaVMeanGain");				// 70
			attr_names.push_back("b5bLnaHMeanGain");				// 71
			attr_names.push_back("b5bLnaVMeanGain");				// 72
			attr_names.push_back("b6LnaHMeanGain");					// 73
			attr_names.push_back("b6LnaVMeanGain");					// 74
			attr_names.push_back("b345ControlRegister"); 				// 75; all the discretes
			attr_names.push_back("b3LnaPidTempSetPoint");			// 76
			attr_names.push_back("b4LnaPidTempSetPoint");			// 77
			attr_names.push_back("b5aLnaPidTempSetPoint");			// 78
			attr_names.push_back("b5bLnaPidTempSetPoint");			// 79
			attr_names.push_back("b6LnaPidTempSetPoint");			// 80
			attr_names.push_back("b3WarmPlateTempSetPoint");		// 81
			attr_names.push_back("b4WarmPlateTempSetPoint");		// 82
			attr_names.push_back("b5aWarmPlateTempSetPoint");		// 83
			attr_names.push_back("b5bWarmPlateTempSetPoint");		// 84
			attr_names.push_back("b6WarmPlateTempSetPoint");		// 85
			attr_names.push_back("b345CalSourceTempSetPoint");		// 86
//			attr_names.push_back("b345CryoMotorState");				//  // see control register
			attr_names.push_back("b345CryoMotorSpeed");				// 87

			attr_names.push_back("b3CapabilityState");				// 88
			attr_names.push_back("b4CapabilityState");				// 89
			attr_names.push_back("b5aCapabilityState");				// 90
			attr_names.push_back("b5bCapabilityState");				// 91
			attr_names.push_back("b6CapabilityState");				// 92
			attr_names.push_back("b3CapTime2OpFull");				// 93
			attr_names.push_back("b4CapTime2OpFull");				// 94
			attr_names.push_back("b5aCapTime2OpFull");				// 95
			attr_names.push_back("b5bCapTime2OpFull");				// 96
			attr_names.push_back("b6CapTime2OpFull");				// 97

			attr_names.push_back("b345OpTime");						// 98
			attr_names.push_back("b345TotOpTime");					// 99
			attr_names.push_back("b345CryoMotorTime");				// 100
			attr_names.push_back("b345VaValveOpenTime");			// 101
			attr_names.push_back("b345ColdHeadCycles");				// 102

			attr_names.push_back("b3DefaultLnaPidTempSetPoint");	// 103
			attr_names.push_back("b4DefaultLnaPidTempSetPoint");	// 104
			attr_names.push_back("b5aDefaultLnaPidTempSetPoint");	// 105
			attr_names.push_back("b5bDefaultLnaPidTempSetPoint");	// 106
			attr_names.push_back("b6DefaultLnaPidTempSetPoint");	// 107
			attr_names.push_back("b3DefaultWarmPlateTempSetPoint");	// 108
			attr_names.push_back("b4DefaultWarmPlateTempSetPoint");	// 109
			attr_names.push_back("b5aDefaultWarmPlateTempSetPoint");// 110
			attr_names.push_back("b5bDefaultWarmPlateTempSetPoint");// 111
			attr_names.push_back("b6DefaultWarmPlateTempSetPoint");	// 112
			attr_names.push_back("b345DefaultCalSourceTempSetPoint");	// 113
			attr_names.push_back("b345DefaultStartState");			// 114

			attr_names.push_back("b345SerialNr");					// 115
			attr_names.push_back("b345LnaSerialNumbers");			// 116
			attr_names.push_back("b345TSensSerialNumbers");			// 117
//			attr_names.push_back("b345PSensSerialNumbers");			//
			attr_names.push_back("b345SwVersion");					// 118
			attr_names.push_back("b345FwVersion");					// 119

//			attr_names.push_back("stateMachineState");				//
			attr_names.push_back("b345CoolDownCounter");			// 120
			attr_names.push_back("b345InterfaceType");				// 121
			attr_names.push_back("b345ErrorCode");					// 122
//			attr_names.push_back("b345EstimatePowerConsumption");			// 123

			/* Get the attributes */
			devattr = spf345Device->read_attributes(attr_names);
			Tango::DevVarStringArray *argout = new Tango::DevVarStringArray();

			/* Unpack the values */
			(*devattr)[0] >> spf345DataFrame.sensors.b3LnaHDrainVoltage;
			(*devattr)[1] >> spf345DataFrame.sensors.b3LnaHDrainCurrent;
			(*devattr)[2] >> spf345DataFrame.sensors.b3LnaHGateVoltage;
			(*devattr)[3] >> spf345DataFrame.sensors.b3LnaVDrainVoltage;
			(*devattr)[4] >> spf345DataFrame.sensors.b3LnaVDrainCurrent;
			(*devattr)[5] >> spf345DataFrame.sensors.b3LnaVGateVoltage;
			(*devattr)[6] >> spf345DataFrame.sensors.b4LnaHDrainVoltage;
			(*devattr)[7] >> spf345DataFrame.sensors.b4LnaHDrainCurrent;
			(*devattr)[8] >> spf345DataFrame.sensors.b4LnaHGateVoltage;
			(*devattr)[9] >> spf345DataFrame.sensors.b4LnaVDrainVoltage;
			(*devattr)[10] >> spf345DataFrame.sensors.b4LnaVDrainCurrent;
			(*devattr)[11] >> spf345DataFrame.sensors.b4LnaVGateVoltage;
			(*devattr)[12] >> spf345DataFrame.sensors.b5aLnaHDrainVoltage;
			(*devattr)[13] >> spf345DataFrame.sensors.b5aLnaHDrainCurrent;
			(*devattr)[14] >> spf345DataFrame.sensors.b5aLnaHGateVoltage;
			(*devattr)[15] >> spf345DataFrame.sensors.b5aLnaVDrainVoltage;
			(*devattr)[16] >> spf345DataFrame.sensors.b5aLnaVDrainCurrent;
			(*devattr)[17] >> spf345DataFrame.sensors.b5aLnaVGateVoltage;
			(*devattr)[18] >> spf345DataFrame.sensors.b5bLnaHDrainVoltage;
			(*devattr)[19] >> spf345DataFrame.sensors.b5bLnaHDrainCurrent;
			(*devattr)[20] >> spf345DataFrame.sensors.b5bLnaHGateVoltage;
			(*devattr)[21] >> spf345DataFrame.sensors.b5bLnaVDrainVoltage;
			(*devattr)[22] >> spf345DataFrame.sensors.b5bLnaVDrainCurrent;
			(*devattr)[23] >> spf345DataFrame.sensors.b5bLnaVGateVoltage;
			(*devattr)[24] >> spf345DataFrame.sensors.b6LnaHDrainVoltage;
			(*devattr)[25] >> spf345DataFrame.sensors.b6LnaHDrainCurrent;
			(*devattr)[26] >> spf345DataFrame.sensors.b6LnaHGateVoltage;
			(*devattr)[27] >> spf345DataFrame.sensors.b6LnaVDrainVoltage;
			(*devattr)[28] >> spf345DataFrame.sensors.b6LnaVDrainCurrent;
			(*devattr)[29] >> spf345DataFrame.sensors.b6LnaVGateVoltage;

			(*devattr)[30] >> spf345DataFrame.sensors.b345CryoPressure;
			(*devattr)[31] >> spf345DataFrame.sensors.b345ManifoldPressure;
			(*devattr)[32] >> spf345DataFrame.sensors.b3LnaTemp;
			(*devattr)[33] >> spf345DataFrame.sensors.b4LnaTemp;
			(*devattr)[34] >> spf345DataFrame.sensors.b5aLnaTemp;
			(*devattr)[35] >> spf345DataFrame.sensors.b5bLnaTemp;
			(*devattr)[36] >> spf345DataFrame.sensors.b6LnaTemp;
			(*devattr)[37] >> spf345DataFrame.sensors.b3OmtTemp;
			(*devattr)[38] >> spf345DataFrame.sensors.b4OmtTemp;
			(*devattr)[39] >> spf345DataFrame.sensors.b5aOmtTemp;
			(*devattr)[40] >> spf345DataFrame.sensors.b5bOmtTemp;
			(*devattr)[41] >> spf345DataFrame.sensors.b6OmtTemp;
			(*devattr)[42] >> spf345DataFrame.sensors.b5aHornTemp;
			(*devattr)[43] >> spf345DataFrame.sensors.b5bHornTemp;
			(*devattr)[44] >> spf345DataFrame.sensors.b6HornTemp;
			(*devattr)[45] >> spf345DataFrame.sensors.b345BodyTemp;
			(*devattr)[46] >> spf345DataFrame.sensors.b345ColdheadStage1Temp;
			(*devattr)[47] >> spf345DataFrame.sensors.b345ColdheadStage2Temp;
			(*devattr)[48] >> spf345DataFrame.sensors.b3WarmPlateTemp;
			(*devattr)[49] >> spf345DataFrame.sensors.b4WarmPlateTemp;
			(*devattr)[50] >> spf345DataFrame.sensors.b5aWarmPlateTemp;
			(*devattr)[51] >> spf345DataFrame.sensors.b5bWarmPlateTemp;
			(*devattr)[52] >> spf345DataFrame.sensors.b6WarmPlateTemp;
			(*devattr)[53] >> spf345DataFrame.sensors.b345CalSourceTemp;
			(*devattr)[54] >> spf345DataFrame.sensors.b345AnalogueInterfaceTemp;
			(*devattr)[55] >> spf345DataFrame.sensors.b345DigitalInterfaceTemp;
			(*devattr)[56] >> spf345DataFrame.sensors.b345SpdHealth;
			(*devattr)[57] >> spf345DataFrame.sensors.b3LnaPidCurrent;
			(*devattr)[58] >> spf345DataFrame.sensors.b4LnaPidCurrent;
			(*devattr)[59] >> spf345DataFrame.sensors.b5aLnaPidCurrent;
			(*devattr)[60] >> spf345DataFrame.sensors.b5bLnaPidCurrent;
			(*devattr)[61] >> spf345DataFrame.sensors.b6LnaPidCurrent;

			(*devattr)[62] >> spf345DataFrame.currentMode; // OperatingState
//			(*devattr)[63] >> spf345DataFrame.isValveOpen; // see controlFlags
			(*devattr)[63] >> spf345DataFrame.expectedOnline;
			(*devattr)[64] >> spf345DataFrame.deviceStatus;

			(*devattr)[65] >> spf345DataFrame.config.meanGain3H;
			(*devattr)[66] >> spf345DataFrame.config.meanGain3V;
			(*devattr)[67] >> spf345DataFrame.config.meanGain4H;
			(*devattr)[68] >> spf345DataFrame.config.meanGain4V;
			(*devattr)[69] >> spf345DataFrame.config.meanGain5aH;
			(*devattr)[70] >> spf345DataFrame.config.meanGain5aV;
			(*devattr)[71] >> spf345DataFrame.config.meanGain5bH;
			(*devattr)[72] >> spf345DataFrame.config.meanGain5bV;
			(*devattr)[73] >> spf345DataFrame.config.meanGain6H;
			(*devattr)[74] >> spf345DataFrame.config.meanGain6V;

			(*devattr)[75] >> spf345DataFrame.controlFlags.Word;

			(*devattr)[76] >> spf345DataFrame.b3LnaPidTempSetPoint;
			(*devattr)[77] >> spf345DataFrame.b4LnaPidTempSetPoint;
			(*devattr)[78] >> spf345DataFrame.b5aLnaPidTempSetPoint;
			(*devattr)[79] >> spf345DataFrame.b5bLnaPidTempSetPoint;
			(*devattr)[80] >> spf345DataFrame.b6LnaPidTempSetPoint;
			(*devattr)[81] >> spf345DataFrame.b3WarmPlateTempSetPoint;
			(*devattr)[82] >> spf345DataFrame.b4WarmPlateTempSetPoint;
			(*devattr)[83] >> spf345DataFrame.b5aWarmPlateTempSetPoint;
			(*devattr)[84] >> spf345DataFrame.b5bWarmPlateTempSetPoint;
			(*devattr)[85] >> spf345DataFrame.b6WarmPlateTempSetPoint;
			(*devattr)[86] >> spf345DataFrame.b345CalSourceTempSetPoint;

//			(*devattr)[87] >> spf345DataFrame.MotorState;	// see controlFlags
			(*devattr)[87] >> spf345DataFrame.cryoMotorSpeed;

			(*devattr)[88] >> spf345DataFrame.b3CapabilityState;
			(*devattr)[89] >> spf345DataFrame.b4CapabilityState;
			(*devattr)[90] >> spf345DataFrame.b5aCapabilityState;
			(*devattr)[91] >> spf345DataFrame.b5bCapabilityState;
			(*devattr)[92] >> spf345DataFrame.b6CapabilityState;
			(*devattr)[93] >> spf345DataFrame.b3CapTime2OpFull;
			(*devattr)[94] >> spf345DataFrame.b4CapTime2OpFull;
			(*devattr)[95] >> spf345DataFrame.b5aCapTime2OpFull;
			(*devattr)[96] >> spf345DataFrame.b5bCapTime2OpFull;
			(*devattr)[97] >> spf345DataFrame.b6CapTime2OpFull;

			(*devattr)[98] >> spf345DataFrame.elapsedOperational;
			(*devattr)[99] >> spf345DataFrame.elapsedTotOp;
			(*devattr)[100] >> spf345DataFrame.elapsedMotor;
			(*devattr)[101] >> spf345DataFrame.elapsedValve;
			(*devattr)[102] >> spf345DataFrame.coldHeadCycles;

			(*devattr)[103] >> spf345DataFrame.b3DefaultLnaPidTempSetPoint;
			(*devattr)[104] >> spf345DataFrame.b4DefaultLnaPidTempSetPoint;
			(*devattr)[105] >> spf345DataFrame.b5aDefaultLnaPidTempSetPoint;
			(*devattr)[106] >> spf345DataFrame.b5bDefaultLnaPidTempSetPoint;
			(*devattr)[107] >> spf345DataFrame.b6DefaultLnaPidTempSetPoint;
			(*devattr)[108] >> spf345DataFrame.b3DefaultWarmPlateTempSetPoint;
			(*devattr)[109] >> spf345DataFrame.b4DefaultWarmPlateTempSetPoint;
			(*devattr)[110] >> spf345DataFrame.b5aDefaultWarmPlateTempSetPoint;
			(*devattr)[111] >> spf345DataFrame.b5bDefaultWarmPlateTempSetPoint;
			(*devattr)[112] >> spf345DataFrame.b6DefaultWarmPlateTempSetPoint;
			(*devattr)[113] >> spf345DataFrame.b345DefaultCalSourceTempSetPoint;
			(*devattr)[114] >> spf345DataFrame.defaultStartState;

			(*devattr)[115] >> spf345DataFrame.config.serial;
//			(*devattr)[116] >> spf345DataFrame.config.lnaSerialNrs;
			(*devattr)[116] >> argout;
				spf345DataFrame.config.lnaSerialNrs[0] = (*argout)[0];
				spf345DataFrame.config.lnaSerialNrs[1] = (*argout)[1];
				spf345DataFrame.config.lnaSerialNrs[2] = (*argout)[2];
				spf345DataFrame.config.lnaSerialNrs[3] = (*argout)[3];
				spf345DataFrame.config.lnaSerialNrs[4] = (*argout)[4];
				spf345DataFrame.config.lnaSerialNrs[5] = (*argout)[5];
				spf345DataFrame.config.lnaSerialNrs[6] = (*argout)[6];
				spf345DataFrame.config.lnaSerialNrs[7] = (*argout)[7];
				spf345DataFrame.config.lnaSerialNrs[8] = (*argout)[8];
				spf345DataFrame.config.lnaSerialNrs[9] = (*argout)[9];
//			(*devattr)[117] >> spf345DataFrame.config.tempSerialNrs;
			(*devattr)[117] >> argout;
				spf345DataFrame.config.tempSerialNrs[0] = (*argout)[0];
//			(*devattr)[118] >> spf345DataFrame.config.pressSerialNrs;
			(*devattr)[118] >> spf345DataFrame.config.swVersion;
			(*devattr)[119] >> spf345DataFrame.config.fwVersion;


//			(*devattr)[120] >> spf345DataFrame.stateMachineState;
			(*devattr)[120] >> spf345DataFrame.b345CoolDownCounter;
			(*devattr)[121] >> spf345DataFrame.interfaceType;
//			(*devattr)[122] >> spf345DataFrame.errorCode;
			(*devattr)[122] >> argout;
				spf345DataFrame.errorCode[0] = (*argout)[0];
				spf345DataFrame.errorCode[1] = (*argout)[1];
				spf345DataFrame.errorCode[2] = (*argout)[2];
			/* Cleanup */
			delete argout;
			delete devattr;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
//			set_b345ExpectedOnline = false;
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return spf345DataFrame;
}

enDeviceStatus Spf345Proxy::GetHealthState()
{
	enDeviceStatus retVal = enDeviceStatus::UNKNOWN;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute att_reply;
			att_reply = spf345Device->read_attribute("b345HealthState");
			att_reply >> retVal;
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::Control(enSetFeedMode setFeedMode)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			string command;

			switch (setFeedMode)
			{
				case enSetFeedMode::STANDBY_LP:
					command = "SetStandby";
					break;
				case enSetFeedMode::OPERATE:
					command = "SetOperate";
					break;
				case enSetFeedMode::MAINTENANCE:
					command = "SetMaintenance";
					break;
				default:
					break;
			}

			spf345Device->command_inout(command); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;

			/*
			if (!cmdResponse.is_empty())
			{
				cout << "Data from device command: \n" << cmdResponse << endl;
			}
			else
			{
				cout << "No data from device command" << endl;
			}
			*/

		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::Regenerate()
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			spf345Device->command_inout("Regenerate"); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::ClearErrors()
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			spf345Device->command_inout("ClearErrorsAndReset"); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::SetVaValveState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345VaValveState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::SetLnaHIllumination(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345LnaHIlluminationEnabled", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetLnaVIllumination(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345LnaVIlluminationEnabled", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

/* Set both LNA-H and LNA-V channels */
bool Spf345Proxy::SetLnaIllumination(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("lnaIlluminationEnabled", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::SetB3LnaPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b3LnaPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}
	return retVal;
}
bool Spf345Proxy::SetB4LnaPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b4LnaPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetB5aLnaPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b5aLnaPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetB5bLnaPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b5bLnaPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetB6LnaPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b6LnaPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345CalSourcePowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetActiveBand(enActiveBand band)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345ActiveBand", band);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::SetLnaTempSetPoint(enLnaPidSetPoint sp, enBandSelect band)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 14;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 16;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 18;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 20;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 22;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 24;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			string bandSetPoint = "b3LnaPidTempSetPoint";
			switch(band)
			{
			case enBandSelect::B3:
				bandSetPoint = "b3LnaPidTempSetPoint";
				break;
			case enBandSelect::B4:
				bandSetPoint = "b4LnaPidTempSetPoint";
				break;
			case enBandSelect::B5A:
				bandSetPoint = "b5aLnaPidTempSetPoint";
				break;
			case enBandSelect::B5B:
				bandSetPoint = "b5bLnaPidTempSetPoint";
				break;
			case enBandSelect::B6:
				bandSetPoint = "b6LnaPidTempSetPoint";
				break;
			default:
				bandSetPoint = "";
			}

			DeviceAttribute value(bandSetPoint, static_cast<float>(tempValue));
//			DeviceAttribute value("b3LnaPidTempSetPoint", tempValue);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetLnaTempSetPointDefault(enLnaPidSetPoint sp, enBandSelect band)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 14;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 16;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 18;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 20;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 22;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 24;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			string bandSetPoint = "b3DefaultLnaPidTempSetPoint";
			switch(band)
			{
			case enBandSelect::B3:
				bandSetPoint = "b3DefaultLnaPidTempSetPoint";
				break;
			case enBandSelect::B4:
				bandSetPoint = "b4DefaultLnaPidTempSetPoint";
				break;
			case enBandSelect::B5A:
				bandSetPoint = "b5aDefaultLnaPidTempSetPoint";
				break;
			case enBandSelect::B5B:
				bandSetPoint = "b5bDefaultLnaPidTempSetPoint";
				break;
			case enBandSelect::B6:
				bandSetPoint = "b6DefaultLnaPidTempSetPoint";
				break;
			default:
				bandSetPoint = "";
			}

			DeviceAttribute value(bandSetPoint, static_cast<float>(tempValue));
//			DeviceAttribute value("b3DefaultLnaPidTempSetPoint", tempValue);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetWarmPlateTempSetPoint(enWarmPlateSetPoint sp, enBandSelect band)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enWarmPlateSetPoint::OFF:
				tempValue = 0;
				break;
			case enWarmPlateSetPoint::SP1:
				tempValue = 273;
				break;
			case enWarmPlateSetPoint::SP2:
				tempValue = 283;
				break;
			case enWarmPlateSetPoint::SP3:
				tempValue = 293;
				break;
			case enWarmPlateSetPoint::SP4:
				tempValue = 303;
				break;
			case enWarmPlateSetPoint::SP5:
				tempValue = 313;
				break;
			case enWarmPlateSetPoint::SP6:
				tempValue = 323;
				break;
			case enWarmPlateSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			string bandSetPoint = "b3WarmPlateTempSetPoint";
			switch(band)
			{
			case enBandSelect::B3:
				bandSetPoint = "b3WarmPlateTempSetPoint";
				break;
			case enBandSelect::B4:
				bandSetPoint = "b4WarmPlateTempSetPoint";
				break;
			case enBandSelect::B5A:
				bandSetPoint = "b5aWarmPlateTempSetPoint";
				break;
			case enBandSelect::B5B:
				bandSetPoint = "b5bWarmPlateTempSetPoint";
				break;
			case enBandSelect::B6:
				bandSetPoint = "b6WarmPlateTempSetPoint";
				break;
			default:
				bandSetPoint = "";
			}

			DeviceAttribute value(bandSetPoint, static_cast<float>(tempValue));
//			DeviceAttribute value("b3WarmPlateTempSetPoint", tempValue);
		spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetWarmPlateTempSetPointDefault(enWarmPlateSetPoint sp, enBandSelect band)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enWarmPlateSetPoint::OFF:
				tempValue = 0;
				break;
			case enWarmPlateSetPoint::SP1:
				tempValue = 273;
				break;
			case enWarmPlateSetPoint::SP2:
				tempValue = 283;
				break;
			case enWarmPlateSetPoint::SP3:
				tempValue = 293;
				break;
			case enWarmPlateSetPoint::SP4:
				tempValue = 303;
				break;
			case enWarmPlateSetPoint::SP5:
				tempValue = 313;
				break;
			case enWarmPlateSetPoint::SP6:
				tempValue = 323;
				break;
			case enWarmPlateSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			string bandSetPoint = "b3DefaultWarmPlateTempSetPoint";
			switch(band)
			{
			case enBandSelect::B3:
				bandSetPoint = "b3DefaultWarmPlateTempSetPoint";
				break;
			case enBandSelect::B4:
				bandSetPoint = "b4DefaultWarmPlateTempSetPoint";
				break;
			case enBandSelect::B5A:
				bandSetPoint = "b5aDefaultWarmPlateTempSetPoint";
				break;
			case enBandSelect::B5B:
				bandSetPoint = "b5bDefaultWarmPlateTempSetPoint";
				break;
			case enBandSelect::B6:
				bandSetPoint = "b6DefaultWarmPlateTempSetPoint";
				break;
			default:
				bandSetPoint = "";
			}

			DeviceAttribute value(bandSetPoint, static_cast<float>(tempValue));
//			DeviceAttribute value("b3DefaultWarmPlateTempSetPoint", tempValue);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetCalSourceTempSetPoint(enCalSourcePidSetPoint sp)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enCalSourcePidSetPoint::OFF:
				tempValue = 0;
				break;
			case enCalSourcePidSetPoint::SP1:
				tempValue = 280;
				break;
			case enCalSourcePidSetPoint::SP2:
				tempValue = 285;
				break;
			case enCalSourcePidSetPoint::SP3:
				tempValue = 290;
				break;
			case enCalSourcePidSetPoint::SP4:
				tempValue = 295;
				break;
			case enCalSourcePidSetPoint::SP5:
				tempValue = 300;
				break;
			case enCalSourcePidSetPoint::SP6:
				tempValue = 305;
				break;
			case enCalSourcePidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b345CalSourceTempSetPoint", static_cast<float>(tempValue));
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}
	return retVal;
}

bool Spf345Proxy::SetCalSourceTempSetPointDefault(enCalSourcePidSetPoint sp)
{
	bool retVal = false;

	try
	{
		// TODO: These should all be verified
		short tempValue = 0;
		switch(sp)
		{
		case enCalSourcePidSetPoint::OFF:
			tempValue = 0;
			break;
		case enCalSourcePidSetPoint::SP1:
			tempValue = 280;
			break;
		case enCalSourcePidSetPoint::SP2:
			tempValue = 285;
			break;
		case enCalSourcePidSetPoint::SP3:
			tempValue = 290;
			break;
		case enCalSourcePidSetPoint::SP4:
			tempValue = 296;
			break;
		case enCalSourcePidSetPoint::SP5:
			tempValue = 300;
			break;
		case enCalSourcePidSetPoint::SP6:
			tempValue = 305;
			break;
		case enCalSourcePidSetPoint::CUSTOM:
			return false;
			break;
		default:
			return false;
			break;
		}

		DeviceAttribute value("b345DefaultCalSourceTempSetPoint", static_cast<float>(tempValue));
		spf345Device->write_attribute(value);

		retVal = true;
	}
	catch (ConnectionFailed &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (CommunicationFailed &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (DeviceUnlocked &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (DevFailed &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enLnaPidSetPoint Spf345Proxy::Convertb345LnaTempSetPointEnum(short setpoint)
{
	enLnaPidSetPoint retVal = enLnaPidSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enLnaPidSetPoint::OFF;
			break;
		case 14:
			retVal = enLnaPidSetPoint::SP1;
			break;
		case 16:
			retVal = enLnaPidSetPoint::SP2;
			break;
		case 18:
			retVal = enLnaPidSetPoint::SP3;
			break;
		case 20:
			retVal = enLnaPidSetPoint::SP4;
			break;
		case 22:
			retVal = enLnaPidSetPoint::SP5;
			break;
		case 24:
			retVal = enLnaPidSetPoint::SP6;
			break;
		default:
			retVal = enLnaPidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}
/* We should convert from a setpoint integer value to a enum value */
enLnaPidSetPoint Spf345Proxy::Convertb345DefaultLnaTempSetPointEnum(short setpoint)
{
	enLnaPidSetPoint retVal = enLnaPidSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enLnaPidSetPoint::OFF;
			break;
		case 14:
			retVal = enLnaPidSetPoint::SP1;
			break;
		case 16:
			retVal = enLnaPidSetPoint::SP2;
			break;
		case 18:
			retVal = enLnaPidSetPoint::SP3;
			break;
		case 20:
			retVal = enLnaPidSetPoint::SP4;
			break;
		case 22:
			retVal = enLnaPidSetPoint::SP5;
			break;
		case 24:
			retVal = enLnaPidSetPoint::SP6;
			break;
		default:
			retVal = enLnaPidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enWarmPlateSetPoint Spf345Proxy::Convertb345WarmPlateTempSetPointEnum(short setpoint)
{
	enWarmPlateSetPoint retVal = enWarmPlateSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enWarmPlateSetPoint::OFF;
			break;
		case 273:
			retVal = enWarmPlateSetPoint::SP1;
			break;
		case 283:
			retVal = enWarmPlateSetPoint::SP2;
			break;
		case 293:
			retVal = enWarmPlateSetPoint::SP3;
			break;
		case 303:
			retVal = enWarmPlateSetPoint::SP4;
			break;
		case 313:
			retVal = enWarmPlateSetPoint::SP5;
			break;
		case 323:
			retVal = enWarmPlateSetPoint::SP6;
			break;
		default:
			retVal = enWarmPlateSetPoint::CUSTOM;
			break;
	}

	return retVal;
}
/* We should convert from a setpoint integer value to a enum value */
enWarmPlateSetPoint Spf345Proxy::Convertb345DefaultWarmPlateTempSetPointEnum(short setpoint)
{
	enWarmPlateSetPoint retVal = enWarmPlateSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enWarmPlateSetPoint::OFF;
			break;
		case 273:
			retVal = enWarmPlateSetPoint::SP1;
			break;
		case 283:
			retVal = enWarmPlateSetPoint::SP2;
			break;
		case 293:
			retVal = enWarmPlateSetPoint::SP3;
			break;
		case 303:
			retVal = enWarmPlateSetPoint::SP4;
			break;
		case 313:
			retVal = enWarmPlateSetPoint::SP5;
			break;
		case 323:
			retVal = enWarmPlateSetPoint::SP6;
			break;
		default:
			retVal = enWarmPlateSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enCalSourcePidSetPoint Spf345Proxy::Convertb345CalSourceTempSetPointEnum(short setpoint)
{
	enCalSourcePidSetPoint retVal = enCalSourcePidSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enCalSourcePidSetPoint::OFF;
			break;
		case 280:
			retVal = enCalSourcePidSetPoint::SP1;
			break;
		case 285:
			retVal = enCalSourcePidSetPoint::SP2;
			break;
		case 290:
			retVal = enCalSourcePidSetPoint::SP3;
			break;
		case 295:
			retVal = enCalSourcePidSetPoint::SP4;
			break;
		case 300:
			retVal = enCalSourcePidSetPoint::SP5;
			break;
		case 305:
			retVal = enCalSourcePidSetPoint::SP6;
			break;
		default:
			retVal = enCalSourcePidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enCalSourcePidSetPoint Spf345Proxy::Convertb345DefaultCalSourceTempSetPointEnum(short setpoint)
{
	enCalSourcePidSetPoint retVal = enCalSourcePidSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enCalSourcePidSetPoint::OFF;
			break;
		case 280:
			retVal = enCalSourcePidSetPoint::SP1;
			break;
		case 285:
			retVal = enCalSourcePidSetPoint::SP2;
			break;
		case 290:
			retVal = enCalSourcePidSetPoint::SP3;
			break;
		case 295:
			retVal = enCalSourcePidSetPoint::SP4;
			break;
		case 300:
			retVal = enCalSourcePidSetPoint::SP5;
			break;
		case 305:
			retVal = enCalSourcePidSetPoint::SP6;
			break;
		default:
			retVal = enCalSourcePidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

bool Spf345Proxy::SetB3PidPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b3PidPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetB4PidPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b4PidPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetB5aPidPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b5aPidPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetB5bPidPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b5bPidPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetB6PidPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b6PidPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetCalSourceTempCtrlState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345CalSourcePidPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::SetCryoMotorState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345CryoMotorState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetCryoMotorSpeed(short rpm)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345CryoMotorSpeed", rpm);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}
	return retVal;
}
bool Spf345Proxy::SetNegHeaterPowerState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345NegHeaterPowerState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf345Proxy::SetVaTurboState(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			DeviceAttribute value("b345VaTurboState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::SetExpectedOnline(bool state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			string command;

			DeviceAttribute value("b345ExpectedOnline", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}

		return retVal;
	}
}

bool Spf345Proxy::SetStartupDefault(b345DefaultStartStateEnum state)
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			string command;

			DeviceAttribute value("b345DefaultStartState", state);
			spf345Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf345Proxy::Test()
{
	bool retVal = false;

	if (spf345Device)
	{
		try
		{
			spf345Device->command_inout("SetMaintenance");
			retVal = true;
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
	}

	return retVal;
}



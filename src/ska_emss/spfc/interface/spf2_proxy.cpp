/*
 * spf2_proxy.cpp
 *
 *  Created on: 27 May 2017
 *      Author: theuns
 */

#include "spf2_proxy.h"
#include "spf2_if.h"
using namespace Spf2Interface;

#include <tango.h>
using namespace Tango;

//extern bool set_b2ExpectedOnline;

class DoubleEventCallBack : public Tango::CallBack
{
	void push_event(Tango::EventData*);
};
void DoubleEventCallBack::push_event(Tango::EventData *myevent)
{
	Tango::DevVarDoubleArray *double_value;
	try
	{
		cout << "DoubleEventCallBack::push_event(): called attribute "
		<< myevent->attr_name
		<< " event "
		<< myevent->event
		<< " (err="
		<< myevent->err
		<< ")" << endl;
		if (!myevent->err)
		{
			*(myevent->attr_value) >> double_value;
			cout << "double value "	<< (*double_value)[0] << endl;
			delete double_value;
		}
	}
	catch (...)
	{
		cout << "DoubleEventCallBack::push_event(): could not extract data !\n";
	}
}

class EnumEventCallBack : public Tango::CallBack
{
	void push_event(Tango::EventData*);
};
void EnumEventCallBack::push_event(Tango::EventData *myevent)
{
	Tango::DevEnum enum_value;
	try
	{
		cout << "EnumEventCallBack::push_event(): called attribute "
		<< myevent->attr_name
		<< " event "
		<< myevent->event
		<< " (err="
		<< myevent->err
		<< ")" << endl;
		if (!myevent->err)
		{
			//(*devattr)[52] >> spf2DataFrame.capabilityState;
			*(myevent->attr_value) >> enum_value;
			cout << "enum value "	<< enum_value << endl;
			//delete enum_value;
		}
	}
	catch (...)
	{
		cout << "DoubleEventCallBack::push_event(): could not extract data !\n";
	}
}

Spf2Proxy::Spf2Proxy(string name)
{
	spf2Device = nullptr;
	deviceName = name;
}

Spf2Proxy::~Spf2Proxy()
{
	delete spf2Device;
	spf2Device = nullptr;
}

bool Spf2Proxy::Init()
{
	bool retVal = false;

	try
	{
		//DoubleEventCallBack *double_callback = new DoubleEventCallBack;
		//EnumEventCallBack *enum_callback = new EnumEventCallBack;

		// create a connection to a TANGO device
		spf2Device = new DeviceProxy(deviceName);

		if (spf2Device)
		{
			// Ping the device
			spf2Device->ping();

			/*
			int event_id;
			const string attr_name("lnaHDrainVoltage1");
			event_id = spf2Device->subscribe_event(attr_name, Tango::CHANGE_EVENT, double_callback);
			cout << "event_client() id = " << event_id << endl;

			int event_id2;
			const string attr_name2("lnaHDrainVoltage2");
			event_id2 = spf2Device->subscribe_event(attr_name2, Tango::CHANGE_EVENT, double_callback);
			cout << "event_client() id2 = " << event_id2 << endl;*/

			/*int event_id2;
			const string attr_name2("operatingState");
			event_id2 = spf2Device->subscribe_event(attr_name2, Tango::CHANGE_EVENT, enum_callback);
			cout << "event_client() id2 = " << event_id2 << endl;
			*/


			// Read a device attribute (string data type)
			DeviceAttribute att_reply;
			att_reply = spf2Device->read_attribute("b2HealthState");
			retVal = true;
		}
	}
	catch (ConnectionFailed &e)
	{
		Except::print_exception(e);
	}
	catch (CommunicationFailed &e)
	{
		Except::print_exception(e);
	}
	catch (DevFailed &e)
	{
		Except::print_exception(e);
	}
	catch (exception &e)
	{
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool Spf2Proxy::Reset()
{
	bool retVal = false;

	if (spf2Device) // Check that device is not null
	{
		try
		{
			spf2Device->command_inout("Reset");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::Restart()
{
	bool retVal = false;

	if (spf2Device) // Check that device is not null
	{
		try
		{
			spf2Device->command_inout("Restart");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::Shutdown()
{
	bool retVal = false;

	if (spf2Device) // Check that device is not null
	{
		try
		{
			spf2Device->command_inout("Shutdown");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

/* Device considered to be online if the Tango Device server has comms and the SM reports hardware detected. */
bool Spf2Proxy::IsOnline()
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			// Ping the device
			int pingTime = spf2Device->ping();

			//if (pingTime < 3000) /* FIXME: Removed during integration tests because of high ARM CPU load and high ping times.
			{
				DeviceAttribute att_reply = spf2Device->read_attribute("hardwareDetected");
				//att_reply >> retVal;

				if (! att_reply.is_empty())
				{
					att_reply >> retVal;
				}
				else
				{
					cout <<  "no hardware status defined!" << endl;
				}
			}
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

Tango::DevString Spf2Proxy::SendRawCommand(string commandString)
{
	Tango::DevString retVal;

	if (spf2Device)
	{
		try
		{
			DeviceData din, dout;

			din << commandString;

			dout = spf2Device->command_inout("SendRawCommand", din); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			if (!dout.is_empty())
			{
				string s;
				dout >> s;
				retVal = new char[s.size() + 1];
				strcpy(retVal, s.c_str());
			}
			else
			{
				retVal = Tango::string_dup("No data received from feed.");
			}
		}
		catch (ConnectionFailed &e)
		{
			retVal = Tango::string_dup("Exception: Connection failed.");
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = Tango::string_dup("Exception: Communication failed.");
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = Tango::string_dup("Exception: Device unlocked.");
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = Tango::string_dup("Exception: Device failed.");
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = Tango::string_dup("Exception: Unhandled.");
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}


/* TODO: Think about changing this to call a pipe function with the dataframe serialised and deserialised */
stFeedDataFrame Spf2Proxy::GetDataFrame()
{
	stFeedDataFrame spf2DataFrame = {};

	if (spf2Device) // Check that device is not null
	{
		try
		{
			vector<DeviceAttribute> *devattr;
			vector<string> attr_names;

			/* Setup attributes to get */
//			attr_names.push_back("controllerVolt");					// 0
//			attr_names.push_back("controllerCurrent");				// 1
//			attr_names.push_back("controllerTemp");					// 2
//			attr_names.push_back("psuAnalogue6vRectifiedVolt");		// 3
//			attr_names.push_back("psuAnalogue6vRegulatorTemp");		// 4
//			attr_names.push_back("psuAnalogue15vRegulatorTemp");	// 5
//			attr_names.push_back("psuDigital6vRegulatorTemp");		// 6
			attr_names.push_back("b2LnaHDrainVoltage");					// 0
//			attr_names.push_back("lnaHDrainVoltage1");				// 7
//			attr_names.push_back("lnaHDrainVoltage2");				// 8
//			attr_names.push_back("lnaHDrainVoltage3");				// 9
			attr_names.push_back("b2LnaHDrainCurrent");					// 1
//			attr_names.push_back("lnaHDrainCurrent1");				// 10
//			attr_names.push_back("lnaHDrainCurrent2");				// 11
//			attr_names.push_back("lnaHDrainCurrent3");				// 12
			attr_names.push_back("b2LnaHGateVoltage");					// 2
//			attr_names.push_back("lnaHGateVoltage1");				// 13
//			attr_names.push_back("lnaHGateVoltage2");				// 14
//			attr_names.push_back("lnaHGateVoltage3");				// 15
			attr_names.push_back("b2LnaVDrainVoltage");					// 3
//			attr_names.push_back("lnaVDrainVoltage1");				// 16
//			attr_names.push_back("lnaVDrainVoltage2");				// 17
//			attr_names.push_back("lnaVDrainVoltage3");				// 18
			attr_names.push_back("b2LnaVDrainCurrent");					// 4
//			attr_names.push_back("lnaVDrainCurrent1");				// 19
//			attr_names.push_back("lnaVDrainCurrent2");				// 20
//			attr_names.push_back("lnaVDrainCurrent3");				// 21
			attr_names.push_back("b2LnaVGateVoltage");					// 5
//			attr_names.push_back("lnaVGateVoltage1");				// 22
//			attr_names.push_back("lnaVGateVoltage2");				// 23
//			attr_names.push_back("lnaVGateVoltage3");				// 24
			attr_names.push_back("b2Amp2HVoltage");						// 6
			attr_names.push_back("b2Amp2HCurrent");					// 7
			attr_names.push_back("b2Amp2VVoltage");					// 8
			attr_names.push_back("b2Amp2VCurrent");					// 9
			attr_names.push_back("b2CalSourceVoltage");				// 10
			attr_names.push_back("b2CalSourceCurrent");				// 11
			attr_names.push_back("b2CalSourceTemp");				// 12
			attr_names.push_back("b2CalSourcePidCurrent");			// 13
			attr_names.push_back("b2LnaPidCurrent");				// 14
			attr_names.push_back("b2CryoPressure");					// 15
			attr_names.push_back("b2ManifoldPressure");				// 16
			attr_names.push_back("b2LnaTemp");						// 17
			attr_names.push_back("b2OmtTemp");						// 18
			attr_names.push_back("b2ColdHeadStage1Temp");			// 19
			attr_names.push_back("b2BodyTemp");						// 20
			attr_names.push_back("b2AnalogueInterfaceTemp");		// 21

			attr_names.push_back("b2OperatingState");				// 22
			attr_names.push_back("b2VaValveState");					// 23
			attr_names.push_back("b2ExpectedOnline");				// 24
			attr_names.push_back("b2HealthState");					// 25

			attr_names.push_back("b2LnaHMeanGain");					// 26
			attr_names.push_back("b2LnaVMeanGain");					// 27
			attr_names.push_back("controlRegister"); 				// 28; all the discretes
			attr_names.push_back("b2LnaPidTempSetPoint");			// 29
			attr_names.push_back("b2CalSourcePidTempSetPoint");		// 30

			attr_names.push_back("b2CryoMotorState");				// 31
			attr_names.push_back("b2CryoMotorSpeed");				// 32

			attr_names.push_back("b2CapabilityState");				// 33
			attr_names.push_back("b2CapTime2OpFull");				// 34

			attr_names.push_back("b2OpTime");						// 35
			attr_names.push_back("b2TotOpTime");					// 36
			attr_names.push_back("b2CryoMotorTime");				// 37
			attr_names.push_back("b2VaValveOpenTime");				// 38

			attr_names.push_back("b2DefaultLnaPidTempSetPoint");	// 39
			attr_names.push_back("b2DefaultCalSourcePidTempSetPoint");	// 40
			attr_names.push_back("b2DefaultStartState");			// 41

			attr_names.push_back("serialNumber");					// 42
			attr_names.push_back("lnaHSerialNumber");				// 43
			attr_names.push_back("lnaVSerialNumber");				// 44
			attr_names.push_back("swVersion");						// 45
			attr_names.push_back("fwVersion");						// 46

			attr_names.push_back("b2PsuMotorPosSupplyCurrent");		// 47
			attr_names.push_back("b2PsuMotorNegSupplyCurrent");		// 48
			attr_names.push_back("b2PsuMotorDriverTemp");			// 49
//			attr_names.push_back("stateMachineState");				// 50
			attr_names.push_back("b2CoolDownCounter");				// 50
			attr_names.push_back("b2SpdHealth");				// 51
			attr_names.push_back("b2InterfaceType");				// 52
			attr_names.push_back("b2ErrorCode");				// 53

			/* Get the attributes */
			devattr = spf2Device->read_attributes(attr_names);
			Tango::DevVarStringArray *argout = new Tango::DevVarStringArray();

			/* Unpack the values */
//			(*devattr)[0]  >> spf2DataFrame.sensors.controllerVolt;
//			(*devattr)[1]  >> spf2DataFrame.sensors.controllerCurrent;
//			(*devattr)[2]  >> spf2DataFrame.sensors.controllerTemp;
//			(*devattr)[3]  >> spf2DataFrame.sensors.psuAnalogue6vRectifiedVolt;
//			(*devattr)[4]  >> spf2DataFrame.sensors.psuAnalogue6vRegulatorTemp;
//			(*devattr)[5]  >> spf2DataFrame.sensors.psuAnalogue15vRegulatorTemp;
//			(*devattr)[6]  >> spf2DataFrame.sensors.psuDigital6vRegulatorTemp;
			(*devattr)[0]  >> spf2DataFrame.sensors.lnaHDrainVoltage;
//			(*devattr)[8]  >> spf2DataFrame.sensors.lnaHDrainVoltage2;
//			(*devattr)[9]  >> spf2DataFrame.sensors.lnaHDrainVoltage3;
			(*devattr)[1] >> spf2DataFrame.sensors.lnaHDrainCurrent;
//			(*devattr)[11] >> spf2DataFrame.sensors.lnaHDrainCurrent2;
//			(*devattr)[12] >> spf2DataFrame.sensors.lnaHDrainCurrent3;
			(*devattr)[2] >> spf2DataFrame.sensors.lnaHGateVoltage;
//			(*devattr)[14] >> spf2DataFrame.sensors.lnaHGateVoltage2;
//			(*devattr)[15] >> spf2DataFrame.sensors.lnaHGateVoltage3;
			(*devattr)[3] >> spf2DataFrame.sensors.lnaVDrainVoltage;
//			(*devattr)[17] >> spf2DataFrame.sensors.lnaVDrainVoltage2;
//			(*devattr)[18] >> spf2DataFrame.sensors.lnaVDrainVoltage3;
			(*devattr)[4] >> spf2DataFrame.sensors.lnaVDrainCurrent;
//			(*devattr)[20] >> spf2DataFrame.sensors.lnaVDrainCurrent2;
//			(*devattr)[21] >> spf2DataFrame.sensors.lnaVDrainCurrent3;
			(*devattr)[5] >> spf2DataFrame.sensors.lnaVGateVoltage;
//			(*devattr)[23] >> spf2DataFrame.sensors.lnaVGateVoltage2;
//			(*devattr)[24] >> spf2DataFrame.sensors.lnaVGateVoltage3;
			(*devattr)[6] >> spf2DataFrame.sensors.amp2HVoltage;
			(*devattr)[7] >> spf2DataFrame.sensors.amp2HCurrent;
			(*devattr)[8] >> spf2DataFrame.sensors.amp2VVoltage;
			(*devattr)[9] >> spf2DataFrame.sensors.amp2VCurrent;
			(*devattr)[10] >> spf2DataFrame.sensors.calsourceVoltage;
			(*devattr)[11] >> spf2DataFrame.sensors.calsourceCurrent;
			(*devattr)[12] >> spf2DataFrame.sensors.calsourceTemp;
			(*devattr)[13] >> spf2DataFrame.sensors.CalSourcePidCurrent;
			(*devattr)[14] >> spf2DataFrame.sensors.LnaPidCurrent;
			(*devattr)[15] >> spf2DataFrame.sensors.cryostatPressure;
			(*devattr)[16] >> spf2DataFrame.sensors.manifoldPressure;
			(*devattr)[17] >> spf2DataFrame.sensors.lnaTemp;
			(*devattr)[18] >> spf2DataFrame.sensors.omtTemp;
			(*devattr)[19] >> spf2DataFrame.sensors.coldheadTemp;
			(*devattr)[20] >> spf2DataFrame.sensors.cryostatBodyTemp;
			(*devattr)[21] >> spf2DataFrame.sensors.analogueInterfaceTemp;

			(*devattr)[22] >> spf2DataFrame.currentMode;
			(*devattr)[23] >> spf2DataFrame.isValveOpen;
			(*devattr)[24] >> spf2DataFrame.expectedOnline;
			(*devattr)[25] >> spf2DataFrame.deviceStatus;

			(*devattr)[26] >> spf2DataFrame.config.lnaHMeanGain;
			(*devattr)[27] >> spf2DataFrame.config.lnaVMeanGain;

			(*devattr)[28] >> spf2DataFrame.controlFlags.Word;

			(*devattr)[29] >> spf2DataFrame.pidLnaTempSetPoint;
			(*devattr)[30] >> spf2DataFrame.pidCalSourceTempSetPoint;

			(*devattr)[31] >> spf2DataFrame.cryoMotorState;
			(*devattr)[32] >> spf2DataFrame.cryoMotorSpeed;

			(*devattr)[33] >> spf2DataFrame.capabilityState;
			(*devattr)[34] >> spf2DataFrame.timeToFullCapability;

			(*devattr)[35] >> spf2DataFrame.elapsedCurrentOperational;
			(*devattr)[36] >> spf2DataFrame.elapsedOperational;
			(*devattr)[37] >> spf2DataFrame.elapsedMotor;
			(*devattr)[38] >> spf2DataFrame.elapsedValve;

			(*devattr)[39] >> spf2DataFrame.pidLnaTempSetPointDefault;
			(*devattr)[40] >> spf2DataFrame.pidCalSourceTempSetPointDefault;
			(*devattr)[41] >> spf2DataFrame.startupDefault;

			(*devattr)[42] >> spf2DataFrame.config.serial;
			(*devattr)[43] >> spf2DataFrame.config.serialLnaH;
			(*devattr)[44] >> spf2DataFrame.config.serialLnaV;
			(*devattr)[45] >> spf2DataFrame.config.swVersion;
			(*devattr)[46] >> spf2DataFrame.config.fwVersion;

			(*devattr)[47]  >> spf2DataFrame.sensors.psuMotorPosSupplyCurrent;
			(*devattr)[48]  >> spf2DataFrame.sensors.psuMotorNegSupplyCurrent;
			(*devattr)[49]  >> spf2DataFrame.sensors.psuMotorDriverTemp;

//			(*devattr)[50]  >> spf2DataFrame.stateMachineState;
			(*devattr)[50] >> spf2DataFrame.b2CoolDownCounter;
			(*devattr)[51] >> spf2DataFrame.sensors.spdHealth;
			(*devattr)[52] >> spf2DataFrame.interfaceType;
//			(*devattr)[53] >> spf2DataFrame.errorCode;
			(*devattr)[53] >> argout;
				spf2DataFrame.errorCode[0] = (*argout)[0];
				spf2DataFrame.errorCode[1] = (*argout)[1];
				spf2DataFrame.errorCode[2] = (*argout)[2];
			/* Cleanup */
			delete argout;
			delete devattr;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
//			set_b2ExpectedOnline = false;
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return spf2DataFrame;
}

enDeviceStatus Spf2Proxy::GetHealthState()
{
	enDeviceStatus retVal = enDeviceStatus::UNKNOWN;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute att_reply;
			att_reply = spf2Device->read_attribute("b2HealthState");
			att_reply >> retVal;
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::Control(enSetFeedMode setFeedMode)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			string command;

			switch (setFeedMode)
			{
				case enSetFeedMode::STANDBY_LP:
					command = "SetStandby";
					break;
				case enSetFeedMode::OPERATE:
					command = "SetOperate";
					break;
				case enSetFeedMode::MAINTENANCE:
					command = "SetMaintenance";
					break;
				default:
					break;
			}

			spf2Device->command_inout(command); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;

			/*
			if (!cmdResponse.is_empty())
			{
				cout << "Data from device command: \n" << cmdResponse << endl;
			}
			else
			{
				cout << "No data from device command" << endl;
			}
			*/

		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::Regenerate()
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			spf2Device->command_inout("Regenerate"); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::ClearErrors()
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			spf2Device->command_inout("ClearErrorsAndReset"); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::SetVacValveState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2VaValveState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::SetLnaHIllumination(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2LnaHIlluminationEnabled", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetLnaVIllumination(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2LnaVIlluminationEnabled", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

/* Set both LNA-H and LNA-V channels */
bool Spf2Proxy::SetLnaIllumination(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("lnaIlluminationEnabled", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::SetLnaHPowerState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2LnaHPowerState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}
	return retVal;
}
bool Spf2Proxy::SetLnaVPowerState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2LnaVPowerState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetAmp2HPowerState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2Amp2HPowerState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetAmp2VPowerState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2Amp2VPowerState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2CalSourcePowerState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::SetRfe1TempSetPoint(enLnaPidSetPoint sp)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 14;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 16;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 18;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 20;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 22;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 24;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b2LnaPidTempSetPoint", tempValue);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetCalSourceTempSetPoint(enCalSourcePidSetPoint sp)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enCalSourcePidSetPoint::OFF:
				tempValue = 0;
				break;
			case enCalSourcePidSetPoint::SP1:
				tempValue = 280;
				break;
			case enCalSourcePidSetPoint::SP2:
				tempValue = 285;
				break;
			case enCalSourcePidSetPoint::SP3:
				tempValue = 290;
				break;
			case enCalSourcePidSetPoint::SP4:
				tempValue = 296;
				break;
			case enCalSourcePidSetPoint::SP5:
				tempValue = 300;
				break;
			case enCalSourcePidSetPoint::SP6:
				tempValue = 305;
				break;
			case enCalSourcePidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b2CalSourcePidTempSetPoint", tempValue);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}
	return retVal;
}

bool Spf2Proxy::SetRfe1TempSetPointDefault(enLnaPidSetPoint sp)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 14;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 16;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 18;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 20;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 22;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 24;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b2DefaultLnaPidTempSetPoint", tempValue);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetCalSourceTempSetPointDefault(enCalSourcePidSetPoint sp)
{
	bool retVal = false;

	try
	{
		// TODO: These should all be verified
		short tempValue = 0;
		switch(sp)
		{
		case enCalSourcePidSetPoint::OFF:
			tempValue = 0;
			break;
		case enCalSourcePidSetPoint::SP1:
			tempValue = 280;
			break;
		case enCalSourcePidSetPoint::SP2:
			tempValue = 285;
			break;
		case enCalSourcePidSetPoint::SP3:
			tempValue = 290;
			break;
		case enCalSourcePidSetPoint::SP4:
			tempValue = 296;
			break;
		case enCalSourcePidSetPoint::SP5:
			tempValue = 300;
			break;
		case enCalSourcePidSetPoint::SP6:
			tempValue = 305;
			break;
		case enCalSourcePidSetPoint::CUSTOM:
			return false;
			break;
		default:
			return false;
			break;
		}

		DeviceAttribute value("b2DefaultCalSourcePidTempSetPoint", tempValue);
		spf2Device->write_attribute(value);

		retVal = true;
	}
	catch (ConnectionFailed &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (CommunicationFailed &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (DeviceUnlocked &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (DevFailed &e)
	{
		retVal = false;
		Except::print_exception(e);
	}
	catch (exception &e)
	{
		retVal = false;
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enLnaPidSetPoint Spf2Proxy::Convertb2LnaTempSetPointEnum(short setpoint)
{
	enLnaPidSetPoint retVal = enLnaPidSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enLnaPidSetPoint::OFF;
			break;
		case 14:
			retVal = enLnaPidSetPoint::SP1;
			break;
		case 16:
			retVal = enLnaPidSetPoint::SP2;
			break;
		case 18:
			retVal = enLnaPidSetPoint::SP3;
			break;
		case 20:
			retVal = enLnaPidSetPoint::SP4;
			break;
		case 22:
			retVal = enLnaPidSetPoint::SP5;
			break;
		case 24:
			retVal = enLnaPidSetPoint::SP6;
			break;
		default:
			retVal = enLnaPidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enCalSourcePidSetPoint Spf2Proxy::Convertb2CalSourceTempSetPointEnum(short setpoint)
{
	enCalSourcePidSetPoint retVal = enCalSourcePidSetPoint::OFF;

	switch (setpoint)
	{
		case 0:
			retVal = enCalSourcePidSetPoint::OFF;
			break;
		case 280:
			retVal = enCalSourcePidSetPoint::SP1;
			break;
		case 285:
			retVal = enCalSourcePidSetPoint::SP2;
			break;
		case 290:
			retVal = enCalSourcePidSetPoint::SP3;
			break;
		case 296:
			retVal = enCalSourcePidSetPoint::SP4;
			break;
		case 300:
			retVal = enCalSourcePidSetPoint::SP5;
			break;
		case 305:
			retVal = enCalSourcePidSetPoint::SP6;
			break;
		default:
			retVal = enCalSourcePidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

bool Spf2Proxy::SetRfe1TempCtrlState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2LnaPidPowerState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetCalSourceTempCtrlState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2CalSourcePidPowerState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::SetCryoMotorState(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2CryoMotorState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf2Proxy::SetCryoMotorSpeed(short rpm)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			DeviceAttribute value("b2CryoMotorSpeed", rpm);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}
	return retVal;
}



bool Spf2Proxy::SetExpectedOnline(bool state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			string command;

			DeviceAttribute value("b2ExpectedOnline", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}

		return retVal;
	}
}

bool Spf2Proxy::SetStartupDefault(b2DefaultStartStateEnum state)
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			string command;

			DeviceAttribute value("b2DefaultStartState", state);
			spf2Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf2Proxy::Test()
{
	bool retVal = false;

	if (spf2Device)
	{
		try
		{
			spf2Device->command_inout("SetMaintenance");
			retVal = true;
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
	}

	return retVal;
}



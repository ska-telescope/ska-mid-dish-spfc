/*
 * spfvac_proxy.cpp
 *
 *  Created on: 28 Mar 2017
 *      Author: theuns
 */

#include "spfvac_proxy.h"

#include <tango.h>
using namespace Tango;

namespace SpfVac
{
	SpfVacProxy::SpfVacProxy(string name)
	{
		deviceName = name;
		spfVacDevice = nullptr;
	}

	SpfVacProxy::~SpfVacProxy()
	{
		delete spfVacDevice;
		spfVacDevice = nullptr;
	}

	bool SpfVacProxy::Init()
	{
		bool retVal = false;

		try
		{
			// create a connection to a TANGO device
			spfVacDevice = new DeviceProxy(deviceName);

			if (spfVacDevice)
			{
				// Ping the device
				spfVacDevice->ping();

				// Read a device attribute (string data type)
				string spr;
				DeviceAttribute att_reply;
				att_reply = spfVacDevice->read_attribute("controllerState");
				att_reply >> spr;
				cout << "Controller State: " << spr << endl;

				retVal = true;
			}
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}

		return retVal;
	}

	bool SpfVacProxy::IsOnline()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				// Ping the device
				int pingTime = spfVacDevice->ping();

				retVal = (pingTime < 20000); /* FIXME: Removed during integration tests because of high ARM CPU load and high ping times. */

//				cout << "SPFVac device ping took " << pingTime << " microseconds" << endl;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::IsAvailable()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				enDeviceMode operatingState = enDeviceMode::OFF;
				enDeviceStatus healthState = enDeviceStatus::UNKNOWN;
				bool expectedOnline = false;

				vector<DeviceAttribute> *devattr;
				vector<string> attr_names;

				/* Setup attributes to get */                     // Index
				attr_names.push_back("expectedOnline");           //  0
				attr_names.push_back("operatingState");           //  1
				attr_names.push_back("healthState");              //  2

				/* Get the attributes */
				devattr = spfVacDevice->read_attributes(attr_names);

				/* Unpack the values */
				(*devattr)[0] >> expectedOnline;
				(*devattr)[1] >> operatingState;
				(*devattr)[2] >> healthState;

				/* Cleanup */
				delete devattr;

				/* If the spfVac is in standby and the health state is normal OR on in operate, it is available */
				if ( ( (operatingState == enDeviceMode::STANDBY_LP) && (healthState == enDeviceStatus::NORMAL) ) || (operatingState == enDeviceMode::OPERATE) || (expectedOnline == false) )
				{
					retVal = true;
				}
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}


	bool SpfVacProxy::IsRunning()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				bool expectedOnline = false;
				bool pumpState = false;

				vector<DeviceAttribute> *devattr;
				vector<string> attr_names;

				/* Setup attributes to get */                     // Index
				attr_names.push_back("expectedOnline");           //  0
				attr_names.push_back("pumpState");          	  //  1

				/* Get the attributes */
				devattr = spfVacDevice->read_attributes(attr_names);

				/* Unpack the values */
				(*devattr)[0] >> expectedOnline;
				(*devattr)[1] >> pumpState;

				retVal = ((expectedOnline == true) ? pumpState : true);
				/*if (operatingState == enDeviceMode::OFF)
				{
					retVal = true;
				}
				else
				{
					retVal = compressorState;
				}*/
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::Request()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				spfVacDevice->command_inout("RequestVacuum");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::Cancel()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				spfVacDevice->command_inout("CancelVacuum");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::SetMaintenance()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				spfVacDevice->command_inout("SetMaintenance");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::SetStandby()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				spfVacDevice->command_inout("SetStandby");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::Reset()
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				spfVacDevice->command_inout("Reset");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	stVacuumPumpDataFrame SpfVacProxy::GetDataFrame()
	{
		stVacuumPumpDataFrame spfVacDataFrame = {};

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				vector<DeviceAttribute> *devattr;
				vector<string> attr_names;

				/* Setup attributes to get */                   // Index
				attr_names.push_back("operatingState");         //  0
				attr_names.push_back("healthState");           	//  1
				attr_names.push_back("hwStatus");              	//  2
				attr_names.push_back("controllerState");    	//  3
				attr_names.push_back("pumpState");    			//  4
				attr_names.push_back("expectedOnline");         //  5
				attr_names.push_back("serialNumber");        	//  6
				attr_names.push_back("swVersion");              //  7
				attr_names.push_back("currentOperationalTime");	//  8
				attr_names.push_back("totalOperationalTime");   //  9
				attr_names.push_back("purgeValveState");        // 10
				attr_names.push_back("requestStatus");	        // 11
				attr_names.push_back("requestedTimeLeft");	        // 12
//				attr_names.push_back("controllerState");	         // 13

				/* Get the attributes */
				devattr = spfVacDevice->read_attributes(attr_names);

				/* Unpack the values */
				(*devattr)[0] >> spfVacDataFrame.operatingState;
				(*devattr)[1] >> spfVacDataFrame.healthState;
				(*devattr)[2] >> spfVacDataFrame.hwStatus;
				(*devattr)[3] >> spfVacDataFrame.controllerState;
				(*devattr)[4] >> spfVacDataFrame.pumpState;
				(*devattr)[5] >> spfVacDataFrame.expectedOnline;
				(*devattr)[6] >> spfVacDataFrame.serialNumber;
				(*devattr)[7] >> spfVacDataFrame.swVersion;
				(*devattr)[8] >> spfVacDataFrame.currentOperationalTime;
				(*devattr)[9] >> spfVacDataFrame.totalOperationalTime;
				(*devattr)[10] >> spfVacDataFrame.purgeValveState;
				(*devattr)[11] >> spfVacDataFrame.requestStatus;
				(*devattr)[12] >> spfVacDataFrame.requestedTimeLeft;
//				(*devattr)[13] >> spfVacDataFrame.stateMachineState;

				/* Cleanup */
				delete devattr;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return spfVacDataFrame;
	}

	bool SpfVacProxy::SetPumpPowerState(bool state)
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				string command;

				DeviceAttribute value("pumpState", state);
				spfVacDevice->write_attribute(value);

				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DeviceUnlocked &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::SetPurgeValveState(bool state)
		{
			bool retVal = false;

			if (spfVacDevice) // Check that device is not null
			{
				try
				{
					string command;

					DeviceAttribute value("purgeValveState", state);
					spfVacDevice->write_attribute(value);

					retVal = true;
				}
				catch (ConnectionFailed &e)
				{
					retVal = false;
					Except::print_exception(e);
				}
				catch (CommunicationFailed &e)
				{
					retVal = false;
					Except::print_exception(e);
				}
				catch (DeviceUnlocked &e)
				{
					retVal = false;
					Except::print_exception(e);
				}
				catch (DevFailed &e)
				{
					retVal = false;
					Except::print_exception(e);
				}
				catch (exception &e)
				{
					retVal = false;
					cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
							<< "Description: " << e.what() << endl;
				}
			}

			return retVal;
		}

	bool SpfVacProxy::SetExpectedOnline(bool state)
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				string command;

				DeviceAttribute value("expectedOnline", state);
				spfVacDevice->write_attribute(value);

				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DeviceUnlocked &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfVacProxy::SetState(enDeviceMode vacState)
	{
		bool retVal = false;

		if (spfVacDevice) // Check that device is not null
		{
			try
			{
				string command;

				switch (vacState)
				{
				case enDeviceMode::MAINTENANCE:
					command = "SetMaintenance";
					break;
				case enDeviceMode::STANDBY_LP:
					command = "SetStandby";
					break;
				default:
					break;
				}

				spfVacDevice->command_inout(command);

			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}
}


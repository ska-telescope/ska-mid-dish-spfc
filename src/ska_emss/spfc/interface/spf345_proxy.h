/*
 * spf345_proxy.h
 *
 *  Created on: 03 Aug 2022
 *      Author: cvn
 */

#ifndef INTERFACE_SPF345_PROXY_H_
#define INTERFACE_SPF345_PROXY_H_

#include <tango.h>
using namespace Tango;

#include <string>
using namespace std;

#include "spf345_if.h"
using namespace Spf345Interface;


class Spf345Proxy
{
public:
	Spf345Proxy(string name);
	~Spf345Proxy();

	bool Init();
	bool Reset();
	bool Restart();
	bool Shutdown();

	/* Raw commands */
	Tango::DevString SendRawCommand(string commandString);

	/* Monitoring */
	bool IsOnline();
	Spf345Interface::enDeviceStatus GetHealthState();
	Spf345Interface::stFeedDataFrame GetDataFrame();

	/* Control */
	bool Control(Spf345Interface::enSetFeedMode feedCommand);
	bool Regenerate();
	bool ClearErrors();

	/* Write attributes */
	bool SetVaValveState(bool state);

	bool SetLnaIllumination(bool state);
	bool SetLnaHIllumination(bool state);
	bool SetLnaVIllumination(bool state);
	bool SetB3LnaPowerState(bool state);
	bool SetB4LnaPowerState(bool state);
	bool SetB5aLnaPowerState(bool state);
	bool SetB5bLnaPowerState(bool state);
	bool SetB6LnaPowerState(bool state);
	bool SetCalSourcePowerState(bool state);
	bool SetActiveBand(Spf345Interface::enActiveBand band);

	bool SetLnaTempSetPoint(Spf345Interface::enLnaPidSetPoint sp, Spf345Interface::enBandSelect band);
	bool SetLnaTempSetPointDefault(Spf345Interface::enLnaPidSetPoint sp, Spf345Interface::enBandSelect band);
	bool SetWarmPlateTempSetPoint(Spf345Interface::enWarmPlateSetPoint sp, Spf345Interface::enBandSelect band);
	bool SetWarmPlateTempSetPointDefault(Spf345Interface::enWarmPlateSetPoint sp, Spf345Interface::enBandSelect band);
	bool SetCalSourceTempSetPoint(Spf345Interface::enCalSourcePidSetPoint sp);
	bool SetCalSourceTempSetPointDefault(Spf345Interface::enCalSourcePidSetPoint sp);


	bool SetB3PidPowerState(bool state);
	bool SetB4PidPowerState(bool state);
	bool SetB5aPidPowerState(bool state);
	bool SetB5bPidPowerState(bool state);
	bool SetB6PidPowerState(bool state);
	bool SetCalSourceTempCtrlState(bool state);


	bool SetCryoMotorState(bool state);
	bool SetCryoMotorSpeed(short speedEnum); // Short is given, but it represents and enum value 0=OFF, 1=HIGH, 2=LOW
	bool SetNegHeaterPowerState(bool state);
	bool SetVaTurboState(bool state);

	bool SetExpectedOnline(bool state);
	bool SetStartupDefault(Spf345Interface::b345DefaultStartStateEnum state);

	Spf345Interface::enLnaPidSetPoint Convertb345LnaTempSetPointEnum(short setpoint);
	Spf345Interface::enLnaPidSetPoint Convertb345DefaultLnaTempSetPointEnum(short setpoint);
	Spf345Interface::enWarmPlateSetPoint Convertb345WarmPlateTempSetPointEnum(short setpoint);
	Spf345Interface::enWarmPlateSetPoint Convertb345DefaultWarmPlateTempSetPointEnum(short setpoint);
	Spf345Interface::enCalSourcePidSetPoint Convertb345CalSourceTempSetPointEnum(short setpoint);
	Spf345Interface::enCalSourcePidSetPoint Convertb345DefaultCalSourceTempSetPointEnum(short setpoint);

	bool Test();

private:
	DeviceProxy *spf345Device;
	string deviceName;
};

#endif /* INTERFACE_SPF345_PROXY_H_ */

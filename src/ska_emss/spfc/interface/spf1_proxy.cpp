/*
 * spf1_proxy.cpp
 *
 *  Created on: 18 August 2017
 *      Author: theuns
 */

#include "spf1_proxy.h"
#include "spf1_if.h"
using namespace Spf1Interface;

#include <tango.h>
using namespace Tango;

//extern bool set_b1ExpectedOnline;

Spf1Proxy::Spf1Proxy(string name)
{
	spf1Device = nullptr;
	deviceName = name;
}

Spf1Proxy::~Spf1Proxy()
{
	delete spf1Device;
	spf1Device = nullptr;
}

bool Spf1Proxy::Init()
{
	bool retVal = false;

	try
	{
		// create a connection to a TANGO device
		spf1Device = new DeviceProxy(deviceName);

		if (spf1Device)
		{
			// Ping the device
			if (spf1Device->ping())
			{
				// Read a device attribute (string data type)
				DeviceAttribute att_reply;
				att_reply = spf1Device->read_attribute("b1HealthState");
				retVal = true;
			}
		}
	}
	catch (ConnectionFailed &e)
	{
		Except::print_exception(e);
	}
	catch (CommunicationFailed &e)
	{
		Except::print_exception(e);
	}
	catch (DevFailed &e)
	{
		Except::print_exception(e);
	}
	catch (exception &e)
	{
		cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
				<< "Description: " << e.what() << endl;
	}

	return retVal;
}

bool Spf1Proxy::Reset()
{
	bool retVal = false;

	if (spf1Device) // Check that device is not null
	{
		try
		{
			spf1Device->command_inout("Reset");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::Restart()
{
	bool retVal = false;

	if (spf1Device) // Check that device is not null
	{
		try
		{
			spf1Device->command_inout("Restart");
			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

/* Device considered to be online if the Tango Device server has comms and the SM reports hardware detected. */
bool Spf1Proxy::IsOnline()
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			// Ping the device
			int pingTime = spf1Device->ping();

			if (pingTime < 3000) /* FIXME: Removed during integration tests because of high ARM CPU load and high ping times.*/
			{
				DeviceAttribute att_reply = spf1Device->read_attribute("b1HardwareDetected");
				//att_reply >> retVal;

				if (! att_reply.is_empty())
				{
					att_reply >> retVal;
				}
				else
				{
					cout <<  "no hardware status defined!" << endl;
				}
			}
		}
		catch (ConnectionFailed &e)
		{
//			Except::print_exception(e);
			cerr << "ConnectionFailed" << endl;
		}
		catch (CommunicationFailed &e)
		{
//			Except::print_exception(e);
			cerr << "CommunicationFailed" << endl;
		}
		catch (DevFailed &e)
		{
//			Except::print_exception(e);
			cerr << "DevFailed" << endl;
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

Tango::DevString Spf1Proxy::SendRawCommand(string commandString)
{
	Tango::DevString retVal;

	if (spf1Device)
	{
		try
		{
			DeviceData din, dout;

			din << commandString;

			dout = spf1Device->command_inout("SendRawCommand", din); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			if (!dout.is_empty())
			{
				string s;
				dout >> s;
				retVal = new char[s.size() + 1];
				strcpy(retVal, s.c_str());
			}
			else
			{
				retVal = Tango::string_dup("No data received from feed.");
			}
		}
		catch (ConnectionFailed &e)
		{
			retVal = Tango::string_dup("Exception: Connection failed.");
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = Tango::string_dup("Exception: Communication failed.");
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = Tango::string_dup("Exception: Device unlocked.");
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = Tango::string_dup("Exception: Device failed.");
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = Tango::string_dup("Exception: Unhandled.");
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}


/* TODO: Think about changing this to call a pipe function with the dataframe serialised and deserialised */
stFeedDataFrame Spf1Proxy::GetDataFrame()
{
	stFeedDataFrame spf1DataFrame = {};

	if (spf1Device)
	{
		try
		{
			vector<DeviceAttribute> *devattr;
			vector<string> attr_names;

			/* Setup attributes to get */
//			attr_names.push_back("controllerVolt");					//  0
//			attr_names.push_back("controllerCurrent");				//  1
//			attr_names.push_back("controllerTemp");					//  2
//			attr_names.push_back("psuAnalogue6vRectifiedVolt");		//  3
//			attr_names.push_back("psuAnalogue6vRegulatorTemp");		//  4
//			attr_names.push_back("psuAnalogue15vRegulatorTemp");	//  5
//			attr_names.push_back("psuDigital6vRegulatorTemp");		//  6
			attr_names.push_back("b1LnaHDrainVoltage");				//  0
			attr_names.push_back("b1LnaHDrainCurrent");				//  1
			attr_names.push_back("b1LnaHGateVoltage");				//  2
			attr_names.push_back("b1LnaVDrainVoltage");				//  3
			attr_names.push_back("b1LnaVDrainCurrent");				//  4
			attr_names.push_back("b1LnaVGateVoltage");				//  5
			attr_names.push_back("b1Amp2HVoltage");					//  6
			attr_names.push_back("b1Amp2HCurrent");					//  7
			attr_names.push_back("b1Amp2VVoltage");					//  8
			attr_names.push_back("b1Amp2VCurrent");					//  9
			attr_names.push_back("b1CalSourceVoltage");				// 10
			attr_names.push_back("b1CalSourceCurrent");				// 11
			attr_names.push_back("b1CalSourceTemp");				// 12
			attr_names.push_back("b1CalSourcePidCurrent");			// 13
			attr_names.push_back("b1LnaPidCurrent");				// 14
			attr_names.push_back("b1LnaVPidCurrent");					// 15
			attr_names.push_back("b1BodyTemp");				// 16
			attr_names.push_back("b1AnalogueInterfaceTemp");			// 17
			attr_names.push_back("b1LnaHTemp");						// 18
			attr_names.push_back("b1LnaVTemp");						// 19
			attr_names.push_back("b1SpdHealth");					// 20

			attr_names.push_back("b1OperatingState");					// 21
			attr_names.push_back("b1ExpectedOnline");					// 22
			attr_names.push_back("b1HealthState");					// 23

			attr_names.push_back("b1LnaHMeanGain");					// 24
			attr_names.push_back("b1LnaVMeanGain");					// 25
			attr_names.push_back("b1ControlRegister"); // all the discretes //26
			attr_names.push_back("b1LnaHPidTempSetPoint");			// 27
			attr_names.push_back("b1LnaVPidTempSetPoint");			// 28
			attr_names.push_back("b1CalSourcePidTempSetPoint");		// 29

			attr_names.push_back("b1CapabilityState");				// 30
			attr_names.push_back("b1CapTime2OpFull");					// 31

			attr_names.push_back("b1OpTime");			// 32
			attr_names.push_back("b1TotOpTime");			// 33

			attr_names.push_back("b1DefaultLnaHPidTempSetPoint");		// 34
			attr_names.push_back("b1DefaultLnaVPidTempSetPoint");		// 35
			attr_names.push_back("b1DefaultCalSourcePidTempSetPoint");// 36
			attr_names.push_back("b1DefaultStartState");					// 37

			attr_names.push_back("b1SerialNumber");					// 38
			attr_names.push_back("b1LnaHSerialNumber");				// 39
			attr_names.push_back("b1LnaVSerialNumber");				// 40
			attr_names.push_back("b1SwVersion");						// 41
			attr_names.push_back("b1FwVersion");						// 42
			attr_names.push_back("b1InterfaceType");				// 43
			attr_names.push_back("b1ErrorCode");				// 44

			/* Get the attributes */
			devattr = spf1Device->read_attributes(attr_names);
			Tango::DevVarStringArray *argout = new Tango::DevVarStringArray();

			/* Unpack the values */
//			(*devattr)[0]  >> spf1DataFrame.sensors.controllerVoltage;
//			(*devattr)[1]  >> spf1DataFrame.sensors.controllerCurrent;
//			(*devattr)[2]  >> spf1DataFrame.sensors.controllerTemperature;
//			(*devattr)[3]  >> spf1DataFrame.sensors.psu9VRectifiedVoltage;
//			(*devattr)[4]  >> spf1DataFrame.sensors.psuAnalogue6vRegulatorTemp;
//			(*devattr)[5]  >> spf1DataFrame.sensors.psuAnalogue15vRegulatorTemp;
//			(*devattr)[6]  >> spf1DataFrame.sensors.psuDigital6vRegulatorTemp;
			(*devattr)[0]  >> spf1DataFrame.sensors.lnaHDrainVoltage;
			(*devattr)[1]  >> spf1DataFrame.sensors.lnaHDrainCurrent;
			(*devattr)[2]  >> spf1DataFrame.sensors.lnaHGateVoltage;
			(*devattr)[3] >> spf1DataFrame.sensors.lnaVDrainVoltage;
			(*devattr)[4] >> spf1DataFrame.sensors.lnaVDrainCurrent;
			(*devattr)[5] >> spf1DataFrame.sensors.lnaVGateVoltage;
			(*devattr)[6] >> spf1DataFrame.sensors.amp2HVoltage;
			(*devattr)[7] >> spf1DataFrame.sensors.amp2HCurrent;
			(*devattr)[8] >> spf1DataFrame.sensors.amp2VVoltage;
			(*devattr)[9] >> spf1DataFrame.sensors.amp2VCurrent;
			(*devattr)[10] >> spf1DataFrame.sensors.calsourceVoltage;
			(*devattr)[11] >> spf1DataFrame.sensors.calsourceCurrent;
			(*devattr)[12] >> spf1DataFrame.sensors.calsourceTemp;
			(*devattr)[13] >> spf1DataFrame.sensors.pidCalsourceCurrent;
			(*devattr)[14] >> spf1DataFrame.sensors.pidLnaHCurrent;
			(*devattr)[15] >> spf1DataFrame.sensors.pidLnaVCurrent;
			(*devattr)[16] >> spf1DataFrame.sensors.chamberBodyTemp;
			(*devattr)[17] >> spf1DataFrame.sensors.analogueInterfaceTemp;
			(*devattr)[18] >> spf1DataFrame.sensors.lnaHTemp;
			(*devattr)[19] >> spf1DataFrame.sensors.lnaVTemp;
			(*devattr)[20] >> spf1DataFrame.sensors.spdHealth;

			(*devattr)[21] >> spf1DataFrame.currentMode;
			(*devattr)[22] >> spf1DataFrame.expectedOnline;
			(*devattr)[23] >> spf1DataFrame.deviceStatus;

			(*devattr)[24] >> spf1DataFrame.config.lnaHMeanGain;
			(*devattr)[25] >> spf1DataFrame.config.lnaVMeanGain;

			(*devattr)[26] >> spf1DataFrame.controlFlags.Word;

			(*devattr)[27] >> spf1DataFrame.pidLnaHTempSetPoint;
			(*devattr)[28] >> spf1DataFrame.pidLnaVTempSetPoint;
			(*devattr)[29] >> spf1DataFrame.pidCalSourceTempSetPoint;

			(*devattr)[30] >> spf1DataFrame.capabilityState;
			(*devattr)[31] >> spf1DataFrame.timeToFullCapability;

			(*devattr)[32] >> spf1DataFrame.elapsedCurrentOperational;
			(*devattr)[33] >> spf1DataFrame.elapsedOperational;

			(*devattr)[34] >> spf1DataFrame.pidLnaHTempSetPointDefault;
			(*devattr)[35] >> spf1DataFrame.pidLnaVTempSetPointDefault;
			(*devattr)[36] >> spf1DataFrame.pidCalSourceTempSetPointDefault;
			(*devattr)[37] >> spf1DataFrame.startupDefault;

			(*devattr)[38] >> spf1DataFrame.config.serial;
			(*devattr)[39] >> spf1DataFrame.config.serialLnaH;
			(*devattr)[40] >> spf1DataFrame.config.serialLnaV;
			(*devattr)[41] >> spf1DataFrame.config.swVersion;
			(*devattr)[42] >> spf1DataFrame.config.fwVersion;
			(*devattr)[43] >> spf1DataFrame.interfaceType;
			(*devattr)[44] >> argout;
				spf1DataFrame.errorCode[0] = (*argout)[0];
				spf1DataFrame.errorCode[1] = (*argout)[1];
				spf1DataFrame.errorCode[2] = (*argout)[2];

			/* Cleanup */
			delete argout;
			delete devattr;
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
//			set_b1ExpectedOnline = false;
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return spf1DataFrame;
}

enDeviceStatus Spf1Proxy::GetHealthState()
{
	enDeviceStatus retVal = enDeviceStatus::UNKNOWN;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute att_reply;
			att_reply = spf1Device->read_attribute("b1HealthState");
			att_reply >> retVal;
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::Control(enSetFeedMode setFeedMode)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			string command;

			switch (setFeedMode)
			{
				case enSetFeedMode::STANDBY_LP:
					command = "SetStandby";
					break;
				case enSetFeedMode::OPERATE:
					command = "SetOperate";
					break;
				case enSetFeedMode::MAINTENANCE:
					command = "SetMaintenance";
					break;
				default:
					break;
			}

			spf1Device->command_inout(command); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;

			/*
			if (!cmdResponse.is_empty())
			{
				cout << "Data from device command: \n" << cmdResponse << endl;
			}
			else
			{
				cout << "No data from device command" << endl;
			}
			*/

		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::ClearErrors()
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			spf1Device->command_inout("ClearErrorsAndReset"); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::SetLnaHPowerState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1LnaHPowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetLnaVPowerState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1LnaVPowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetAmp2HPowerState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1Amp2HPowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetAmp2VPowerState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1Amp2VPowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetCalSourcePowerState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1CalSourcePowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::SetLnaHTempSetPoint(enLnaPidSetPoint sp)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 283;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 288;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 293;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 298;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 303;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 308;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b1LnaHPidTempSetPoint", tempValue);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetLnaVTempSetPoint(enLnaPidSetPoint sp)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 283;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 288;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 293;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 298;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 303;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 308;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b1LnaVPidTempSetPoint", tempValue);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetCalSourceTempSetPoint(enCalSourcePidSetPoint sp)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enCalSourcePidSetPoint::OFF:
				tempValue = 0;
				break;
			case enCalSourcePidSetPoint::SP1:
				tempValue = 325;
				break;
			case enCalSourcePidSetPoint::SP2:
				tempValue = 330;
				break;
			case enCalSourcePidSetPoint::SP3:
				tempValue = 335;
				break;
			case enCalSourcePidSetPoint::SP4:
				tempValue = 340;
				break;
			case enCalSourcePidSetPoint::SP5:
				tempValue = 345;
				break;
			case enCalSourcePidSetPoint::SP6:
				tempValue = 350;
				break;
			case enCalSourcePidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b1CalSourcePidTempSetPoint", tempValue);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::SetLnaHTempSetPointDefault(enLnaPidSetPoint sp)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 283;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 288;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 293;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 298;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 303;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 308;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b1DefaultLnaHPidTempSetPoint", tempValue);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetLnaVTempSetPointDefault(enLnaPidSetPoint sp)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enLnaPidSetPoint::OFF:
				tempValue = 0;
				break;
			case enLnaPidSetPoint::SP1:
				tempValue = 283;
				break;
			case enLnaPidSetPoint::SP2:
				tempValue = 288;
				break;
			case enLnaPidSetPoint::SP3:
				tempValue = 293;
				break;
			case enLnaPidSetPoint::SP4:
				tempValue = 298;
				break;
			case enLnaPidSetPoint::SP5:
				tempValue = 303;
				break;
			case enLnaPidSetPoint::SP6:
				tempValue = 308;
				break;
			case enLnaPidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b1DefaultLnaVPidTempSetPoint", tempValue);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetCalSourceTempSetPointDefault(enCalSourcePidSetPoint sp)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			// TODO: These should all be verified
			short tempValue = 0;
			switch(sp)
			{
			case enCalSourcePidSetPoint::OFF:
				tempValue = 0;
				break;
			case enCalSourcePidSetPoint::SP1:
				tempValue = 325;
				break;
			case enCalSourcePidSetPoint::SP2:
				tempValue = 330;
				break;
			case enCalSourcePidSetPoint::SP3:
				tempValue = 335;
				break;
			case enCalSourcePidSetPoint::SP4:
				tempValue = 340;
				break;
			case enCalSourcePidSetPoint::SP5:
				tempValue = 345;
				break;
			case enCalSourcePidSetPoint::SP6:
				tempValue = 350;
				break;
			case enCalSourcePidSetPoint::CUSTOM:
				return false;
				break;
			default:
				return false;
				break;
			}

			DeviceAttribute value("b1DefaultCalSourcePidTempSetPoint", tempValue);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enLnaPidSetPoint Spf1Proxy::Convertb1LnaHTempSetPointEnum(short setpoint)
{
	enLnaPidSetPoint retVal = enLnaPidSetPoint::OFF;

	switch (setpoint)
	{
		case 1:
			retVal = enLnaPidSetPoint::OFF;
			break;
		case 283:
			retVal = enLnaPidSetPoint::SP1;
			break;
		case 288:
			retVal = enLnaPidSetPoint::SP2;
			break;
		case 293:
			retVal = enLnaPidSetPoint::SP3;
			break;
		case 298:
			retVal = enLnaPidSetPoint::SP4;
			break;
		case 303:
			retVal = enLnaPidSetPoint::SP5;
			break;
		case 308:
			retVal = enLnaPidSetPoint::SP6;
			break;
		default:
			retVal = enLnaPidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

enLnaPidSetPoint Spf1Proxy::Convertb1LnaVTempSetPointEnum(short setpoint)
{
	enLnaPidSetPoint retVal = enLnaPidSetPoint::OFF;

	switch (setpoint)
	{
		case 1:
			retVal = enLnaPidSetPoint::OFF;
			break;
		case 283:
			retVal = enLnaPidSetPoint::SP1;
			break;
		case 288:
			retVal = enLnaPidSetPoint::SP2;
			break;
		case 293:
			retVal = enLnaPidSetPoint::SP3;
			break;
		case 298:
			retVal = enLnaPidSetPoint::SP4;
			break;
		case 303:
			retVal = enLnaPidSetPoint::SP5;
			break;
		case 308:
			retVal = enLnaPidSetPoint::SP6;
			break;
		default:
			retVal = enLnaPidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

/* We should convert from a setpoint integer value to a enum value */
enCalSourcePidSetPoint Spf1Proxy::Convertb1CalSourceTempSetPointEnum(short setpoint)
{
	enCalSourcePidSetPoint retVal = enCalSourcePidSetPoint::OFF;

	switch (setpoint)
	{
		case 1:
			retVal = enCalSourcePidSetPoint::OFF;
			break;
		case 325:
			retVal = enCalSourcePidSetPoint::SP1;
			break;
		case 330:
			retVal = enCalSourcePidSetPoint::SP2;
			break;
		case 335:
			retVal = enCalSourcePidSetPoint::SP3;
			break;
		case 340:
			retVal = enCalSourcePidSetPoint::SP4;
			break;
		case 345:
			retVal = enCalSourcePidSetPoint::SP5;
			break;
		case 350:
			retVal = enCalSourcePidSetPoint::SP6;
			break;
		default:
			retVal = enCalSourcePidSetPoint::CUSTOM;
			break;
	}

	return retVal;
}

bool Spf1Proxy::SetLnaHTempCtrlState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1LnaHPidPowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}
	return retVal;
}
bool Spf1Proxy::SetLnaVTempCtrlState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1LnaVPidPowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}
bool Spf1Proxy::SetCalSourceTempCtrlState(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			DeviceAttribute value("b1CalSourcePidPowerState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::SetExpectedOnline(bool state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			string command;

			DeviceAttribute value("b1ExpectedOnline", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::SetStartupDefault(b1DefaultStartStateEnum state)
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			string command;

			DeviceAttribute value("b1DefaultStartState", state);
			spf1Device->write_attribute(value);

			retVal = true;
		}
		catch (ConnectionFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DeviceUnlocked &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			retVal = false;
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			retVal = false;
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}
	}

	return retVal;
}

bool Spf1Proxy::Test()
{
	bool retVal = false;

	if (spf1Device)
	{
		try
		{
			spf1Device->command_inout("SetMaintenance");
			retVal = true;
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
	}

	return retVal;
}



/*
 * spfhe_proxy.cpp
 *
 *  Created on: 23 May 2017
 *      Author: theuns
 */

#include "spfhe_proxy.h"

#include <tango.h>
using namespace Tango;

namespace SpfHe
{
	SpfHeProxy::SpfHeProxy(string name)
	{
		deviceName = name;
		spfHeDevice = nullptr;
	}

	SpfHeProxy::~SpfHeProxy()
	{
		delete spfHeDevice;
		spfHeDevice = nullptr;
	}

	bool SpfHeProxy::Init()
	{
		bool retVal = false;

		try
		{
			// create a connection to a TANGO device
			spfHeDevice = new DeviceProxy(deviceName);

			if (spfHeDevice) // Check that device is not null
			{
				// Ping the device
				spfHeDevice->ping();

				// Read a device attribute (string data type)
				string spr;
				DeviceAttribute att_reply;
				att_reply = spfHeDevice->read_attribute("controllerState");
				att_reply >> spr;
				cout << "Controller State: " << spr << endl;

				retVal = true;
			}
		}
		catch (ConnectionFailed &e)
		{
			Except::print_exception(e);
		}
		catch (CommunicationFailed &e)
		{
			Except::print_exception(e);
		}
		catch (DevFailed &e)
		{
			Except::print_exception(e);
		}
		catch (exception &e)
		{
			cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
					<< "Description: " << e.what() << endl;
		}

		return retVal;
	}

	bool SpfHeProxy::IsOnline()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				// create a connection to a TANGO device

				// Ping the device
				int pingTime = spfHeDevice->ping();

				retVal = (pingTime < 20000); /* FIXME: Removed during integration tests because of high ARM CPU load and high ping times. */

//				cout << "SPFHe device ping took " << pingTime << " microseconds" << endl;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::IsAvailable()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				enDeviceMode operatingState = enDeviceMode::OFF;
				enDeviceStatus healthState = enDeviceStatus::UNKNOWN;
				bool expectedOnline = false;

				vector<DeviceAttribute> *devattr;
				vector<string> attr_names;

				/* Setup attributes to get */                     // Index
				attr_names.push_back("expectedOnline");           //  0
				attr_names.push_back("operatingState");           //  1
				attr_names.push_back("healthState");              //  2

				/* Get the attributes */
				devattr = spfHeDevice->read_attributes(attr_names);

				/* Unpack the values */
				(*devattr)[0] >> expectedOnline;
				(*devattr)[1] >> operatingState;
				(*devattr)[2] >> healthState;

				/* Cleanup */
				delete devattr;

				/* If the spfHe is in standby and the health state is normal OR on in operate, it is available */
				if ( ( (operatingState == enDeviceMode::STANDBY_LP) && (healthState == enDeviceStatus::NORMAL) ) || (operatingState == enDeviceMode::OPERATE) || (expectedOnline == false) )
				{
					retVal = true;
				}
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	/* Return true if the heCompressor is running and has comms */
	bool SpfHeProxy::IsRunning()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				bool expectedOnline = false;
				bool compressorState = false;

				vector<DeviceAttribute> *devattr;
				vector<string> attr_names;

				/* Setup attributes to get */                     // Index
				attr_names.push_back("expectedOnline");           //  0
				attr_names.push_back("compressorState");          //  1

				/* Get the attributes */
				devattr = spfHeDevice->read_attributes(attr_names);

				/* Unpack the values */
				(*devattr)[0] >> expectedOnline;
				(*devattr)[1] >> compressorState;

				retVal = ((expectedOnline == true) ? compressorState : true);
				/*if (operatingState == enDeviceMode::OFF)
				{
					retVal = true;
				}
				else
				{
					retVal = compressorState;
				}*/
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::attrPressureOk()
	{
		bool retVal;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				bool expectedOnline = false;
				bool pressureOk = false;


				vector<DeviceAttribute> *devattr;
				vector<string> attr_names;

				/* Setup attributes to get */                 // Index
				attr_names.push_back("expectedOnline");       //  0
				attr_names.push_back("pressureOk");           //  1

				/* Get the attributes */
				devattr = spfHeDevice->read_attributes(attr_names);

				/* Unpack the values */
				(*devattr)[0] >> expectedOnline;
				(*devattr)[1] >> pressureOk;

				retVal = ((expectedOnline == true) ? pressureOk : true);

			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}


	bool SpfHeProxy::SetCompressorPowerState(bool state)
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				string command;

				DeviceAttribute value("compressorState", state);
				spfHeDevice->write_attribute(value);

				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DeviceUnlocked &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::Request()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("RequestHelium");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::Cancel()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("CancelHelium");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::SetMaintenance()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("SetMaintenance");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::SetStandby()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("SetStandby");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::Reset()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("Reset");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::ResetSM()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("ResetSM");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::Restart()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("Restart");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::ClearErrors()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("ClearErrorsAndReset");
				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	stHeliumCompressorDataFrame SpfHeProxy::GetDataFrame()
	{
		stHeliumCompressorDataFrame spfHeDataFrame = {};

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				vector<DeviceAttribute> *devattr;
				vector<string> attr_names;

				/* Setup attributes to get */                        // Index
				attr_names.push_back("controllerVolt");              //  0
				attr_names.push_back("controllerCurrent");           //  1
				attr_names.push_back("controllerTemp");              //  2
				attr_names.push_back("compressorSupplyPressure");    //  3
				attr_names.push_back("compressorReturnPressure");    //  4
				attr_names.push_back("compressorMotorTemp");         //  5
				attr_names.push_back("compressorSupplyTemp");        //  6

				attr_names.push_back("hwStatus");                    //  7
				attr_names.push_back("operatingState");              //  8
				attr_names.push_back("healthState");                 //  9

				attr_names.push_back("serialNumber");                // 10
				attr_names.push_back("fwVersion");                   // 11
				attr_names.push_back("swVersion");                   // 12
				attr_names.push_back("expectedOnline");              // 13

				attr_names.push_back("compressorState");             // 14
				attr_names.push_back("pressureOk");                  // 15
				attr_names.push_back("currentOperationalTime");      // 16
				attr_names.push_back("totalOperationalTime");        // 17
				attr_names.push_back("errorRegister");        		 // 18
				attr_names.push_back("requestStatus");	        	 // 19
//				attr_names.push_back("controllerState");	         // 20

				/* Get the attributes */
				devattr = spfHeDevice->read_attributes(attr_names);

				/* Unpack the values */
				(*devattr)[0] >> spfHeDataFrame.sensors.controllerVoltage;
				(*devattr)[1] >> spfHeDataFrame.sensors.controllerCurrent;
				(*devattr)[2] >> spfHeDataFrame.sensors.controllerTemperature;
				(*devattr)[3] >> spfHeDataFrame.sensors.compressorSupplyPressure;
				(*devattr)[4] >> spfHeDataFrame.sensors.compressorReturnPressure;
				(*devattr)[5] >> spfHeDataFrame.sensors.compressorMotorTemperature;
				(*devattr)[6] >> spfHeDataFrame.sensors.compressorSupplyTemperature;

				(*devattr)[7] >> spfHeDataFrame.compStatus;
				(*devattr)[8] >> spfHeDataFrame.currentMode;
				(*devattr)[9] >> spfHeDataFrame.deviceStatus;

				(*devattr)[10] >> spfHeDataFrame.serial;
				(*devattr)[11] >> spfHeDataFrame.fwVersion;
				(*devattr)[12] >> spfHeDataFrame.swVersion;
				(*devattr)[13] >> spfHeDataFrame.expectedOnline;

				(*devattr)[14] >> spfHeDataFrame.compressorState;
				(*devattr)[15] >> spfHeDataFrame.pressureOk;
				(*devattr)[16] >> spfHeDataFrame.elapsedCurrentOperational;
				(*devattr)[17] >> spfHeDataFrame.elapsedOperational;
				(*devattr)[18] >> spfHeDataFrame.errorFlags.Word;
				(*devattr)[19] >> spfHeDataFrame.requestStatus;
//				(*devattr)[20] >> spfHeDataFrame.stateMachineState;

				/* Cleanup */
				delete devattr;
			}
			catch (ConnectionFailed &e)
			{
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		} else
		{
//			spfHeDataFrame.requestStatus = new Tango::DevVarBooleanArray[4];
//			(*spfHeDataFrame.requestStatus)[0] = false;
//			(*spfHeDataFrame.requestStatus)[1] = false;
//			(*spfHeDataFrame.requestStatus)[2] = false;
//			(*spfHeDataFrame.requestStatus)[3] = false;
		}


		return spfHeDataFrame;
	}

	bool SpfHeProxy::SetExpectedOnline(bool state)
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				string command;

				DeviceAttribute value("expectedOnline", state);
				spfHeDevice->write_attribute(value);

				retVal = true;
			}
			catch (ConnectionFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DeviceUnlocked &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				retVal = false;
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = false;
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	Tango::DevString SpfHeProxy::SendRawCommand(string commandString)
	{
		Tango::DevString retVal;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				DeviceData din, dout;

				din << commandString;

				dout = spfHeDevice->command_inout("SendRawCommand", din); // Exception: ConnectionFailed, CommunicationFailed, DeviceUnlocked, DevFailed from device

				if (!dout.is_empty())
				{
					string s;
					dout >> s;
					retVal = new char[s.size() + 1];
					strcpy(retVal, s.c_str());
				}
				else
				{
					retVal = Tango::string_dup("No data received from HCC.");
				}
			}
			catch (ConnectionFailed &e)
			{
				retVal = Tango::string_dup("Exception: Connection failed.");
				Except::print_exception(e);
			}
			catch (CommunicationFailed &e)
			{
				retVal = Tango::string_dup("Exception: Communication failed.");
				Except::print_exception(e);
			}
			catch (DeviceUnlocked &e)
			{
				retVal = Tango::string_dup("Exception: Device unlocked.");
				Except::print_exception(e);
			}
			catch (DevFailed &e)
			{
				retVal = Tango::string_dup("Exception: Device failed.");
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				retVal = Tango::string_dup("Exception: Unhandled.");
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}

	bool SpfHeProxy::Test()
	{
		bool retVal = false;

		if (spfHeDevice) // Check that device is not null
		{
			try
			{
				spfHeDevice->command_inout("SetMaintenance");
				retVal = true;
			}
			catch (DevFailed &e)
			{
				Except::print_exception(e);
			}
			catch (exception &e)
			{
				cerr << "Unhandled exception in File: " << __FILE__ << "Function: " << __PRETTY_FUNCTION__ << "on Line: " <<__LINE__ << "\n"
						<< "Description: " << e.what() << endl;
			}
		}

		return retVal;
	}
}


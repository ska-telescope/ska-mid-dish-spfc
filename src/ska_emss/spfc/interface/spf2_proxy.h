/*
 * spf2_proxy.h
 *
 *  Created on: 27 May 2017
 *      Author: theuns
 */

#ifndef INTERFACE_SPF2_PROXY_H_
#define INTERFACE_SPF2_PROXY_H_

#include <tango.h>
using namespace Tango;

#include <string>
using namespace std;

#include "spf2_if.h"
using namespace Spf2Interface;


class Spf2Proxy
{
public:
	Spf2Proxy(string name);
	~Spf2Proxy();

	bool Init();
	bool Reset();
	bool Restart();
	bool Shutdown();

	/* Raw commands */
	Tango::DevString SendRawCommand(string commandString);

	/* Monitoring */
	bool IsOnline();
	Spf2Interface::enDeviceStatus GetHealthState();
	Spf2Interface::stFeedDataFrame GetDataFrame();

	/* Control */
	bool Control(Spf2Interface::enSetFeedMode feedCommand);
	bool Regenerate();
	bool ClearErrors();

	/* Write attributes */
	bool SetVacValveState(bool state);

	bool SetLnaIllumination(bool state);
	bool SetLnaHIllumination(bool state);
	bool SetLnaVIllumination(bool state);
	bool SetLnaHPowerState(bool state);
	bool SetLnaVPowerState(bool state);
	bool SetAmp2HPowerState(bool state);
	bool SetAmp2VPowerState(bool state);
	bool SetCalSourcePowerState(bool state);

	bool SetRfe1TempSetPoint(Spf2Interface::enLnaPidSetPoint sp);
	bool SetRfe1TempSetPointDefault(Spf2Interface::enLnaPidSetPoint sp);
	bool SetCalSourceTempSetPoint(Spf2Interface::enCalSourcePidSetPoint sp);
	bool SetCalSourceTempSetPointDefault(Spf2Interface::enCalSourcePidSetPoint sp);


	bool SetRfe1TempCtrlState(bool state);
	bool SetCalSourceTempCtrlState(bool state);


	bool SetCryoMotorState(bool state);
	bool SetCryoMotorSpeed(short speedEnum); // Short is given, but it represents and enum value 0=OFF, 1=HIGH, 2=LOW

	bool SetExpectedOnline(bool state);
	bool SetStartupDefault(Spf2Interface::b2DefaultStartStateEnum state);

	Spf2Interface::enLnaPidSetPoint Convertb2LnaTempSetPointEnum(short setpoint);
	Spf2Interface::enCalSourcePidSetPoint Convertb2CalSourceTempSetPointEnum(short setpoint);

	bool Test();

private:
	DeviceProxy *spf2Device;
	string deviceName;
};

#endif /* INTERFACE_SPF2_PROXY_H_ */

/*
 * Digital.cpp
 *
 *  Created on: 14 Dec 2016
 *      Author: theuns
 */

#include <string>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "Digital.h"

using namespace std;

/*Digital::Digital(int gpio)
 {
 this->gpionum = gpio;
 }*/

Digital::Digital(int gnum) :
		valuefd(-1), directionfd(-1), exportfd(-1), unexportfd(-1), gpionum(
				gnum) {
	//Instantiate GPIOClass object for GPIO pin number "gnum"
	this->Export();
}

Digital::~Digital() {
	this->Unexport();
}

int Digital::Export() {
	int statusVal = -1;
	string exportStr = "/sys/class/gpio/export";

	this->exportfd = statusVal = open(exportStr.c_str(), O_WRONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS GPIO export device");
		return statusVal;
	}

	string numStr = to_string(this->gpionum);

	/* Debug */
	//cout << "Export :" << numStr << endl;
	statusVal = write(this->exportfd, numStr.c_str(), numStr.length());
	if (statusVal < 0) {
		perror("could not write to SYSFS GPIO export device");
		return statusVal;
	}

	statusVal = close(this->exportfd);
	if (statusVal < 0) {
		perror("could not close SYSFS GPIO export device");
		return statusVal;
	}

	return statusVal;
}

int Digital::Unexport() {
	int statusVal = -1;
	string unexportStr = "/sys/class/gpio/unexport";
	this->unexportfd = statusVal = open(unexportStr.c_str(), O_WRONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS GPIO unexport device");
		return statusVal;
	}

	string numStr = to_string(this->gpionum);
	statusVal = write(this->unexportfd, numStr.c_str(), numStr.length());
	if (statusVal < 0) {
		perror("could not write to SYSFS GPIO unexport device");
		return statusVal;
	}

	statusVal = close(this->unexportfd);
	if (statusVal < 0) {
		perror("could not close SYSFS GPIO unexport device");
		return statusVal;
	}

	return statusVal;
}

int Digital::SetDir(int dir, bool invert) {
	int statusVal = -1;
	string dirStr;
	string setdirStr = "/sys/class/gpio/gpio" + to_string(this->gpionum)
			+ "/direction";

	this->directionfd = statusVal = open(setdirStr.c_str(), O_WRONLY | O_SYNC); // open direction file for gpio
	if (statusVal < 0) {
		perror("could not open SYSFS GPIO direction device");
		return statusVal;
	}

	if (dir == 0) {
		if (invert) {
			dirStr = "high";
		} else {
			dirStr = "low";
		}
	} else if (dir == 1) {
		dirStr = "in";
	} else {
		perror(
				"Invalid direction value. Should be 1 for \"in\" or 0 for \"out\". \n");
		return statusVal;
	}

	statusVal = write(this->directionfd, dirStr.c_str(), dirStr.length());
	if (statusVal < 0) {
		perror("could not write to SYSFS GPIO direction device");
		return statusVal;
	}

	statusVal = close(this->directionfd);
	if (statusVal < 0) {
		perror("could not close SYSFS GPIO direction device");
		return statusVal;
	}

	return statusVal;
}

// Function returns 0 if successful
int Digital::SetVal(int val) {
	int statusVal = -1;
	string valStr;
	string setValStr = "/sys/class/gpio/gpio" + to_string(this->gpionum)
			+ "/value";

	this->valuefd = statusVal = open(setValStr.c_str(), O_WRONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS GPIO value device");
		return statusVal;
	}

	if (val != 1 && val != 0) {
		fprintf(stderr, "Invalid  value. Should be \"1\" or \"0\". \n");
		return statusVal;
	}

	valStr = to_string(val);

	statusVal = write(this->valuefd, valStr.c_str(), valStr.length());
	if (statusVal < 0) {
		perror("could not write to SYSFS GPIO value device");
		return statusVal;
	}

	statusVal = close(this->valuefd);
	if (statusVal < 0) {
		perror("could not close SYSFS GPIO value device");
		return statusVal;
	}

	return statusVal;
}

int Digital::GetVal(int& val) {
	string tempval;
	string getValStr = "/sys/class/gpio/gpio" + to_string(this->gpionum)
			+ "/value";
	char buff[2];
	int statusVal = -1;
	this->valuefd = statusVal = open(getValStr.c_str(), O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS GPIO value device");
		return statusVal;
	}

	statusVal = read(this->valuefd, &buff, 1);
	if (statusVal < 0) {
		perror("could not read SYSFS GPIO value device");
		return statusVal;
	}

	buff[1] = '\0';
	tempval = string(buff);

	if (tempval.compare("1") != 0 && tempval.compare("0") != 0) {
		perror("Invalid  value read. Should be \"1\" or \"0\". \n");
		return statusVal;
	}

	try {
		val = std::stoi(tempval);

		//string::size_type sz;   // alias of size_t
		//val = stoi(tempval, &sz);
	} catch (std::exception const &exc) {
		// an error occurred, for example when the string was something like "abc" and
		// could thus not be interpreted as a number
	}

	statusVal = close(this->valuefd);
	if (statusVal < 0) {
		perror("could not close SYSFS GPIO value device");
		return statusVal;
	}

	/* Status of 0 indicates success */
	return statusVal;
}

int Digital::GetPinNumber() {
	return this->gpionum;
}

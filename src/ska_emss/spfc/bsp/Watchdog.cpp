/*
 * Watchdog.cpp
 *
 *  Created on: 9 Feb 2017
 *      Author: theuns
 */

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <linux/watchdog.h>
#include <sys/ioctl.h>

#include "Watchdog.h"

using namespace std;

Watchdog::Watchdog(int interval) :
		watchdogfd(-1), _interval(interval) {
}

Watchdog::~Watchdog() {
	this->Disable();
}

int Watchdog::Start() {
	this->Enable();
	this->SetInterval(_interval);
	this->Kick();
	return 1;
}

int Watchdog::GetInfo() {
	watchdog_info wdi;

	int statusVal = -1;
	statusVal = ioctl(this->watchdogfd, WDIOC_GETSUPPORT, &wdi);
	if (statusVal < 0) {
		perror("could not get watchdog support");
		return statusVal;
	}

	cout << "Watchdog Identity: " << wdi.identity << endl;
	cout << "Watchdog Options : " << wdi.options << endl;
	cout << "Watchdog Firmware: " << wdi.firmware_version << endl;

	return statusVal;
}

int Watchdog::Kick() {
	int statusVal = -1;
	statusVal = ioctl(this->watchdogfd, WDIOC_KEEPALIVE, NULL);
	//statusVal = write(this->watchdogfd, "w", 1);
	if (statusVal < 0) {
		perror("could not kick to dog");
		return statusVal;
	}

	return statusVal;
}

/* Once the watchdog device file is open, the watchdog will be activated by
 the driver. Note that the file stays open for the duration of the Watchdog instance */
int Watchdog::Enable() {
	int statusVal = -1;

	this->watchdogfd = statusVal = open("/dev/watchdog", O_RDWR);
	if (statusVal < 0) {
		perror("could not open watchdog device");
		return statusVal;
	}

	return statusVal;
}

/*hand over to Linux system*/
int Watchdog::Disable() {
	int statusVal = -1;

	statusVal = write(this->watchdogfd, "V", 1);
	if (statusVal < 0) {
		perror("could not write to watchdog device");
		return statusVal;
	}

	statusVal = close(this->watchdogfd);
	if (statusVal < 0) {
		perror("could not close watchdog device");
		return statusVal;
	}

	return statusVal;
}

/* Check if last boot is caused by watchdog */
/* if status != 0 -> Watchdog
 * if status == 0 -> Power-On-Reset
 */
int Watchdog::GetLastBoot(int & status) {
	int statusVal = -1;
	statusVal = ioctl(this->watchdogfd, WDIOC_GETBOOTSTATUS, &status);
	if (statusVal < 0) {
		perror("could not read watchdog boot status");
		return statusVal;
	}

	return statusVal;
}

/*Watchdog timeout interval (in secs) */
int Watchdog::SetInterval(int interval) {
	int statusVal = -1;

	if (interval > 0) {
		statusVal = ioctl(this->watchdogfd, WDIOC_SETTIMEOUT, &interval);
		if (statusVal < 0) {
			perror("could not read watchdog boot status");
			return statusVal;
		}
	}

	return statusVal;
}

int Watchdog::GetInterval(int & interval) {
	int statusVal = -1;

	statusVal = ioctl(this->watchdogfd, WDIOC_GETTIMEOUT, &interval);
	if (statusVal < 0) {
		perror("could not read watchdog interval");
		return statusVal;
	}

	return statusVal;
}

/*
 * TimeRecorder.h
 *
 *  Created on: 9 Feb 2017
 *      Author: theuns
 */

#ifndef TIMERECORDER_H_
#define TIMERECORDER_H_

class TimeRecorder {
public:
	TimeRecorder();
	~TimeRecorder();
	int GetElapsedTime(int &val);
	int GetElapsedTime();
	int GetAlarmTime(int &val);
	int GetEventCount(int &val);

private:
	int valueElapsedfd;
	int valueAlarmfd;
	int valueEventfd;
};

#endif /* TIMERECORDER_H_ */

/*
 * Digital.h
 *
 *  Created on: 14 Dec 2016
 *      Author: theuns
 */

#ifndef BSP_DIGITAL_H_
#define BSP_DIGITAL_H_

class Digital {
public:
	Digital(int gpio);
	~Digital();

	/* Operations */
	int SetDir(int dir, bool invert = false); // Set GPIO Direction
	int SetVal(int val); // Set GPIO Value (putput pins)
	int GetVal(int& val); // Get GPIO Value (input/ output pins)
	int GetPinNumber(); // return the GPIO number associated with the instance of an object

private:
	int Export(); // exports GPIO
	int Unexport(); // unexport GPIO
	int gpionum; // GPIO number associated with the instance of an object

	int valuefd;
	int directionfd;
	int exportfd;
	int unexportfd;

};

#endif /* BSP_DIGITAL_H_ */

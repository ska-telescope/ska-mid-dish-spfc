/*
 * Adc.cpp
 *
 *  Created on: 15 Dec 2016
 *      Author: theuns
 */

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "Adc.h"

using namespace std;

#define VrefARM (3.3)

Adc::Adc(int channel) :
		valuefd(-1), enablefd(-1), highresfd(-1), channelnum(channel) {
	this->Enable();
}

Adc::~Adc() {
	//this->Disable();
}

int Adc::Enable() {
	int statusVal = -1;
	string enableStr = "/sys/class/misc/adc/ch" + to_string(this->channelnum)
			+ "_enable";

	this->enablefd = statusVal = open(enableStr.c_str(), O_WRONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS ADC enable device");
		return statusVal;
	}

	string numStr = "1";

	statusVal = write(this->enablefd, numStr.c_str(), numStr.length());
	if (statusVal < 0) {
		perror("could not write to SYSFS ADC enable device");
		return statusVal;
	}

	statusVal = close(this->enablefd);
	if (statusVal < 0) {
		perror("could not close SYSFS ADC enable device");
		return statusVal;
	}

	return statusVal;
}

int Adc::Disable() {
	int statusVal = -1;
	string enableStr = "/sys/class/misc/adc/ch" + to_string(this->channelnum)
			+ "_enable";

	this->enablefd = statusVal = open(enableStr.c_str(), O_WRONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS ADC enable device");
		return statusVal;
	}

	string numStr = "0";

	statusVal = write(this->enablefd, numStr.c_str(), numStr.length());
	if (statusVal < 0) {
		perror("could not write to SYSFS ADC enable device");
		return statusVal;
	}

	statusVal = close(this->enablefd);
	if (statusVal < 0) {
		perror("could not close SYSFS ADC enable device");
		return statusVal;
	}

	return statusVal;
}

int Adc::GetPrecision() {
	/* Open file */
	string getValStr = "/sys/class/misc/adc/highres";
	unsigned char tmp;
	int statusVal = -1;
	this->highresfd = statusVal = open(getValStr.c_str(), O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open ADC highres file");
		return statusVal;
	}

	/* Read file */
	statusVal = read(this->highresfd, &tmp, 1);
	if (statusVal < 0) {
		perror("could not read ADC highres file");
		return statusVal;
	}

	/* Do stuff */
	if (tmp == '1') {
		this->highres = 1;
	} else {
		this->highres = 0;
	}

	/* Close file */
	statusVal = close(this->highresfd);
	if (statusVal < 0) {
		perror("could not close ADC highres file");
		return statusVal;
	}

	return statusVal;
}

int Adc::Read(double &val) {
	string getValStr = "/sys/class/misc/adc/ch" + to_string(this->channelnum)
			+ "_value";
	int size = 16;
	char buff[size];
	int statusVal = -1;

	this->valuefd = statusVal = open(getValStr.c_str(), O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS ADC value device");
		return statusVal;
	}

	/* Clear the buffer */
	memset(buff, 0, size);
	statusVal = read(this->valuefd, &buff, size);
	if (statusVal < 0) {
		perror("could not read SYSFS ADC value device");
		return statusVal;
	}

	/* Ensure that the buffer is null terminated */
	buff[size] = '\0';

	long long llval = stoll(buff);
	val = (double) ((double) llval / 1023) * VrefARM;

	//FIXME: cout << "BUFF: " << buff << " llval: " << llval << " val: " << val << endl;

	statusVal = close(this->valuefd);
	if (statusVal < 0) {
		perror("could not close SYSFS GPIO value device");
		return statusVal;
	}

	return statusVal;
}

double Adc::Read() {
	string getValStr = "/sys/class/misc/adc/ch" + to_string(this->channelnum)
			+ "_value";
	int size = 16;
	char buff[size];
	int statusVal = -1;
	double retVal;

	this->valuefd = statusVal = open(getValStr.c_str(), O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS ADC value device");
		return statusVal;
	}

	/* Clear the buffer */
	memset(buff, 0, size);
	statusVal = read(this->valuefd, &buff, size);
	if (statusVal < 0) {
		perror("could not read SYSFS ADC value device");
		return statusVal;
	}

	/* Ensure that the buffer is null terminated */
	buff[size] = '\0';

	unsigned long long ullval = stoull(buff);
	retVal = (double) ((double) ullval / 1023) * VrefARM;

	statusVal = close(this->valuefd);
	if (statusVal < 0) {
		perror("could not close SYSFS GPIO value device");
		return statusVal;
	}

	return retVal;;
}

int Adc::GetChannelNumber() {
	return this->channelnum;
}


/*
 * Watchdog.h
 *
 *  Created on: 9 Feb 2017
 *      Author: theuns
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

class Watchdog {
public:
	Watchdog(int _interval);
	~Watchdog();

	int Start();
	int GetLastBoot(int & status);
	int SetInterval(int interval);
	int GetInterval(int & interval);
	int Kick();
	int GetInfo();

private:
	int Enable();
	int Disable();

	int watchdogfd;
	int _interval;

};

#endif /* WATCHDOG_H_ */

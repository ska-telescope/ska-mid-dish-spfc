/*
 * TimeRecorder.cpp
 *
 *  Created on: 9 Feb 2017
 *      Author: theuns
 */

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "TimeRecorder.h"

using namespace std;

TimeRecorder::TimeRecorder() :
		valueElapsedfd(-1), valueAlarmfd(-1), valueEventfd(-1) {

}

TimeRecorder::~TimeRecorder() {
	/* TODO: Check that all files are closed */
}

int TimeRecorder::GetElapsedTime(int &val) {
	string getValStr = "/sys/class/i2c-adapter/i2c-0/0-006b/elapsed_time";
	int size = 16;
	char buff[size];
	int statusVal = -1;

	this->valueElapsedfd = statusVal = open(getValStr.c_str(),
			O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS elapsed time value device");
		return statusVal;
	}

	/* Clear the buffer */
	memset(buff, 0, size);
	statusVal = read(this->valueElapsedfd, &buff, size);
	if (statusVal < 0) {
		perror("could not read SYSFS elapsed time value device");
		return statusVal;
	}

	/* Ensure that the buffer is null terminated */
	buff[size] = '\0';

	unsigned long long ullval = stoull(buff);
	val = static_cast<int>(ullval / (1000)); // Data given in ms and we need seconds

	statusVal = close(this->valueElapsedfd);
	if (statusVal < 0) {
		perror("could not close SYSFS elapsed time value device");
		return statusVal;
	}

	return statusVal;
}

int TimeRecorder::GetElapsedTime() {
	string getValStr = "/sys/class/i2c-adapter/i2c-0/0-006b/elapsed_time";
	int size = 16;
	char buff[size];
	int statusVal = -1;
	int retVal = 0;

	this->valueElapsedfd = statusVal = open(getValStr.c_str(),
			O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS elapsed time value device");
		return statusVal;
	}

	/* Clear the buffer */
	memset(buff, 0, size);
	statusVal = read(this->valueElapsedfd, &buff, size);
	if (statusVal < 0) {
		perror("could not read SYSFS elapsed time value device");
		return statusVal;
	}

	/* Ensure that the buffer is null terminated */
	buff[size] = '\0';

	unsigned long long ullval = stoull(buff);
	retVal = static_cast<int>(ullval / (1000)); // Data given in ms and we need seconds

	statusVal = close(this->valueElapsedfd);
	if (statusVal < 0) {
		perror("could not close SYSFS elapsed time value device");
		return statusVal;
	}

	return retVal;
}

int TimeRecorder::GetAlarmTime(int &val) {
	string getValStr = "/sys/class/i2c-adapter/i2c-0/0-006b/alarm_time";
	int size = 16;
	char buff[size];
	int statusVal = -1;

	this->valueAlarmfd = statusVal = open(getValStr.c_str(), O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS alarm time value device");
		return statusVal;
	}

	/* Clear the buffer */
	memset(buff, 0, size);
	statusVal = read(this->valueAlarmfd, &buff, size);
	if (statusVal < 0) {
		perror("could not read SYSFS alarm time value device");
		return statusVal;
	}

	/* Ensure that the buffer is null terminated */
	buff[size] = '\0';

	int ival = stoi(buff);
	val = (int) (ival / 1000);

	statusVal = close(this->valueAlarmfd);
	if (statusVal < 0) {
		perror("could not close SYSFS alarm time value device");
		return statusVal;
	}

	return statusVal;
}

int TimeRecorder::GetEventCount(int &val) {
	string getValStr = "/sys/class/i2c-adapter/i2c-0/0-006b/event_count";
	int size = 16;
	char buff[size];
	int statusVal = -1;

	this->valueEventfd = statusVal = open(getValStr.c_str(), O_RDONLY | O_SYNC);
	if (statusVal < 0) {
		perror("could not open SYSFS event count value device");
		return statusVal;
	}

	/* Clear the buffer */
	memset(buff, 0, size);
	statusVal = read(this->valueEventfd, &buff, size);
	if (statusVal < 0) {
		perror("could not read SYSFS event count value device");
		return statusVal;
	}

	/* Ensure that the buffer is null terminated */
	buff[size] = '\0';

	val = stoi(buff);

	statusVal = close(this->valueEventfd);
	if (statusVal < 0) {
		perror("could not close SYSFS event count value device");
		return statusVal;
	}

	return statusVal;
}


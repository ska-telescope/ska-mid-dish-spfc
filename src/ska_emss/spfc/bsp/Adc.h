/*
 * Adc.h
 *
 *  Created on: 15 Dec 2016
 *      Author: theuns
 */

#ifndef BSP_ADC_H_
#define BSP_ADC_H_

class Adc {
public:
	Adc(int channel);
	~Adc();

	/* Operations */
	int GetPrecision(); // Get ADC precision
	int Read(double &val); // Get ADC value in volt
	double Read();
	int GetChannelNumber(); // return the ADC channel number associated with the instance of an object

private:
	int Enable(); // Enables ADC
	int Disable(); // Disables ADC
	int channelnum; // Channel number associated with the instance of an object
	int highres;

	int valuefd;
	int enablefd;
	int highresfd;
};

#endif /* BSP_ADC_H_ */

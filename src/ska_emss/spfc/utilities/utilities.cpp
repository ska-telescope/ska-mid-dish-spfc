/*
 * utilities.cpp
 *
 *  Created on: 14 Mar 2017
 *      Author: theuns
 */

#include "utilities.h"

namespace util {
/* Helper functions */

std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, std::back_inserter(elems));
	return elems;
}

std::string trim(const std::string &s) {
	std::string::const_iterator it = s.begin();
	while (it != s.end() && isspace(*it))
		it++;

	std::string::const_reverse_iterator rit = s.rbegin();
	while (rit.base() != it && isspace(*rit))
		rit++;

	return std::string(it, rit.base());
}

double InterpolateLinear(double x0, double y0, double x1, double y1, double y) {
	double tempval;
	tempval = (x0 + (y - y0) * (x1 - x0) / (y1 - y0));
	return tempval;
}

/*
 std::string& trim_left_in_place(std::string& str) {
 size_t i = 0;
 while(i < str.size() && isspace(str[i])) { ++i; };
 return str.erase(0, i);
 }

 std::string& trim_right_in_place(std::string& str) {
 size_t i = str.size();
 while(i > 0 && isspace(str[i - 1])) { --i; };
 return str.erase(i, str.size());
 }

 std::string& trim_in_place(std::string& str) {
 return trim_left_in_place(trim_right_in_place(str));
 }

 // returns newly created strings


 std::string trim(std::string str) {
 return trim_left_in_place(trim_right_in_place(str));
 }
 */
} /* namespace util */

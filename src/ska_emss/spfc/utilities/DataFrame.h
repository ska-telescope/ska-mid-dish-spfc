/*
 * DataFrame.h
 *
 *  Created on: 4 May 2017
 *      Author: theuns
 */

#ifndef UTILITIES_DATAFRAME_H_
#define UTILITIES_DATAFRAME_H_

#include <iostream>
#include <mutex>

using namespace std;

namespace util {

template<class T>
class DataFrame {
public:
	DataFrame() {
	}
	;
	~DataFrame() {
	}
	;
	void SetData(T data) {
		// Mutually exclusive access
		lock_guard < mutex > guard(_mutex);
		_data = data; // Assume primitive data types or structs with primitive types
	}
	T GetData() {
		// Mutually exclusive access
		lock_guard < mutex > guard(_mutex);
		return _data;
	}

private:
	mutex _mutex;
	T _data;
};

} /* namespace util */

#endif /* UTILITIES_DATAFRAME_H_ */

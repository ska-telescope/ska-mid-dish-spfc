/*
 * spfc_ctrl.cpp
 *
 *  Created on: 28 Mar 2017
 *      Author: theuns
 */

#define _GLIBCXX_USE_NANOSLEEP 1

#include <cstdio>
#include <memory>
#include <array>


#include <iostream>
#include <string>
#include <stdexcept>
#include <stdio.h>
#include <chrono>
#include <ctime>
#include <thread>
#include <pthread.h>
#include <numeric>
#include <vector>
#include <algorithm>
#include "DataLogger.h"
#include "spfc_ctrl.h"
#include "system.h"

bool stopLogThread = false;
bool SDCardRO = false;

using namespace std;

SpfController::SpfController() {
	// TODO Auto-generated constructor stub
	sm_thread = 0;
	stopLogThread = false;
#ifdef ARM
	adcPcbVolt = new Adc(ADC_PCB_VOLT_CH);
	adcPcbCurrent = new Adc(ADC_PCB_CURRENT_CH);
	adcPcbTemp = new Adc(ADC_PCB_TEMP_CH);
#endif
	settingsFile = new IniParser(SPFC_SETTINGS);

	pcbTime = new TimeRecorder();

	IniKeyLoadOrCreateStr("BandInFocus", "UNKNOWN", "Variables");
	settingsFile->Save();

	IniParser spfcConfig(SPFC_CONFIG); /* To get SPFC serial number */
	serialNumber = spfcConfig.GetString("SPFC", "Serial_Nr");
	deviceLocation = "";

	if (serialNumber == "") {
		serialNumber = "0000";
	}
}

SpfController::~SpfController() {
	delete adcPcbVolt;
	delete adcPcbCurrent;
	delete adcPcbTemp;
	delete pcbTime;
}

bool SpfController::SDCardAvailable() {
	bool retVal = false;

	std::ifstream mmcSizeFile("/sys/block/mmcblk0/size");
	if (mmcSizeFile.is_open()) {
		unsigned long long size;
		retVal = (mmcSizeFile >> size) && (size > 0);
		mmcSizeFile.close();
	}

	return retVal; // Will return false if file could not be opened */
}

// Read: "/dev/mmcblk0p1 on /media/card type vfat (rw" - returns True if ReadOnly enabled, False if ReadWrite
bool SpfController::SDCheckRO() {
//	return SDCardRW;

	string mountPoint = "/media/card";
    std::ifstream mountFile("/proc/mounts");

    if (mountFile.is_open()) {
        std::string line;
        while (std::getline(mountFile, line)) {
            std::istringstream iss(line);
            std::string device, mountPath, fsType, options;
            iss >> device >> mountPath >> fsType >> options;

            if (mountPath == mountPoint) {
                std::istringstream optionsStream(options);
                std::string option;
                while (std::getline(optionsStream, option, ',')) {
                    if (option == "ro") {
                        mountFile.close();
                        return true;
                    }
                }
            }
        }

        mountFile.close();
    }

    return false;
}

double SpfController::GetPcbVolt() {
#ifdef X86
	double pcbVolt = 4.92;
#else
	double pcbVolt = (adcPcbVolt->Read() / 3.0) * 5;
#endif
	return pcbVolt;
	//return (adcPcbVolt->Read() / 3.0) * 5;
}

double SpfController::GetPcbCurrent() {
#ifdef X86
	double pcbCurrent = 400.92;
#else
	double pcbCurrent = adcPcbCurrent->Read() * 10 * 1000 / 19.3;
#endif
	return pcbCurrent;
	//return adcPcbCurrent->Read()*10*1000/19.3;
}

double SpfController::GetPcbTemp() {
#ifdef X86
	double pcbTemp = 40.92;
#else
	double pcbTemp = (adcPcbTemp->Read() - 0.5) * 100;
#endif
	return pcbTemp;
	//return (adcPcbTemp->Read() - 0.5) * 100;
}

// Returns elapsed time in seconds
int SpfController::GetElapsedTime() {
#ifdef X86
	int elapsedTime = 1234;
#else
	int elapsedTime = pcbTime->GetElapsedTime();
#endif
	return elapsedTime;
}

std::string SpfController::GetSerialNumber() {
	return serialNumber;
}

std::string SpfController::GetDeviceLocation() {
	return deviceLocation;
}

std::string SpfController::GetDevLogLocation() {
	return devLogLocation;
}


int SpfController::SetDeviceLocation(std::string value) {
	deviceLocation = value;
    /* Try to identify the AP location from the deviceLocation
     * eg.: mid_dsh_0000/spf/spf2 -> M0000
     */
    if (deviceLocation.find("/") > 0)
    {
		if (deviceLocation.compare(0, 8, "mid_dsh_") == 0)
		{ // get the AP# between "mid_dsh_" and "/"
			devLogLocation = "M" + deviceLocation.substr(8, deviceLocation.find("/")-8);
		} else
		{ // eg.: tent0003/spf/spf2 -> tent0003
			devLogLocation = deviceLocation.substr(0, deviceLocation.find("/"));
		}
    } else
    { // eg.: tent3
    	devLogLocation = deviceLocation;
    }
    std::replace(devLogLocation.begin(), devLogLocation.end(), '_', '-');

	return 0;
}

void *SPFC_logger(void * arg) {
	/*============================ */
	/* Setup Logger for spfc data */
	/*============================ */
	int sensorId;
	int failCnt = 0;
	std::string line;
	std::string _logPath;
	SpfController * spfc = new SpfController();
	const chrono::milliseconds logInterval { 1000 };
	sleep(1); // wait for potential previous thread to stop before starting this thread loop.
	stopLogThread = false;
	cout << "***logging thread# " << pthread_self() << " started:" << endl;

	std::string &s1 = *static_cast<std::string*>(arg);
	spfc->SetDeviceLocation(s1);

//	IniParser spfcSettings(SPFC_SETTINGS); /* To get logging location */
	bool logToSDCard = spfc->getLogToSdCard(); //spfcSettings.GetBool("LogToSDCard", "Variables");
	SDCardRO = spfc->SDCheckRO();

	if (logToSDCard && spfc->SDCardAvailable() && !SDCardRO) {
		_logPath = BASE_PATH_SD LOG_PATH;
	} else {
		_logPath = BASE_PATH_LOCAL LOG_PATH;
	}
	cout << "Log device " << s1 << " to path " << _logPath << endl;

	/* If logpath does not exist, create it!
	 * Using a system call is not the MOST efficient way, but this seems to be the easiest for now.
	 * Permission to create directories are needed
	 */
	//system(cmdLine.c_str()); // We can't do anything about it if the system call fails.
	std::filesystem::create_directories(_logPath.c_str());
	/*============================ */
	/* Log spfc data */
	/*============================ */
	chrono::system_clock::time_point nextStartTime { chrono::system_clock::now() };
	chrono::system_clock::time_point failedTime = nextStartTime;

	while (!stopLogThread) {
		/* Timestamp */
		double timestamp = chrono::duration_cast < chrono::seconds
				> (chrono::system_clock::now().time_since_epoch()).count();

		line += util::NumberToString(timestamp);
		line += "\t";

		/* Sensors */
		sensorId = 1;
		line += std::to_string(sensorId++);
		line += ":";
		line += util::NumberToString(spfc->GetPcbVolt(), 2);
		line += "\t";
		line += std::to_string(sensorId++);
		line += ":";
		line += util::NumberToString(spfc->GetPcbCurrent(), 2);
		line += "\t";
		line += std::to_string(sensorId++);
		line += ":";
		line += util::NumberToString(spfc->GetPcbTemp(), 2);	//line += "\t";

		auto now = chrono::system_clock::now();
		auto in_time_t = chrono::system_clock::to_time_t(now);

		std::stringstream ss;
		//ss << std::put_time(std::localtime(&in_time_t), "%Y%m%d");

		char timeBuf[24];
		std::strftime(timeBuf, sizeof(timeBuf), "%Y%m%d",
				std::localtime(&in_time_t));
		ss << timeBuf;

		std::string fileName = _logPath + "/" + spfc->GetDevLogLocation() + "_SPFC" + spfc->GetSerialNumber()
				+ "_" + ss.str() + ".log";

		if (!DataLogger::AppendLine(fileName, line)) // assumes failed write to SD card
		{
			if (!SDCardRO) // if SDCardRO=true, then log failed on local Flash memory; do not try to remount SD card.
			{
				failedTime = nextStartTime;
				if ((failCnt += 1) > 3) // failed > 3 times within 60 seconds
				{
					SDCardRO = spfc->SDCheckRO(); // setLogToSdCard(false) from spfc.c
					stopLogThread = true; // auto restart if spfc is deleted when closing this loop
//					std::string *s2 = (std::string*)(arg);
					spfc->RestartLogger(&s1);
				} else // remount SD card as RW
				{
					std::string cmdLine = "/home/root/remountSD.sh"; // remount the SD card to be RW
					system(cmdLine.c_str()); // We can't do anything about it if the system call fails.
				}
			}
		} else if ((failCnt > 0) && nextStartTime > (failedTime + 60*logInterval) )
		{	// Reset the failCnt if > 60s since last write fail
			failCnt = 0;
			cout << "Log fail-count reset" << endl;
		}
		line = ""; // clear the data line for next save

		nextStartTime = nextStartTime + logInterval;
		std::this_thread::sleep_until(nextStartTime);

	}

	cout << "***logging thread# " << pthread_self() << " stopped:" << endl;
	delete spfc;
	pthread_exit(0);
	return (void*) 0;
}

enBandInFocus SpfController::convertBandInFocus(std::string value) {
	enBandInFocus retVal = enBandInFocus::UNKNOWN;
	for (unsigned int band = 0; band < LENGTH(bandName); band++) {
		if (bandName[band].compare(value) == 0) {
			return (enBandInFocus) band;
		}
	}
	return retVal;
}

enBandInFocus SpfController::getBandInFocus() {
	return convertBandInFocus(
			settingsFile->GetString("BandInFocus", "Variables"));
}

bool SpfController::setBandInFocus(enBandInFocus band) {
	if (settingsFile->SetValue("BandInFocus", bandName[(int) band], "", "Variables")) {
		return settingsFile->Save();
	}
	return false;
}

bool SpfController::getLogToSdCard() {
	return settingsFile->GetInt("LogToSDCard", "Variables");
}

bool SpfController::setLogToSdCard(bool sdCard) {
	if (settingsFile->SetInt("LogToSDCard", sdCard, "", "Variables")) {
		return settingsFile->Save();
	}
	return false;
}

std::string SpfController::IniKeyLoadOrCreateStr(std::string key,
		std::string value, std::string section) {
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
			{
		settingsFile->SetValue(key, value, "", section);
		return value;
	} else {
		return settingsFile->GetString(key, section);
	}
}

int SpfController::IniKeyLoadOrCreateInt(std::string key, int value,
		std::string section) {
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
			{
		settingsFile->SetInt(key, value, "", section);
		return value;
	} else {
		return settingsFile->GetInt(key, section);
	}
}

unsigned int SpfController::IniKeyLoadOrCreateUInt(std::string key,
		unsigned int value, std::string section) {
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
			{
		settingsFile->SetUInt(key, value, "", section);
		return value;
	} else {
		return settingsFile->GetUInt(key, section);
	}
}

double SpfController::IniKeyLoadOrCreateDouble(std::string key, double value,
		std::string section) {
	if (!settingsFile->GetValue(key, section).compare("")) // equal returns 0? I think, thus, if value does not exist, create it
			{
		settingsFile->SetDouble(key, value, "", section);
		return value;
	} else {
		return settingsFile->GetDouble(key, section);
	}
}

bool SpfController::Run(std::string *devName) {
	cout << "STARTING SPFC_LOGGING THREAD" << endl;

	bool retVal = false;

	pthread_attr_t attr;
	pthread_attr_init(&attr);

	// SCHED_FIFO corresponds to real-time preemptive priority-based scheduler
	// NOTE: This scheduling policy requires the superuser privileges
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);

	struct sched_param param;
	param.sched_priority = 1 + (sched_get_priority_max(SCHED_FIFO) - 60);

	pthread_attr_setschedparam(&attr, &param);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	int res = pthread_create(&sm_thread, &attr, SPFC_logger, devName);
	if (res != 0) {
		perror("SPFC_logger() pthread create");
		// TODO: handle error here ...
	} else {
		retVal = true;
	}

	return retVal;
}

bool SpfController::RestartLogger(std::string *devName) {
	stopLogThread = true;
	sleep(2);
	Run(devName);
	return true;
}

bool SpfController::Shutdown() {
	stopLogThread = true;
	return true;
}


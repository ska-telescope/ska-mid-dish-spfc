/*
 * spfc_ctrl.h
 *
 *  Created on: 28 Mar 2017
 *      Author: theuns
 */

#ifndef SYSTEM_SPFC_CTRL_H_
#define SYSTEM_SPFC_CTRL_H_

#include <string>
#include <unistd.h>
#include "system.h"
#include "Adc.h"
#include "TimeRecorder.h"
#include "utilities.h"
#include "IniParser.h"
#include <filesystem>
#define ARM
//#define X86

enum class _enBandInFocus : short
{
	UNKNOWN = 0,
	B1, B2, B3, B4, B5a, B5b, B6, Bs, Bku
};
typedef _enBandInFocus enBandInFocus;

class SpfController {
public:
	SpfController();
	~SpfController();

	bool SDCardAvailable();
	bool SDCheckRO();
	double GetPcbVolt();
	double GetPcbCurrent();
	double GetPcbTemp();
	int GetElapsedTime();

	std::string GetSerialNumber();
	std::string GetDeviceLocation();
	std::string GetDevLogLocation();
	int SetDeviceLocation(std::string value);
	enBandInFocus convertBandInFocus(std::string value);
	enBandInFocus getBandInFocus();
	bool setBandInFocus(enBandInFocus band);
	bool getLogToSdCard();
	bool setLogToSdCard(bool sdCard);
	std::string IniKeyLoadOrCreateStr(std::string key, std::string value,
			std::string section);
	int IniKeyLoadOrCreateInt(std::string key, int value, std::string section);
	unsigned int IniKeyLoadOrCreateUInt(std::string key, unsigned int value,
			std::string section);
	double IniKeyLoadOrCreateDouble(std::string key, double value,
			std::string section);
	bool Run(std::string *devName);
	bool RestartLogger(std::string *devName);
	bool Shutdown();

private:
	std::string bandName[10] = { "UNKNOWN", "B1", "B2", "B3", "B4", "B5a", "B5b",
			"B6", "Bs", "Bku" };
	Adc* adcPcbVolt;
	Adc* adcPcbCurrent;
	Adc* adcPcbTemp;

	TimeRecorder* pcbTime;

	std::string serialNumber;
	std::string deviceLocation; // = "mid_dsh_unkn/spf/spfc";
	std::string devLogLocation; // = "unknown";

	pthread_t sm_thread;

	IniParser * settingsFile;
//    stSettings * settings;

};

#endif /* SYSTEM_SPFC_CTRL_H_ */

DESCRPTION = "Katcp library"
HOMEPAGE = "https://github.com/ska-sa/katcp_devel.git"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://COPYING;md5=d32239bcb673463ab874e80d47fae504"

#DEFAULT_PREFERENCE = "-1"

SRCREV = "0639e19e4a4e79ea4c3a6680b4a850be570cd179"
PV = "5.0+git${SRCPV}"
PR = "r3"

#    file://katcp-git-5.0.tar.gz 
SRC_URI = " \
    git://github.com/ska-sa/katcp_devel.git;protocol=https \
    file://01-makefile-install-fixes.patch \
    file://02-update-speed-fix-discrete.patch \
"

S = "${WORKDIR}/git"

# The preset EXTRA_OEMAKE's "-e" flag appears to prohibit the makefile from including ../Makefile.inc
EXTRA_OEMAKE = ""

do_compile() {
    oe_runmake -C katcp libkatcp.so
    oe_runmake -C log kcplog
    (cd katcp; mv libkatcp.so libkatcp.so.0.0.0)
}

do_install() {
    oe_runmake -C katcp PREFIX=${D}/usr install
    oe_libinstall -C katcp -a libkatcp ${D}${libdir}
    oe_runmake -C log PREFIX=${D}/usr install
    rm -rf ${D}${libdir}/kcs
}

FILES_${PN} += "${bindir}/* ${sbindir}/*"

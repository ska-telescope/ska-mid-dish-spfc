FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
PRINC := "${@int(PRINC) + 1}"

SRC_URI += "file://nice"
SYSTEMD_AUTO_ENABLE ??= "enable"

do_install_append () {
        install -m 0755 ${WORKDIR}/nice ${D}${bindir}/nice
        # Uncomment lines below for Anacron
        install -m 0600 ${WORKDIR}/anacrontab ${D}${sysconfdir}/
        install -d ${D}${localstatedir}/spool/anacron
        install -m 0644 -D ${WORKDIR}/anacron.service ${D}${systemd_unitdir}/system/anacron.service
        install -m 0644 -D ${WORKDIR}/anacron.timer ${D}${systemd_unitdir}/system/anacron.timer
}

SRC_URI += "file://anacron.service \
        file://anacron.timer \
        file://anacrontab \
"

EXTRA_OECONF += "--enable-anacron"

SYSTEMD_SERVICE_${PN}-systemd += "anacron.timer"

FILES_${PN}-systemd += "${systemd_unitdir}/system/anacron.service"

# systemd_postinst_append() {
#    if [ -z "$D" ]; then
#        systemctl enable anacron.service
#    fi
# }

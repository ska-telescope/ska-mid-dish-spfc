require recipes-bsp/u-boot/u-boot.inc

PR = "r1"

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=1707d6db1d42237583f50183a5651ecb"

COMPATIBLE_MACHINE = "(stamp9g20evb)"

S = "${WORKDIR}/u-boot-${PV}"

SRC_URI = "ftp://ftp.denx.de/pub/u-boot/u-boot-${PV}.tar.bz2"

SRC_URI_append_stamp9g20evb = " \
    file://u-boot-2013.01.patch \
    file://noSPI.patch \
"

SRC_URI[md5sum] = "e58a8a7f78972248190d83de0dc362ce"
SRC_URI[sha256sum] = "9ca0f35f4394281312d03c581e24a263cf3e0d9703f15103ff2ead5e5aa43f90"

PACKAGE_ARCH = "${MACHINE_ARCH}"

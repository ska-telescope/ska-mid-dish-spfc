#!/bin/sh

if [ -f $1 ];
then  
        systemctl stop rsc.service
	cp $1 /usr/sbin/rsc
	systemctl start rsc.service
else
        echo "No file selected"
        echo "Please include bin file in parameter 1"
fi

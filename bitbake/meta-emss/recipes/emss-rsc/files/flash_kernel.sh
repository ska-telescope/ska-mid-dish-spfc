#!/bin/sh

if [ -f $1 ]; 
then
	flash_erase /dev/mtd4 0 0
	nandwrite -p /dev/mtd4 $1
else
	echo "No image selected"
	echo "Please include image in parameter 1"
fi

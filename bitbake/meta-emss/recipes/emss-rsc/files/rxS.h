#ifndef __RXS__HEADER__
#define __RXS__HEADER__


	#ifndef WIN32
		#include <katcp.h>
	#endif

	#include <uartRpc.h>
	#include <configFile.h>
	#include <states.h>

	#define RSC_IFACE_TESTING (1)

	#define LIBRXS_KATCP_CONFIRATION_ARGUMENT	"53121"

	#define LIBRXS_CF_SIMULATE				0x00000001
	#define LIBRXS_CF_VERBOSE				0x00000002
	#define LIBRXS_CF_KATCP_STATE_LOCKED	0x00000004


	#define ENABLE 1
	#define DISABLE 0

	#define UART_ERRORCTR_MAX  8

	typedef enum
	{
		// !! IMPORTANT: do not change order

		rss_unknown = -1,

		rss_initialize =  0,			// initializing state machine

		rss_init_rxDisconnected,
		rss_rxDisconnected,				// receiver hardware is disconnected
		rss_clean_rxDisconnected,

		rss_operating,					// just a virtual state. Used as health check threshold only

		rss_init_rxConnected,
		rss_rxConnected,				// normal operational
		rcc_finalize_rxConnected,
		rss_clean_rxConnected,

		rss_maintenance,				// just a virtual state. Used as health check threshold only

		rss_init_service,
		rss_service,					// being in service mode
		rss_finalize_service,
		rss_clean_service,

		rss_init_flashRcm,				// enter update receiver controller mode.
		rss_flashRcm,
		rss_clean_flashRcm,
		rss_done_flashRcm,

		rss_error,						// just a virtual state. Used as health check threshold only

		rss_init_failed,
		rss_failed,
		rss_clean_failed,

		rss_reboot,						// just a virtual state. Used as health check threshold only

		rss_init_restart,
		rss_restart,
		rss_clean_restart,

		rss_disabled,					// kill libRxS state machine. No normal state.

		rss_nOfStates					// only logical. reprenents number of present states


	} t_rxsSystemState;




	#define RXS_SYSTEM_STATE_STRING_MAP { \
		"initialize", \
		\
		"init_rxDisconneted", \
		"rxDisconnected", \
		"clean_rxDisconnected", \
		\
		"operating", \
		\
		"init_rxConnected", \
		"rxConnected", \
		"finalize_rxConnected", \
		"clean_rxConnected", \
		\
		"maintenance",	\
		\
		"init_service", \
		"service", \
		"finalize_service", \
		"clean_service", \
		\
		"init_flashing_file_to_rcm", \
		"flashing_file_to_rcm", \
		"clean_flashing_file_to_rcm", \
		"done_fashing_file_to_rcm", \
		\
		"error",	\
		\
		"init_failed", \
		"failed", \
		"clean_failed", \
		\
		"reboot",	\
		\
		"init_restart", \
		"restart", \
		"clean_restart", \
		\
		"disabled", \
		\
		NULL}




	#define RXS_SELECTABLE_SYSTEM_STATES { \
		rss_rxDisconnected,  \
		rss_rxConnected,	 \
		rss_service, 		 \
		rss_flashRcm,		 \
		rss_restart,		 \
		rss_unknown} // end of list delimiter. no valid state


	#define RXS_SELECTABLE_STARTUP_STATES { \
		rss_rxDisconnected,  				\
		rss_rxConnected, 					\
		rss_service, 						\
		rss_unknown} // end of list delimiter. no valid state




	typedef enum {
		// keep t_rxs_camState in consixtence with SATES_CAM_CAMSTATES
		cams_invalid		= -1,
		cams_undefined 		= 0,
		cams_error,
		cams_transitional,
		cams_softoff,
		cams_available,
		cams_debug,
		cams_initializing,
		cams_unavailable,
		cams_degraded,
		cams_maintenance,
		cams_failure,
	} t_rxs_camState;


	#define RXS_CAM_STATE_NAME_MAP	{	"undefined",			\
										"error",				\
										"transitional",			\
										"soft-off",				\
										"available",			\
										"debug",				\
										"initializing",			\
										"unavailable",			\
										"degraded-performance", \
										"maintenance",			\
										"failure",				\
										NULL}


	#define RXS_CAM_STATE_NAME_MAP_SIZE     ( (sizeof((char*[])RXS_CAM_STATE_NAME_MAP) / sizeof(char *)) - 1 )



	#define RSM_OPMODE_NAME_MAP {	"soft-off", 				\
									"cold-operational", 		\
									"debug", 					\
									"regenerate-to-warm", 		\
									"regenerate-to-cold-op",	\
									"regenerate-to-warm-fin",	\
									"disconnect", 				\
									NULL}

	#define RSM_OPMODE_NAME_MAP_SIZE 	( sizeof((char *[])RSM_OPMODE_NAME_MAP) / sizeof(char *) - 1)


	typedef struct {
		int state;
		char *name;
	} t_rxs_valueNameMap;


	typedef struct {

		char *name;
		t_rxsSystemState systemState;

	}t_rxs_commandableSystemStates;



	typedef struct {

		char *camCommand;
		STATES_RSM_OPMODE opMode;

	} t_rxs_command_rsmOpmode_map;

	typedef struct {
		t_rxs_camState 		rxsCamstate;
		STATES_CAM_STATECAM rsmCamStates;
	} t_rxS_camStatesMap;



	#define RXS_SYSTEMSTATE_LEN_STRINGMAP ((sizeof((char *[])RXS_SYSTEM_STATE_STRING_MAP) / sizeof(char *)) - 1)

	extern t_rxsSystemState selectableSystemStates[];
	extern t_rxsSystemState selectableStartupStates[];

	extern char *rxs_systemState_stringMap[];
	extern U32  rxs_systemState_lenStringMap;

	extern char *rxs_operationState_stringMap[];
	//extern U32  rxs_opereationState_lenStringMap;

	extern t_rxs_valueNameMap rxs_command_systemState_map[];


	typedef enum {

		scs_init = 0,
		scs_idle,
		scs_wait_available,
		scs_readSensor,
		scs_pauseReading,

	} t_rxS_sensorCollectorsState;


	typedef struct
	{
		U32 ctrlFlags;
		t_cfgFile *cfg;
		struct katcp_dispatch *katcp;

		t_rxsSystemState systemState;
		t_rxsSystemState successorState;
		t_rxsSystemState startupSystemState;

		int masterTask_run;
		pthread_t masterTask_thread;

		t_rxS_sensorCollectorsState sensorCollector_state;
		int sensorCollector_ctr;
		double sensorCollector_time;

		char *uartName;
		int uartfd;
		U32 uartBaudrate;
		U32 uartErrorCtr;

		U32 uartBaudrate_bootloader;

		U8 *requestBuffer;
		U8 *replyBuffer;
		U8 *clientRequestBuffer;
		U8 *memClientRequestFiFo;
		U8 *memClientAnswFiFo;
		char *memErrMsgBuffer;

		int uartRpc_run;
		t_rcmRpc *rscService;
		t_uartRpc *rpc;
		pthread_t uartRpc_thread;

		struct cmu_protocol *cmu;
		char *errormsg;

		int expectedOnline;

		int collectSensorValues_verbose;

		char *flashRcm_binFileName;
		float flashRcm_percentDone;


	} t_rxS;


	#define RXS_CONFIGFILE						"/var/lib/rsc/rxs/libRxS.cfg"
	#define RXS_KATCP_RECVNAME					"RXS"
	#define RXS_KATCP_CMDSEPERATOR_CMD			'-'
	#define RXS_KATCP_CMDSEPERATOR_SENS			'.'

	#define RXS_UARTRPC_UART					"/dev/ttyS3"
	//#define RXS_UARTRPC_BAUDRATE_ALT			115200
	//#define RXS_UARTRPC_BAUDRATE				19200
	#define RXS_UARTRPC_BAUDRATE				57600
	//#define RXS_UARTRPC_BAUDRATE_BOOTLOADER	115200
	#define RXS_UARTRPC_BAUDRATE_BOOTLOADER		57600
	#define RXS_UARTRPC_BUFFERSIZE				1024
	#define RXS_UARTRPC_BUFFERSIZE_ERRMSG		1024
	#define RXS_UARTRPC_REQUEST_TIMEOUT_MS		1500


	#define RXS_TIMEOUT_NREAD					50000


	#define RXS_SENSOR_COLLECTOR_IDLE_TIME		1.0 //s
	#define RXS_SENSOR_COLLECTOR_PAUSE_TIME		0.01 //s

	extern U32 rxS_uartBaudrate_testApplication;	// change before creating new instance to change baudrate.

	char *rxS_rcm_system_softwareVersion(t_uartRpc *);
	int rcs_rcm_vacValve_open(t_uartRpc *rpc, U32 value);
	int rcs_rsm_setOpMode(t_uartRpc *, I32);

	int rxS_error(t_rxS *, char *, ...);
	int rxS_info(t_rxS *, char *, ...);
	char *rxS_build(void);

	void rxS_set_simulate(int);
	int rxS_set_verbose(t_rxS *, int);

	void rxS_change_configFile(char *);
	t_rxS *rxS_create(struct katcp_dispatch *katcp, char *, struct cmu_protocol *);
	int rxS_destroy(t_rxS **rxSPP);


	int rxs_uart_open(t_rxS *rxS);
	int rxS_uart_close(t_rxS *rxS);

	int rxS_isReceiverConnected(t_rxS *);
	int rxS_vacuumValve_isOpen(t_rxS *);

	// funcction for changing system states
	int rxS_librxs_starrtupstate_set(t_rxS *, t_rxsSystemState);
	int rxS_lockstate_set(t_rxS *, int);
	int rxS_selectedOnline_set(t_rxS *, int);

	t_rxsSystemState rxS_systemState_from_str(char *);
	t_rxsSystemState rxS_systemState_fromCommand(char *);
	char *rxS_systemState_toStr(t_rxsSystemState);

	int rxS_systemStatus_is_systemState_selectable(t_rxsSystemState);
	int rxS_systemStatus_is_startupState_selectable(t_rxsSystemState);

	int rxS_systemStatus_switch_systemState(t_rxS *rxS, t_rxsSystemState);

	// states defined by emss
	t_rxs_camState rxS_systemStatus_getCamState(t_rxS *rxS, int *);


	int rxS_systemStatus_rcm_switchOpMode(t_rxS *, STATES_RSM_OPMODE);
	STATES_RSM_OPMODE rxS_rsmOpMode_from_str(char *);


#endif

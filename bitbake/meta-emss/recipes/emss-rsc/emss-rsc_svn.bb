DESCRIPTION = "EMSS Receiver System Controller (RSC)"
LICENSE = "EMSS"
LIC_FILES_CHKSUM = "file://License;md5=8b5b6a5673fe685ee2425a573f0beb9f"

#SRCREV = "${AUTOREV}"
SRCREV = "14814"

PV = "4.40_1100+svnr${SRCPV}"
PR = "r1"

DEPENDS = "katcp"
RDEPENDS = "cronie"
#RDEPENDS = "connman"

SRC_URI = " \
	svn://athens.emss.co.za/repositories/Antennas/Projects/MK/Software;module=RSC;protocol=https \
	file://profile \
	file://katcp-log-rotations \
	file://katcp-avahi.service \
	file://elapsed_time.sh \
	file://enable_debugger.sh \
	file://flash_kernel.sh \
	file://view_clock_speeds.sh \
	file://updateRSC.sh \
	file://remountSD.sh \
	file://build_versions.patch \
"


S = "${WORKDIR}/RSC"

inherit systemd

SYSTEMD_PACKAGES = "${PN}-systemd"
SYSTEMD_SERVICE = "rsc.service katcp-log.service katcp-log_m.service"
SRC_URI += "file://rsc.service \
	    file://katcp-log.service \
	    file://katcp-log_m.service \
	    file://0anacron-daily \
	    file://0anacron-weekly \
	    file://0anacron-monthly \
	    file://1logzip \
	    file://logCleaner \
	    file://hwclock-sync \
	    "


#inherit useradd

#USERADD_PACKAGES = "${PN}"
#USERADD_PARAM_${PN} = " \
#	--password rsc \
#	--home /home/rsc \
#"
#	--user-group rsc \
#	--system \
#	--no-create-home \
#	--shell /bin/sh \

PARALLEL_MAKE = ""

do_configure() {
    :
}

do_compile() {
	oe_runmake -C qpc/ports/arm/linux/gnu all CONF=rel LIB=${AR}
	oe_runmake all CONF=rel
}

do_install() {
	install -D -m 0755 ${S}/rsc ${D}${sbindir}/rsc
	# Add profile script to add sbin to PATH variable for rsc user
#	install -D -m 0644 ${WORKDIR}/profile ${D}${sysconfdir}/profile.d/rsc
	# Add Avahi service description for discoverablity of katcp servers
	install -D -m 0644 ${WORKDIR}/katcp-avahi.service ${D}${sysconfdir}/avahi/services/katcp.service
	# Add cron script for KATCP log rotation
	install -D -m 0744 ${WORKDIR}/0anacron-weekly ${D}${sysconfdir}/cron.weekly/0anacron-weekly
	install -D -m 0744 ${WORKDIR}/katcp-log-rotations ${D}${sysconfdir}/cron.weekly/katcp-log-rotations
	install -D -m 0744 ${WORKDIR}/hwclock-sync ${D}${sysconfdir}/cron.weekly/hwclock-sync
	install -D -m 0744 ${WORKDIR}/0anacron-daily ${D}${sysconfdir}/cron.daily/0anacron-daily
	install -D -m 0744 ${WORKDIR}/1logzip ${D}${sysconfdir}/cron.daily/1logzip
	install -D -m 0744 ${WORKDIR}/logCleaner ${D}${sysconfdir}/cron.daily/logCleaner
	# Create some directories where we can store files
#	install -d -m 0755 -o rsc -g rsc ${D}/home/rsc
	install -d -m 0755 -o root -g root ${D}/home/root/main
	install -d -m 0755 -o root -g root ${D}/var/lib/rsc/uploads
	# Utility scripts, usefull for debugging and testing
	install -m 0744 ${WORKDIR}/elapsed_time.sh ${D}/home/root/elapsed_time.sh
	install -m 0744 ${WORKDIR}/enable_debugger.sh ${D}/home/root/enable_debugger.sh
	install -m 0744 ${WORKDIR}/flash_kernel.sh ${D}/home/root/flash_kernel.sh
	install -m 0744 ${WORKDIR}/view_clock_speeds.sh ${D}/home/root/view_clock_speeds.sh
	install -m 0744 ${WORKDIR}/updateRSC.sh ${D}/home/root/updateRSC.sh
	install -m 0744 ${WORKDIR}/remountSD.sh ${D}/home/root/remountSD.sh
}

FILES_${PN} += " \
	${sysconfdir}/profile.d \
	${sysconfdir}/avahi \
	/home/root \
	/home/rsc \
"

# Welcome to the SKA Dish SPFC project.
==========================================

## Getting started

Clone the project (Makefiles incl):

```bash
git clone --recurse-submodules git@gitlab.com:ska-telescope/ska-mid-dish-spfc.git
```

(VSCode users) reopen the project in a devcontainer so that you have tango etc pre-installed. Once that's done, from a terminal inside this running container, you can do linting, testing or registering the SPFC.



### Description

Single Pixel Feeder Controller (SPFC) is a linux device that controls and monitors the following devices:
1. SPF Band 1
2. SPF Band 2
3. SPF Band 345
4. SPF Vacuum services
5. SPF Helium services

SPFC is a self contained credit card size device powered by 400 MHZ ARM processor, running Busybox Linux OS on 512 MB ROM. It has 128MB of RAM. SPFC has Python2 installed. One can SSH into it to change a tango device configuration file before registering to a tango database. Please note that no SSH is needed in order to register it to the tango database. There are two types of SPFC devices in the ITF:

1. SPF Controller which is the SPFC unit used by multiple software teams.
2. SPF Dev which is the SPFC development unit we use as Atlas team for testing.

## Linting

This is useful for keeping your code up to standard.

```bash
make python-format && make python-lint
```

## Testing

Always test your code, preferably locally. This command is the one that tests the python code on your local machine, and may require the use of mocks:

```bash
make python-test
```

### Manual Testing

You can do manual testing against a local deployment running in minikube. In order to create the environment, you will need to have a running minikube, see <https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube>. Once that has been setup, run the following:

```
eval $(minikube docker-env)
make oci-build-all
make itf-spfc-install-chart
```

This deploys the `ska-spfc-deployer` helm chart which is a pod job that deploys a pod job that deploys files to SPFC and registers the SPFC Tango device to the database.


### Registering SPFC device to tango database using gitlab job

There is a set of commands that one can run to register the SPFC device to the tango database.
This is important, for example, in cases where registration SPFC needs to communicate with Dish.LMC

To test the SPFC registration to tango database, please ensure that the TANGO_HOST environment variable is set
on the local laptop to be used for testing.
To set the TANGO_HOST variable one can run the `export TANGO_HOST=<ip_address>:<port_number>` command.
Secondly please update the CI/CD environment variable(s) in the `ska-mid-dish-spfc` repository.
They all have descriptions and guide on how they can be used before triggering the deployment.


Once done, one can trigger the registration of the SPFC from the gitlab job called `deploy-spfc-register` 
within the deploy stage of the pipeline. A pod called `ska-spfc-deployer-*` will be deployed in the namespace
to register the SPFC.
To remove the deployed `ska-spfc-deployer-*` pod job one can run the `remove-spfc-register` job within the
deploy stage of the pipeline.


### Compiling tango and other SPFC software
The following steps need `docker buildx` to be installed.
There are currently two jobs in the gitlab pipelines. One is for building the container for compiling 
the SPFC (`add-spfc-builder`) and other one is for running the compiled SPFC software (`run-spfc-builder`).

The `image-pre-build` command in the `build-spfc.mk` makefile is used to prepare the docker buildx and to use 
quick emulator before the docker buildx image can be built. 

The `image-build` command in the `build-spfc.mk` is used to build the spfc-builder image and to push it to 
the registry. One can comment out the `docker login -u ...` and remove the `--push` flag in order to build
docker images locally.

### Using ska-spfc-deployer helm chart
The following steps below are a guideline on how to deploy the ska-spfc-deployer helm  chart:
To include the ska-spfc-deployer helm chart use the following layout below within your helm chart:

- name: ska-spfc-deployer
    version: (use the latest version of the SPFC)
    repository: https://artefact.skao.int/repository/helm-internal
    condition: ska-spfc-deployer.enabled

Please ensure that the values.yaml file of your helm chart, has an entry under "global" indicating the DishID
that the SPFC will be communicating with:

global:
  dish_id: ska001

### Using ska-spfc-builder helm chart
The ska-spfc-builder helm chart is used to prepare development environment for building cpp-tango for the SPFC.
The following parameters are required:

NAMESPACE : The namespace to which to deploy the pod for building the cpp-tango for the SPFC.